import { Component, Input, OnChanges, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../../.services/components.service';
import { PageService } from '../../.services/page.service';

import * as moment from 'moment';

@Component({
  selector: 'abstract-page-table',
  templateUrl: './page-table.component.html',
  styleUrls: ['./page-table.component.scss']
})
export class PageTableComponent implements OnInit, OnChanges {

  @ViewChild('dialog') dialog: TemplateRef<any>;
  @Input('type') type: any = '';

  settings = {
    mode: 'external',
    actions: {
      add: false
    },
    sort: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      title: {
        title: 'Title',
        type: 'html',
        valuePrepareFunction: (value, rowData) => {
          if (rowData.parent) {
            return `<i class="fa-solid fa-arrow-turn-up fa-rotate-90 row-icon" title="Translation"></i> ${
              value
            }`;
          } else {
            return value;
          }
        }
      },
      language_id: {
        title: 'Language',
        type: 'html',
        valuePrepareFunction: (value, rowData) => {
          return `<div class="text-center">${
            rowData.language_id_data ? rowData.language_id_data.name : ''
          }</div>`;
        }
      },
      link: {
        title: 'Link',
        type: 'html',
        valuePrepareFunction: (value, rowData) => {
          return `<a href="${rowData.link_path}" target="_blank">/${
            value == 'index' ? '' : `${value}/`
          }</a>`;
        }
      },
      template: {
        title: 'Template',
        type: 'html',
        valuePrepareFunction: (value, rowData) => {
          return `<a href="${rowData.template_path}" target="_blank">${value}</a>`;
        }
      },
      type: {
        title: 'Status',
        type: 'html',
        class: 'status',
        valuePrepareFunction: (value) => {
          return '<div class="badge-wrapper text-center"><span class="badge ' + (value == 'public' ? 'success' : 'info') + ' text-center">' 
          + (value == 'public' ? 'Public' : 'Private') 
          + '</span></div>';
        }
      },
      create_at: {
        title: 'Created',
        type: 'html',
        class: 'datetime',
        valuePrepareFunction: (value) => {
          return value 
          ? `<div class="text-center">${moment(value).format('L')}<br />${moment(value).format('LTS')}</div>` 
          : '-';
        }
      },
      active: {
        title: 'Status',
        type: 'html',
        class: 'status',
        valuePrepareFunction: (value) => {
          return '<div class="badge-wrapper text-center"><span class="badge ' + (value == '1' ? 'success' : 'disabled') + ' text-center">' 
          + (value == '1' ? 'Active' : 'Inactive') 
          + '</span></div>';
        }
      }
    },
  };

  pages: any = null;
  loaded: boolean = false;

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private page: PageService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.initialize(
      this.type == 'active' ? true
      : this.type == 'inactive' ? false
      : ''
    ).then(() => {
      this.loaded = true;
    });
  }

  initialize(active = null) {
		return new Promise<void>(async (resolve) => {
      await this.page.listMain(active).then(async (response: any) => {
        let pages = [];
        const format = async () => {
          for (let page of response) {
            await pages.push(page);
            const insertPage = async () => {
              for (let translate of page.translate) {
                translate.parent = page.id;
                await pages.push(translate);
              }
            }
            await insertPage();
          }
        }
        await format();
        this.pages = pages;
        console.log(this.pages);
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      resolve();
		});
  }

  openEdit(event) {
    this.router.navigateByUrl(
      '/page/' + event.data.id + '/edit', 
      {
        state: event.data
      }
    );
  }

  delete(event, field = null) {
    let context = 'Do you want to delete' + ' ' + 'ID' + ':' + event.data.id + '?';
    if (field) {
      context = 'Do you want to delete' 
      + ' "' + event.data[field] + '" (' + 'ID' + ': ' + event.data.id + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe((data) => {
      if (data) {
        this.page.delete(event.data.id).then((response: any) => {
          this.initialize(
            this.type == 'active' ? true
            : this.type == 'inactive' ? false
            : ''
          ).then(() => {
            this.loaded = true;
          });
          this.components.showToastStatus('success', 'Success', 'Successfully deleted api');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

}
