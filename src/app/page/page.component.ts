import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../.services/components.service';
import { ApiService } from '../.services/api.service';

@Component({
  selector: 'abstract-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogSort') dialogSort: TemplateRef<any>;

  parseInt = parseInt;

  path = '/page';
  active = 'list';
  activeID = null;

  type = '';
  types = [
    {
      title: 'All',
      value: ''
    },
    {
      title: 'Active',
      value: 'active'
    },
    {
      title: 'Inactive',
      value: 'inactive'
    }
  ];

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private api: ApiService,
    private components: ComponentsService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
  }

  prepare() {
    this.router.events.subscribe((enter: NavigationEnd) => {
      if (enter instanceof NavigationEnd) {
        if (enter.url.indexOf(this.path) >= 0) {
          this.activeID = null;
          if (enter.url.includes('create')) {
            this.active = 'create';
          } else if (enter.url.includes('edit')) {
            const urlParts = enter.url.split('/');
            this.activeID = urlParts[2];
            this.active = 'edit';
          } else {
            this.active = 'list';
            if (enter.url.includes('/active')) {
              this.type = 'active';
            } else if (enter.url.includes('/inactive')) {
              this.type = 'inactive';
            } else {
              this.type = '';
            }
          }
        }
      }
    });
  }

  navigate(childURL = '') {
    if (this.active == 'create' || this.active == 'edit') {
      let context = '';
      if (this.active == 'create') {
        context = 'Do you want to exit create page?';
      } else if (this.active == 'edit') {
        context = 'Do you want to exit edit page?';
      }
      this.dialogService.open(
        this.dialog, {
          context: context
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

  onSelectStatus(event) {
    this.router.navigate([event == 'all' ? this.path : this.path + '/' + event]);
  }

}
