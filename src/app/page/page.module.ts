import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  FormsModule, 
  ReactiveFormsModule 
} from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';

import { 
  NbCardModule, 
  NbActionsModule, 
  NbIconModule, 
  NbSelectModule, 
  NbInputModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbButtonModule, 
  NbBadgeModule, 
  NbListModule, 
  NbUserModule, 
  NbSpinnerModule 
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ClipboardModule } from 'ngx-clipboard';

import { MediaBrowserModule } from '../media-browser/media-browser.module';
import { IconModule } from '../icon/icon.module';

import { PageRoutingModule } from './page-routing.module';
import { PageComponent } from './page.component';
import { PageCreateComponent } from './page-create/page-create.component';
import { PageEditComponent } from './page-edit/page-edit.component';
import { PageTableComponent } from './page-table/page-table.component';
import { PageViewComponent } from './page-view/page-view.component';


@NgModule({
  declarations: [
    PageComponent,
    PageCreateComponent,
    PageEditComponent,
    PageTableComponent,
    PageViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    DragDropModule,
    ClipboardModule,
    MediaBrowserModule,
    IconModule,
    PageRoutingModule
  ],
  exports: [
    PageCreateComponent,
    PageEditComponent,
    PageTableComponent,
    PageViewComponent
  ]
})
export class PageModule { }
