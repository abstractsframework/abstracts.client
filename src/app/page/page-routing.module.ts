import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageCreateComponent } from './page-create/page-create.component';
import { PageEditComponent } from './page-edit/page-edit.component';
import { PageTableComponent } from './page-table/page-table.component';
import { PageViewComponent } from './page-view/page-view.component';
import { PageComponent } from './page.component';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {
        path: '',
        component: PageTableComponent
      },
      {
        path: 'active',
        component: PageTableComponent
      },
      {
        path: 'inactive',
        component: PageTableComponent
      },
      {
        path: 'create',
        component: PageCreateComponent
      },
      {
        path: ':id',
        component: PageViewComponent
      },
      {
        path: ':id/edit',
        component: PageEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
