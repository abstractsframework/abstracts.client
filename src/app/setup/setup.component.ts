import { Component, OnInit } from '@angular/core';
import { 
  FormGroup, 
  FormBuilder, 
  Validators 
} from '@angular/forms';
import { Router } from '@angular/router';

import { KJUR } from 'jsrsasign';

import { RestService } from '../.services/rest.service';
import { StorageService } from '../.services/storage.service';

@Component({
  selector: 'abstract-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {

  formGroup: FormGroup;

  submitted: boolean = false;
  message: any = null;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private storage: StorageService,
    private rest: RestService
  ) { }

  ngOnInit(): void {
    this.canActivate();
    this.initializeForm();
  }

  canActivate() {
    this.rest.validateAPICredential().then(() => {
    }).catch(() => {});
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      title: [
        null, 
        Validators.required
      ],
      host: [
        null, 
        Validators.required
      ],
      version: [
        null, 
        Validators.required
      ],
      credentialKey: [
        null
      ],
      credentialSecret: [
        null
      ],
      encryptionSSLKeyEnable: [
        false
      ],
      encryptionSSLPublicKey: [
        null,
      ],
      encryptionSSLAlgorithm: [
        null
      ],
      encryptionGeneralKeyEnable: [
        false
      ],
      encryptionGeneralAlgorithm: [
        null
      ],
      encryptionGeneralKey: [
        null
      ]
    });
  }

  submit() {
    this.message = null;
    this.submitted = true;
    if (this.formGroup.valid) {
      this.set(this.formGroup.value).then(async () => {
        this.message = {
          type: 'success',
          content: 'Successfully setup'
        }
        const projects: any = await this.storage.get('projects', true, true);
				if (projects && projects.length) {
					this.router.navigate(['./projects']);
				} else {
					this.router.navigate(['./login']);
				}
      }).catch(() => {
        this.message = {
          type: 'danger',
          content: 'Unable to setup'
        }
        this.submitted = false;
      });
    }
  }

  async set(value) {
    return new Promise(async (resolve, reject) => {
      let projects = await this.storage.get('projects', true, true);
      const project = {
        title: value.title,
        host: value.host.trim(),
        version: value.version,
        credential: {
          key: value.credentialKey.trim(),
          secret: value.credentialSecret.trim(),
        },
        encryption: {
          SSLAlgorithm: (
            value.encryptionSSLAlgorithm
            && value.encryptionSSLAlgorithm
            && value.encryptionSSLAlgorithm != ''
          ) ? value.encryptionSSLAlgorithm.trim() : false,
          SSLPublicKey: (
            value.encryptionSSLKeyEnable
            && value.encryptionSSLPublicKey
            && value.encryptionSSLPublicKey != ''
          ) ? value.encryptionSSLPublicKey.trim() : false,
          generalAlgorithm: (
            value.encryptionGeneralKeyEnable
            && value.encryptionGeneralAlgorithm
            && value.encryptionGeneralAlgorithm != ''
          ) ? value.encryptionGeneralAlgorithm.trim() : false,
          generalKey: (
            value.encryptionGeneralKeyEnable
            && value.encryptionGeneralKey
            && value.encryptionGeneralKey != ''
          ) ? value.encryptionGeneralKey.trim() : false,
        }
      }

      if (!projects || !projects.length) {
        projects = [];
      }
      await projects.push(project);

      await this.storage.set('current-project', (projects.length - 1));
      await this.storage.set('projects', projects, true, true);
      
      if (value.encryptionSSLKeyEnable) {
        if (
          !value.encryptionSSLPublicKey
          || value.encryptionSSLPublicKey == ''
        ) {
          this.formGroup.patchValue({
            encryptionSSLKeyEnable: false
          });
        }
      }
      if (value.encryptionGeneralKeyEnable) {
        if (
          !value.encryptionGeneralKey
          || value.encryptionGeneralKey == ''
        ) {
          this.formGroup.patchValue({
            encryptionGeneralKeyEnable: false
          });
        }
      }

      resolve(true);

    });
  }

}
