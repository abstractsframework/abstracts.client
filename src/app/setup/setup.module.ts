import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  NbAccordionModule,
  NbButtonModule, 
  NbCardModule, 
  NbCheckboxModule, 
  NbInputModule, 
  NbSelectModule,
  NbSpinnerModule,
  NbStepperModule
} from '@nebular/theme';

import { SetupRoutingModule } from './setup-routing.module';

import { SetupComponent } from './setup.component';


@NgModule({
  declarations: [
    SetupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NbStepperModule,
    NbButtonModule,
    NbInputModule,
    NbCheckboxModule,
    NbSelectModule,
    NbCardModule,
    NbAccordionModule,
    NbSpinnerModule,
    SetupRoutingModule
  ]
})
export class SetupModule { }
