import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconComponent } from './icon.component';
import { NbButtonModule, NbIconModule, NbInputModule, NbTabsetModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    IconComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbInputModule,
    NbIconModule,
    NbButtonModule,
    NbTabsetModule
  ],
  exports: [
    IconComponent
  ]
})
export class IconModule { }
