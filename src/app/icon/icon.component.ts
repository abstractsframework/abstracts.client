import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NbIconLibraries } from '@nebular/theme';

@Component({
  selector: 'abstract-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss']
})
export class IconComponent implements OnInit {

  @Input() selected: any;
  @Output() select: EventEmitter<any> = new EventEmitter<any>();

  iconsSimpleLine: Array<any> = [
    {
      icon: 'si si-action-redo',
      name: ''
    },
    {
      icon: 'si si-action-undo',
      name: ''
    },
    {
      icon: 'si si-arrow-down',
      name: ''
    },
    {
      icon: 'si si-arrow-left',
      name: ''
    },
    {
      icon: 'si si-arrow-right',
      name: ''
    },
    {
      icon: 'si si-arrow-up',
      name: ''
    },
    {
      icon: 'si si-anchor',
      name: ''
    },
    {
      icon: 'si si-badge',
      name: ''
    },
    {
      icon: 'si si-bag',
      name: ''
    },
    {
      icon: 'si si-ban',
      name: ''
    },
    {
      icon: 'si si-bar-chart',
      name: ''
    },
    {
      icon: 'si si-basket',
      name: ''
    },
    {
      icon: 'si si-basket-loaded',
      name: ''
    },
    {
      icon: 'si si-bell',
      name: ''
    },
    {
      icon: 'si si-book-open',
      name: ''
    },
    {
      icon: 'si si-briefcase',
      name: ''
    },
    {
      icon: 'si si-bubble',
      name: ''
    },
    {
      icon: 'si si-bubbles',
      name: ''
    },
    {
      icon: 'si si-bulb',
      name: ''
    },
    {
      icon: 'si si-calculator',
      name: ''
    },
    {
      icon: 'si si-calendar',
      name: ''
    },
    {
      icon: 'si si-call-end',
      name: ''
    },
    {
      icon: 'si si-call-in',
      name: ''
    },
    {
      icon: 'si si-call-out',
      name: ''
    },
    {
      icon: 'si si-camcorder',
      name: ''
    },
    {
      icon: 'si si-camera',
      name: ''
    },
    {
      icon: 'si si-check',
      name: ''
    },
    {
      icon: 'si si-chemistry',
      name: ''
    },
    {
      icon: 'si si-clock',
      name: ''
    },
    {
      icon: 'si si-cloud-download',
      name: ''
    },
    {
      icon: 'si si-cloud-upload',
      name: ''
    },
    {
      icon: 'si si-compass',
      name: ''
    },
    {
      icon: 'si si-control-end',
      name: ''
    },
    {
      icon: 'si si-control-forward',
      name: ''
    },
    {
      icon: 'si si-control-play',
      name: ''
    },
    {
      icon: 'si si-control-pause',
      name: ''
    },
    {
      icon: 'si si-control-rewind',
      name: ''
    },
    {
      icon: 'si si-control-start',
      name: ''
    },
    {
      icon: 'si si-credit-card',
      name: ''
    },
    {
      icon: 'si si-crop',
      name: ''
    },
    {
      icon: 'si si-cup',
      name: ''
    },
    {
      icon: 'si si-cursor',
      name: ''
    },
    {
      icon: 'si si-cursor-move',
      name: ''
    },
    {
      icon: 'si si-diamond',
      name: ''
    },
    {
      icon: 'si si-direction',
      name: ''
    },
    {
      icon: 'si si-directions',
      name: ''
    },
    {
      icon: 'si si-disc',
      name: ''
    },
    {
      icon: 'si si-dislike',
      name: ''
    },
    {
      icon: 'si si-doc',
      name: ''
    },
    {
      icon: 'si si-docs',
      name: ''
    },
    {
      icon: 'si si-drawer',
      name: ''
    },
    {
      icon: 'si si-drop',
      name: ''
    },
    {
      icon: 'si si-earphones',
      name: ''
    },
    {
      icon: 'si si-earphones-alt',
      name: ''
    },
    {
      icon: 'si si-emoticon-smile',
      name: ''
    },
    {
      icon: 'si si-energy',
      name: ''
    },
    {
      icon: 'si si-envelope',
      name: ''
    },
    {
      icon: 'si si-envelope-letter',
      name: ''
    },
    {
      icon: 'si si-envelope-open',
      name: ''
    },
    {
      icon: 'si si-equalizer',
      name: ''
    },
    {
      icon: 'si si-eye',
      name: ''
    },
    {
      icon: 'si si-eyeglasses',
      name: ''
    },
    {
      icon: 'si si-feed',
      name: ''
    },
    {
      icon: 'si si-film',
      name: ''
    },
    {
      icon: 'si si-fire',
      name: ''
    },
    {
      icon: 'si si-flag',
      name: ''
    },
    {
      icon: 'si si-folder',
      name: ''
    },
    {
      icon: 'si si-folder-alt',
      name: ''
    },
    {
      icon: 'si si-frame',
      name: ''
    },
    {
      icon: 'si si-game-controller',
      name: ''
    },
    {
      icon: 'si si-ghost',
      name: ''
    },
    {
      icon: 'si si-globe',
      name: ''
    },
    {
      icon: 'si si-globe-alt',
      name: ''
    },
    {
      icon: 'si si-graduation',
      name: ''
    },
    {
      icon: 'si si-graph',
      name: ''
    },
    {
      icon: 'si si-grid',
      name: ''
    },
    {
      icon: 'si si-handbag',
      name: ''
    },
    {
      icon: 'si si-heart',
      name: ''
    },
    {
      icon: 'si si-home',
      name: ''
    },
    {
      icon: 'si si-hourglass',
      name: ''
    },
    {
      icon: 'si si-info',
      name: ''
    },
    {
      icon: 'si si-key',
      name: ''
    },
    {
      icon: 'si si-layers',
      name: ''
    },
    {
      icon: 'si si-like',
      name: ''
    },
    {
      icon: 'si si-link',
      name: ''
    },
    {
      icon: 'si si-list',
      name: ''
    },
    {
      icon: 'si si-lock',
      name: ''
    },
    {
      icon: 'si si-lock-open',
      name: ''
    },
    {
      icon: 'si si-login',
      name: ''
    },
    {
      icon: 'si si-logout',
      name: ''
    },
    {
      icon: 'si si-loop',
      name: ''
    },
    {
      icon: 'si si-magic-wand',
      name: ''
    },
    {
      icon: 'si si-magnet',
      name: ''
    },
    {
      icon: 'si si-magnifier',
      name: ''
    },
    {
      icon: 'si si-magnifier-add',
      name: ''
    },
    {
      icon: 'si si-magnifier-remove',
      name: ''
    },
    {
      icon: 'si si-map',
      name: ''
    },
    {
      icon: 'si si-microphone',
      name: ''
    },
    {
      icon: 'si si-mouse',
      name: ''
    },
    {
      icon: 'si si-moustache',
      name: ''
    },
    {
      icon: 'si si-music-tone',
      name: ''
    },
    {
      icon: 'si si-music-tone-alt',
      name: ''
    },
    {
      icon: 'si si-note',
      name: ''
    },
    {
      icon: 'si si-notebook',
      name: ''
    },
    {
      icon: 'si si-paper-clip',
      name: ''
    },
    {
      icon: 'si si-paper-plane',
      name: ''
    },
    {
      icon: 'si si-pencil',
      name: ''
    },
    {
      icon: 'si si-picture',
      name: ''
    },
    {
      icon: 'si si-pie-chart',
      name: ''
    },
    {
      icon: 'si si-pin',
      name: ''
    },
    {
      icon: 'si si-plane',
      name: ''
    },
    {
      icon: 'si si-playlist',
      name: ''
    },
    {
      icon: 'si si-plus',
      name: ''
    },
    {
      icon: 'si si-pointer',
      name: ''
    },
    {
      icon: 'si si-power',
      name: ''
    },
    {
      icon: 'si si-present',
      name: ''
    },
    {
      icon: 'si si-printer',
      name: ''
    },
    {
      icon: 'si si-puzzle',
      name: ''
    },
    {
      icon: 'si si-question',
      name: ''
    },
    {
      icon: 'si si-refresh',
      name: ''
    },
    {
      icon: 'si si-reload',
      name: ''
    },
    {
      icon: 'si si-rocket',
      name: ''
    },
    {
      icon: 'si si-screen-desktop',
      name: ''
    },
    {
      icon: 'si si-screen-smartphone',
      name: ''
    },
    {
      icon: 'si si-screen-tablet',
      name: ''
    },
    {
      icon: 'si si-settings',
      name: ''
    },
    {
      icon: 'si si-share',
      name: ''
    },
    {
      icon: 'si si-share-alt',
      name: ''
    },
    {
      icon: 'si si-shield',
      name: ''
    },
    {
      icon: 'si si-shuffle',
      name: ''
    },
    {
      icon: 'si si-size-actual',
      name: ''
    },
    {
      icon: 'si si-size-fullscreen',
      name: ''
    },
    {
      icon: 'si si-social-dribbble',
      name: ''
    },
    {
      icon: 'si si-social-dropbox',
      name: ''
    },
    {
      icon: 'si si-social-facebook',
      name: ''
    },
    {
      icon: 'si si-social-tumblr',
      name: ''
    },
    {
      icon: 'si si-social-twitter',
      name: ''
    },
    {
      icon: 'si si-social-youtube',
      name: ''
    },
    {
      icon: 'si si-speech',
      name: ''
    },
    {
      icon: 'si si-speedometer',
      name: ''
    },
    {
      icon: 'si si-star',
      name: ''
    },
    {
      icon: 'si si-support',
      name: ''
    },
    {
      icon: 'si si-symbol-female',
      name: ''
    },
    {
      icon: 'si si-symbol-male',
      name: ''
    },
    {
      icon: 'si si-tag',
      name: ''
    },
    {
      icon: 'si si-target',
      name: ''
    },
    {
      icon: 'si si-trash',
      name: ''
    },
    {
      icon: 'si si-trophy',
      name: ''
    },
    {
      icon: 'si si-umbrella',
      name: ''
    },
    {
      icon: 'si si-user',
      name: ''
    },
    {
      icon: 'si si-user-female',
      name: ''
    },
    {
      icon: 'si si-user-follow',
      name: ''
    },
    {
      icon: 'si si-user-following',
      name: ''
    },
    {
      icon: 'si si-user-unfollow',
      name: ''
    },
    {
      icon: 'si si-vector',
      name: ''
    },
    {
      icon: 'si si-volume-1',
      name: ''
    },
    {
      icon: 'si si-volume-2',
      name: ''
    },
    {
      icon: 'si si-volume-off',
      name: ''
    },
    {
      icon: 'si si-wallet',
      name: ''
    },
    {
      icon: 'si si-wrench',
      name: ''
    }
  ]

  iconsFontAwesome: Array<any> = [
    {
      icon: 'far fa-address-book fa-fw',
      name: 'address-book'
    },
    {
      icon: 'far fa-address-card fa-fw',
      name: 'address-card'
    },
    {
      icon: 'far fa-bell fa-fw',
      name: 'bell'
    },
    {
      icon: 'far fa-bell-slash fa-fw',
      name: 'bell-slash'
    },
    {
      icon: 'far fa-bookmark fa-fw',
      name: 'bookmark'
    },
    {
      icon: 'far fa-building fa-fw',
      name: 'building'
    },
    {
      icon: 'far fa-calendar fa-fw',
      name: 'calendar'
    },
    {
      icon: 'far fa-calendar-check fa-fw',
      name: 'calendar-check'
    },
    {
      icon: 'far fa-calendar-days fa-fw',
      name: 'calendar-days'
    },
    {
      icon: 'far fa-calendar-minus fa-fw',
      name: 'calendar-minus'
    },
    {
      icon: 'far fa-calendar-plus fa-fw',
      name: 'calendar-plus'
    },
    {
      icon: 'far fa-calendar-xmark fa-fw',
      name: 'calendar-xmark'
    },
    {
      icon: 'far fa-chart-bar fa-fw',
      name: 'chart-bar'
    },
    {
      icon: 'far fa-chess-bishop fa-fw',
      name: 'chess-bishop'
    },
    {
      icon: 'far fa-chess-king fa-fw',
      name: 'chess-king'
    },
    {
      icon: 'far fa-chess-knight fa-fw',
      name: 'chess-knight'
    },
    {
      icon: 'far fa-chess-pawn fa-fw',
      name: 'chess-pawn'
    },
    {
      icon: 'far fa-chess-queen fa-fw',
      name: 'chess-queen'
    },
    {
      icon: 'far fa-chess-rook fa-fw',
      name: 'chess-rook'
    },
    {
      icon: 'far fa-circle fa-fw',
      name: 'circle'
    },
    {
      icon: 'far fa-circle-check fa-fw',
      name: 'circle-check'
    },
    {
      icon: 'far fa-circle-dot fa-fw',
      name: 'circle-dot'
    },
    {
      icon: 'far fa-circle-down fa-fw',
      name: 'circle-down'
    },
    {
      icon: 'far fa-circle-left fa-fw',
      name: 'circle-left'
    },
    {
      icon: 'far fa-circle-pause fa-fw',
      name: 'circle-pause'
    },
    {
      icon: 'far fa-circle-play fa-fw',
      name: 'circle-play'
    },
    {
      icon: 'far fa-circle-question fa-fw',
      name: 'circle-question'
    },
    {
      icon: 'far fa-circle-right fa-fw',
      name: 'circle-right'
    },
    {
      icon: 'far fa-circle-stop fa-fw',
      name: 'circle-stop'
    },
    {
      icon: 'far fa-circle-up fa-fw',
      name: 'circle-up'
    },
    {
      icon: 'far fa-circle-user fa-fw',
      name: 'circle-user'
    },
    {
      icon: 'far fa-circle-xmark fa-fw',
      name: 'circle-xmark'
    },
    {
      icon: 'far fa-clipboard fa-fw',
      name: 'clipboard'
    },
    {
      icon: 'far fa-clock fa-fw',
      name: 'clock'
    },
    {
      icon: 'far fa-clone fa-fw',
      name: 'clone'
    },
    {
      icon: 'far fa-closed-captioning fa-fw',
      name: 'closed-captioning'
    },
    {
      icon: 'far fa-comment fa-fw',
      name: 'comment'
    },
    {
      icon: 'far fa-comment-dots fa-fw',
      name: 'comment-dots'
    },
    {
      icon: 'far fa-comments fa-fw',
      name: 'comments'
    },
    {
      icon: 'far fa-compass fa-fw',
      name: 'compass'
    },
    {
      icon: 'far fa-copy fa-fw',
      name: 'copy'
    },
    {
      icon: 'far fa-copyright fa-fw',
      name: 'copyright'
    },
    {
      icon: 'far fa-credit-card fa-fw',
      name: 'credit-card'
    },
    {
      icon: 'far fa-envelope fa-fw',
      name: 'envelope'
    },
    {
      icon: 'far fa-envelope-open fa-fw',
      name: 'envelope-open'
    },
    {
      icon: 'far fa-eye fa-fw',
      name: 'eye'
    },
    {
      icon: 'far fa-eye-slash fa-fw',
      name: 'eye-slash'
    },
    {
      icon: 'far fa-face-angry fa-fw',
      name: 'face-angry'
    },
    {
      icon: 'far fa-face-dizzy fa-fw',
      name: 'face-dizzy'
    },
    {
      icon: 'far fa-face-flushed fa-fw',
      name: 'face-flushed'
    },
    {
      icon: 'far fa-face-frown fa-fw',
      name: 'face-frown'
    },
    {
      icon: 'far fa-face-frown-open fa-fw',
      name: 'face-frown-open'
    },
    {
      icon: 'far fa-face-grimace fa-fw',
      name: 'face-grimace'
    },
    {
      icon: 'far fa-face-grin fa-fw',
      name: 'face-grin'
    },
    {
      icon: 'far fa-face-grin-beam fa-fw',
      name: 'face-grin-beam'
    },
    {
      icon: 'far fa-face-grin-beam-sweat fa-fw',
      name: 'face-grin-beam-sweat'
    },
    {
      icon: 'far fa-face-grin-hearts fa-fw',
      name: 'face-grin-hearts'
    },
    {
      icon: 'far fa-face-grin-squint fa-fw',
      name: 'face-grin-squint'
    },
    {
      icon: 'far fa-face-grin-squint-tears fa-fw',
      name: 'face-grin-squint-tears'
    },
    {
      icon: 'far fa-face-grin-stars fa-fw',
      name: 'face-grin-stars'
    },
    {
      icon: 'far fa-face-grin-tears fa-fw',
      name: 'face-grin-tears'
    },
    {
      icon: 'far fa-face-grin-tongue fa-fw',
      name: 'face-grin-tongue'
    },
    {
      icon: 'far fa-face-grin-tongue-squint fa-fw',
      name: 'face-grin-tongue-squint'
    },
    {
      icon: 'far fa-face-grin-tongue-wink fa-fw',
      name: 'face-grin-tongue-wink'
    },
    {
      icon: 'far fa-face-grin-wide fa-fw',
      name: 'face-grin-wide'
    },
    {
      icon: 'far fa-face-grin-wink fa-fw',
      name: 'face-grin-wink'
    },
    {
      icon: 'far fa-face-kiss fa-fw',
      name: 'face-kiss'
    },
    {
      icon: 'far fa-face-kiss-beam fa-fw',
      name: 'face-kiss-beam'
    },
    {
      icon: 'far fa-face-kiss-wink-heart fa-fw',
      name: 'face-kiss-wink-heart'
    },
    {
      icon: 'far fa-face-laugh fa-fw',
      name: 'face-laugh'
    },
    {
      icon: 'far fa-face-laugh-beam fa-fw',
      name: 'face-laugh-beam'
    },
    {
      icon: 'far fa-face-laugh-squint fa-fw',
      name: 'face-laugh-squint'
    },
    {
      icon: 'far fa-face-laugh-wink fa-fw',
      name: 'face-laugh-wink'
    },
    {
      icon: 'far fa-face-meh fa-fw',
      name: 'face-meh'
    },
    {
      icon: 'far fa-face-meh-blank fa-fw',
      name: 'face-meh-blank'
    },
    {
      icon: 'far fa-face-rolling-eyes fa-fw',
      name: 'face-rolling-eyes'
    },
    {
      icon: 'far fa-face-sad-cry fa-fw',
      name: 'face-sad-cry'
    },
    {
      icon: 'far fa-face-sad-tear fa-fw',
      name: 'face-sad-tear'
    },
    {
      icon: 'far fa-face-smile fa-fw',
      name: 'face-smile'
    },
    {
      icon: 'far fa-face-smile-beam fa-fw',
      name: 'face-smile-beam'
    },
    {
      icon: 'far fa-face-smile-wink fa-fw',
      name: 'face-smile-wink'
    },
    {
      icon: 'far fa-face-surprise fa-fw',
      name: 'face-surprise'
    },
    {
      icon: 'far fa-face-tired fa-fw',
      name: 'face-tired'
    },
    {
      icon: 'far fa-file fa-fw',
      name: 'file'
    },
    {
      icon: 'far fa-file-audio fa-fw',
      name: 'file-audio'
    },
    {
      icon: 'far fa-file-code fa-fw',
      name: 'file-code'
    },
    {
      icon: 'far fa-file-excel fa-fw',
      name: 'file-excel'
    },
    {
      icon: 'far fa-file-image fa-fw',
      name: 'file-image'
    },
    {
      icon: 'far fa-file-lines fa-fw',
      name: 'file-lines'
    },
    {
      icon: 'far fa-file-pdf fa-fw',
      name: 'file-pdf'
    },
    {
      icon: 'far fa-file-powerpoint fa-fw',
      name: 'file-powerpoint'
    },
    {
      icon: 'far fa-file-video fa-fw',
      name: 'file-video'
    },
    {
      icon: 'far fa-file-word fa-fw',
      name: 'file-word'
    },
    {
      icon: 'far fa-file-zipper fa-fw',
      name: 'file-zipper'
    },
    {
      icon: 'far fa-flag fa-fw',
      name: 'flag'
    },
    {
      icon: 'far fa-floppy-disk fa-fw',
      name: 'floppy-disk'
    },
    {
      icon: 'far fa-folder fa-fw',
      name: 'folder'
    },
    {
      icon: 'far fa-folder-closed fa-fw',
      name: 'folder-closed'
    },
    {
      icon: 'far fa-folder-open fa-fw',
      name: 'folder-open'
    },
    {
      icon: 'far fa-font-awesome fa-fw',
      name: 'font-awesome'
    },
    {
      icon: 'far fa-futbol fa-fw',
      name: 'futbol'
    },
    {
      icon: 'far fa-gem fa-fw',
      name: 'gem'
    },
    {
      icon: 'far fa-hand fa-fw',
      name: 'hand'
    },
    {
      icon: 'far fa-hand-back-fist fa-fw',
      name: 'hand-back-fist'
    },
    {
      icon: 'far fa-hand-lizard fa-fw',
      name: 'hand-lizard'
    },
    {
      icon: 'far fa-hand-peace fa-fw',
      name: 'hand-peace'
    },
    {
      icon: 'far fa-hand-point-down fa-fw',
      name: 'hand-point-down'
    },
    {
      icon: 'far fa-hand-point-left fa-fw',
      name: 'hand-point-left'
    },
    {
      icon: 'far fa-hand-point-right fa-fw',
      name: 'hand-point-right'
    },
    {
      icon: 'far fa-hand-point-up fa-fw',
      name: 'hand-point-up'
    },
    {
      icon: 'far fa-hand-pointer fa-fw',
      name: 'hand-pointer'
    },
    {
      icon: 'far fa-hand-scissors fa-fw',
      name: 'hand-scissors'
    },
    {
      icon: 'far fa-hand-spock fa-fw',
      name: 'hand-spock'
    },
    {
      icon: 'far fa-handshake fa-fw',
      name: 'handshake'
    },
    {
      icon: 'far fa-hard-drive fa-fw',
      name: 'hard-drive'
    },
    {
      icon: 'far fa-heart fa-fw',
      name: 'heart'
    },
    {
      icon: 'far fa-hospital fa-fw',
      name: 'hospital'
    },
    {
      icon: 'far fa-hourglass fa-fw',
      name: 'hourglass'
    },
    {
      icon: 'far fa-id-badge fa-fw',
      name: 'id-badge'
    },
    {
      icon: 'far fa-id-card fa-fw',
      name: 'id-card'
    },
    {
      icon: 'far fa-image fa-fw',
      name: 'image'
    },
    {
      icon: 'far fa-images fa-fw',
      name: 'images'
    },
    {
      icon: 'far fa-keyboard fa-fw',
      name: 'keyboard'
    },
    {
      icon: 'far fa-lemon fa-fw',
      name: 'lemon'
    },
    {
      icon: 'far fa-life-ring fa-fw',
      name: 'life-ring'
    },
    {
      icon: 'far fa-lightbulb fa-fw',
      name: 'lightbulb'
    },
    {
      icon: 'far fa-map fa-fw',
      name: 'map'
    },
    {
      icon: 'far fa-message fa-fw',
      name: 'message'
    },
    {
      icon: 'far fa-money-bill-1 fa-fw',
      name: 'money-bill-1'
    },
    {
      icon: 'far fa-moon fa-fw',
      name: 'moon'
    },
    {
      icon: 'far fa-newspaper fa-fw',
      name: 'newspaper'
    },
    {
      icon: 'far fa-note-sticky fa-fw',
      name: 'note-sticky'
    },
    {
      icon: 'far fa-object-group fa-fw',
      name: 'object-group'
    },
    {
      icon: 'far fa-object-ungroup fa-fw',
      name: 'object-ungroup'
    },
    {
      icon: 'far fa-paper-plane fa-fw',
      name: 'paper-plane'
    },
    {
      icon: 'far fa-paste fa-fw',
      name: 'paste'
    },
    {
      icon: 'far fa-pen-to-square fa-fw',
      name: 'pen-to-square'
    },
    {
      icon: 'far fa-rectangle-list fa-fw',
      name: 'rectangle-list'
    },
    {
      icon: 'far fa-rectangle-xmark fa-fw',
      name: 'rectangle-xmark'
    },
    {
      icon: 'far fa-registered fa-fw',
      name: 'registered'
    },
    {
      icon: 'far fa-share-from-square fa-fw',
      name: 'share-from-square'
    },
    {
      icon: 'far fa-snowflake fa-fw',
      name: 'snowflake'
    },
    {
      icon: 'far fa-square fa-fw',
      name: 'square'
    },
    {
      icon: 'far fa-square-caret-down fa-fw',
      name: 'square-caret-down'
    },
    {
      icon: 'far fa-square-caret-left fa-fw',
      name: 'square-caret-left'
    },
    {
      icon: 'far fa-square-caret-right fa-fw',
      name: 'square-caret-right'
    },
    {
      icon: 'far fa-square-caret-up fa-fw',
      name: 'square-caret-up'
    },
    {
      icon: 'far fa-square-check fa-fw',
      name: 'square-check'
    },
    {
      icon: 'far fa-square-full fa-fw',
      name: 'square-full'
    },
    {
      icon: 'far fa-square-minus fa-fw',
      name: 'square-minus'
    },
    {
      icon: 'far fa-square-plus fa-fw',
      name: 'square-plus'
    },
    {
      icon: 'far fa-star fa-fw',
      name: 'star'
    },
    {
      icon: 'far fa-star-half fa-fw',
      name: 'star-half'
    },
    {
      icon: 'far fa-star-half-stroke fa-fw',
      name: 'star-half-stroke'
    },
    {
      icon: 'far fa-sun fa-fw',
      name: 'sun'
    },
    {
      icon: 'far fa-thumbs-down fa-fw',
      name: 'thumbs-down'
    },
    {
      icon: 'far fa-thumbs-up fa-fw',
      name: 'thumbs-up'
    },
    {
      icon: 'far fa-trash-can fa-fw',
      name: 'trash-can'
    },
    {
      icon: 'far fa-user fa-fw',
      name: 'user'
    },
    {
      icon: 'far fa-window-maximize fa-fw',
      name: 'window-maximize'
    },
    {
      icon: 'far fa-window-minimize fa-fw',
      name: 'window-minimize'
    },
    {
      icon: 'far fa-window-restore fa-fw',
      name: 'window-restore'
    },
    {
      icon: 'fas fa-0 fa-fw',
      name: '0'
    },
    {
      icon: 'fas fa-1 fa-fw',
      name: '1'
    },
    {
      icon: 'fas fa-2 fa-fw',
      name: '2'
    },
    {
      icon: 'fas fa-3 fa-fw',
      name: '3'
    },
    {
      icon: 'fas fa-4 fa-fw',
      name: '4'
    },
    {
      icon: 'fas fa-5 fa-fw',
      name: '5'
    },
    {
      icon: 'fas fa-6 fa-fw',
      name: '6'
    },
    {
      icon: 'fas fa-7 fa-fw',
      name: '7'
    },
    {
      icon: 'fas fa-8 fa-fw',
      name: '8'
    },
    {
      icon: 'fas fa-9 fa-fw',
      name: '9'
    },
    {
      icon: 'fas fa-a fa-fw',
      name: 'a'
    },
    {
      icon: 'fas fa-address-book fa-fw',
      name: 'address-book'
    },
    {
      icon: 'fas fa-address-card fa-fw',
      name: 'address-card'
    },
    {
      icon: 'fas fa-align-center fa-fw',
      name: 'align-center'
    },
    {
      icon: 'fas fa-align-justify fa-fw',
      name: 'align-justify'
    },
    {
      icon: 'fas fa-align-left fa-fw',
      name: 'align-left'
    },
    {
      icon: 'fas fa-align-right fa-fw',
      name: 'align-right'
    },
    {
      icon: 'fas fa-anchor fa-fw',
      name: 'anchor'
    },
    {
      icon: 'fas fa-anchor-circle-check fa-fw',
      name: 'anchor-circle-check'
    },
    {
      icon: 'fas fa-anchor-circle-exclamation fa-fw',
      name: 'anchor-circle-exclamation'
    },
    {
      icon: 'fas fa-anchor-circle-xmark fa-fw',
      name: 'anchor-circle-xmark'
    },
    {
      icon: 'fas fa-anchor-lock fa-fw',
      name: 'anchor-lock'
    },
    {
      icon: 'fas fa-angle-down fa-fw',
      name: 'angle-down'
    },
    {
      icon: 'fas fa-angle-left fa-fw',
      name: 'angle-left'
    },
    {
      icon: 'fas fa-angle-right fa-fw',
      name: 'angle-right'
    },
    {
      icon: 'fas fa-angle-up fa-fw',
      name: 'angle-up'
    },
    {
      icon: 'fas fa-angles-down fa-fw',
      name: 'angles-down'
    },
    {
      icon: 'fas fa-angles-left fa-fw',
      name: 'angles-left'
    },
    {
      icon: 'fas fa-angles-right fa-fw',
      name: 'angles-right'
    },
    {
      icon: 'fas fa-angles-up fa-fw',
      name: 'angles-up'
    },
    {
      icon: 'fas fa-ankh fa-fw',
      name: 'ankh'
    },
    {
      icon: 'fas fa-apple-whole fa-fw',
      name: 'apple-whole'
    },
    {
      icon: 'fas fa-archway fa-fw',
      name: 'archway'
    },
    {
      icon: 'fas fa-arrow-down fa-fw',
      name: 'arrow-down'
    },
    {
      icon: 'fas fa-arrow-down-1-9 fa-fw',
      name: 'arrow-down-1-9'
    },
    {
      icon: 'fas fa-arrow-down-9-1 fa-fw',
      name: 'arrow-down-9-1'
    },
    {
      icon: 'fas fa-arrow-down-a-z fa-fw',
      name: 'arrow-down-a-z'
    },
    {
      icon: 'fas fa-arrow-down-long fa-fw',
      name: 'arrow-down-long'
    },
    {
      icon: 'fas fa-arrow-down-short-wide fa-fw',
      name: 'arrow-down-short-wide'
    },
    {
      icon: 'fas fa-arrow-down-up-across-line fa-fw',
      name: 'arrow-down-up-across-line'
    },
    {
      icon: 'fas fa-arrow-down-up-lock fa-fw',
      name: 'arrow-down-up-lock'
    },
    {
      icon: 'fas fa-arrow-down-wide-short fa-fw',
      name: 'arrow-down-wide-short'
    },
    {
      icon: 'fas fa-arrow-down-z-a fa-fw',
      name: 'arrow-down-z-a'
    },
    {
      icon: 'fas fa-arrow-left fa-fw',
      name: 'arrow-left'
    },
    {
      icon: 'fas fa-arrow-left-long fa-fw',
      name: 'arrow-left-long'
    },
    {
      icon: 'fas fa-arrow-pointer fa-fw',
      name: 'arrow-pointer'
    },
    {
      icon: 'fas fa-arrow-right fa-fw',
      name: 'arrow-right'
    },
    {
      icon: 'fas fa-arrow-right-arrow-left fa-fw',
      name: 'arrow-right-arrow-left'
    },
    {
      icon: 'fas fa-arrow-right-from-bracket fa-fw',
      name: 'arrow-right-from-bracket'
    },
    {
      icon: 'fas fa-arrow-right-long fa-fw',
      name: 'arrow-right-long'
    },
    {
      icon: 'fas fa-arrow-right-to-bracket fa-fw',
      name: 'arrow-right-to-bracket'
    },
    {
      icon: 'fas fa-arrow-right-to-city fa-fw',
      name: 'arrow-right-to-city'
    },
    {
      icon: 'fas fa-arrow-rotate-left fa-fw',
      name: 'arrow-rotate-left'
    },
    {
      icon: 'fas fa-arrow-rotate-right fa-fw',
      name: 'arrow-rotate-right'
    },
    {
      icon: 'fas fa-arrow-trend-down fa-fw',
      name: 'arrow-trend-down'
    },
    {
      icon: 'fas fa-arrow-trend-up fa-fw',
      name: 'arrow-trend-up'
    },
    {
      icon: 'fas fa-arrow-turn-down fa-fw',
      name: 'arrow-turn-down'
    },
    {
      icon: 'fas fa-arrow-turn-up fa-fw',
      name: 'arrow-turn-up'
    },
    {
      icon: 'fas fa-arrow-up fa-fw',
      name: 'arrow-up'
    },
    {
      icon: 'fas fa-arrow-up-1-9 fa-fw',
      name: 'arrow-up-1-9'
    },
    {
      icon: 'fas fa-arrow-up-9-1 fa-fw',
      name: 'arrow-up-9-1'
    },
    {
      icon: 'fas fa-arrow-up-a-z fa-fw',
      name: 'arrow-up-a-z'
    },
    {
      icon: 'fas fa-arrow-up-from-bracket fa-fw',
      name: 'arrow-up-from-bracket'
    },
    {
      icon: 'fas fa-arrow-up-from-ground-water fa-fw',
      name: 'arrow-up-from-ground-water'
    },
    {
      icon: 'fas fa-arrow-up-from-water-pump fa-fw',
      name: 'arrow-up-from-water-pump'
    },
    {
      icon: 'fas fa-arrow-up-long fa-fw',
      name: 'arrow-up-long'
    },
    {
      icon: 'fas fa-arrow-up-right-dots fa-fw',
      name: 'arrow-up-right-dots'
    },
    {
      icon: 'fas fa-arrow-up-right-from-square fa-fw',
      name: 'arrow-up-right-from-square'
    },
    {
      icon: 'fas fa-arrow-up-short-wide fa-fw',
      name: 'arrow-up-short-wide'
    },
    {
      icon: 'fas fa-arrow-up-wide-short fa-fw',
      name: 'arrow-up-wide-short'
    },
    {
      icon: 'fas fa-arrow-up-z-a fa-fw',
      name: 'arrow-up-z-a'
    },
    {
      icon: 'fas fa-arrows-down-to-line fa-fw',
      name: 'arrows-down-to-line'
    },
    {
      icon: 'fas fa-arrows-down-to-people fa-fw',
      name: 'arrows-down-to-people'
    },
    {
      icon: 'fas fa-arrows-left-right fa-fw',
      name: 'arrows-left-right'
    },
    {
      icon: 'fas fa-arrows-left-right-to-line fa-fw',
      name: 'arrows-left-right-to-line'
    },
    {
      icon: 'fas fa-arrows-rotate fa-fw',
      name: 'arrows-rotate'
    },
    {
      icon: 'fas fa-arrows-spin fa-fw',
      name: 'arrows-spin'
    },
    {
      icon: 'fas fa-arrows-split-up-and-left fa-fw',
      name: 'arrows-split-up-and-left'
    },
    {
      icon: 'fas fa-arrows-to-circle fa-fw',
      name: 'arrows-to-circle'
    },
    {
      icon: 'fas fa-arrows-to-dot fa-fw',
      name: 'arrows-to-dot'
    },
    {
      icon: 'fas fa-arrows-to-eye fa-fw',
      name: 'arrows-to-eye'
    },
    {
      icon: 'fas fa-arrows-turn-right fa-fw',
      name: 'arrows-turn-right'
    },
    {
      icon: 'fas fa-arrows-turn-to-dots fa-fw',
      name: 'arrows-turn-to-dots'
    },
    {
      icon: 'fas fa-arrows-up-down fa-fw',
      name: 'arrows-up-down'
    },
    {
      icon: 'fas fa-arrows-up-down-left-right fa-fw',
      name: 'arrows-up-down-left-right'
    },
    {
      icon: 'fas fa-arrows-up-to-line fa-fw',
      name: 'arrows-up-to-line'
    },
    {
      icon: 'fas fa-asterisk fa-fw',
      name: 'asterisk'
    },
    {
      icon: 'fas fa-at fa-fw',
      name: 'at'
    },
    {
      icon: 'fas fa-atom fa-fw',
      name: 'atom'
    },
    {
      icon: 'fas fa-audio-description fa-fw',
      name: 'audio-description'
    },
    {
      icon: 'fas fa-austral-sign fa-fw',
      name: 'austral-sign'
    },
    {
      icon: 'fas fa-award fa-fw',
      name: 'award'
    },
    {
      icon: 'fas fa-b fa-fw',
      name: 'b'
    },
    {
      icon: 'fas fa-baby fa-fw',
      name: 'baby'
    },
    {
      icon: 'fas fa-baby-carriage fa-fw',
      name: 'baby-carriage'
    },
    {
      icon: 'fas fa-backward fa-fw',
      name: 'backward'
    },
    {
      icon: 'fas fa-backward-fast fa-fw',
      name: 'backward-fast'
    },
    {
      icon: 'fas fa-backward-step fa-fw',
      name: 'backward-step'
    },
    {
      icon: 'fas fa-bacon fa-fw',
      name: 'bacon'
    },
    {
      icon: 'fas fa-bacteria fa-fw',
      name: 'bacteria'
    },
    {
      icon: 'fas fa-bacterium fa-fw',
      name: 'bacterium'
    },
    {
      icon: 'fas fa-bag-shopping fa-fw',
      name: 'bag-shopping'
    },
    {
      icon: 'fas fa-bahai fa-fw',
      name: 'bahai'
    },
    {
      icon: 'fas fa-baht-sign fa-fw',
      name: 'baht-sign'
    },
    {
      icon: 'fas fa-ban fa-fw',
      name: 'ban'
    },
    {
      icon: 'fas fa-ban-smoking fa-fw',
      name: 'ban-smoking'
    },
    {
      icon: 'fas fa-bandage fa-fw',
      name: 'bandage'
    },
    {
      icon: 'fas fa-barcode fa-fw',
      name: 'barcode'
    },
    {
      icon: 'fas fa-bars fa-fw',
      name: 'bars'
    },
    {
      icon: 'fas fa-bars-progress fa-fw',
      name: 'bars-progress'
    },
    {
      icon: 'fas fa-bars-staggered fa-fw',
      name: 'bars-staggered'
    },
    {
      icon: 'fas fa-baseball fa-fw',
      name: 'baseball'
    },
    {
      icon: 'fas fa-baseball-bat-ball fa-fw',
      name: 'baseball-bat-ball'
    },
    {
      icon: 'fas fa-basket-shopping fa-fw',
      name: 'basket-shopping'
    },
    {
      icon: 'fas fa-basketball fa-fw',
      name: 'basketball'
    },
    {
      icon: 'fas fa-bath fa-fw',
      name: 'bath'
    },
    {
      icon: 'fas fa-battery-empty fa-fw',
      name: 'battery-empty'
    },
    {
      icon: 'fas fa-battery-full fa-fw',
      name: 'battery-full'
    },
    {
      icon: 'fas fa-battery-half fa-fw',
      name: 'battery-half'
    },
    {
      icon: 'fas fa-battery-quarter fa-fw',
      name: 'battery-quarter'
    },
    {
      icon: 'fas fa-battery-three-quarters fa-fw',
      name: 'battery-three-quarters'
    },
    {
      icon: 'fas fa-bed fa-fw',
      name: 'bed'
    },
    {
      icon: 'fas fa-bed-pulse fa-fw',
      name: 'bed-pulse'
    },
    {
      icon: 'fas fa-beer-mug-empty fa-fw',
      name: 'beer-mug-empty'
    },
    {
      icon: 'fas fa-bell fa-fw',
      name: 'bell'
    },
    {
      icon: 'fas fa-bell-concierge fa-fw',
      name: 'bell-concierge'
    },
    {
      icon: 'fas fa-bell-slash fa-fw',
      name: 'bell-slash'
    },
    {
      icon: 'fas fa-bezier-curve fa-fw',
      name: 'bezier-curve'
    },
    {
      icon: 'fas fa-bicycle fa-fw',
      name: 'bicycle'
    },
    {
      icon: 'fas fa-binoculars fa-fw',
      name: 'binoculars'
    },
    {
      icon: 'fas fa-biohazard fa-fw',
      name: 'biohazard'
    },
    {
      icon: 'fas fa-bitcoin-sign fa-fw',
      name: 'bitcoin-sign'
    },
    {
      icon: 'fas fa-blender fa-fw',
      name: 'blender'
    },
    {
      icon: 'fas fa-blender-phone fa-fw',
      name: 'blender-phone'
    },
    {
      icon: 'fas fa-blog fa-fw',
      name: 'blog'
    },
    {
      icon: 'fas fa-bold fa-fw',
      name: 'bold'
    },
    {
      icon: 'fas fa-bolt fa-fw',
      name: 'bolt'
    },
    {
      icon: 'fas fa-bolt-lightning fa-fw',
      name: 'bolt-lightning'
    },
    {
      icon: 'fas fa-bomb fa-fw',
      name: 'bomb'
    },
    {
      icon: 'fas fa-bone fa-fw',
      name: 'bone'
    },
    {
      icon: 'fas fa-bong fa-fw',
      name: 'bong'
    },
    {
      icon: 'fas fa-book fa-fw',
      name: 'book'
    },
    {
      icon: 'fas fa-book-atlas fa-fw',
      name: 'book-atlas'
    },
    {
      icon: 'fas fa-book-bible fa-fw',
      name: 'book-bible'
    },
    {
      icon: 'fas fa-book-bookmark fa-fw',
      name: 'book-bookmark'
    },
    {
      icon: 'fas fa-book-journal-whills fa-fw',
      name: 'book-journal-whills'
    },
    {
      icon: 'fas fa-book-medical fa-fw',
      name: 'book-medical'
    },
    {
      icon: 'fas fa-book-open fa-fw',
      name: 'book-open'
    },
    {
      icon: 'fas fa-book-open-reader fa-fw',
      name: 'book-open-reader'
    },
    {
      icon: 'fas fa-book-quran fa-fw',
      name: 'book-quran'
    },
    {
      icon: 'fas fa-book-skull fa-fw',
      name: 'book-skull'
    },
    {
      icon: 'fas fa-bookmark fa-fw',
      name: 'bookmark'
    },
    {
      icon: 'fas fa-border-all fa-fw',
      name: 'border-all'
    },
    {
      icon: 'fas fa-border-none fa-fw',
      name: 'border-none'
    },
    {
      icon: 'fas fa-border-top-left fa-fw',
      name: 'border-top-left'
    },
    {
      icon: 'fas fa-bore-hole fa-fw',
      name: 'bore-hole'
    },
    {
      icon: 'fas fa-bottle-droplet fa-fw',
      name: 'bottle-droplet'
    },
    {
      icon: 'fas fa-bottle-water fa-fw',
      name: 'bottle-water'
    },
    {
      icon: 'fas fa-bowl-food fa-fw',
      name: 'bowl-food'
    },
    {
      icon: 'fas fa-bowl-rice fa-fw',
      name: 'bowl-rice'
    },
    {
      icon: 'fas fa-bowling-ball fa-fw',
      name: 'bowling-ball'
    },
    {
      icon: 'fas fa-box fa-fw',
      name: 'box'
    },
    {
      icon: 'fas fa-box-archive fa-fw',
      name: 'box-archive'
    },
    {
      icon: 'fas fa-box-open fa-fw',
      name: 'box-open'
    },
    {
      icon: 'fas fa-box-tissue fa-fw',
      name: 'box-tissue'
    },
    {
      icon: 'fas fa-boxes-packing fa-fw',
      name: 'boxes-packing'
    },
    {
      icon: 'fas fa-boxes-stacked fa-fw',
      name: 'boxes-stacked'
    },
    {
      icon: 'fas fa-braille fa-fw',
      name: 'braille'
    },
    {
      icon: 'fas fa-brain fa-fw',
      name: 'brain'
    },
    {
      icon: 'fas fa-brazilian-real-sign fa-fw',
      name: 'brazilian-real-sign'
    },
    {
      icon: 'fas fa-bread-slice fa-fw',
      name: 'bread-slice'
    },
    {
      icon: 'fas fa-bridge fa-fw',
      name: 'bridge'
    },
    {
      icon: 'fas fa-bridge-circle-check fa-fw',
      name: 'bridge-circle-check'
    },
    {
      icon: 'fas fa-bridge-circle-exclamation fa-fw',
      name: 'bridge-circle-exclamation'
    },
    {
      icon: 'fas fa-bridge-circle-xmark fa-fw',
      name: 'bridge-circle-xmark'
    },
    {
      icon: 'fas fa-bridge-lock fa-fw',
      name: 'bridge-lock'
    },
    {
      icon: 'fas fa-bridge-water fa-fw',
      name: 'bridge-water'
    },
    {
      icon: 'fas fa-briefcase fa-fw',
      name: 'briefcase'
    },
    {
      icon: 'fas fa-briefcase-medical fa-fw',
      name: 'briefcase-medical'
    },
    {
      icon: 'fas fa-broom fa-fw',
      name: 'broom'
    },
    {
      icon: 'fas fa-broom-ball fa-fw',
      name: 'broom-ball'
    },
    {
      icon: 'fas fa-brush fa-fw',
      name: 'brush'
    },
    {
      icon: 'fas fa-bucket fa-fw',
      name: 'bucket'
    },
    {
      icon: 'fas fa-bug fa-fw',
      name: 'bug'
    },
    {
      icon: 'fas fa-bug-slash fa-fw',
      name: 'bug-slash'
    },
    {
      icon: 'fas fa-bugs fa-fw',
      name: 'bugs'
    },
    {
      icon: 'fas fa-building fa-fw',
      name: 'building'
    },
    {
      icon: 'fas fa-building-circle-arrow-right fa-fw',
      name: 'building-circle-arrow-right'
    },
    {
      icon: 'fas fa-building-circle-check fa-fw',
      name: 'building-circle-check'
    },
    {
      icon: 'fas fa-building-circle-exclamation fa-fw',
      name: 'building-circle-exclamation'
    },
    {
      icon: 'fas fa-building-circle-xmark fa-fw',
      name: 'building-circle-xmark'
    },
    {
      icon: 'fas fa-building-columns fa-fw',
      name: 'building-columns'
    },
    {
      icon: 'fas fa-building-flag fa-fw',
      name: 'building-flag'
    },
    {
      icon: 'fas fa-building-lock fa-fw',
      name: 'building-lock'
    },
    {
      icon: 'fas fa-building-ngo fa-fw',
      name: 'building-ngo'
    },
    {
      icon: 'fas fa-building-shield fa-fw',
      name: 'building-shield'
    },
    {
      icon: 'fas fa-building-un fa-fw',
      name: 'building-un'
    },
    {
      icon: 'fas fa-building-user fa-fw',
      name: 'building-user'
    },
    {
      icon: 'fas fa-building-wheat fa-fw',
      name: 'building-wheat'
    },
    {
      icon: 'fas fa-bullhorn fa-fw',
      name: 'bullhorn'
    },
    {
      icon: 'fas fa-bullseye fa-fw',
      name: 'bullseye'
    },
    {
      icon: 'fas fa-burger fa-fw',
      name: 'burger'
    },
    {
      icon: 'fas fa-burst fa-fw',
      name: 'burst'
    },
    {
      icon: 'fas fa-bus fa-fw',
      name: 'bus'
    },
    {
      icon: 'fas fa-bus-simple fa-fw',
      name: 'bus-simple'
    },
    {
      icon: 'fas fa-business-time fa-fw',
      name: 'business-time'
    },
    {
      icon: 'fas fa-c fa-fw',
      name: 'c'
    },
    {
      icon: 'fas fa-cake-candles fa-fw',
      name: 'cake-candles'
    },
    {
      icon: 'fas fa-calculator fa-fw',
      name: 'calculator'
    },
    {
      icon: 'fas fa-calendar fa-fw',
      name: 'calendar'
    },
    {
      icon: 'fas fa-calendar-check fa-fw',
      name: 'calendar-check'
    },
    {
      icon: 'fas fa-calendar-day fa-fw',
      name: 'calendar-day'
    },
    {
      icon: 'fas fa-calendar-days fa-fw',
      name: 'calendar-days'
    },
    {
      icon: 'fas fa-calendar-minus fa-fw',
      name: 'calendar-minus'
    },
    {
      icon: 'fas fa-calendar-plus fa-fw',
      name: 'calendar-plus'
    },
    {
      icon: 'fas fa-calendar-week fa-fw',
      name: 'calendar-week'
    },
    {
      icon: 'fas fa-calendar-xmark fa-fw',
      name: 'calendar-xmark'
    },
    {
      icon: 'fas fa-camera fa-fw',
      name: 'camera'
    },
    {
      icon: 'fas fa-camera-retro fa-fw',
      name: 'camera-retro'
    },
    {
      icon: 'fas fa-camera-rotate fa-fw',
      name: 'camera-rotate'
    },
    {
      icon: 'fas fa-campground fa-fw',
      name: 'campground'
    },
    {
      icon: 'fas fa-candy-cane fa-fw',
      name: 'candy-cane'
    },
    {
      icon: 'fas fa-cannabis fa-fw',
      name: 'cannabis'
    },
    {
      icon: 'fas fa-capsules fa-fw',
      name: 'capsules'
    },
    {
      icon: 'fas fa-car fa-fw',
      name: 'car'
    },
    {
      icon: 'fas fa-car-battery fa-fw',
      name: 'car-battery'
    },
    {
      icon: 'fas fa-car-burst fa-fw',
      name: 'car-burst'
    },
    {
      icon: 'fas fa-car-on fa-fw',
      name: 'car-on'
    },
    {
      icon: 'fas fa-car-rear fa-fw',
      name: 'car-rear'
    },
    {
      icon: 'fas fa-car-side fa-fw',
      name: 'car-side'
    },
    {
      icon: 'fas fa-car-tunnel fa-fw',
      name: 'car-tunnel'
    },
    {
      icon: 'fas fa-caravan fa-fw',
      name: 'caravan'
    },
    {
      icon: 'fas fa-caret-down fa-fw',
      name: 'caret-down'
    },
    {
      icon: 'fas fa-caret-left fa-fw',
      name: 'caret-left'
    },
    {
      icon: 'fas fa-caret-right fa-fw',
      name: 'caret-right'
    },
    {
      icon: 'fas fa-caret-up fa-fw',
      name: 'caret-up'
    },
    {
      icon: 'fas fa-carrot fa-fw',
      name: 'carrot'
    },
    {
      icon: 'fas fa-cart-arrow-down fa-fw',
      name: 'cart-arrow-down'
    },
    {
      icon: 'fas fa-cart-flatbed fa-fw',
      name: 'cart-flatbed'
    },
    {
      icon: 'fas fa-cart-flatbed-suitcase fa-fw',
      name: 'cart-flatbed-suitcase'
    },
    {
      icon: 'fas fa-cart-plus fa-fw',
      name: 'cart-plus'
    },
    {
      icon: 'fas fa-cart-shopping fa-fw',
      name: 'cart-shopping'
    },
    {
      icon: 'fas fa-cash-register fa-fw',
      name: 'cash-register'
    },
    {
      icon: 'fas fa-cat fa-fw',
      name: 'cat'
    },
    {
      icon: 'fas fa-cedi-sign fa-fw',
      name: 'cedi-sign'
    },
    {
      icon: 'fas fa-cent-sign fa-fw',
      name: 'cent-sign'
    },
    {
      icon: 'fas fa-certificate fa-fw',
      name: 'certificate'
    },
    {
      icon: 'fas fa-chair fa-fw',
      name: 'chair'
    },
    {
      icon: 'fas fa-chalkboard fa-fw',
      name: 'chalkboard'
    },
    {
      icon: 'fas fa-chalkboard-user fa-fw',
      name: 'chalkboard-user'
    },
    {
      icon: 'fas fa-champagne-glasses fa-fw',
      name: 'champagne-glasses'
    },
    {
      icon: 'fas fa-charging-station fa-fw',
      name: 'charging-station'
    },
    {
      icon: 'fas fa-chart-area fa-fw',
      name: 'chart-area'
    },
    {
      icon: 'fas fa-chart-bar fa-fw',
      name: 'chart-bar'
    },
    {
      icon: 'fas fa-chart-column fa-fw',
      name: 'chart-column'
    },
    {
      icon: 'fas fa-chart-gantt fa-fw',
      name: 'chart-gantt'
    },
    {
      icon: 'fas fa-chart-line fa-fw',
      name: 'chart-line'
    },
    {
      icon: 'fas fa-chart-pie fa-fw',
      name: 'chart-pie'
    },
    {
      icon: 'fas fa-chart-simple fa-fw',
      name: 'chart-simple'
    },
    {
      icon: 'fas fa-check fa-fw',
      name: 'check'
    },
    {
      icon: 'fas fa-check-double fa-fw',
      name: 'check-double'
    },
    {
      icon: 'fas fa-check-to-slot fa-fw',
      name: 'check-to-slot'
    },
    {
      icon: 'fas fa-cheese fa-fw',
      name: 'cheese'
    },
    {
      icon: 'fas fa-chess fa-fw',
      name: 'chess'
    },
    {
      icon: 'fas fa-chess-bishop fa-fw',
      name: 'chess-bishop'
    },
    {
      icon: 'fas fa-chess-board fa-fw',
      name: 'chess-board'
    },
    {
      icon: 'fas fa-chess-king fa-fw',
      name: 'chess-king'
    },
    {
      icon: 'fas fa-chess-knight fa-fw',
      name: 'chess-knight'
    },
    {
      icon: 'fas fa-chess-pawn fa-fw',
      name: 'chess-pawn'
    },
    {
      icon: 'fas fa-chess-queen fa-fw',
      name: 'chess-queen'
    },
    {
      icon: 'fas fa-chess-rook fa-fw',
      name: 'chess-rook'
    },
    {
      icon: 'fas fa-chevron-down fa-fw',
      name: 'chevron-down'
    },
    {
      icon: 'fas fa-chevron-left fa-fw',
      name: 'chevron-left'
    },
    {
      icon: 'fas fa-chevron-right fa-fw',
      name: 'chevron-right'
    },
    {
      icon: 'fas fa-chevron-up fa-fw',
      name: 'chevron-up'
    },
    {
      icon: 'fas fa-child fa-fw',
      name: 'child'
    },
    {
      icon: 'fas fa-child-dress fa-fw',
      name: 'child-dress'
    },
    {
      icon: 'fas fa-child-reaching fa-fw',
      name: 'child-reaching'
    },
    {
      icon: 'fas fa-child-rifle fa-fw',
      name: 'child-rifle'
    },
    {
      icon: 'fas fa-children fa-fw',
      name: 'children'
    },
    {
      icon: 'fas fa-church fa-fw',
      name: 'church'
    },
    {
      icon: 'fas fa-circle fa-fw',
      name: 'circle'
    },
    {
      icon: 'fas fa-circle-arrow-down fa-fw',
      name: 'circle-arrow-down'
    },
    {
      icon: 'fas fa-circle-arrow-left fa-fw',
      name: 'circle-arrow-left'
    },
    {
      icon: 'fas fa-circle-arrow-right fa-fw',
      name: 'circle-arrow-right'
    },
    {
      icon: 'fas fa-circle-arrow-up fa-fw',
      name: 'circle-arrow-up'
    },
    {
      icon: 'fas fa-circle-check fa-fw',
      name: 'circle-check'
    },
    {
      icon: 'fas fa-circle-chevron-down fa-fw',
      name: 'circle-chevron-down'
    },
    {
      icon: 'fas fa-circle-chevron-left fa-fw',
      name: 'circle-chevron-left'
    },
    {
      icon: 'fas fa-circle-chevron-right fa-fw',
      name: 'circle-chevron-right'
    },
    {
      icon: 'fas fa-circle-chevron-up fa-fw',
      name: 'circle-chevron-up'
    },
    {
      icon: 'fas fa-circle-dollar-to-slot fa-fw',
      name: 'circle-dollar-to-slot'
    },
    {
      icon: 'fas fa-circle-dot fa-fw',
      name: 'circle-dot'
    },
    {
      icon: 'fas fa-circle-down fa-fw',
      name: 'circle-down'
    },
    {
      icon: 'fas fa-circle-exclamation fa-fw',
      name: 'circle-exclamation'
    },
    {
      icon: 'fas fa-circle-h fa-fw',
      name: 'circle-h'
    },
    {
      icon: 'fas fa-circle-half-stroke fa-fw',
      name: 'circle-half-stroke'
    },
    {
      icon: 'fas fa-circle-info fa-fw',
      name: 'circle-info'
    },
    {
      icon: 'fas fa-circle-left fa-fw',
      name: 'circle-left'
    },
    {
      icon: 'fas fa-circle-minus fa-fw',
      name: 'circle-minus'
    },
    {
      icon: 'fas fa-circle-nodes fa-fw',
      name: 'circle-nodes'
    },
    {
      icon: 'fas fa-circle-notch fa-fw',
      name: 'circle-notch'
    },
    {
      icon: 'fas fa-circle-pause fa-fw',
      name: 'circle-pause'
    },
    {
      icon: 'fas fa-circle-play fa-fw',
      name: 'circle-play'
    },
    {
      icon: 'fas fa-circle-plus fa-fw',
      name: 'circle-plus'
    },
    {
      icon: 'fas fa-circle-question fa-fw',
      name: 'circle-question'
    },
    {
      icon: 'fas fa-circle-radiation fa-fw',
      name: 'circle-radiation'
    },
    {
      icon: 'fas fa-circle-right fa-fw',
      name: 'circle-right'
    },
    {
      icon: 'fas fa-circle-stop fa-fw',
      name: 'circle-stop'
    },
    {
      icon: 'fas fa-circle-up fa-fw',
      name: 'circle-up'
    },
    {
      icon: 'fas fa-circle-user fa-fw',
      name: 'circle-user'
    },
    {
      icon: 'fas fa-circle-xmark fa-fw',
      name: 'circle-xmark'
    },
    {
      icon: 'fas fa-city fa-fw',
      name: 'city'
    },
    {
      icon: 'fas fa-clapperboard fa-fw',
      name: 'clapperboard'
    },
    {
      icon: 'fas fa-clipboard fa-fw',
      name: 'clipboard'
    },
    {
      icon: 'fas fa-clipboard-check fa-fw',
      name: 'clipboard-check'
    },
    {
      icon: 'fas fa-clipboard-list fa-fw',
      name: 'clipboard-list'
    },
    {
      icon: 'fas fa-clipboard-question fa-fw',
      name: 'clipboard-question'
    },
    {
      icon: 'fas fa-clipboard-user fa-fw',
      name: 'clipboard-user'
    },
    {
      icon: 'fas fa-clock fa-fw',
      name: 'clock'
    },
    {
      icon: 'fas fa-clock-rotate-left fa-fw',
      name: 'clock-rotate-left'
    },
    {
      icon: 'fas fa-clone fa-fw',
      name: 'clone'
    },
    {
      icon: 'fas fa-closed-captioning fa-fw',
      name: 'closed-captioning'
    },
    {
      icon: 'fas fa-cloud fa-fw',
      name: 'cloud'
    },
    {
      icon: 'fas fa-cloud-arrow-down fa-fw',
      name: 'cloud-arrow-down'
    },
    {
      icon: 'fas fa-cloud-arrow-up fa-fw',
      name: 'cloud-arrow-up'
    },
    {
      icon: 'fas fa-cloud-bolt fa-fw',
      name: 'cloud-bolt'
    },
    {
      icon: 'fas fa-cloud-meatball fa-fw',
      name: 'cloud-meatball'
    },
    {
      icon: 'fas fa-cloud-moon fa-fw',
      name: 'cloud-moon'
    },
    {
      icon: 'fas fa-cloud-moon-rain fa-fw',
      name: 'cloud-moon-rain'
    },
    {
      icon: 'fas fa-cloud-rain fa-fw',
      name: 'cloud-rain'
    },
    {
      icon: 'fas fa-cloud-showers-heavy fa-fw',
      name: 'cloud-showers-heavy'
    },
    {
      icon: 'fas fa-cloud-showers-water fa-fw',
      name: 'cloud-showers-water'
    },
    {
      icon: 'fas fa-cloud-sun fa-fw',
      name: 'cloud-sun'
    },
    {
      icon: 'fas fa-cloud-sun-rain fa-fw',
      name: 'cloud-sun-rain'
    },
    {
      icon: 'fas fa-clover fa-fw',
      name: 'clover'
    },
    {
      icon: 'fas fa-code fa-fw',
      name: 'code'
    },
    {
      icon: 'fas fa-code-branch fa-fw',
      name: 'code-branch'
    },
    {
      icon: 'fas fa-code-commit fa-fw',
      name: 'code-commit'
    },
    {
      icon: 'fas fa-code-compare fa-fw',
      name: 'code-compare'
    },
    {
      icon: 'fas fa-code-fork fa-fw',
      name: 'code-fork'
    },
    {
      icon: 'fas fa-code-merge fa-fw',
      name: 'code-merge'
    },
    {
      icon: 'fas fa-code-pull-request fa-fw',
      name: 'code-pull-request'
    },
    {
      icon: 'fas fa-coins fa-fw',
      name: 'coins'
    },
    {
      icon: 'fas fa-colon-sign fa-fw',
      name: 'colon-sign'
    },
    {
      icon: 'fas fa-comment fa-fw',
      name: 'comment'
    },
    {
      icon: 'fas fa-comment-dollar fa-fw',
      name: 'comment-dollar'
    },
    {
      icon: 'fas fa-comment-dots fa-fw',
      name: 'comment-dots'
    },
    {
      icon: 'fas fa-comment-medical fa-fw',
      name: 'comment-medical'
    },
    {
      icon: 'fas fa-comment-slash fa-fw',
      name: 'comment-slash'
    },
    {
      icon: 'fas fa-comment-sms fa-fw',
      name: 'comment-sms'
    },
    {
      icon: 'fas fa-comments fa-fw',
      name: 'comments'
    },
    {
      icon: 'fas fa-comments-dollar fa-fw',
      name: 'comments-dollar'
    },
    {
      icon: 'fas fa-compact-disc fa-fw',
      name: 'compact-disc'
    },
    {
      icon: 'fas fa-compass fa-fw',
      name: 'compass'
    },
    {
      icon: 'fas fa-compass-drafting fa-fw',
      name: 'compass-drafting'
    },
    {
      icon: 'fas fa-compress fa-fw',
      name: 'compress'
    },
    {
      icon: 'fas fa-computer fa-fw',
      name: 'computer'
    },
    {
      icon: 'fas fa-computer-mouse fa-fw',
      name: 'computer-mouse'
    },
    {
      icon: 'fas fa-cookie fa-fw',
      name: 'cookie'
    },
    {
      icon: 'fas fa-cookie-bite fa-fw',
      name: 'cookie-bite'
    },
    {
      icon: 'fas fa-copy fa-fw',
      name: 'copy'
    },
    {
      icon: 'fas fa-copyright fa-fw',
      name: 'copyright'
    },
    {
      icon: 'fas fa-couch fa-fw',
      name: 'couch'
    },
    {
      icon: 'fas fa-cow fa-fw',
      name: 'cow'
    },
    {
      icon: 'fas fa-credit-card fa-fw',
      name: 'credit-card'
    },
    {
      icon: 'fas fa-crop fa-fw',
      name: 'crop'
    },
    {
      icon: 'fas fa-crop-simple fa-fw',
      name: 'crop-simple'
    },
    {
      icon: 'fas fa-cross fa-fw',
      name: 'cross'
    },
    {
      icon: 'fas fa-crosshairs fa-fw',
      name: 'crosshairs'
    },
    {
      icon: 'fas fa-crow fa-fw',
      name: 'crow'
    },
    {
      icon: 'fas fa-crown fa-fw',
      name: 'crown'
    },
    {
      icon: 'fas fa-crutch fa-fw',
      name: 'crutch'
    },
    {
      icon: 'fas fa-cruzeiro-sign fa-fw',
      name: 'cruzeiro-sign'
    },
    {
      icon: 'fas fa-cube fa-fw',
      name: 'cube'
    },
    {
      icon: 'fas fa-cubes fa-fw',
      name: 'cubes'
    },
    {
      icon: 'fas fa-cubes-stacked fa-fw',
      name: 'cubes-stacked'
    },
    {
      icon: 'fas fa-d fa-fw',
      name: 'd'
    },
    {
      icon: 'fas fa-database fa-fw',
      name: 'database'
    },
    {
      icon: 'fas fa-delete-left fa-fw',
      name: 'delete-left'
    },
    {
      icon: 'fas fa-democrat fa-fw',
      name: 'democrat'
    },
    {
      icon: 'fas fa-desktop fa-fw',
      name: 'desktop'
    },
    {
      icon: 'fas fa-dharmachakra fa-fw',
      name: 'dharmachakra'
    },
    {
      icon: 'fas fa-diagram-next fa-fw',
      name: 'diagram-next'
    },
    {
      icon: 'fas fa-diagram-predecessor fa-fw',
      name: 'diagram-predecessor'
    },
    {
      icon: 'fas fa-diagram-project fa-fw',
      name: 'diagram-project'
    },
    {
      icon: 'fas fa-diagram-successor fa-fw',
      name: 'diagram-successor'
    },
    {
      icon: 'fas fa-diamond fa-fw',
      name: 'diamond'
    },
    {
      icon: 'fas fa-diamond-turn-right fa-fw',
      name: 'diamond-turn-right'
    },
    {
      icon: 'fas fa-dice fa-fw',
      name: 'dice'
    },
    {
      icon: 'fas fa-dice-d20 fa-fw',
      name: 'dice-d20'
    },
    {
      icon: 'fas fa-dice-d6 fa-fw',
      name: 'dice-d6'
    },
    {
      icon: 'fas fa-dice-five fa-fw',
      name: 'dice-five'
    },
    {
      icon: 'fas fa-dice-four fa-fw',
      name: 'dice-four'
    },
    {
      icon: 'fas fa-dice-one fa-fw',
      name: 'dice-one'
    },
    {
      icon: 'fas fa-dice-six fa-fw',
      name: 'dice-six'
    },
    {
      icon: 'fas fa-dice-three fa-fw',
      name: 'dice-three'
    },
    {
      icon: 'fas fa-dice-two fa-fw',
      name: 'dice-two'
    },
    {
      icon: 'fas fa-disease fa-fw',
      name: 'disease'
    },
    {
      icon: 'fas fa-display fa-fw',
      name: 'display'
    },
    {
      icon: 'fas fa-divide fa-fw',
      name: 'divide'
    },
    {
      icon: 'fas fa-dna fa-fw',
      name: 'dna'
    },
    {
      icon: 'fas fa-dog fa-fw',
      name: 'dog'
    },
    {
      icon: 'fas fa-dollar-sign fa-fw',
      name: 'dollar-sign'
    },
    {
      icon: 'fas fa-dolly fa-fw',
      name: 'dolly'
    },
    {
      icon: 'fas fa-dong-sign fa-fw',
      name: 'dong-sign'
    },
    {
      icon: 'fas fa-door-closed fa-fw',
      name: 'door-closed'
    },
    {
      icon: 'fas fa-door-open fa-fw',
      name: 'door-open'
    },
    {
      icon: 'fas fa-dove fa-fw',
      name: 'dove'
    },
    {
      icon: 'fas fa-down-left-and-up-right-to-center fa-fw',
      name: 'down-left-and-up-right-to-center'
    },
    {
      icon: 'fas fa-down-long fa-fw',
      name: 'down-long'
    },
    {
      icon: 'fas fa-download fa-fw',
      name: 'download'
    },
    {
      icon: 'fas fa-dragon fa-fw',
      name: 'dragon'
    },
    {
      icon: 'fas fa-draw-polygon fa-fw',
      name: 'draw-polygon'
    },
    {
      icon: 'fas fa-droplet fa-fw',
      name: 'droplet'
    },
    {
      icon: 'fas fa-droplet-slash fa-fw',
      name: 'droplet-slash'
    },
    {
      icon: 'fas fa-drum fa-fw',
      name: 'drum'
    },
    {
      icon: 'fas fa-drum-steelpan fa-fw',
      name: 'drum-steelpan'
    },
    {
      icon: 'fas fa-drumstick-bite fa-fw',
      name: 'drumstick-bite'
    },
    {
      icon: 'fas fa-dumbbell fa-fw',
      name: 'dumbbell'
    },
    {
      icon: 'fas fa-dumpster fa-fw',
      name: 'dumpster'
    },
    {
      icon: 'fas fa-dumpster-fire fa-fw',
      name: 'dumpster-fire'
    },
    {
      icon: 'fas fa-dungeon fa-fw',
      name: 'dungeon'
    },
    {
      icon: 'fas fa-e fa-fw',
      name: 'e'
    },
    {
      icon: 'fas fa-ear-deaf fa-fw',
      name: 'ear-deaf'
    },
    {
      icon: 'fas fa-ear-listen fa-fw',
      name: 'ear-listen'
    },
    {
      icon: 'fas fa-earth-africa fa-fw',
      name: 'earth-africa'
    },
    {
      icon: 'fas fa-earth-americas fa-fw',
      name: 'earth-americas'
    },
    {
      icon: 'fas fa-earth-asia fa-fw',
      name: 'earth-asia'
    },
    {
      icon: 'fas fa-earth-europe fa-fw',
      name: 'earth-europe'
    },
    {
      icon: 'fas fa-earth-oceania fa-fw',
      name: 'earth-oceania'
    },
    {
      icon: 'fas fa-egg fa-fw',
      name: 'egg'
    },
    {
      icon: 'fas fa-eject fa-fw',
      name: 'eject'
    },
    {
      icon: 'fas fa-elevator fa-fw',
      name: 'elevator'
    },
    {
      icon: 'fas fa-ellipsis fa-fw',
      name: 'ellipsis'
    },
    {
      icon: 'fas fa-ellipsis-vertical fa-fw',
      name: 'ellipsis-vertical'
    },
    {
      icon: 'fas fa-envelope fa-fw',
      name: 'envelope'
    },
    {
      icon: 'fas fa-envelope-circle-check fa-fw',
      name: 'envelope-circle-check'
    },
    {
      icon: 'fas fa-envelope-open fa-fw',
      name: 'envelope-open'
    },
    {
      icon: 'fas fa-envelope-open-text fa-fw',
      name: 'envelope-open-text'
    },
    {
      icon: 'fas fa-envelopes-bulk fa-fw',
      name: 'envelopes-bulk'
    },
    {
      icon: 'fas fa-equals fa-fw',
      name: 'equals'
    },
    {
      icon: 'fas fa-eraser fa-fw',
      name: 'eraser'
    },
    {
      icon: 'fas fa-ethernet fa-fw',
      name: 'ethernet'
    },
    {
      icon: 'fas fa-euro-sign fa-fw',
      name: 'euro-sign'
    },
    {
      icon: 'fas fa-exclamation fa-fw',
      name: 'exclamation'
    },
    {
      icon: 'fas fa-expand fa-fw',
      name: 'expand'
    },
    {
      icon: 'fas fa-explosion fa-fw',
      name: 'explosion'
    },
    {
      icon: 'fas fa-eye fa-fw',
      name: 'eye'
    },
    {
      icon: 'fas fa-eye-dropper fa-fw',
      name: 'eye-dropper'
    },
    {
      icon: 'fas fa-eye-low-vision fa-fw',
      name: 'eye-low-vision'
    },
    {
      icon: 'fas fa-eye-slash fa-fw',
      name: 'eye-slash'
    },
    {
      icon: 'fas fa-f fa-fw',
      name: 'f'
    },
    {
      icon: 'fas fa-face-angry fa-fw',
      name: 'face-angry'
    },
    {
      icon: 'fas fa-face-dizzy fa-fw',
      name: 'face-dizzy'
    },
    {
      icon: 'fas fa-face-flushed fa-fw',
      name: 'face-flushed'
    },
    {
      icon: 'fas fa-face-frown fa-fw',
      name: 'face-frown'
    },
    {
      icon: 'fas fa-face-frown-open fa-fw',
      name: 'face-frown-open'
    },
    {
      icon: 'fas fa-face-grimace fa-fw',
      name: 'face-grimace'
    },
    {
      icon: 'fas fa-face-grin fa-fw',
      name: 'face-grin'
    },
    {
      icon: 'fas fa-face-grin-beam fa-fw',
      name: 'face-grin-beam'
    },
    {
      icon: 'fas fa-face-grin-beam-sweat fa-fw',
      name: 'face-grin-beam-sweat'
    },
    {
      icon: 'fas fa-face-grin-hearts fa-fw',
      name: 'face-grin-hearts'
    },
    {
      icon: 'fas fa-face-grin-squint fa-fw',
      name: 'face-grin-squint'
    },
    {
      icon: 'fas fa-face-grin-squint-tears fa-fw',
      name: 'face-grin-squint-tears'
    },
    {
      icon: 'fas fa-face-grin-stars fa-fw',
      name: 'face-grin-stars'
    },
    {
      icon: 'fas fa-face-grin-tears fa-fw',
      name: 'face-grin-tears'
    },
    {
      icon: 'fas fa-face-grin-tongue fa-fw',
      name: 'face-grin-tongue'
    },
    {
      icon: 'fas fa-face-grin-tongue-squint fa-fw',
      name: 'face-grin-tongue-squint'
    },
    {
      icon: 'fas fa-face-grin-tongue-wink fa-fw',
      name: 'face-grin-tongue-wink'
    },
    {
      icon: 'fas fa-face-grin-wide fa-fw',
      name: 'face-grin-wide'
    },
    {
      icon: 'fas fa-face-grin-wink fa-fw',
      name: 'face-grin-wink'
    },
    {
      icon: 'fas fa-face-kiss fa-fw',
      name: 'face-kiss'
    },
    {
      icon: 'fas fa-face-kiss-beam fa-fw',
      name: 'face-kiss-beam'
    },
    {
      icon: 'fas fa-face-kiss-wink-heart fa-fw',
      name: 'face-kiss-wink-heart'
    },
    {
      icon: 'fas fa-face-laugh fa-fw',
      name: 'face-laugh'
    },
    {
      icon: 'fas fa-face-laugh-beam fa-fw',
      name: 'face-laugh-beam'
    },
    {
      icon: 'fas fa-face-laugh-squint fa-fw',
      name: 'face-laugh-squint'
    },
    {
      icon: 'fas fa-face-laugh-wink fa-fw',
      name: 'face-laugh-wink'
    },
    {
      icon: 'fas fa-face-meh fa-fw',
      name: 'face-meh'
    },
    {
      icon: 'fas fa-face-meh-blank fa-fw',
      name: 'face-meh-blank'
    },
    {
      icon: 'fas fa-face-rolling-eyes fa-fw',
      name: 'face-rolling-eyes'
    },
    {
      icon: 'fas fa-face-sad-cry fa-fw',
      name: 'face-sad-cry'
    },
    {
      icon: 'fas fa-face-sad-tear fa-fw',
      name: 'face-sad-tear'
    },
    {
      icon: 'fas fa-face-smile fa-fw',
      name: 'face-smile'
    },
    {
      icon: 'fas fa-face-smile-beam fa-fw',
      name: 'face-smile-beam'
    },
    {
      icon: 'fas fa-face-smile-wink fa-fw',
      name: 'face-smile-wink'
    },
    {
      icon: 'fas fa-face-surprise fa-fw',
      name: 'face-surprise'
    },
    {
      icon: 'fas fa-face-tired fa-fw',
      name: 'face-tired'
    },
    {
      icon: 'fas fa-fan fa-fw',
      name: 'fan'
    },
    {
      icon: 'fas fa-faucet fa-fw',
      name: 'faucet'
    },
    {
      icon: 'fas fa-faucet-drip fa-fw',
      name: 'faucet-drip'
    },
    {
      icon: 'fas fa-fax fa-fw',
      name: 'fax'
    },
    {
      icon: 'fas fa-feather fa-fw',
      name: 'feather'
    },
    {
      icon: 'fas fa-feather-pointed fa-fw',
      name: 'feather-pointed'
    },
    {
      icon: 'fas fa-ferry fa-fw',
      name: 'ferry'
    },
    {
      icon: 'fas fa-file fa-fw',
      name: 'file'
    },
    {
      icon: 'fas fa-file-arrow-down fa-fw',
      name: 'file-arrow-down'
    },
    {
      icon: 'fas fa-file-arrow-up fa-fw',
      name: 'file-arrow-up'
    },
    {
      icon: 'fas fa-file-audio fa-fw',
      name: 'file-audio'
    },
    {
      icon: 'fas fa-file-circle-check fa-fw',
      name: 'file-circle-check'
    },
    {
      icon: 'fas fa-file-circle-exclamation fa-fw',
      name: 'file-circle-exclamation'
    },
    {
      icon: 'fas fa-file-circle-minus fa-fw',
      name: 'file-circle-minus'
    },
    {
      icon: 'fas fa-file-circle-plus fa-fw',
      name: 'file-circle-plus'
    },
    {
      icon: 'fas fa-file-circle-question fa-fw',
      name: 'file-circle-question'
    },
    {
      icon: 'fas fa-file-circle-xmark fa-fw',
      name: 'file-circle-xmark'
    },
    {
      icon: 'fas fa-file-code fa-fw',
      name: 'file-code'
    },
    {
      icon: 'fas fa-file-contract fa-fw',
      name: 'file-contract'
    },
    {
      icon: 'fas fa-file-csv fa-fw',
      name: 'file-csv'
    },
    {
      icon: 'fas fa-file-excel fa-fw',
      name: 'file-excel'
    },
    {
      icon: 'fas fa-file-export fa-fw',
      name: 'file-export'
    },
    {
      icon: 'fas fa-file-image fa-fw',
      name: 'file-image'
    },
    {
      icon: 'fas fa-file-import fa-fw',
      name: 'file-import'
    },
    {
      icon: 'fas fa-file-invoice fa-fw',
      name: 'file-invoice'
    },
    {
      icon: 'fas fa-file-invoice-dollar fa-fw',
      name: 'file-invoice-dollar'
    },
    {
      icon: 'fas fa-file-lines fa-fw',
      name: 'file-lines'
    },
    {
      icon: 'fas fa-file-medical fa-fw',
      name: 'file-medical'
    },
    {
      icon: 'fas fa-file-pdf fa-fw',
      name: 'file-pdf'
    },
    {
      icon: 'fas fa-file-pen fa-fw',
      name: 'file-pen'
    },
    {
      icon: 'fas fa-file-powerpoint fa-fw',
      name: 'file-powerpoint'
    },
    {
      icon: 'fas fa-file-prescription fa-fw',
      name: 'file-prescription'
    },
    {
      icon: 'fas fa-file-shield fa-fw',
      name: 'file-shield'
    },
    {
      icon: 'fas fa-file-signature fa-fw',
      name: 'file-signature'
    },
    {
      icon: 'fas fa-file-video fa-fw',
      name: 'file-video'
    },
    {
      icon: 'fas fa-file-waveform fa-fw',
      name: 'file-waveform'
    },
    {
      icon: 'fas fa-file-word fa-fw',
      name: 'file-word'
    },
    {
      icon: 'fas fa-file-zipper fa-fw',
      name: 'file-zipper'
    },
    {
      icon: 'fas fa-fill fa-fw',
      name: 'fill'
    },
    {
      icon: 'fas fa-fill-drip fa-fw',
      name: 'fill-drip'
    },
    {
      icon: 'fas fa-film fa-fw',
      name: 'film'
    },
    {
      icon: 'fas fa-filter fa-fw',
      name: 'filter'
    },
    {
      icon: 'fas fa-filter-circle-dollar fa-fw',
      name: 'filter-circle-dollar'
    },
    {
      icon: 'fas fa-filter-circle-xmark fa-fw',
      name: 'filter-circle-xmark'
    },
    {
      icon: 'fas fa-fingerprint fa-fw',
      name: 'fingerprint'
    },
    {
      icon: 'fas fa-fire fa-fw',
      name: 'fire'
    },
    {
      icon: 'fas fa-fire-burner fa-fw',
      name: 'fire-burner'
    },
    {
      icon: 'fas fa-fire-extinguisher fa-fw',
      name: 'fire-extinguisher'
    },
    {
      icon: 'fas fa-fire-flame-curved fa-fw',
      name: 'fire-flame-curved'
    },
    {
      icon: 'fas fa-fire-flame-simple fa-fw',
      name: 'fire-flame-simple'
    },
    {
      icon: 'fas fa-fish fa-fw',
      name: 'fish'
    },
    {
      icon: 'fas fa-fish-fins fa-fw',
      name: 'fish-fins'
    },
    {
      icon: 'fas fa-flag fa-fw',
      name: 'flag'
    },
    {
      icon: 'fas fa-flag-checkered fa-fw',
      name: 'flag-checkered'
    },
    {
      icon: 'fas fa-flag-usa fa-fw',
      name: 'flag-usa'
    },
    {
      icon: 'fas fa-flask fa-fw',
      name: 'flask'
    },
    {
      icon: 'fas fa-flask-vial fa-fw',
      name: 'flask-vial'
    },
    {
      icon: 'fas fa-floppy-disk fa-fw',
      name: 'floppy-disk'
    },
    {
      icon: 'fas fa-florin-sign fa-fw',
      name: 'florin-sign'
    },
    {
      icon: 'fas fa-folder fa-fw',
      name: 'folder'
    },
    {
      icon: 'fas fa-folder-closed fa-fw',
      name: 'folder-closed'
    },
    {
      icon: 'fas fa-folder-minus fa-fw',
      name: 'folder-minus'
    },
    {
      icon: 'fas fa-folder-open fa-fw',
      name: 'folder-open'
    },
    {
      icon: 'fas fa-folder-plus fa-fw',
      name: 'folder-plus'
    },
    {
      icon: 'fas fa-folder-tree fa-fw',
      name: 'folder-tree'
    },
    {
      icon: 'fas fa-font fa-fw',
      name: 'font'
    },
    {
      icon: 'fas fa-font-awesome fa-fw',
      name: 'font-awesome'
    },
    {
      icon: 'fas fa-football fa-fw',
      name: 'football'
    },
    {
      icon: 'fas fa-forward fa-fw',
      name: 'forward'
    },
    {
      icon: 'fas fa-forward-fast fa-fw',
      name: 'forward-fast'
    },
    {
      icon: 'fas fa-forward-step fa-fw',
      name: 'forward-step'
    },
    {
      icon: 'fas fa-franc-sign fa-fw',
      name: 'franc-sign'
    },
    {
      icon: 'fas fa-frog fa-fw',
      name: 'frog'
    },
    {
      icon: 'fas fa-futbol fa-fw',
      name: 'futbol'
    },
    {
      icon: 'fas fa-g fa-fw',
      name: 'g'
    },
    {
      icon: 'fas fa-gamepad fa-fw',
      name: 'gamepad'
    },
    {
      icon: 'fas fa-gas-pump fa-fw',
      name: 'gas-pump'
    },
    {
      icon: 'fas fa-gauge fa-fw',
      name: 'gauge'
    },
    {
      icon: 'fas fa-gauge-high fa-fw',
      name: 'gauge-high'
    },
    {
      icon: 'fas fa-gauge-simple fa-fw',
      name: 'gauge-simple'
    },
    {
      icon: 'fas fa-gauge-simple-high fa-fw',
      name: 'gauge-simple-high'
    },
    {
      icon: 'fas fa-gavel fa-fw',
      name: 'gavel'
    },
    {
      icon: 'fas fa-gear fa-fw',
      name: 'gear'
    },
    {
      icon: 'fas fa-gears fa-fw',
      name: 'gears'
    },
    {
      icon: 'fas fa-gem fa-fw',
      name: 'gem'
    },
    {
      icon: 'fas fa-genderless fa-fw',
      name: 'genderless'
    },
    {
      icon: 'fas fa-ghost fa-fw',
      name: 'ghost'
    },
    {
      icon: 'fas fa-gift fa-fw',
      name: 'gift'
    },
    {
      icon: 'fas fa-gifts fa-fw',
      name: 'gifts'
    },
    {
      icon: 'fas fa-glass-water fa-fw',
      name: 'glass-water'
    },
    {
      icon: 'fas fa-glass-water-droplet fa-fw',
      name: 'glass-water-droplet'
    },
    {
      icon: 'fas fa-glasses fa-fw',
      name: 'glasses'
    },
    {
      icon: 'fas fa-globe fa-fw',
      name: 'globe'
    },
    {
      icon: 'fas fa-golf-ball-tee fa-fw',
      name: 'golf-ball-tee'
    },
    {
      icon: 'fas fa-gopuram fa-fw',
      name: 'gopuram'
    },
    {
      icon: 'fas fa-graduation-cap fa-fw',
      name: 'graduation-cap'
    },
    {
      icon: 'fas fa-greater-than fa-fw',
      name: 'greater-than'
    },
    {
      icon: 'fas fa-greater-than-equal fa-fw',
      name: 'greater-than-equal'
    },
    {
      icon: 'fas fa-grip fa-fw',
      name: 'grip'
    },
    {
      icon: 'fas fa-grip-lines fa-fw',
      name: 'grip-lines'
    },
    {
      icon: 'fas fa-grip-lines-vertical fa-fw',
      name: 'grip-lines-vertical'
    },
    {
      icon: 'fas fa-grip-vertical fa-fw',
      name: 'grip-vertical'
    },
    {
      icon: 'fas fa-group-arrows-rotate fa-fw',
      name: 'group-arrows-rotate'
    },
    {
      icon: 'fas fa-guarani-sign fa-fw',
      name: 'guarani-sign'
    },
    {
      icon: 'fas fa-guitar fa-fw',
      name: 'guitar'
    },
    {
      icon: 'fas fa-gun fa-fw',
      name: 'gun'
    },
    {
      icon: 'fas fa-h fa-fw',
      name: 'h'
    },
    {
      icon: 'fas fa-hammer fa-fw',
      name: 'hammer'
    },
    {
      icon: 'fas fa-hamsa fa-fw',
      name: 'hamsa'
    },
    {
      icon: 'fas fa-hand fa-fw',
      name: 'hand'
    },
    {
      icon: 'fas fa-hand-back-fist fa-fw',
      name: 'hand-back-fist'
    },
    {
      icon: 'fas fa-hand-dots fa-fw',
      name: 'hand-dots'
    },
    {
      icon: 'fas fa-hand-fist fa-fw',
      name: 'hand-fist'
    },
    {
      icon: 'fas fa-hand-holding fa-fw',
      name: 'hand-holding'
    },
    {
      icon: 'fas fa-hand-holding-dollar fa-fw',
      name: 'hand-holding-dollar'
    },
    {
      icon: 'fas fa-hand-holding-droplet fa-fw',
      name: 'hand-holding-droplet'
    },
    {
      icon: 'fas fa-hand-holding-hand fa-fw',
      name: 'hand-holding-hand'
    },
    {
      icon: 'fas fa-hand-holding-heart fa-fw',
      name: 'hand-holding-heart'
    },
    {
      icon: 'fas fa-hand-holding-medical fa-fw',
      name: 'hand-holding-medical'
    },
    {
      icon: 'fas fa-hand-lizard fa-fw',
      name: 'hand-lizard'
    },
    {
      icon: 'fas fa-hand-middle-finger fa-fw',
      name: 'hand-middle-finger'
    },
    {
      icon: 'fas fa-hand-peace fa-fw',
      name: 'hand-peace'
    },
    {
      icon: 'fas fa-hand-point-down fa-fw',
      name: 'hand-point-down'
    },
    {
      icon: 'fas fa-hand-point-left fa-fw',
      name: 'hand-point-left'
    },
    {
      icon: 'fas fa-hand-point-right fa-fw',
      name: 'hand-point-right'
    },
    {
      icon: 'fas fa-hand-point-up fa-fw',
      name: 'hand-point-up'
    },
    {
      icon: 'fas fa-hand-pointer fa-fw',
      name: 'hand-pointer'
    },
    {
      icon: 'fas fa-hand-scissors fa-fw',
      name: 'hand-scissors'
    },
    {
      icon: 'fas fa-hand-sparkles fa-fw',
      name: 'hand-sparkles'
    },
    {
      icon: 'fas fa-hand-spock fa-fw',
      name: 'hand-spock'
    },
    {
      icon: 'fas fa-handcuffs fa-fw',
      name: 'handcuffs'
    },
    {
      icon: 'fas fa-hands fa-fw',
      name: 'hands'
    },
    {
      icon: 'fas fa-hands-asl-interpreting fa-fw',
      name: 'hands-asl-interpreting'
    },
    {
      icon: 'fas fa-hands-bound fa-fw',
      name: 'hands-bound'
    },
    {
      icon: 'fas fa-hands-bubbles fa-fw',
      name: 'hands-bubbles'
    },
    {
      icon: 'fas fa-hands-clapping fa-fw',
      name: 'hands-clapping'
    },
    {
      icon: 'fas fa-hands-holding fa-fw',
      name: 'hands-holding'
    },
    {
      icon: 'fas fa-hands-holding-child fa-fw',
      name: 'hands-holding-child'
    },
    {
      icon: 'fas fa-hands-holding-circle fa-fw',
      name: 'hands-holding-circle'
    },
    {
      icon: 'fas fa-hands-praying fa-fw',
      name: 'hands-praying'
    },
    {
      icon: 'fas fa-handshake fa-fw',
      name: 'handshake'
    },
    {
      icon: 'fas fa-handshake-angle fa-fw',
      name: 'handshake-angle'
    },
    {
      icon: 'fas fa-handshake-simple fa-fw',
      name: 'handshake-simple'
    },
    {
      icon: 'fas fa-handshake-simple-slash fa-fw',
      name: 'handshake-simple-slash'
    },
    {
      icon: 'fas fa-handshake-slash fa-fw',
      name: 'handshake-slash'
    },
    {
      icon: 'fas fa-hanukiah fa-fw',
      name: 'hanukiah'
    },
    {
      icon: 'fas fa-hard-drive fa-fw',
      name: 'hard-drive'
    },
    {
      icon: 'fas fa-hashtag fa-fw',
      name: 'hashtag'
    },
    {
      icon: 'fas fa-hat-cowboy fa-fw',
      name: 'hat-cowboy'
    },
    {
      icon: 'fas fa-hat-cowboy-side fa-fw',
      name: 'hat-cowboy-side'
    },
    {
      icon: 'fas fa-hat-wizard fa-fw',
      name: 'hat-wizard'
    },
    {
      icon: 'fas fa-head-side-cough fa-fw',
      name: 'head-side-cough'
    },
    {
      icon: 'fas fa-head-side-cough-slash fa-fw',
      name: 'head-side-cough-slash'
    },
    {
      icon: 'fas fa-head-side-mask fa-fw',
      name: 'head-side-mask'
    },
    {
      icon: 'fas fa-head-side-virus fa-fw',
      name: 'head-side-virus'
    },
    {
      icon: 'fas fa-heading fa-fw',
      name: 'heading'
    },
    {
      icon: 'fas fa-headphones fa-fw',
      name: 'headphones'
    },
    {
      icon: 'fas fa-headphones-simple fa-fw',
      name: 'headphones-simple'
    },
    {
      icon: 'fas fa-headset fa-fw',
      name: 'headset'
    },
    {
      icon: 'fas fa-heart fa-fw',
      name: 'heart'
    },
    {
      icon: 'fas fa-heart-circle-bolt fa-fw',
      name: 'heart-circle-bolt'
    },
    {
      icon: 'fas fa-heart-circle-check fa-fw',
      name: 'heart-circle-check'
    },
    {
      icon: 'fas fa-heart-circle-exclamation fa-fw',
      name: 'heart-circle-exclamation'
    },
    {
      icon: 'fas fa-heart-circle-minus fa-fw',
      name: 'heart-circle-minus'
    },
    {
      icon: 'fas fa-heart-circle-plus fa-fw',
      name: 'heart-circle-plus'
    },
    {
      icon: 'fas fa-heart-circle-xmark fa-fw',
      name: 'heart-circle-xmark'
    },
    {
      icon: 'fas fa-heart-crack fa-fw',
      name: 'heart-crack'
    },
    {
      icon: 'fas fa-heart-pulse fa-fw',
      name: 'heart-pulse'
    },
    {
      icon: 'fas fa-helicopter fa-fw',
      name: 'helicopter'
    },
    {
      icon: 'fas fa-helicopter-symbol fa-fw',
      name: 'helicopter-symbol'
    },
    {
      icon: 'fas fa-helmet-safety fa-fw',
      name: 'helmet-safety'
    },
    {
      icon: 'fas fa-helmet-un fa-fw',
      name: 'helmet-un'
    },
    {
      icon: 'fas fa-highlighter fa-fw',
      name: 'highlighter'
    },
    {
      icon: 'fas fa-hill-avalanche fa-fw',
      name: 'hill-avalanche'
    },
    {
      icon: 'fas fa-hill-rockslide fa-fw',
      name: 'hill-rockslide'
    },
    {
      icon: 'fas fa-hippo fa-fw',
      name: 'hippo'
    },
    {
      icon: 'fas fa-hockey-puck fa-fw',
      name: 'hockey-puck'
    },
    {
      icon: 'fas fa-holly-berry fa-fw',
      name: 'holly-berry'
    },
    {
      icon: 'fas fa-horse fa-fw',
      name: 'horse'
    },
    {
      icon: 'fas fa-horse-head fa-fw',
      name: 'horse-head'
    },
    {
      icon: 'fas fa-hospital fa-fw',
      name: 'hospital'
    },
    {
      icon: 'fas fa-hospital-user fa-fw',
      name: 'hospital-user'
    },
    {
      icon: 'fas fa-hot-tub-person fa-fw',
      name: 'hot-tub-person'
    },
    {
      icon: 'fas fa-hotdog fa-fw',
      name: 'hotdog'
    },
    {
      icon: 'fas fa-hotel fa-fw',
      name: 'hotel'
    },
    {
      icon: 'fas fa-hourglass fa-fw',
      name: 'hourglass'
    },
    {
      icon: 'fas fa-hourglass-empty fa-fw',
      name: 'hourglass-empty'
    },
    {
      icon: 'fas fa-hourglass-end fa-fw',
      name: 'hourglass-end'
    },
    {
      icon: 'fas fa-hourglass-start fa-fw',
      name: 'hourglass-start'
    },
    {
      icon: 'fas fa-house fa-fw',
      name: 'house'
    },
    {
      icon: 'fas fa-house-chimney fa-fw',
      name: 'house-chimney'
    },
    {
      icon: 'fas fa-house-chimney-crack fa-fw',
      name: 'house-chimney-crack'
    },
    {
      icon: 'fas fa-house-chimney-medical fa-fw',
      name: 'house-chimney-medical'
    },
    {
      icon: 'fas fa-house-chimney-user fa-fw',
      name: 'house-chimney-user'
    },
    {
      icon: 'fas fa-house-chimney-window fa-fw',
      name: 'house-chimney-window'
    },
    {
      icon: 'fas fa-house-circle-check fa-fw',
      name: 'house-circle-check'
    },
    {
      icon: 'fas fa-house-circle-exclamation fa-fw',
      name: 'house-circle-exclamation'
    },
    {
      icon: 'fas fa-house-circle-xmark fa-fw',
      name: 'house-circle-xmark'
    },
    {
      icon: 'fas fa-house-crack fa-fw',
      name: 'house-crack'
    },
    {
      icon: 'fas fa-house-fire fa-fw',
      name: 'house-fire'
    },
    {
      icon: 'fas fa-house-flag fa-fw',
      name: 'house-flag'
    },
    {
      icon: 'fas fa-house-flood-water fa-fw',
      name: 'house-flood-water'
    },
    {
      icon: 'fas fa-house-flood-water-circle-arrow-right fa-fw',
      name: 'house-flood-water-circle-arrow-right'
    },
    {
      icon: 'fas fa-house-laptop fa-fw',
      name: 'house-laptop'
    },
    {
      icon: 'fas fa-house-lock fa-fw',
      name: 'house-lock'
    },
    {
      icon: 'fas fa-house-medical fa-fw',
      name: 'house-medical'
    },
    {
      icon: 'fas fa-house-medical-circle-check fa-fw',
      name: 'house-medical-circle-check'
    },
    {
      icon: 'fas fa-house-medical-circle-exclamation fa-fw',
      name: 'house-medical-circle-exclamation'
    },
    {
      icon: 'fas fa-house-medical-circle-xmark fa-fw',
      name: 'house-medical-circle-xmark'
    },
    {
      icon: 'fas fa-house-medical-flag fa-fw',
      name: 'house-medical-flag'
    },
    {
      icon: 'fas fa-house-signal fa-fw',
      name: 'house-signal'
    },
    {
      icon: 'fas fa-house-tsunami fa-fw',
      name: 'house-tsunami'
    },
    {
      icon: 'fas fa-house-user fa-fw',
      name: 'house-user'
    },
    {
      icon: 'fas fa-hryvnia-sign fa-fw',
      name: 'hryvnia-sign'
    },
    {
      icon: 'fas fa-hurricane fa-fw',
      name: 'hurricane'
    },
    {
      icon: 'fas fa-i fa-fw',
      name: 'i'
    },
    {
      icon: 'fas fa-i-cursor fa-fw',
      name: 'i-cursor'
    },
    {
      icon: 'fas fa-ice-cream fa-fw',
      name: 'ice-cream'
    },
    {
      icon: 'fas fa-icicles fa-fw',
      name: 'icicles'
    },
    {
      icon: 'fas fa-icons fa-fw',
      name: 'icons'
    },
    {
      icon: 'fas fa-id-badge fa-fw',
      name: 'id-badge'
    },
    {
      icon: 'fas fa-id-card fa-fw',
      name: 'id-card'
    },
    {
      icon: 'fas fa-id-card-clip fa-fw',
      name: 'id-card-clip'
    },
    {
      icon: 'fas fa-igloo fa-fw',
      name: 'igloo'
    },
    {
      icon: 'fas fa-image fa-fw',
      name: 'image'
    },
    {
      icon: 'fas fa-image-portrait fa-fw',
      name: 'image-portrait'
    },
    {
      icon: 'fas fa-images fa-fw',
      name: 'images'
    },
    {
      icon: 'fas fa-inbox fa-fw',
      name: 'inbox'
    },
    {
      icon: 'fas fa-indent fa-fw',
      name: 'indent'
    },
    {
      icon: 'fas fa-indian-rupee-sign fa-fw',
      name: 'indian-rupee-sign'
    },
    {
      icon: 'fas fa-industry fa-fw',
      name: 'industry'
    },
    {
      icon: 'fas fa-infinity fa-fw',
      name: 'infinity'
    },
    {
      icon: 'fas fa-info fa-fw',
      name: 'info'
    },
    {
      icon: 'fas fa-italic fa-fw',
      name: 'italic'
    },
    {
      icon: 'fas fa-j fa-fw',
      name: 'j'
    },
    {
      icon: 'fas fa-jar fa-fw',
      name: 'jar'
    },
    {
      icon: 'fas fa-jar-wheat fa-fw',
      name: 'jar-wheat'
    },
    {
      icon: 'fas fa-jedi fa-fw',
      name: 'jedi'
    },
    {
      icon: 'fas fa-jet-fighter fa-fw',
      name: 'jet-fighter'
    },
    {
      icon: 'fas fa-jet-fighter-up fa-fw',
      name: 'jet-fighter-up'
    },
    {
      icon: 'fas fa-joint fa-fw',
      name: 'joint'
    },
    {
      icon: 'fas fa-jug-detergent fa-fw',
      name: 'jug-detergent'
    },
    {
      icon: 'fas fa-k fa-fw',
      name: 'k'
    },
    {
      icon: 'fas fa-kaaba fa-fw',
      name: 'kaaba'
    },
    {
      icon: 'fas fa-key fa-fw',
      name: 'key'
    },
    {
      icon: 'fas fa-keyboard fa-fw',
      name: 'keyboard'
    },
    {
      icon: 'fas fa-khanda fa-fw',
      name: 'khanda'
    },
    {
      icon: 'fas fa-kip-sign fa-fw',
      name: 'kip-sign'
    },
    {
      icon: 'fas fa-kit-medical fa-fw',
      name: 'kit-medical'
    },
    {
      icon: 'fas fa-kitchen-set fa-fw',
      name: 'kitchen-set'
    },
    {
      icon: 'fas fa-kiwi-bird fa-fw',
      name: 'kiwi-bird'
    },
    {
      icon: 'fas fa-l fa-fw',
      name: 'l'
    },
    {
      icon: 'fas fa-land-mine-on fa-fw',
      name: 'land-mine-on'
    },
    {
      icon: 'fas fa-landmark fa-fw',
      name: 'landmark'
    },
    {
      icon: 'fas fa-landmark-dome fa-fw',
      name: 'landmark-dome'
    },
    {
      icon: 'fas fa-landmark-flag fa-fw',
      name: 'landmark-flag'
    },
    {
      icon: 'fas fa-language fa-fw',
      name: 'language'
    },
    {
      icon: 'fas fa-laptop fa-fw',
      name: 'laptop'
    },
    {
      icon: 'fas fa-laptop-code fa-fw',
      name: 'laptop-code'
    },
    {
      icon: 'fas fa-laptop-file fa-fw',
      name: 'laptop-file'
    },
    {
      icon: 'fas fa-laptop-medical fa-fw',
      name: 'laptop-medical'
    },
    {
      icon: 'fas fa-lari-sign fa-fw',
      name: 'lari-sign'
    },
    {
      icon: 'fas fa-layer-group fa-fw',
      name: 'layer-group'
    },
    {
      icon: 'fas fa-leaf fa-fw',
      name: 'leaf'
    },
    {
      icon: 'fas fa-left-long fa-fw',
      name: 'left-long'
    },
    {
      icon: 'fas fa-left-right fa-fw',
      name: 'left-right'
    },
    {
      icon: 'fas fa-lemon fa-fw',
      name: 'lemon'
    },
    {
      icon: 'fas fa-less-than fa-fw',
      name: 'less-than'
    },
    {
      icon: 'fas fa-less-than-equal fa-fw',
      name: 'less-than-equal'
    },
    {
      icon: 'fas fa-life-ring fa-fw',
      name: 'life-ring'
    },
    {
      icon: 'fas fa-lightbulb fa-fw',
      name: 'lightbulb'
    },
    {
      icon: 'fas fa-lines-leaning fa-fw',
      name: 'lines-leaning'
    },
    {
      icon: 'fas fa-link fa-fw',
      name: 'link'
    },
    {
      icon: 'fas fa-link-slash fa-fw',
      name: 'link-slash'
    },
    {
      icon: 'fas fa-lira-sign fa-fw',
      name: 'lira-sign'
    },
    {
      icon: 'fas fa-list fa-fw',
      name: 'list'
    },
    {
      icon: 'fas fa-list-check fa-fw',
      name: 'list-check'
    },
    {
      icon: 'fas fa-list-ol fa-fw',
      name: 'list-ol'
    },
    {
      icon: 'fas fa-list-ul fa-fw',
      name: 'list-ul'
    },
    {
      icon: 'fas fa-litecoin-sign fa-fw',
      name: 'litecoin-sign'
    },
    {
      icon: 'fas fa-location-arrow fa-fw',
      name: 'location-arrow'
    },
    {
      icon: 'fas fa-location-crosshairs fa-fw',
      name: 'location-crosshairs'
    },
    {
      icon: 'fas fa-location-dot fa-fw',
      name: 'location-dot'
    },
    {
      icon: 'fas fa-location-pin fa-fw',
      name: 'location-pin'
    },
    {
      icon: 'fas fa-location-pin-lock fa-fw',
      name: 'location-pin-lock'
    },
    {
      icon: 'fas fa-lock fa-fw',
      name: 'lock'
    },
    {
      icon: 'fas fa-lock-open fa-fw',
      name: 'lock-open'
    },
    {
      icon: 'fas fa-locust fa-fw',
      name: 'locust'
    },
    {
      icon: 'fas fa-lungs fa-fw',
      name: 'lungs'
    },
    {
      icon: 'fas fa-lungs-virus fa-fw',
      name: 'lungs-virus'
    },
    {
      icon: 'fas fa-m fa-fw',
      name: 'm'
    },
    {
      icon: 'fas fa-magnet fa-fw',
      name: 'magnet'
    },
    {
      icon: 'fas fa-magnifying-glass fa-fw',
      name: 'magnifying-glass'
    },
    {
      icon: 'fas fa-magnifying-glass-arrow-right fa-fw',
      name: 'magnifying-glass-arrow-right'
    },
    {
      icon: 'fas fa-magnifying-glass-chart fa-fw',
      name: 'magnifying-glass-chart'
    },
    {
      icon: 'fas fa-magnifying-glass-dollar fa-fw',
      name: 'magnifying-glass-dollar'
    },
    {
      icon: 'fas fa-magnifying-glass-location fa-fw',
      name: 'magnifying-glass-location'
    },
    {
      icon: 'fas fa-magnifying-glass-minus fa-fw',
      name: 'magnifying-glass-minus'
    },
    {
      icon: 'fas fa-magnifying-glass-plus fa-fw',
      name: 'magnifying-glass-plus'
    },
    {
      icon: 'fas fa-manat-sign fa-fw',
      name: 'manat-sign'
    },
    {
      icon: 'fas fa-map fa-fw',
      name: 'map'
    },
    {
      icon: 'fas fa-map-location fa-fw',
      name: 'map-location'
    },
    {
      icon: 'fas fa-map-location-dot fa-fw',
      name: 'map-location-dot'
    },
    {
      icon: 'fas fa-map-pin fa-fw',
      name: 'map-pin'
    },
    {
      icon: 'fas fa-marker fa-fw',
      name: 'marker'
    },
    {
      icon: 'fas fa-mars fa-fw',
      name: 'mars'
    },
    {
      icon: 'fas fa-mars-and-venus fa-fw',
      name: 'mars-and-venus'
    },
    {
      icon: 'fas fa-mars-and-venus-burst fa-fw',
      name: 'mars-and-venus-burst'
    },
    {
      icon: 'fas fa-mars-double fa-fw',
      name: 'mars-double'
    },
    {
      icon: 'fas fa-mars-stroke fa-fw',
      name: 'mars-stroke'
    },
    {
      icon: 'fas fa-mars-stroke-right fa-fw',
      name: 'mars-stroke-right'
    },
    {
      icon: 'fas fa-mars-stroke-up fa-fw',
      name: 'mars-stroke-up'
    },
    {
      icon: 'fas fa-martini-glass fa-fw',
      name: 'martini-glass'
    },
    {
      icon: 'fas fa-martini-glass-citrus fa-fw',
      name: 'martini-glass-citrus'
    },
    {
      icon: 'fas fa-martini-glass-empty fa-fw',
      name: 'martini-glass-empty'
    },
    {
      icon: 'fas fa-mask fa-fw',
      name: 'mask'
    },
    {
      icon: 'fas fa-mask-face fa-fw',
      name: 'mask-face'
    },
    {
      icon: 'fas fa-mask-ventilator fa-fw',
      name: 'mask-ventilator'
    },
    {
      icon: 'fas fa-masks-theater fa-fw',
      name: 'masks-theater'
    },
    {
      icon: 'fas fa-mattress-pillow fa-fw',
      name: 'mattress-pillow'
    },
    {
      icon: 'fas fa-maximize fa-fw',
      name: 'maximize'
    },
    {
      icon: 'fas fa-medal fa-fw',
      name: 'medal'
    },
    {
      icon: 'fas fa-memory fa-fw',
      name: 'memory'
    },
    {
      icon: 'fas fa-menorah fa-fw',
      name: 'menorah'
    },
    {
      icon: 'fas fa-mercury fa-fw',
      name: 'mercury'
    },
    {
      icon: 'fas fa-message fa-fw',
      name: 'message'
    },
    {
      icon: 'fas fa-meteor fa-fw',
      name: 'meteor'
    },
    {
      icon: 'fas fa-microchip fa-fw',
      name: 'microchip'
    },
    {
      icon: 'fas fa-microphone fa-fw',
      name: 'microphone'
    },
    {
      icon: 'fas fa-microphone-lines fa-fw',
      name: 'microphone-lines'
    },
    {
      icon: 'fas fa-microphone-lines-slash fa-fw',
      name: 'microphone-lines-slash'
    },
    {
      icon: 'fas fa-microphone-slash fa-fw',
      name: 'microphone-slash'
    },
    {
      icon: 'fas fa-microscope fa-fw',
      name: 'microscope'
    },
    {
      icon: 'fas fa-mill-sign fa-fw',
      name: 'mill-sign'
    },
    {
      icon: 'fas fa-minimize fa-fw',
      name: 'minimize'
    },
    {
      icon: 'fas fa-minus fa-fw',
      name: 'minus'
    },
    {
      icon: 'fas fa-mitten fa-fw',
      name: 'mitten'
    },
    {
      icon: 'fas fa-mobile fa-fw',
      name: 'mobile'
    },
    {
      icon: 'fas fa-mobile-button fa-fw',
      name: 'mobile-button'
    },
    {
      icon: 'fas fa-mobile-retro fa-fw',
      name: 'mobile-retro'
    },
    {
      icon: 'fas fa-mobile-screen fa-fw',
      name: 'mobile-screen'
    },
    {
      icon: 'fas fa-mobile-screen-button fa-fw',
      name: 'mobile-screen-button'
    },
    {
      icon: 'fas fa-money-bill fa-fw',
      name: 'money-bill'
    },
    {
      icon: 'fas fa-money-bill-1 fa-fw',
      name: 'money-bill-1'
    },
    {
      icon: 'fas fa-money-bill-1-wave fa-fw',
      name: 'money-bill-1-wave'
    },
    {
      icon: 'fas fa-money-bill-transfer fa-fw',
      name: 'money-bill-transfer'
    },
    {
      icon: 'fas fa-money-bill-trend-up fa-fw',
      name: 'money-bill-trend-up'
    },
    {
      icon: 'fas fa-money-bill-wave fa-fw',
      name: 'money-bill-wave'
    },
    {
      icon: 'fas fa-money-bill-wheat fa-fw',
      name: 'money-bill-wheat'
    },
    {
      icon: 'fas fa-money-bills fa-fw',
      name: 'money-bills'
    },
    {
      icon: 'fas fa-money-check fa-fw',
      name: 'money-check'
    },
    {
      icon: 'fas fa-money-check-dollar fa-fw',
      name: 'money-check-dollar'
    },
    {
      icon: 'fas fa-monument fa-fw',
      name: 'monument'
    },
    {
      icon: 'fas fa-moon fa-fw',
      name: 'moon'
    },
    {
      icon: 'fas fa-mortar-pestle fa-fw',
      name: 'mortar-pestle'
    },
    {
      icon: 'fas fa-mosque fa-fw',
      name: 'mosque'
    },
    {
      icon: 'fas fa-mosquito fa-fw',
      name: 'mosquito'
    },
    {
      icon: 'fas fa-mosquito-net fa-fw',
      name: 'mosquito-net'
    },
    {
      icon: 'fas fa-motorcycle fa-fw',
      name: 'motorcycle'
    },
    {
      icon: 'fas fa-mound fa-fw',
      name: 'mound'
    },
    {
      icon: 'fas fa-mountain fa-fw',
      name: 'mountain'
    },
    {
      icon: 'fas fa-mountain-city fa-fw',
      name: 'mountain-city'
    },
    {
      icon: 'fas fa-mountain-sun fa-fw',
      name: 'mountain-sun'
    },
    {
      icon: 'fas fa-mug-hot fa-fw',
      name: 'mug-hot'
    },
    {
      icon: 'fas fa-mug-saucer fa-fw',
      name: 'mug-saucer'
    },
    {
      icon: 'fas fa-music fa-fw',
      name: 'music'
    },
    {
      icon: 'fas fa-n fa-fw',
      name: 'n'
    },
    {
      icon: 'fas fa-naira-sign fa-fw',
      name: 'naira-sign'
    },
    {
      icon: 'fas fa-network-wired fa-fw',
      name: 'network-wired'
    },
    {
      icon: 'fas fa-neuter fa-fw',
      name: 'neuter'
    },
    {
      icon: 'fas fa-newspaper fa-fw',
      name: 'newspaper'
    },
    {
      icon: 'fas fa-not-equal fa-fw',
      name: 'not-equal'
    },
    {
      icon: 'fas fa-note-sticky fa-fw',
      name: 'note-sticky'
    },
    {
      icon: 'fas fa-notes-medical fa-fw',
      name: 'notes-medical'
    },
    {
      icon: 'fas fa-o fa-fw',
      name: 'o'
    },
    {
      icon: 'fas fa-object-group fa-fw',
      name: 'object-group'
    },
    {
      icon: 'fas fa-object-ungroup fa-fw',
      name: 'object-ungroup'
    },
    {
      icon: 'fas fa-oil-can fa-fw',
      name: 'oil-can'
    },
    {
      icon: 'fas fa-oil-well fa-fw',
      name: 'oil-well'
    },
    {
      icon: 'fas fa-om fa-fw',
      name: 'om'
    },
    {
      icon: 'fas fa-otter fa-fw',
      name: 'otter'
    },
    {
      icon: 'fas fa-outdent fa-fw',
      name: 'outdent'
    },
    {
      icon: 'fas fa-p fa-fw',
      name: 'p'
    },
    {
      icon: 'fas fa-pager fa-fw',
      name: 'pager'
    },
    {
      icon: 'fas fa-paint-roller fa-fw',
      name: 'paint-roller'
    },
    {
      icon: 'fas fa-paintbrush fa-fw',
      name: 'paintbrush'
    },
    {
      icon: 'fas fa-palette fa-fw',
      name: 'palette'
    },
    {
      icon: 'fas fa-pallet fa-fw',
      name: 'pallet'
    },
    {
      icon: 'fas fa-panorama fa-fw',
      name: 'panorama'
    },
    {
      icon: 'fas fa-paper-plane fa-fw',
      name: 'paper-plane'
    },
    {
      icon: 'fas fa-paperclip fa-fw',
      name: 'paperclip'
    },
    {
      icon: 'fas fa-parachute-box fa-fw',
      name: 'parachute-box'
    },
    {
      icon: 'fas fa-paragraph fa-fw',
      name: 'paragraph'
    },
    {
      icon: 'fas fa-passport fa-fw',
      name: 'passport'
    },
    {
      icon: 'fas fa-paste fa-fw',
      name: 'paste'
    },
    {
      icon: 'fas fa-pause fa-fw',
      name: 'pause'
    },
    {
      icon: 'fas fa-paw fa-fw',
      name: 'paw'
    },
    {
      icon: 'fas fa-peace fa-fw',
      name: 'peace'
    },
    {
      icon: 'fas fa-pen fa-fw',
      name: 'pen'
    },
    {
      icon: 'fas fa-pen-clip fa-fw',
      name: 'pen-clip'
    },
    {
      icon: 'fas fa-pen-fancy fa-fw',
      name: 'pen-fancy'
    },
    {
      icon: 'fas fa-pen-nib fa-fw',
      name: 'pen-nib'
    },
    {
      icon: 'fas fa-pen-ruler fa-fw',
      name: 'pen-ruler'
    },
    {
      icon: 'fas fa-pen-to-square fa-fw',
      name: 'pen-to-square'
    },
    {
      icon: 'fas fa-pencil fa-fw',
      name: 'pencil'
    },
    {
      icon: 'fas fa-people-arrows-left-right fa-fw',
      name: 'people-arrows-left-right'
    },
    {
      icon: 'fas fa-people-carry-box fa-fw',
      name: 'people-carry-box'
    },
    {
      icon: 'fas fa-people-group fa-fw',
      name: 'people-group'
    },
    {
      icon: 'fas fa-people-line fa-fw',
      name: 'people-line'
    },
    {
      icon: 'fas fa-people-pulling fa-fw',
      name: 'people-pulling'
    },
    {
      icon: 'fas fa-people-robbery fa-fw',
      name: 'people-robbery'
    },
    {
      icon: 'fas fa-people-roof fa-fw',
      name: 'people-roof'
    },
    {
      icon: 'fas fa-pepper-hot fa-fw',
      name: 'pepper-hot'
    },
    {
      icon: 'fas fa-percent fa-fw',
      name: 'percent'
    },
    {
      icon: 'fas fa-person fa-fw',
      name: 'person'
    },
    {
      icon: 'fas fa-person-arrow-down-to-line fa-fw',
      name: 'person-arrow-down-to-line'
    },
    {
      icon: 'fas fa-person-arrow-up-from-line fa-fw',
      name: 'person-arrow-up-from-line'
    },
    {
      icon: 'fas fa-person-biking fa-fw',
      name: 'person-biking'
    },
    {
      icon: 'fas fa-person-booth fa-fw',
      name: 'person-booth'
    },
    {
      icon: 'fas fa-person-breastfeeding fa-fw',
      name: 'person-breastfeeding'
    },
    {
      icon: 'fas fa-person-burst fa-fw',
      name: 'person-burst'
    },
    {
      icon: 'fas fa-person-cane fa-fw',
      name: 'person-cane'
    },
    {
      icon: 'fas fa-person-chalkboard fa-fw',
      name: 'person-chalkboard'
    },
    {
      icon: 'fas fa-person-circle-check fa-fw',
      name: 'person-circle-check'
    },
    {
      icon: 'fas fa-person-circle-exclamation fa-fw',
      name: 'person-circle-exclamation'
    },
    {
      icon: 'fas fa-person-circle-minus fa-fw',
      name: 'person-circle-minus'
    },
    {
      icon: 'fas fa-person-circle-plus fa-fw',
      name: 'person-circle-plus'
    },
    {
      icon: 'fas fa-person-circle-question fa-fw',
      name: 'person-circle-question'
    },
    {
      icon: 'fas fa-person-circle-xmark fa-fw',
      name: 'person-circle-xmark'
    },
    {
      icon: 'fas fa-person-digging fa-fw',
      name: 'person-digging'
    },
    {
      icon: 'fas fa-person-dots-from-line fa-fw',
      name: 'person-dots-from-line'
    },
    {
      icon: 'fas fa-person-dress fa-fw',
      name: 'person-dress'
    },
    {
      icon: 'fas fa-person-dress-burst fa-fw',
      name: 'person-dress-burst'
    },
    {
      icon: 'fas fa-person-drowning fa-fw',
      name: 'person-drowning'
    },
    {
      icon: 'fas fa-person-falling fa-fw',
      name: 'person-falling'
    },
    {
      icon: 'fas fa-person-falling-burst fa-fw',
      name: 'person-falling-burst'
    },
    {
      icon: 'fas fa-person-half-dress fa-fw',
      name: 'person-half-dress'
    },
    {
      icon: 'fas fa-person-harassing fa-fw',
      name: 'person-harassing'
    },
    {
      icon: 'fas fa-person-hiking fa-fw',
      name: 'person-hiking'
    },
    {
      icon: 'fas fa-person-military-pointing fa-fw',
      name: 'person-military-pointing'
    },
    {
      icon: 'fas fa-person-military-rifle fa-fw',
      name: 'person-military-rifle'
    },
    {
      icon: 'fas fa-person-military-to-person fa-fw',
      name: 'person-military-to-person'
    },
    {
      icon: 'fas fa-person-praying fa-fw',
      name: 'person-praying'
    },
    {
      icon: 'fas fa-person-pregnant fa-fw',
      name: 'person-pregnant'
    },
    {
      icon: 'fas fa-person-rays fa-fw',
      name: 'person-rays'
    },
    {
      icon: 'fas fa-person-rifle fa-fw',
      name: 'person-rifle'
    },
    {
      icon: 'fas fa-person-running fa-fw',
      name: 'person-running'
    },
    {
      icon: 'fas fa-person-shelter fa-fw',
      name: 'person-shelter'
    },
    {
      icon: 'fas fa-person-skating fa-fw',
      name: 'person-skating'
    },
    {
      icon: 'fas fa-person-skiing fa-fw',
      name: 'person-skiing'
    },
    {
      icon: 'fas fa-person-skiing-nordic fa-fw',
      name: 'person-skiing-nordic'
    },
    {
      icon: 'fas fa-person-snowboarding fa-fw',
      name: 'person-snowboarding'
    },
    {
      icon: 'fas fa-person-swimming fa-fw',
      name: 'person-swimming'
    },
    {
      icon: 'fas fa-person-through-window fa-fw',
      name: 'person-through-window'
    },
    {
      icon: 'fas fa-person-walking fa-fw',
      name: 'person-walking'
    },
    {
      icon: 'fas fa-person-walking-arrow-loop-left fa-fw',
      name: 'person-walking-arrow-loop-left'
    },
    {
      icon: 'fas fa-person-walking-arrow-right fa-fw',
      name: 'person-walking-arrow-right'
    },
    {
      icon: 'fas fa-person-walking-dashed-line-arrow-right fa-fw',
      name: 'person-walking-dashed-line-arrow-right'
    },
    {
      icon: 'fas fa-person-walking-luggage fa-fw',
      name: 'person-walking-luggage'
    },
    {
      icon: 'fas fa-person-walking-with-cane fa-fw',
      name: 'person-walking-with-cane'
    },
    {
      icon: 'fas fa-peseta-sign fa-fw',
      name: 'peseta-sign'
    },
    {
      icon: 'fas fa-peso-sign fa-fw',
      name: 'peso-sign'
    },
    {
      icon: 'fas fa-phone fa-fw',
      name: 'phone'
    },
    {
      icon: 'fas fa-phone-flip fa-fw',
      name: 'phone-flip'
    },
    {
      icon: 'fas fa-phone-slash fa-fw',
      name: 'phone-slash'
    },
    {
      icon: 'fas fa-phone-volume fa-fw',
      name: 'phone-volume'
    },
    {
      icon: 'fas fa-photo-film fa-fw',
      name: 'photo-film'
    },
    {
      icon: 'fas fa-piggy-bank fa-fw',
      name: 'piggy-bank'
    },
    {
      icon: 'fas fa-pills fa-fw',
      name: 'pills'
    },
    {
      icon: 'fas fa-pizza-slice fa-fw',
      name: 'pizza-slice'
    },
    {
      icon: 'fas fa-place-of-worship fa-fw',
      name: 'place-of-worship'
    },
    {
      icon: 'fas fa-plane fa-fw',
      name: 'plane'
    },
    {
      icon: 'fas fa-plane-arrival fa-fw',
      name: 'plane-arrival'
    },
    {
      icon: 'fas fa-plane-circle-check fa-fw',
      name: 'plane-circle-check'
    },
    {
      icon: 'fas fa-plane-circle-exclamation fa-fw',
      name: 'plane-circle-exclamation'
    },
    {
      icon: 'fas fa-plane-circle-xmark fa-fw',
      name: 'plane-circle-xmark'
    },
    {
      icon: 'fas fa-plane-departure fa-fw',
      name: 'plane-departure'
    },
    {
      icon: 'fas fa-plane-lock fa-fw',
      name: 'plane-lock'
    },
    {
      icon: 'fas fa-plane-slash fa-fw',
      name: 'plane-slash'
    },
    {
      icon: 'fas fa-plane-up fa-fw',
      name: 'plane-up'
    },
    {
      icon: 'fas fa-plant-wilt fa-fw',
      name: 'plant-wilt'
    },
    {
      icon: 'fas fa-plate-wheat fa-fw',
      name: 'plate-wheat'
    },
    {
      icon: 'fas fa-play fa-fw',
      name: 'play'
    },
    {
      icon: 'fas fa-plug fa-fw',
      name: 'plug'
    },
    {
      icon: 'fas fa-plug-circle-bolt fa-fw',
      name: 'plug-circle-bolt'
    },
    {
      icon: 'fas fa-plug-circle-check fa-fw',
      name: 'plug-circle-check'
    },
    {
      icon: 'fas fa-plug-circle-exclamation fa-fw',
      name: 'plug-circle-exclamation'
    },
    {
      icon: 'fas fa-plug-circle-minus fa-fw',
      name: 'plug-circle-minus'
    },
    {
      icon: 'fas fa-plug-circle-plus fa-fw',
      name: 'plug-circle-plus'
    },
    {
      icon: 'fas fa-plug-circle-xmark fa-fw',
      name: 'plug-circle-xmark'
    },
    {
      icon: 'fas fa-plus fa-fw',
      name: 'plus'
    },
    {
      icon: 'fas fa-plus-minus fa-fw',
      name: 'plus-minus'
    },
    {
      icon: 'fas fa-podcast fa-fw',
      name: 'podcast'
    },
    {
      icon: 'fas fa-poo fa-fw',
      name: 'poo'
    },
    {
      icon: 'fas fa-poo-storm fa-fw',
      name: 'poo-storm'
    },
    {
      icon: 'fas fa-poop fa-fw',
      name: 'poop'
    },
    {
      icon: 'fas fa-power-off fa-fw',
      name: 'power-off'
    },
    {
      icon: 'fas fa-prescription fa-fw',
      name: 'prescription'
    },
    {
      icon: 'fas fa-prescription-bottle fa-fw',
      name: 'prescription-bottle'
    },
    {
      icon: 'fas fa-prescription-bottle-medical fa-fw',
      name: 'prescription-bottle-medical'
    },
    {
      icon: 'fas fa-print fa-fw',
      name: 'print'
    },
    {
      icon: 'fas fa-pump-medical fa-fw',
      name: 'pump-medical'
    },
    {
      icon: 'fas fa-pump-soap fa-fw',
      name: 'pump-soap'
    },
    {
      icon: 'fas fa-puzzle-piece fa-fw',
      name: 'puzzle-piece'
    },
    {
      icon: 'fas fa-q fa-fw',
      name: 'q'
    },
    {
      icon: 'fas fa-qrcode fa-fw',
      name: 'qrcode'
    },
    {
      icon: 'fas fa-question fa-fw',
      name: 'question'
    },
    {
      icon: 'fas fa-quote-left fa-fw',
      name: 'quote-left'
    },
    {
      icon: 'fas fa-quote-right fa-fw',
      name: 'quote-right'
    },
    {
      icon: 'fas fa-r fa-fw',
      name: 'r'
    },
    {
      icon: 'fas fa-radiation fa-fw',
      name: 'radiation'
    },
    {
      icon: 'fas fa-radio fa-fw',
      name: 'radio'
    },
    {
      icon: 'fas fa-rainbow fa-fw',
      name: 'rainbow'
    },
    {
      icon: 'fas fa-ranking-star fa-fw',
      name: 'ranking-star'
    },
    {
      icon: 'fas fa-receipt fa-fw',
      name: 'receipt'
    },
    {
      icon: 'fas fa-record-vinyl fa-fw',
      name: 'record-vinyl'
    },
    {
      icon: 'fas fa-rectangle-ad fa-fw',
      name: 'rectangle-ad'
    },
    {
      icon: 'fas fa-rectangle-list fa-fw',
      name: 'rectangle-list'
    },
    {
      icon: 'fas fa-rectangle-xmark fa-fw',
      name: 'rectangle-xmark'
    },
    {
      icon: 'fas fa-recycle fa-fw',
      name: 'recycle'
    },
    {
      icon: 'fas fa-registered fa-fw',
      name: 'registered'
    },
    {
      icon: 'fas fa-repeat fa-fw',
      name: 'repeat'
    },
    {
      icon: 'fas fa-reply fa-fw',
      name: 'reply'
    },
    {
      icon: 'fas fa-reply-all fa-fw',
      name: 'reply-all'
    },
    {
      icon: 'fas fa-republican fa-fw',
      name: 'republican'
    },
    {
      icon: 'fas fa-restroom fa-fw',
      name: 'restroom'
    },
    {
      icon: 'fas fa-retweet fa-fw',
      name: 'retweet'
    },
    {
      icon: 'fas fa-ribbon fa-fw',
      name: 'ribbon'
    },
    {
      icon: 'fas fa-right-from-bracket fa-fw',
      name: 'right-from-bracket'
    },
    {
      icon: 'fas fa-right-left fa-fw',
      name: 'right-left'
    },
    {
      icon: 'fas fa-right-long fa-fw',
      name: 'right-long'
    },
    {
      icon: 'fas fa-right-to-bracket fa-fw',
      name: 'right-to-bracket'
    },
    {
      icon: 'fas fa-ring fa-fw',
      name: 'ring'
    },
    {
      icon: 'fas fa-road fa-fw',
      name: 'road'
    },
    {
      icon: 'fas fa-road-barrier fa-fw',
      name: 'road-barrier'
    },
    {
      icon: 'fas fa-road-bridge fa-fw',
      name: 'road-bridge'
    },
    {
      icon: 'fas fa-road-circle-check fa-fw',
      name: 'road-circle-check'
    },
    {
      icon: 'fas fa-road-circle-exclamation fa-fw',
      name: 'road-circle-exclamation'
    },
    {
      icon: 'fas fa-road-circle-xmark fa-fw',
      name: 'road-circle-xmark'
    },
    {
      icon: 'fas fa-road-lock fa-fw',
      name: 'road-lock'
    },
    {
      icon: 'fas fa-road-spikes fa-fw',
      name: 'road-spikes'
    },
    {
      icon: 'fas fa-robot fa-fw',
      name: 'robot'
    },
    {
      icon: 'fas fa-rocket fa-fw',
      name: 'rocket'
    },
    {
      icon: 'fas fa-rotate fa-fw',
      name: 'rotate'
    },
    {
      icon: 'fas fa-rotate-left fa-fw',
      name: 'rotate-left'
    },
    {
      icon: 'fas fa-rotate-right fa-fw',
      name: 'rotate-right'
    },
    {
      icon: 'fas fa-route fa-fw',
      name: 'route'
    },
    {
      icon: 'fas fa-rss fa-fw',
      name: 'rss'
    },
    {
      icon: 'fas fa-ruble-sign fa-fw',
      name: 'ruble-sign'
    },
    {
      icon: 'fas fa-rug fa-fw',
      name: 'rug'
    },
    {
      icon: 'fas fa-ruler fa-fw',
      name: 'ruler'
    },
    {
      icon: 'fas fa-ruler-combined fa-fw',
      name: 'ruler-combined'
    },
    {
      icon: 'fas fa-ruler-horizontal fa-fw',
      name: 'ruler-horizontal'
    },
    {
      icon: 'fas fa-ruler-vertical fa-fw',
      name: 'ruler-vertical'
    },
    {
      icon: 'fas fa-rupee-sign fa-fw',
      name: 'rupee-sign'
    },
    {
      icon: 'fas fa-rupiah-sign fa-fw',
      name: 'rupiah-sign'
    },
    {
      icon: 'fas fa-s fa-fw',
      name: 's'
    },
    {
      icon: 'fas fa-sack-dollar fa-fw',
      name: 'sack-dollar'
    },
    {
      icon: 'fas fa-sack-xmark fa-fw',
      name: 'sack-xmark'
    },
    {
      icon: 'fas fa-sailboat fa-fw',
      name: 'sailboat'
    },
    {
      icon: 'fas fa-satellite fa-fw',
      name: 'satellite'
    },
    {
      icon: 'fas fa-satellite-dish fa-fw',
      name: 'satellite-dish'
    },
    {
      icon: 'fas fa-scale-balanced fa-fw',
      name: 'scale-balanced'
    },
    {
      icon: 'fas fa-scale-unbalanced fa-fw',
      name: 'scale-unbalanced'
    },
    {
      icon: 'fas fa-scale-unbalanced-flip fa-fw',
      name: 'scale-unbalanced-flip'
    },
    {
      icon: 'fas fa-school fa-fw',
      name: 'school'
    },
    {
      icon: 'fas fa-school-circle-check fa-fw',
      name: 'school-circle-check'
    },
    {
      icon: 'fas fa-school-circle-exclamation fa-fw',
      name: 'school-circle-exclamation'
    },
    {
      icon: 'fas fa-school-circle-xmark fa-fw',
      name: 'school-circle-xmark'
    },
    {
      icon: 'fas fa-school-flag fa-fw',
      name: 'school-flag'
    },
    {
      icon: 'fas fa-school-lock fa-fw',
      name: 'school-lock'
    },
    {
      icon: 'fas fa-scissors fa-fw',
      name: 'scissors'
    },
    {
      icon: 'fas fa-screwdriver fa-fw',
      name: 'screwdriver'
    },
    {
      icon: 'fas fa-screwdriver-wrench fa-fw',
      name: 'screwdriver-wrench'
    },
    {
      icon: 'fas fa-scroll fa-fw',
      name: 'scroll'
    },
    {
      icon: 'fas fa-scroll-torah fa-fw',
      name: 'scroll-torah'
    },
    {
      icon: 'fas fa-sd-card fa-fw',
      name: 'sd-card'
    },
    {
      icon: 'fas fa-section fa-fw',
      name: 'section'
    },
    {
      icon: 'fas fa-seedling fa-fw',
      name: 'seedling'
    },
    {
      icon: 'fas fa-server fa-fw',
      name: 'server'
    },
    {
      icon: 'fas fa-shapes fa-fw',
      name: 'shapes'
    },
    {
      icon: 'fas fa-share fa-fw',
      name: 'share'
    },
    {
      icon: 'fas fa-share-from-square fa-fw',
      name: 'share-from-square'
    },
    {
      icon: 'fas fa-share-nodes fa-fw',
      name: 'share-nodes'
    },
    {
      icon: 'fas fa-sheet-plastic fa-fw',
      name: 'sheet-plastic'
    },
    {
      icon: 'fas fa-shekel-sign fa-fw',
      name: 'shekel-sign'
    },
    {
      icon: 'fas fa-shield fa-fw',
      name: 'shield'
    },
    {
      icon: 'fas fa-shield-cat fa-fw',
      name: 'shield-cat'
    },
    {
      icon: 'fas fa-shield-dog fa-fw',
      name: 'shield-dog'
    },
    {
      icon: 'fas fa-shield-halved fa-fw',
      name: 'shield-halved'
    },
    {
      icon: 'fas fa-shield-heart fa-fw',
      name: 'shield-heart'
    },
    {
      icon: 'fas fa-shield-virus fa-fw',
      name: 'shield-virus'
    },
    {
      icon: 'fas fa-ship fa-fw',
      name: 'ship'
    },
    {
      icon: 'fas fa-shirt fa-fw',
      name: 'shirt'
    },
    {
      icon: 'fas fa-shoe-prints fa-fw',
      name: 'shoe-prints'
    },
    {
      icon: 'fas fa-shop fa-fw',
      name: 'shop'
    },
    {
      icon: 'fas fa-shop-lock fa-fw',
      name: 'shop-lock'
    },
    {
      icon: 'fas fa-shop-slash fa-fw',
      name: 'shop-slash'
    },
    {
      icon: 'fas fa-shower fa-fw',
      name: 'shower'
    },
    {
      icon: 'fas fa-shrimp fa-fw',
      name: 'shrimp'
    },
    {
      icon: 'fas fa-shuffle fa-fw',
      name: 'shuffle'
    },
    {
      icon: 'fas fa-shuttle-space fa-fw',
      name: 'shuttle-space'
    },
    {
      icon: 'fas fa-sign-hanging fa-fw',
      name: 'sign-hanging'
    },
    {
      icon: 'fas fa-signal fa-fw',
      name: 'signal'
    },
    {
      icon: 'fas fa-signature fa-fw',
      name: 'signature'
    },
    {
      icon: 'fas fa-signs-post fa-fw',
      name: 'signs-post'
    },
    {
      icon: 'fas fa-sim-card fa-fw',
      name: 'sim-card'
    },
    {
      icon: 'fas fa-sink fa-fw',
      name: 'sink'
    },
    {
      icon: 'fas fa-sitemap fa-fw',
      name: 'sitemap'
    },
    {
      icon: 'fas fa-skull fa-fw',
      name: 'skull'
    },
    {
      icon: 'fas fa-skull-crossbones fa-fw',
      name: 'skull-crossbones'
    },
    {
      icon: 'fas fa-slash fa-fw',
      name: 'slash'
    },
    {
      icon: 'fas fa-sleigh fa-fw',
      name: 'sleigh'
    },
    {
      icon: 'fas fa-sliders fa-fw',
      name: 'sliders'
    },
    {
      icon: 'fas fa-smog fa-fw',
      name: 'smog'
    },
    {
      icon: 'fas fa-smoking fa-fw',
      name: 'smoking'
    },
    {
      icon: 'fas fa-snowflake fa-fw',
      name: 'snowflake'
    },
    {
      icon: 'fas fa-snowman fa-fw',
      name: 'snowman'
    },
    {
      icon: 'fas fa-snowplow fa-fw',
      name: 'snowplow'
    },
    {
      icon: 'fas fa-soap fa-fw',
      name: 'soap'
    },
    {
      icon: 'fas fa-socks fa-fw',
      name: 'socks'
    },
    {
      icon: 'fas fa-solar-panel fa-fw',
      name: 'solar-panel'
    },
    {
      icon: 'fas fa-sort fa-fw',
      name: 'sort'
    },
    {
      icon: 'fas fa-sort-down fa-fw',
      name: 'sort-down'
    },
    {
      icon: 'fas fa-sort-up fa-fw',
      name: 'sort-up'
    },
    {
      icon: 'fas fa-spa fa-fw',
      name: 'spa'
    },
    {
      icon: 'fas fa-spaghetti-monster-flying fa-fw',
      name: 'spaghetti-monster-flying'
    },
    {
      icon: 'fas fa-spell-check fa-fw',
      name: 'spell-check'
    },
    {
      icon: 'fas fa-spider fa-fw',
      name: 'spider'
    },
    {
      icon: 'fas fa-spinner fa-fw',
      name: 'spinner'
    },
    {
      icon: 'fas fa-splotch fa-fw',
      name: 'splotch'
    },
    {
      icon: 'fas fa-spoon fa-fw',
      name: 'spoon'
    },
    {
      icon: 'fas fa-spray-can fa-fw',
      name: 'spray-can'
    },
    {
      icon: 'fas fa-spray-can-sparkles fa-fw',
      name: 'spray-can-sparkles'
    },
    {
      icon: 'fas fa-square fa-fw',
      name: 'square'
    },
    {
      icon: 'fas fa-square-arrow-up-right fa-fw',
      name: 'square-arrow-up-right'
    },
    {
      icon: 'fas fa-square-caret-down fa-fw',
      name: 'square-caret-down'
    },
    {
      icon: 'fas fa-square-caret-left fa-fw',
      name: 'square-caret-left'
    },
    {
      icon: 'fas fa-square-caret-right fa-fw',
      name: 'square-caret-right'
    },
    {
      icon: 'fas fa-square-caret-up fa-fw',
      name: 'square-caret-up'
    },
    {
      icon: 'fas fa-square-check fa-fw',
      name: 'square-check'
    },
    {
      icon: 'fas fa-square-envelope fa-fw',
      name: 'square-envelope'
    },
    {
      icon: 'fas fa-square-full fa-fw',
      name: 'square-full'
    },
    {
      icon: 'fas fa-square-h fa-fw',
      name: 'square-h'
    },
    {
      icon: 'fas fa-square-minus fa-fw',
      name: 'square-minus'
    },
    {
      icon: 'fas fa-square-nfi fa-fw',
      name: 'square-nfi'
    },
    {
      icon: 'fas fa-square-parking fa-fw',
      name: 'square-parking'
    },
    {
      icon: 'fas fa-square-pen fa-fw',
      name: 'square-pen'
    },
    {
      icon: 'fas fa-square-person-confined fa-fw',
      name: 'square-person-confined'
    },
    {
      icon: 'fas fa-square-phone fa-fw',
      name: 'square-phone'
    },
    {
      icon: 'fas fa-square-phone-flip fa-fw',
      name: 'square-phone-flip'
    },
    {
      icon: 'fas fa-square-plus fa-fw',
      name: 'square-plus'
    },
    {
      icon: 'fas fa-square-poll-horizontal fa-fw',
      name: 'square-poll-horizontal'
    },
    {
      icon: 'fas fa-square-poll-vertical fa-fw',
      name: 'square-poll-vertical'
    },
    {
      icon: 'fas fa-square-root-variable fa-fw',
      name: 'square-root-variable'
    },
    {
      icon: 'fas fa-square-rss fa-fw',
      name: 'square-rss'
    },
    {
      icon: 'fas fa-square-share-nodes fa-fw',
      name: 'square-share-nodes'
    },
    {
      icon: 'fas fa-square-up-right fa-fw',
      name: 'square-up-right'
    },
    {
      icon: 'fas fa-square-virus fa-fw',
      name: 'square-virus'
    },
    {
      icon: 'fas fa-square-xmark fa-fw',
      name: 'square-xmark'
    },
    {
      icon: 'fas fa-staff-aesculapius fa-fw',
      name: 'staff-aesculapius'
    },
    {
      icon: 'fas fa-stairs fa-fw',
      name: 'stairs'
    },
    {
      icon: 'fas fa-stamp fa-fw',
      name: 'stamp'
    },
    {
      icon: 'fas fa-star fa-fw',
      name: 'star'
    },
    {
      icon: 'fas fa-star-and-crescent fa-fw',
      name: 'star-and-crescent'
    },
    {
      icon: 'fas fa-star-half fa-fw',
      name: 'star-half'
    },
    {
      icon: 'fas fa-star-half-stroke fa-fw',
      name: 'star-half-stroke'
    },
    {
      icon: 'fas fa-star-of-david fa-fw',
      name: 'star-of-david'
    },
    {
      icon: 'fas fa-star-of-life fa-fw',
      name: 'star-of-life'
    },
    {
      icon: 'fas fa-sterling-sign fa-fw',
      name: 'sterling-sign'
    },
    {
      icon: 'fas fa-stethoscope fa-fw',
      name: 'stethoscope'
    },
    {
      icon: 'fas fa-stop fa-fw',
      name: 'stop'
    },
    {
      icon: 'fas fa-stopwatch fa-fw',
      name: 'stopwatch'
    },
    {
      icon: 'fas fa-stopwatch-20 fa-fw',
      name: 'stopwatch-20'
    },
    {
      icon: 'fas fa-store fa-fw',
      name: 'store'
    },
    {
      icon: 'fas fa-store-slash fa-fw',
      name: 'store-slash'
    },
    {
      icon: 'fas fa-street-view fa-fw',
      name: 'street-view'
    },
    {
      icon: 'fas fa-strikethrough fa-fw',
      name: 'strikethrough'
    },
    {
      icon: 'fas fa-stroopwafel fa-fw',
      name: 'stroopwafel'
    },
    {
      icon: 'fas fa-subscript fa-fw',
      name: 'subscript'
    },
    {
      icon: 'fas fa-suitcase fa-fw',
      name: 'suitcase'
    },
    {
      icon: 'fas fa-suitcase-medical fa-fw',
      name: 'suitcase-medical'
    },
    {
      icon: 'fas fa-suitcase-rolling fa-fw',
      name: 'suitcase-rolling'
    },
    {
      icon: 'fas fa-sun fa-fw',
      name: 'sun'
    },
    {
      icon: 'fas fa-sun-plant-wilt fa-fw',
      name: 'sun-plant-wilt'
    },
    {
      icon: 'fas fa-superscript fa-fw',
      name: 'superscript'
    },
    {
      icon: 'fas fa-swatchbook fa-fw',
      name: 'swatchbook'
    },
    {
      icon: 'fas fa-synagogue fa-fw',
      name: 'synagogue'
    },
    {
      icon: 'fas fa-syringe fa-fw',
      name: 'syringe'
    },
    {
      icon: 'fas fa-t fa-fw',
      name: 't'
    },
    {
      icon: 'fas fa-table fa-fw',
      name: 'table'
    },
    {
      icon: 'fas fa-table-cells fa-fw',
      name: 'table-cells'
    },
    {
      icon: 'fas fa-table-cells-large fa-fw',
      name: 'table-cells-large'
    },
    {
      icon: 'fas fa-table-columns fa-fw',
      name: 'table-columns'
    },
    {
      icon: 'fas fa-table-list fa-fw',
      name: 'table-list'
    },
    {
      icon: 'fas fa-table-tennis-paddle-ball fa-fw',
      name: 'table-tennis-paddle-ball'
    },
    {
      icon: 'fas fa-tablet fa-fw',
      name: 'tablet'
    },
    {
      icon: 'fas fa-tablet-button fa-fw',
      name: 'tablet-button'
    },
    {
      icon: 'fas fa-tablet-screen-button fa-fw',
      name: 'tablet-screen-button'
    },
    {
      icon: 'fas fa-tablets fa-fw',
      name: 'tablets'
    },
    {
      icon: 'fas fa-tachograph-digital fa-fw',
      name: 'tachograph-digital'
    },
    {
      icon: 'fas fa-tag fa-fw',
      name: 'tag'
    },
    {
      icon: 'fas fa-tags fa-fw',
      name: 'tags'
    },
    {
      icon: 'fas fa-tape fa-fw',
      name: 'tape'
    },
    {
      icon: 'fas fa-tarp fa-fw',
      name: 'tarp'
    },
    {
      icon: 'fas fa-tarp-droplet fa-fw',
      name: 'tarp-droplet'
    },
    {
      icon: 'fas fa-taxi fa-fw',
      name: 'taxi'
    },
    {
      icon: 'fas fa-teeth fa-fw',
      name: 'teeth'
    },
    {
      icon: 'fas fa-teeth-open fa-fw',
      name: 'teeth-open'
    },
    {
      icon: 'fas fa-temperature-arrow-down fa-fw',
      name: 'temperature-arrow-down'
    },
    {
      icon: 'fas fa-temperature-arrow-up fa-fw',
      name: 'temperature-arrow-up'
    },
    {
      icon: 'fas fa-temperature-empty fa-fw',
      name: 'temperature-empty'
    },
    {
      icon: 'fas fa-temperature-full fa-fw',
      name: 'temperature-full'
    },
    {
      icon: 'fas fa-temperature-half fa-fw',
      name: 'temperature-half'
    },
    {
      icon: 'fas fa-temperature-high fa-fw',
      name: 'temperature-high'
    },
    {
      icon: 'fas fa-temperature-low fa-fw',
      name: 'temperature-low'
    },
    {
      icon: 'fas fa-temperature-quarter fa-fw',
      name: 'temperature-quarter'
    },
    {
      icon: 'fas fa-temperature-three-quarters fa-fw',
      name: 'temperature-three-quarters'
    },
    {
      icon: 'fas fa-tenge-sign fa-fw',
      name: 'tenge-sign'
    },
    {
      icon: 'fas fa-tent fa-fw',
      name: 'tent'
    },
    {
      icon: 'fas fa-tent-arrow-down-to-line fa-fw',
      name: 'tent-arrow-down-to-line'
    },
    {
      icon: 'fas fa-tent-arrow-left-right fa-fw',
      name: 'tent-arrow-left-right'
    },
    {
      icon: 'fas fa-tent-arrow-turn-left fa-fw',
      name: 'tent-arrow-turn-left'
    },
    {
      icon: 'fas fa-tent-arrows-down fa-fw',
      name: 'tent-arrows-down'
    },
    {
      icon: 'fas fa-tents fa-fw',
      name: 'tents'
    },
    {
      icon: 'fas fa-terminal fa-fw',
      name: 'terminal'
    },
    {
      icon: 'fas fa-text-height fa-fw',
      name: 'text-height'
    },
    {
      icon: 'fas fa-text-slash fa-fw',
      name: 'text-slash'
    },
    {
      icon: 'fas fa-text-width fa-fw',
      name: 'text-width'
    },
    {
      icon: 'fas fa-thermometer fa-fw',
      name: 'thermometer'
    },
    {
      icon: 'fas fa-thumbs-down fa-fw',
      name: 'thumbs-down'
    },
    {
      icon: 'fas fa-thumbs-up fa-fw',
      name: 'thumbs-up'
    },
    {
      icon: 'fas fa-thumbtack fa-fw',
      name: 'thumbtack'
    },
    {
      icon: 'fas fa-ticket fa-fw',
      name: 'ticket'
    },
    {
      icon: 'fas fa-ticket-simple fa-fw',
      name: 'ticket-simple'
    },
    {
      icon: 'fas fa-timeline fa-fw',
      name: 'timeline'
    },
    {
      icon: 'fas fa-toggle-off fa-fw',
      name: 'toggle-off'
    },
    {
      icon: 'fas fa-toggle-on fa-fw',
      name: 'toggle-on'
    },
    {
      icon: 'fas fa-toilet fa-fw',
      name: 'toilet'
    },
    {
      icon: 'fas fa-toilet-paper fa-fw',
      name: 'toilet-paper'
    },
    {
      icon: 'fas fa-toilet-paper-slash fa-fw',
      name: 'toilet-paper-slash'
    },
    {
      icon: 'fas fa-toilet-portable fa-fw',
      name: 'toilet-portable'
    },
    {
      icon: 'fas fa-toilets-portable fa-fw',
      name: 'toilets-portable'
    },
    {
      icon: 'fas fa-toolbox fa-fw',
      name: 'toolbox'
    },
    {
      icon: 'fas fa-tooth fa-fw',
      name: 'tooth'
    },
    {
      icon: 'fas fa-torii-gate fa-fw',
      name: 'torii-gate'
    },
    {
      icon: 'fas fa-tornado fa-fw',
      name: 'tornado'
    },
    {
      icon: 'fas fa-tower-broadcast fa-fw',
      name: 'tower-broadcast'
    },
    {
      icon: 'fas fa-tower-cell fa-fw',
      name: 'tower-cell'
    },
    {
      icon: 'fas fa-tower-observation fa-fw',
      name: 'tower-observation'
    },
    {
      icon: 'fas fa-tractor fa-fw',
      name: 'tractor'
    },
    {
      icon: 'fas fa-trademark fa-fw',
      name: 'trademark'
    },
    {
      icon: 'fas fa-traffic-light fa-fw',
      name: 'traffic-light'
    },
    {
      icon: 'fas fa-trailer fa-fw',
      name: 'trailer'
    },
    {
      icon: 'fas fa-train fa-fw',
      name: 'train'
    },
    {
      icon: 'fas fa-train-subway fa-fw',
      name: 'train-subway'
    },
    {
      icon: 'fas fa-train-tram fa-fw',
      name: 'train-tram'
    },
    {
      icon: 'fas fa-transgender fa-fw',
      name: 'transgender'
    },
    {
      icon: 'fas fa-trash fa-fw',
      name: 'trash'
    },
    {
      icon: 'fas fa-trash-arrow-up fa-fw',
      name: 'trash-arrow-up'
    },
    {
      icon: 'fas fa-trash-can fa-fw',
      name: 'trash-can'
    },
    {
      icon: 'fas fa-trash-can-arrow-up fa-fw',
      name: 'trash-can-arrow-up'
    },
    {
      icon: 'fas fa-tree fa-fw',
      name: 'tree'
    },
    {
      icon: 'fas fa-tree-city fa-fw',
      name: 'tree-city'
    },
    {
      icon: 'fas fa-triangle-exclamation fa-fw',
      name: 'triangle-exclamation'
    },
    {
      icon: 'fas fa-trophy fa-fw',
      name: 'trophy'
    },
    {
      icon: 'fas fa-trowel fa-fw',
      name: 'trowel'
    },
    {
      icon: 'fas fa-trowel-bricks fa-fw',
      name: 'trowel-bricks'
    },
    {
      icon: 'fas fa-truck fa-fw',
      name: 'truck'
    },
    {
      icon: 'fas fa-truck-arrow-right fa-fw',
      name: 'truck-arrow-right'
    },
    {
      icon: 'fas fa-truck-droplet fa-fw',
      name: 'truck-droplet'
    },
    {
      icon: 'fas fa-truck-fast fa-fw',
      name: 'truck-fast'
    },
    {
      icon: 'fas fa-truck-field fa-fw',
      name: 'truck-field'
    },
    {
      icon: 'fas fa-truck-field-un fa-fw',
      name: 'truck-field-un'
    },
    {
      icon: 'fas fa-truck-front fa-fw',
      name: 'truck-front'
    },
    {
      icon: 'fas fa-truck-medical fa-fw',
      name: 'truck-medical'
    },
    {
      icon: 'fas fa-truck-monster fa-fw',
      name: 'truck-monster'
    },
    {
      icon: 'fas fa-truck-moving fa-fw',
      name: 'truck-moving'
    },
    {
      icon: 'fas fa-truck-pickup fa-fw',
      name: 'truck-pickup'
    },
    {
      icon: 'fas fa-truck-plane fa-fw',
      name: 'truck-plane'
    },
    {
      icon: 'fas fa-truck-ramp-box fa-fw',
      name: 'truck-ramp-box'
    },
    {
      icon: 'fas fa-tty fa-fw',
      name: 'tty'
    },
    {
      icon: 'fas fa-turkish-lira-sign fa-fw',
      name: 'turkish-lira-sign'
    },
    {
      icon: 'fas fa-turn-down fa-fw',
      name: 'turn-down'
    },
    {
      icon: 'fas fa-turn-up fa-fw',
      name: 'turn-up'
    },
    {
      icon: 'fas fa-tv fa-fw',
      name: 'tv'
    },
    {
      icon: 'fas fa-u fa-fw',
      name: 'u'
    },
    {
      icon: 'fas fa-umbrella fa-fw',
      name: 'umbrella'
    },
    {
      icon: 'fas fa-umbrella-beach fa-fw',
      name: 'umbrella-beach'
    },
    {
      icon: 'fas fa-underline fa-fw',
      name: 'underline'
    },
    {
      icon: 'fas fa-universal-access fa-fw',
      name: 'universal-access'
    },
    {
      icon: 'fas fa-unlock fa-fw',
      name: 'unlock'
    },
    {
      icon: 'fas fa-unlock-keyhole fa-fw',
      name: 'unlock-keyhole'
    },
    {
      icon: 'fas fa-up-down fa-fw',
      name: 'up-down'
    },
    {
      icon: 'fas fa-up-down-left-right fa-fw',
      name: 'up-down-left-right'
    },
    {
      icon: 'fas fa-up-long fa-fw',
      name: 'up-long'
    },
    {
      icon: 'fas fa-up-right-and-down-left-from-center fa-fw',
      name: 'up-right-and-down-left-from-center'
    },
    {
      icon: 'fas fa-up-right-from-square fa-fw',
      name: 'up-right-from-square'
    },
    {
      icon: 'fas fa-upload fa-fw',
      name: 'upload'
    },
    {
      icon: 'fas fa-user fa-fw',
      name: 'user'
    },
    {
      icon: 'fas fa-user-astronaut fa-fw',
      name: 'user-astronaut'
    },
    {
      icon: 'fas fa-user-check fa-fw',
      name: 'user-check'
    },
    {
      icon: 'fas fa-user-clock fa-fw',
      name: 'user-clock'
    },
    {
      icon: 'fas fa-user-doctor fa-fw',
      name: 'user-doctor'
    },
    {
      icon: 'fas fa-user-gear fa-fw',
      name: 'user-gear'
    },
    {
      icon: 'fas fa-user-graduate fa-fw',
      name: 'user-graduate'
    },
    {
      icon: 'fas fa-user-group fa-fw',
      name: 'user-group'
    },
    {
      icon: 'fas fa-user-injured fa-fw',
      name: 'user-injured'
    },
    {
      icon: 'fas fa-user-large fa-fw',
      name: 'user-large'
    },
    {
      icon: 'fas fa-user-large-slash fa-fw',
      name: 'user-large-slash'
    },
    {
      icon: 'fas fa-user-lock fa-fw',
      name: 'user-lock'
    },
    {
      icon: 'fas fa-user-minus fa-fw',
      name: 'user-minus'
    },
    {
      icon: 'fas fa-user-ninja fa-fw',
      name: 'user-ninja'
    },
    {
      icon: 'fas fa-user-nurse fa-fw',
      name: 'user-nurse'
    },
    {
      icon: 'fas fa-user-pen fa-fw',
      name: 'user-pen'
    },
    {
      icon: 'fas fa-user-plus fa-fw',
      name: 'user-plus'
    },
    {
      icon: 'fas fa-user-secret fa-fw',
      name: 'user-secret'
    },
    {
      icon: 'fas fa-user-shield fa-fw',
      name: 'user-shield'
    },
    {
      icon: 'fas fa-user-slash fa-fw',
      name: 'user-slash'
    },
    {
      icon: 'fas fa-user-tag fa-fw',
      name: 'user-tag'
    },
    {
      icon: 'fas fa-user-tie fa-fw',
      name: 'user-tie'
    },
    {
      icon: 'fas fa-user-xmark fa-fw',
      name: 'user-xmark'
    },
    {
      icon: 'fas fa-users fa-fw',
      name: 'users'
    },
    {
      icon: 'fas fa-users-between-lines fa-fw',
      name: 'users-between-lines'
    },
    {
      icon: 'fas fa-users-gear fa-fw',
      name: 'users-gear'
    },
    {
      icon: 'fas fa-users-line fa-fw',
      name: 'users-line'
    },
    {
      icon: 'fas fa-users-rays fa-fw',
      name: 'users-rays'
    },
    {
      icon: 'fas fa-users-rectangle fa-fw',
      name: 'users-rectangle'
    },
    {
      icon: 'fas fa-users-slash fa-fw',
      name: 'users-slash'
    },
    {
      icon: 'fas fa-users-viewfinder fa-fw',
      name: 'users-viewfinder'
    },
    {
      icon: 'fas fa-utensils fa-fw',
      name: 'utensils'
    },
    {
      icon: 'fas fa-v fa-fw',
      name: 'v'
    },
    {
      icon: 'fas fa-van-shuttle fa-fw',
      name: 'van-shuttle'
    },
    {
      icon: 'fas fa-vault fa-fw',
      name: 'vault'
    },
    {
      icon: 'fas fa-vector-square fa-fw',
      name: 'vector-square'
    },
    {
      icon: 'fas fa-venus fa-fw',
      name: 'venus'
    },
    {
      icon: 'fas fa-venus-double fa-fw',
      name: 'venus-double'
    },
    {
      icon: 'fas fa-venus-mars fa-fw',
      name: 'venus-mars'
    },
    {
      icon: 'fas fa-vest fa-fw',
      name: 'vest'
    },
    {
      icon: 'fas fa-vest-patches fa-fw',
      name: 'vest-patches'
    },
    {
      icon: 'fas fa-vial fa-fw',
      name: 'vial'
    },
    {
      icon: 'fas fa-vial-circle-check fa-fw',
      name: 'vial-circle-check'
    },
    {
      icon: 'fas fa-vial-virus fa-fw',
      name: 'vial-virus'
    },
    {
      icon: 'fas fa-vials fa-fw',
      name: 'vials'
    },
    {
      icon: 'fas fa-video fa-fw',
      name: 'video'
    },
    {
      icon: 'fas fa-video-slash fa-fw',
      name: 'video-slash'
    },
    {
      icon: 'fas fa-vihara fa-fw',
      name: 'vihara'
    },
    {
      icon: 'fas fa-virus fa-fw',
      name: 'virus'
    },
    {
      icon: 'fas fa-virus-covid fa-fw',
      name: 'virus-covid'
    },
    {
      icon: 'fas fa-virus-covid-slash fa-fw',
      name: 'virus-covid-slash'
    },
    {
      icon: 'fas fa-virus-slash fa-fw',
      name: 'virus-slash'
    },
    {
      icon: 'fas fa-viruses fa-fw',
      name: 'viruses'
    },
    {
      icon: 'fas fa-voicemail fa-fw',
      name: 'voicemail'
    },
    {
      icon: 'fas fa-volcano fa-fw',
      name: 'volcano'
    },
    {
      icon: 'fas fa-volleyball fa-fw',
      name: 'volleyball'
    },
    {
      icon: 'fas fa-volume-high fa-fw',
      name: 'volume-high'
    },
    {
      icon: 'fas fa-volume-low fa-fw',
      name: 'volume-low'
    },
    {
      icon: 'fas fa-volume-off fa-fw',
      name: 'volume-off'
    },
    {
      icon: 'fas fa-volume-xmark fa-fw',
      name: 'volume-xmark'
    },
    {
      icon: 'fas fa-vr-cardboard fa-fw',
      name: 'vr-cardboard'
    },
    {
      icon: 'fas fa-w fa-fw',
      name: 'w'
    },
    {
      icon: 'fas fa-walkie-talkie fa-fw',
      name: 'walkie-talkie'
    },
    {
      icon: 'fas fa-wallet fa-fw',
      name: 'wallet'
    },
    {
      icon: 'fas fa-wand-magic fa-fw',
      name: 'wand-magic'
    },
    {
      icon: 'fas fa-wand-magic-sparkles fa-fw',
      name: 'wand-magic-sparkles'
    },
    {
      icon: 'fas fa-wand-sparkles fa-fw',
      name: 'wand-sparkles'
    },
    {
      icon: 'fas fa-warehouse fa-fw',
      name: 'warehouse'
    },
    {
      icon: 'fas fa-water fa-fw',
      name: 'water'
    },
    {
      icon: 'fas fa-water-ladder fa-fw',
      name: 'water-ladder'
    },
    {
      icon: 'fas fa-wave-square fa-fw',
      name: 'wave-square'
    },
    {
      icon: 'fas fa-weight-hanging fa-fw',
      name: 'weight-hanging'
    },
    {
      icon: 'fas fa-weight-scale fa-fw',
      name: 'weight-scale'
    },
    {
      icon: 'fas fa-wheat-awn fa-fw',
      name: 'wheat-awn'
    },
    {
      icon: 'fas fa-wheat-awn-circle-exclamation fa-fw',
      name: 'wheat-awn-circle-exclamation'
    },
    {
      icon: 'fas fa-wheelchair fa-fw',
      name: 'wheelchair'
    },
    {
      icon: 'fas fa-wheelchair-move fa-fw',
      name: 'wheelchair-move'
    },
    {
      icon: 'fas fa-whiskey-glass fa-fw',
      name: 'whiskey-glass'
    },
    {
      icon: 'fas fa-wifi fa-fw',
      name: 'wifi'
    },
    {
      icon: 'fas fa-wind fa-fw',
      name: 'wind'
    },
    {
      icon: 'fas fa-window-maximize fa-fw',
      name: 'window-maximize'
    },
    {
      icon: 'fas fa-window-minimize fa-fw',
      name: 'window-minimize'
    },
    {
      icon: 'fas fa-window-restore fa-fw',
      name: 'window-restore'
    },
    {
      icon: 'fas fa-wine-bottle fa-fw',
      name: 'wine-bottle'
    },
    {
      icon: 'fas fa-wine-glass fa-fw',
      name: 'wine-glass'
    },
    {
      icon: 'fas fa-wine-glass-empty fa-fw',
      name: 'wine-glass-empty'
    },
    {
      icon: 'fas fa-won-sign fa-fw',
      name: 'won-sign'
    },
    {
      icon: 'fas fa-worm fa-fw',
      name: 'worm'
    },
    {
      icon: 'fas fa-wrench fa-fw',
      name: 'wrench'
    },
    {
      icon: 'fas fa-x fa-fw',
      name: 'x'
    },
    {
      icon: 'fas fa-x-ray fa-fw',
      name: 'x-ray'
    },
    {
      icon: 'fas fa-xmark fa-fw',
      name: 'xmark'
    },
    {
      icon: 'fas fa-xmarks-lines fa-fw',
      name: 'xmarks-lines'
    },
    {
      icon: 'fas fa-y fa-fw',
      name: 'y'
    },
    {
      icon: 'fas fa-yen-sign fa-fw',
      name: 'yen-sign'
    },
    {
      icon: 'fas fa-yin-yang fa-fw',
      name: 'yin-yang'
    },
    {
      icon: 'fas fa-z fa-fw',
      name: 'z'
    },
    {
      icon: 'fab fa-42-group fa-fw',
      name: '42-group'
    },
    {
      icon: 'fab fa-500px fa-fw',
      name: '500px'
    },
    {
      icon: 'fab fa-accessible-icon fa-fw',
      name: 'accessible-icon'
    },
    {
      icon: 'fab fa-accusoft fa-fw',
      name: 'accusoft'
    },
    {
      icon: 'fab fa-adn fa-fw',
      name: 'adn'
    },
    {
      icon: 'fab fa-adversal fa-fw',
      name: 'adversal'
    },
    {
      icon: 'fab fa-affiliatetheme fa-fw',
      name: 'affiliatetheme'
    },
    {
      icon: 'fab fa-airbnb fa-fw',
      name: 'airbnb'
    },
    {
      icon: 'fab fa-algolia fa-fw',
      name: 'algolia'
    },
    {
      icon: 'fab fa-alipay fa-fw',
      name: 'alipay'
    },
    {
      icon: 'fab fa-amazon fa-fw',
      name: 'amazon'
    },
    {
      icon: 'fab fa-amazon-pay fa-fw',
      name: 'amazon-pay'
    },
    {
      icon: 'fab fa-amilia fa-fw',
      name: 'amilia'
    },
    {
      icon: 'fab fa-android fa-fw',
      name: 'android'
    },
    {
      icon: 'fab fa-angellist fa-fw',
      name: 'angellist'
    },
    {
      icon: 'fab fa-angrycreative fa-fw',
      name: 'angrycreative'
    },
    {
      icon: 'fab fa-angular fa-fw',
      name: 'angular'
    },
    {
      icon: 'fab fa-app-store fa-fw',
      name: 'app-store'
    },
    {
      icon: 'fab fa-app-store-ios fa-fw',
      name: 'app-store-ios'
    },
    {
      icon: 'fab fa-apper fa-fw',
      name: 'apper'
    },
    {
      icon: 'fab fa-apple fa-fw',
      name: 'apple'
    },
    {
      icon: 'fab fa-apple-pay fa-fw',
      name: 'apple-pay'
    },
    {
      icon: 'fab fa-artstation fa-fw',
      name: 'artstation'
    },
    {
      icon: 'fab fa-asymmetrik fa-fw',
      name: 'asymmetrik'
    },
    {
      icon: 'fab fa-atlassian fa-fw',
      name: 'atlassian'
    },
    {
      icon: 'fab fa-audible fa-fw',
      name: 'audible'
    },
    {
      icon: 'fab fa-autoprefixer fa-fw',
      name: 'autoprefixer'
    },
    {
      icon: 'fab fa-avianex fa-fw',
      name: 'avianex'
    },
    {
      icon: 'fab fa-aviato fa-fw',
      name: 'aviato'
    },
    {
      icon: 'fab fa-aws fa-fw',
      name: 'aws'
    },
    {
      icon: 'fab fa-bandcamp fa-fw',
      name: 'bandcamp'
    },
    {
      icon: 'fab fa-battle-net fa-fw',
      name: 'battle-net'
    },
    {
      icon: 'fab fa-behance fa-fw',
      name: 'behance'
    },
    {
      icon: 'fab fa-behance-square fa-fw',
      name: 'behance-square'
    },
    {
      icon: 'fab fa-bilibili fa-fw',
      name: 'bilibili'
    },
    {
      icon: 'fab fa-bimobject fa-fw',
      name: 'bimobject'
    },
    {
      icon: 'fab fa-bitbucket fa-fw',
      name: 'bitbucket'
    },
    {
      icon: 'fab fa-bitcoin fa-fw',
      name: 'bitcoin'
    },
    {
      icon: 'fab fa-bity fa-fw',
      name: 'bity'
    },
    {
      icon: 'fab fa-black-tie fa-fw',
      name: 'black-tie'
    },
    {
      icon: 'fab fa-blackberry fa-fw',
      name: 'blackberry'
    },
    {
      icon: 'fab fa-blogger fa-fw',
      name: 'blogger'
    },
    {
      icon: 'fab fa-blogger-b fa-fw',
      name: 'blogger-b'
    },
    {
      icon: 'fab fa-bluetooth fa-fw',
      name: 'bluetooth'
    },
    {
      icon: 'fab fa-bluetooth-b fa-fw',
      name: 'bluetooth-b'
    },
    {
      icon: 'fab fa-bootstrap fa-fw',
      name: 'bootstrap'
    },
    {
      icon: 'fab fa-bots fa-fw',
      name: 'bots'
    },
    {
      icon: 'fab fa-btc fa-fw',
      name: 'btc'
    },
    {
      icon: 'fab fa-buffer fa-fw',
      name: 'buffer'
    },
    {
      icon: 'fab fa-buromobelexperte fa-fw',
      name: 'buromobelexperte'
    },
    {
      icon: 'fab fa-buy-n-large fa-fw',
      name: 'buy-n-large'
    },
    {
      icon: 'fab fa-buysellads fa-fw',
      name: 'buysellads'
    },
    {
      icon: 'fab fa-canadian-maple-leaf fa-fw',
      name: 'canadian-maple-leaf'
    },
    {
      icon: 'fab fa-cc-amazon-pay fa-fw',
      name: 'cc-amazon-pay'
    },
    {
      icon: 'fab fa-cc-amex fa-fw',
      name: 'cc-amex'
    },
    {
      icon: 'fab fa-cc-apple-pay fa-fw',
      name: 'cc-apple-pay'
    },
    {
      icon: 'fab fa-cc-diners-club fa-fw',
      name: 'cc-diners-club'
    },
    {
      icon: 'fab fa-cc-discover fa-fw',
      name: 'cc-discover'
    },
    {
      icon: 'fab fa-cc-jcb fa-fw',
      name: 'cc-jcb'
    },
    {
      icon: 'fab fa-cc-mastercard fa-fw',
      name: 'cc-mastercard'
    },
    {
      icon: 'fab fa-cc-paypal fa-fw',
      name: 'cc-paypal'
    },
    {
      icon: 'fab fa-cc-stripe fa-fw',
      name: 'cc-stripe'
    },
    {
      icon: 'fab fa-cc-visa fa-fw',
      name: 'cc-visa'
    },
    {
      icon: 'fab fa-centercode fa-fw',
      name: 'centercode'
    },
    {
      icon: 'fab fa-centos fa-fw',
      name: 'centos'
    },
    {
      icon: 'fab fa-chrome fa-fw',
      name: 'chrome'
    },
    {
      icon: 'fab fa-chromecast fa-fw',
      name: 'chromecast'
    },
    {
      icon: 'fab fa-cloudflare fa-fw',
      name: 'cloudflare'
    },
    {
      icon: 'fab fa-cloudscale fa-fw',
      name: 'cloudscale'
    },
    {
      icon: 'fab fa-cloudsmith fa-fw',
      name: 'cloudsmith'
    },
    {
      icon: 'fab fa-cloudversify fa-fw',
      name: 'cloudversify'
    },
    {
      icon: 'fab fa-cmplid fa-fw',
      name: 'cmplid'
    },
    {
      icon: 'fab fa-codepen fa-fw',
      name: 'codepen'
    },
    {
      icon: 'fab fa-codiepie fa-fw',
      name: 'codiepie'
    },
    {
      icon: 'fab fa-confluence fa-fw',
      name: 'confluence'
    },
    {
      icon: 'fab fa-connectdevelop fa-fw',
      name: 'connectdevelop'
    },
    {
      icon: 'fab fa-contao fa-fw',
      name: 'contao'
    },
    {
      icon: 'fab fa-cotton-bureau fa-fw',
      name: 'cotton-bureau'
    },
    {
      icon: 'fab fa-cpanel fa-fw',
      name: 'cpanel'
    },
    {
      icon: 'fab fa-creative-commons fa-fw',
      name: 'creative-commons'
    },
    {
      icon: 'fab fa-creative-commons-by fa-fw',
      name: 'creative-commons-by'
    },
    {
      icon: 'fab fa-creative-commons-nc fa-fw',
      name: 'creative-commons-nc'
    },
    {
      icon: 'fab fa-creative-commons-nc-eu fa-fw',
      name: 'creative-commons-nc-eu'
    },
    {
      icon: 'fab fa-creative-commons-nc-jp fa-fw',
      name: 'creative-commons-nc-jp'
    },
    {
      icon: 'fab fa-creative-commons-nd fa-fw',
      name: 'creative-commons-nd'
    },
    {
      icon: 'fab fa-creative-commons-pd fa-fw',
      name: 'creative-commons-pd'
    },
    {
      icon: 'fab fa-creative-commons-pd-alt fa-fw',
      name: 'creative-commons-pd-alt'
    },
    {
      icon: 'fab fa-creative-commons-remix fa-fw',
      name: 'creative-commons-remix'
    },
    {
      icon: 'fab fa-creative-commons-sa fa-fw',
      name: 'creative-commons-sa'
    },
    {
      icon: 'fab fa-creative-commons-sampling fa-fw',
      name: 'creative-commons-sampling'
    },
    {
      icon: 'fab fa-creative-commons-sampling-plus fa-fw',
      name: 'creative-commons-sampling-plus'
    },
    {
      icon: 'fab fa-creative-commons-share fa-fw',
      name: 'creative-commons-share'
    },
    {
      icon: 'fab fa-creative-commons-zero fa-fw',
      name: 'creative-commons-zero'
    },
    {
      icon: 'fab fa-critical-role fa-fw',
      name: 'critical-role'
    },
    {
      icon: 'fab fa-css3 fa-fw',
      name: 'css3'
    },
    {
      icon: 'fab fa-css3-alt fa-fw',
      name: 'css3-alt'
    },
    {
      icon: 'fab fa-cuttlefish fa-fw',
      name: 'cuttlefish'
    },
    {
      icon: 'fab fa-d-and-d fa-fw',
      name: 'd-and-d'
    },
    {
      icon: 'fab fa-d-and-d-beyond fa-fw',
      name: 'd-and-d-beyond'
    },
    {
      icon: 'fab fa-dailymotion fa-fw',
      name: 'dailymotion'
    },
    {
      icon: 'fab fa-dashcube fa-fw',
      name: 'dashcube'
    },
    {
      icon: 'fab fa-deezer fa-fw',
      name: 'deezer'
    },
    {
      icon: 'fab fa-delicious fa-fw',
      name: 'delicious'
    },
    {
      icon: 'fab fa-deploydog fa-fw',
      name: 'deploydog'
    },
    {
      icon: 'fab fa-deskpro fa-fw',
      name: 'deskpro'
    },
    {
      icon: 'fab fa-dev fa-fw',
      name: 'dev'
    },
    {
      icon: 'fab fa-deviantart fa-fw',
      name: 'deviantart'
    },
    {
      icon: 'fab fa-dhl fa-fw',
      name: 'dhl'
    },
    {
      icon: 'fab fa-diaspora fa-fw',
      name: 'diaspora'
    },
    {
      icon: 'fab fa-digg fa-fw',
      name: 'digg'
    },
    {
      icon: 'fab fa-digital-ocean fa-fw',
      name: 'digital-ocean'
    },
    {
      icon: 'fab fa-discord fa-fw',
      name: 'discord'
    },
    {
      icon: 'fab fa-discourse fa-fw',
      name: 'discourse'
    },
    {
      icon: 'fab fa-dochub fa-fw',
      name: 'dochub'
    },
    {
      icon: 'fab fa-docker fa-fw',
      name: 'docker'
    },
    {
      icon: 'fab fa-draft2digital fa-fw',
      name: 'draft2digital'
    },
    {
      icon: 'fab fa-dribbble fa-fw',
      name: 'dribbble'
    },
    {
      icon: 'fab fa-dribbble-square fa-fw',
      name: 'dribbble-square'
    },
    {
      icon: 'fab fa-dropbox fa-fw',
      name: 'dropbox'
    },
    {
      icon: 'fab fa-drupal fa-fw',
      name: 'drupal'
    },
    {
      icon: 'fab fa-dyalog fa-fw',
      name: 'dyalog'
    },
    {
      icon: 'fab fa-earlybirds fa-fw',
      name: 'earlybirds'
    },
    {
      icon: 'fab fa-ebay fa-fw',
      name: 'ebay'
    },
    {
      icon: 'fab fa-edge fa-fw',
      name: 'edge'
    },
    {
      icon: 'fab fa-edge-legacy fa-fw',
      name: 'edge-legacy'
    },
    {
      icon: 'fab fa-elementor fa-fw',
      name: 'elementor'
    },
    {
      icon: 'fab fa-ello fa-fw',
      name: 'ello'
    },
    {
      icon: 'fab fa-ember fa-fw',
      name: 'ember'
    },
    {
      icon: 'fab fa-empire fa-fw',
      name: 'empire'
    },
    {
      icon: 'fab fa-envira fa-fw',
      name: 'envira'
    },
    {
      icon: 'fab fa-erlang fa-fw',
      name: 'erlang'
    },
    {
      icon: 'fab fa-ethereum fa-fw',
      name: 'ethereum'
    },
    {
      icon: 'fab fa-etsy fa-fw',
      name: 'etsy'
    },
    {
      icon: 'fab fa-evernote fa-fw',
      name: 'evernote'
    },
    {
      icon: 'fab fa-expeditedssl fa-fw',
      name: 'expeditedssl'
    },
    {
      icon: 'fab fa-facebook fa-fw',
      name: 'facebook'
    },
    {
      icon: 'fab fa-facebook-f fa-fw',
      name: 'facebook-f'
    },
    {
      icon: 'fab fa-facebook-messenger fa-fw',
      name: 'facebook-messenger'
    },
    {
      icon: 'fab fa-facebook-square fa-fw',
      name: 'facebook-square'
    },
    {
      icon: 'fab fa-fantasy-flight-games fa-fw',
      name: 'fantasy-flight-games'
    },
    {
      icon: 'fab fa-fedex fa-fw',
      name: 'fedex'
    },
    {
      icon: 'fab fa-fedora fa-fw',
      name: 'fedora'
    },
    {
      icon: 'fab fa-figma fa-fw',
      name: 'figma'
    },
    {
      icon: 'fab fa-firefox fa-fw',
      name: 'firefox'
    },
    {
      icon: 'fab fa-firefox-browser fa-fw',
      name: 'firefox-browser'
    },
    {
      icon: 'fab fa-first-order fa-fw',
      name: 'first-order'
    },
    {
      icon: 'fab fa-first-order-alt fa-fw',
      name: 'first-order-alt'
    },
    {
      icon: 'fab fa-firstdraft fa-fw',
      name: 'firstdraft'
    },
    {
      icon: 'fab fa-flickr fa-fw',
      name: 'flickr'
    },
    {
      icon: 'fab fa-flipboard fa-fw',
      name: 'flipboard'
    },
    {
      icon: 'fab fa-fly fa-fw',
      name: 'fly'
    },
    {
      icon: 'fab fa-font-awesome fa-fw',
      name: 'font-awesome'
    },
    {
      icon: 'fab fa-fonticons fa-fw',
      name: 'fonticons'
    },
    {
      icon: 'fab fa-fonticons-fi fa-fw',
      name: 'fonticons-fi'
    },
    {
      icon: 'fab fa-fort-awesome fa-fw',
      name: 'fort-awesome'
    },
    {
      icon: 'fab fa-fort-awesome-alt fa-fw',
      name: 'fort-awesome-alt'
    },
    {
      icon: 'fab fa-forumbee fa-fw',
      name: 'forumbee'
    },
    {
      icon: 'fab fa-foursquare fa-fw',
      name: 'foursquare'
    },
    {
      icon: 'fab fa-free-code-camp fa-fw',
      name: 'free-code-camp'
    },
    {
      icon: 'fab fa-freebsd fa-fw',
      name: 'freebsd'
    },
    {
      icon: 'fab fa-fulcrum fa-fw',
      name: 'fulcrum'
    },
    {
      icon: 'fab fa-galactic-republic fa-fw',
      name: 'galactic-republic'
    },
    {
      icon: 'fab fa-galactic-senate fa-fw',
      name: 'galactic-senate'
    },
    {
      icon: 'fab fa-get-pocket fa-fw',
      name: 'get-pocket'
    },
    {
      icon: 'fab fa-gg fa-fw',
      name: 'gg'
    },
    {
      icon: 'fab fa-gg-circle fa-fw',
      name: 'gg-circle'
    },
    {
      icon: 'fab fa-git fa-fw',
      name: 'git'
    },
    {
      icon: 'fab fa-git-alt fa-fw',
      name: 'git-alt'
    },
    {
      icon: 'fab fa-git-square fa-fw',
      name: 'git-square'
    },
    {
      icon: 'fab fa-github fa-fw',
      name: 'github'
    },
    {
      icon: 'fab fa-github-alt fa-fw',
      name: 'github-alt'
    },
    {
      icon: 'fab fa-github-square fa-fw',
      name: 'github-square'
    },
    {
      icon: 'fab fa-gitkraken fa-fw',
      name: 'gitkraken'
    },
    {
      icon: 'fab fa-gitlab fa-fw',
      name: 'gitlab'
    },
    {
      icon: 'fab fa-gitter fa-fw',
      name: 'gitter'
    },
    {
      icon: 'fab fa-glide fa-fw',
      name: 'glide'
    },
    {
      icon: 'fab fa-glide-g fa-fw',
      name: 'glide-g'
    },
    {
      icon: 'fab fa-gofore fa-fw',
      name: 'gofore'
    },
    {
      icon: 'fab fa-golang fa-fw',
      name: 'golang'
    },
    {
      icon: 'fab fa-goodreads fa-fw',
      name: 'goodreads'
    },
    {
      icon: 'fab fa-goodreads-g fa-fw',
      name: 'goodreads-g'
    },
    {
      icon: 'fab fa-google fa-fw',
      name: 'google'
    },
    {
      icon: 'fab fa-google-drive fa-fw',
      name: 'google-drive'
    },
    {
      icon: 'fab fa-google-pay fa-fw',
      name: 'google-pay'
    },
    {
      icon: 'fab fa-google-play fa-fw',
      name: 'google-play'
    },
    {
      icon: 'fab fa-google-plus fa-fw',
      name: 'google-plus'
    },
    {
      icon: 'fab fa-google-plus-g fa-fw',
      name: 'google-plus-g'
    },
    {
      icon: 'fab fa-google-plus-square fa-fw',
      name: 'google-plus-square'
    },
    {
      icon: 'fab fa-google-wallet fa-fw',
      name: 'google-wallet'
    },
    {
      icon: 'fab fa-gratipay fa-fw',
      name: 'gratipay'
    },
    {
      icon: 'fab fa-grav fa-fw',
      name: 'grav'
    },
    {
      icon: 'fab fa-gripfire fa-fw',
      name: 'gripfire'
    },
    {
      icon: 'fab fa-grunt fa-fw',
      name: 'grunt'
    },
    {
      icon: 'fab fa-guilded fa-fw',
      name: 'guilded'
    },
    {
      icon: 'fab fa-gulp fa-fw',
      name: 'gulp'
    },
    {
      icon: 'fab fa-hacker-news fa-fw',
      name: 'hacker-news'
    },
    {
      icon: 'fab fa-hacker-news-square fa-fw',
      name: 'hacker-news-square'
    },
    {
      icon: 'fab fa-hackerrank fa-fw',
      name: 'hackerrank'
    },
    {
      icon: 'fab fa-hashnode fa-fw',
      name: 'hashnode'
    },
    {
      icon: 'fab fa-hips fa-fw',
      name: 'hips'
    },
    {
      icon: 'fab fa-hire-a-helper fa-fw',
      name: 'hire-a-helper'
    },
    {
      icon: 'fab fa-hive fa-fw',
      name: 'hive'
    },
    {
      icon: 'fab fa-hooli fa-fw',
      name: 'hooli'
    },
    {
      icon: 'fab fa-hornbill fa-fw',
      name: 'hornbill'
    },
    {
      icon: 'fab fa-hotjar fa-fw',
      name: 'hotjar'
    },
    {
      icon: 'fab fa-houzz fa-fw',
      name: 'houzz'
    },
    {
      icon: 'fab fa-html5 fa-fw',
      name: 'html5'
    },
    {
      icon: 'fab fa-hubspot fa-fw',
      name: 'hubspot'
    },
    {
      icon: 'fab fa-ideal fa-fw',
      name: 'ideal'
    },
    {
      icon: 'fab fa-imdb fa-fw',
      name: 'imdb'
    },
    {
      icon: 'fab fa-instagram fa-fw',
      name: 'instagram'
    },
    {
      icon: 'fab fa-instagram-square fa-fw',
      name: 'instagram-square'
    },
    {
      icon: 'fab fa-instalod fa-fw',
      name: 'instalod'
    },
    {
      icon: 'fab fa-intercom fa-fw',
      name: 'intercom'
    },
    {
      icon: 'fab fa-internet-explorer fa-fw',
      name: 'internet-explorer'
    },
    {
      icon: 'fab fa-invision fa-fw',
      name: 'invision'
    },
    {
      icon: 'fab fa-ioxhost fa-fw',
      name: 'ioxhost'
    },
    {
      icon: 'fab fa-itch-io fa-fw',
      name: 'itch-io'
    },
    {
      icon: 'fab fa-itunes fa-fw',
      name: 'itunes'
    },
    {
      icon: 'fab fa-itunes-note fa-fw',
      name: 'itunes-note'
    },
    {
      icon: 'fab fa-java fa-fw',
      name: 'java'
    },
    {
      icon: 'fab fa-jedi-order fa-fw',
      name: 'jedi-order'
    },
    {
      icon: 'fab fa-jenkins fa-fw',
      name: 'jenkins'
    },
    {
      icon: 'fab fa-jira fa-fw',
      name: 'jira'
    },
    {
      icon: 'fab fa-joget fa-fw',
      name: 'joget'
    },
    {
      icon: 'fab fa-joomla fa-fw',
      name: 'joomla'
    },
    {
      icon: 'fab fa-js fa-fw',
      name: 'js'
    },
    {
      icon: 'fab fa-js-square fa-fw',
      name: 'js-square'
    },
    {
      icon: 'fab fa-jsfiddle fa-fw',
      name: 'jsfiddle'
    },
    {
      icon: 'fab fa-kaggle fa-fw',
      name: 'kaggle'
    },
    {
      icon: 'fab fa-keybase fa-fw',
      name: 'keybase'
    },
    {
      icon: 'fab fa-keycdn fa-fw',
      name: 'keycdn'
    },
    {
      icon: 'fab fa-kickstarter fa-fw',
      name: 'kickstarter'
    },
    {
      icon: 'fab fa-kickstarter-k fa-fw',
      name: 'kickstarter-k'
    },
    {
      icon: 'fab fa-korvue fa-fw',
      name: 'korvue'
    },
    {
      icon: 'fab fa-laravel fa-fw',
      name: 'laravel'
    },
    {
      icon: 'fab fa-lastfm fa-fw',
      name: 'lastfm'
    },
    {
      icon: 'fab fa-lastfm-square fa-fw',
      name: 'lastfm-square'
    },
    {
      icon: 'fab fa-leanpub fa-fw',
      name: 'leanpub'
    },
    {
      icon: 'fab fa-less fa-fw',
      name: 'less'
    },
    {
      icon: 'fab fa-line fa-fw',
      name: 'line'
    },
    {
      icon: 'fab fa-linkedin fa-fw',
      name: 'linkedin'
    },
    {
      icon: 'fab fa-linkedin-in fa-fw',
      name: 'linkedin-in'
    },
    {
      icon: 'fab fa-linode fa-fw',
      name: 'linode'
    },
    {
      icon: 'fab fa-linux fa-fw',
      name: 'linux'
    },
    {
      icon: 'fab fa-lyft fa-fw',
      name: 'lyft'
    },
    {
      icon: 'fab fa-magento fa-fw',
      name: 'magento'
    },
    {
      icon: 'fab fa-mailchimp fa-fw',
      name: 'mailchimp'
    },
    {
      icon: 'fab fa-mandalorian fa-fw',
      name: 'mandalorian'
    },
    {
      icon: 'fab fa-markdown fa-fw',
      name: 'markdown'
    },
    {
      icon: 'fab fa-mastodon fa-fw',
      name: 'mastodon'
    },
    {
      icon: 'fab fa-maxcdn fa-fw',
      name: 'maxcdn'
    },
    {
      icon: 'fab fa-mdb fa-fw',
      name: 'mdb'
    },
    {
      icon: 'fab fa-medapps fa-fw',
      name: 'medapps'
    },
    {
      icon: 'fab fa-medium fa-fw',
      name: 'medium'
    },
    {
      icon: 'fab fa-medrt fa-fw',
      name: 'medrt'
    },
    {
      icon: 'fab fa-meetup fa-fw',
      name: 'meetup'
    },
    {
      icon: 'fab fa-megaport fa-fw',
      name: 'megaport'
    },
    {
      icon: 'fab fa-mendeley fa-fw',
      name: 'mendeley'
    },
    {
      icon: 'fab fa-microblog fa-fw',
      name: 'microblog'
    },
    {
      icon: 'fab fa-microsoft fa-fw',
      name: 'microsoft'
    },
    {
      icon: 'fab fa-mix fa-fw',
      name: 'mix'
    },
    {
      icon: 'fab fa-mixcloud fa-fw',
      name: 'mixcloud'
    },
    {
      icon: 'fab fa-mixer fa-fw',
      name: 'mixer'
    },
    {
      icon: 'fab fa-mizuni fa-fw',
      name: 'mizuni'
    },
    {
      icon: 'fab fa-modx fa-fw',
      name: 'modx'
    },
    {
      icon: 'fab fa-monero fa-fw',
      name: 'monero'
    },
    {
      icon: 'fab fa-napster fa-fw',
      name: 'napster'
    },
    {
      icon: 'fab fa-neos fa-fw',
      name: 'neos'
    },
    {
      icon: 'fab fa-nfc-directional fa-fw',
      name: 'nfc-directional'
    },
    {
      icon: 'fab fa-nfc-symbol fa-fw',
      name: 'nfc-symbol'
    },
    {
      icon: 'fab fa-nimblr fa-fw',
      name: 'nimblr'
    },
    {
      icon: 'fab fa-node fa-fw',
      name: 'node'
    },
    {
      icon: 'fab fa-node-js fa-fw',
      name: 'node-js'
    },
    {
      icon: 'fab fa-npm fa-fw',
      name: 'npm'
    },
    {
      icon: 'fab fa-ns8 fa-fw',
      name: 'ns8'
    },
    {
      icon: 'fab fa-nutritionix fa-fw',
      name: 'nutritionix'
    },
    {
      icon: 'fab fa-octopus-deploy fa-fw',
      name: 'octopus-deploy'
    },
    {
      icon: 'fab fa-odnoklassniki fa-fw',
      name: 'odnoklassniki'
    },
    {
      icon: 'fab fa-odnoklassniki-square fa-fw',
      name: 'odnoklassniki-square'
    },
    {
      icon: 'fab fa-old-republic fa-fw',
      name: 'old-republic'
    },
    {
      icon: 'fab fa-opencart fa-fw',
      name: 'opencart'
    },
    {
      icon: 'fab fa-openid fa-fw',
      name: 'openid'
    },
    {
      icon: 'fab fa-opera fa-fw',
      name: 'opera'
    },
    {
      icon: 'fab fa-optin-monster fa-fw',
      name: 'optin-monster'
    },
    {
      icon: 'fab fa-orcid fa-fw',
      name: 'orcid'
    },
    {
      icon: 'fab fa-osi fa-fw',
      name: 'osi'
    },
    {
      icon: 'fab fa-padlet fa-fw',
      name: 'padlet'
    },
    {
      icon: 'fab fa-page4 fa-fw',
      name: 'page4'
    },
    {
      icon: 'fab fa-pagelines fa-fw',
      name: 'pagelines'
    },
    {
      icon: 'fab fa-palfed fa-fw',
      name: 'palfed'
    },
    {
      icon: 'fab fa-patreon fa-fw',
      name: 'patreon'
    },
    {
      icon: 'fab fa-paypal fa-fw',
      name: 'paypal'
    },
    {
      icon: 'fab fa-perbyte fa-fw',
      name: 'perbyte'
    },
    {
      icon: 'fab fa-periscope fa-fw',
      name: 'periscope'
    },
    {
      icon: 'fab fa-phabricator fa-fw',
      name: 'phabricator'
    },
    {
      icon: 'fab fa-phoenix-framework fa-fw',
      name: 'phoenix-framework'
    },
    {
      icon: 'fab fa-phoenix-squadron fa-fw',
      name: 'phoenix-squadron'
    },
    {
      icon: 'fab fa-php fa-fw',
      name: 'php'
    },
    {
      icon: 'fab fa-pied-piper fa-fw',
      name: 'pied-piper'
    },
    {
      icon: 'fab fa-pied-piper-alt fa-fw',
      name: 'pied-piper-alt'
    },
    {
      icon: 'fab fa-pied-piper-hat fa-fw',
      name: 'pied-piper-hat'
    },
    {
      icon: 'fab fa-pied-piper-pp fa-fw',
      name: 'pied-piper-pp'
    },
    {
      icon: 'fab fa-pied-piper-square fa-fw',
      name: 'pied-piper-square'
    },
    {
      icon: 'fab fa-pinterest fa-fw',
      name: 'pinterest'
    },
    {
      icon: 'fab fa-pinterest-p fa-fw',
      name: 'pinterest-p'
    },
    {
      icon: 'fab fa-pinterest-square fa-fw',
      name: 'pinterest-square'
    },
    {
      icon: 'fab fa-pix fa-fw',
      name: 'pix'
    },
    {
      icon: 'fab fa-playstation fa-fw',
      name: 'playstation'
    },
    {
      icon: 'fab fa-product-hunt fa-fw',
      name: 'product-hunt'
    },
    {
      icon: 'fab fa-pushed fa-fw',
      name: 'pushed'
    },
    {
      icon: 'fab fa-python fa-fw',
      name: 'python'
    },
    {
      icon: 'fab fa-qq fa-fw',
      name: 'qq'
    },
    {
      icon: 'fab fa-quinscape fa-fw',
      name: 'quinscape'
    },
    {
      icon: 'fab fa-quora fa-fw',
      name: 'quora'
    },
    {
      icon: 'fab fa-r-project fa-fw',
      name: 'r-project'
    },
    {
      icon: 'fab fa-raspberry-pi fa-fw',
      name: 'raspberry-pi'
    },
    {
      icon: 'fab fa-ravelry fa-fw',
      name: 'ravelry'
    },
    {
      icon: 'fab fa-react fa-fw',
      name: 'react'
    },
    {
      icon: 'fab fa-reacteurope fa-fw',
      name: 'reacteurope'
    },
    {
      icon: 'fab fa-readme fa-fw',
      name: 'readme'
    },
    {
      icon: 'fab fa-rebel fa-fw',
      name: 'rebel'
    },
    {
      icon: 'fab fa-red-river fa-fw',
      name: 'red-river'
    },
    {
      icon: 'fab fa-reddit fa-fw',
      name: 'reddit'
    },
    {
      icon: 'fab fa-reddit-alien fa-fw',
      name: 'reddit-alien'
    },
    {
      icon: 'fab fa-reddit-square fa-fw',
      name: 'reddit-square'
    },
    {
      icon: 'fab fa-redhat fa-fw',
      name: 'redhat'
    },
    {
      icon: 'fab fa-renren fa-fw',
      name: 'renren'
    },
    {
      icon: 'fab fa-replyd fa-fw',
      name: 'replyd'
    },
    {
      icon: 'fab fa-researchgate fa-fw',
      name: 'researchgate'
    },
    {
      icon: 'fab fa-resolving fa-fw',
      name: 'resolving'
    },
    {
      icon: 'fab fa-rev fa-fw',
      name: 'rev'
    },
    {
      icon: 'fab fa-rocketchat fa-fw',
      name: 'rocketchat'
    },
    {
      icon: 'fab fa-rockrms fa-fw',
      name: 'rockrms'
    },
    {
      icon: 'fab fa-rust fa-fw',
      name: 'rust'
    },
    {
      icon: 'fab fa-safari fa-fw',
      name: 'safari'
    },
    {
      icon: 'fab fa-salesforce fa-fw',
      name: 'salesforce'
    },
    {
      icon: 'fab fa-sass fa-fw',
      name: 'sass'
    },
    {
      icon: 'fab fa-schlix fa-fw',
      name: 'schlix'
    },
    {
      icon: 'fab fa-screenpal fa-fw',
      name: 'screenpal'
    },
    {
      icon: 'fab fa-scribd fa-fw',
      name: 'scribd'
    },
    {
      icon: 'fab fa-searchengin fa-fw',
      name: 'searchengin'
    },
    {
      icon: 'fab fa-sellcast fa-fw',
      name: 'sellcast'
    },
    {
      icon: 'fab fa-sellsy fa-fw',
      name: 'sellsy'
    },
    {
      icon: 'fab fa-servicestack fa-fw',
      name: 'servicestack'
    },
    {
      icon: 'fab fa-shirtsinbulk fa-fw',
      name: 'shirtsinbulk'
    },
    {
      icon: 'fab fa-shopify fa-fw',
      name: 'shopify'
    },
    {
      icon: 'fab fa-shopware fa-fw',
      name: 'shopware'
    },
    {
      icon: 'fab fa-simplybuilt fa-fw',
      name: 'simplybuilt'
    },
    {
      icon: 'fab fa-sistrix fa-fw',
      name: 'sistrix'
    },
    {
      icon: 'fab fa-sith fa-fw',
      name: 'sith'
    },
    {
      icon: 'fab fa-sitrox fa-fw',
      name: 'sitrox'
    },
    {
      icon: 'fab fa-sketch fa-fw',
      name: 'sketch'
    },
    {
      icon: 'fab fa-skyatlas fa-fw',
      name: 'skyatlas'
    },
    {
      icon: 'fab fa-skype fa-fw',
      name: 'skype'
    },
    {
      icon: 'fab fa-slack fa-fw',
      name: 'slack'
    },
    {
      icon: 'fab fa-slideshare fa-fw',
      name: 'slideshare'
    },
    {
      icon: 'fab fa-snapchat fa-fw',
      name: 'snapchat'
    },
    {
      icon: 'fab fa-snapchat-square fa-fw',
      name: 'snapchat-square'
    },
    {
      icon: 'fab fa-soundcloud fa-fw',
      name: 'soundcloud'
    },
    {
      icon: 'fab fa-sourcetree fa-fw',
      name: 'sourcetree'
    },
    {
      icon: 'fab fa-speakap fa-fw',
      name: 'speakap'
    },
    {
      icon: 'fab fa-speaker-deck fa-fw',
      name: 'speaker-deck'
    },
    {
      icon: 'fab fa-spotify fa-fw',
      name: 'spotify'
    },
    {
      icon: 'fab fa-square-font-awesome fa-fw',
      name: 'square-font-awesome'
    },
    {
      icon: 'fab fa-square-font-awesome-stroke fa-fw',
      name: 'square-font-awesome-stroke'
    },
    {
      icon: 'fab fa-squarespace fa-fw',
      name: 'squarespace'
    },
    {
      icon: 'fab fa-stack-exchange fa-fw',
      name: 'stack-exchange'
    },
    {
      icon: 'fab fa-stack-overflow fa-fw',
      name: 'stack-overflow'
    },
    {
      icon: 'fab fa-stackpath fa-fw',
      name: 'stackpath'
    },
    {
      icon: 'fab fa-staylinked fa-fw',
      name: 'staylinked'
    },
    {
      icon: 'fab fa-steam fa-fw',
      name: 'steam'
    },
    {
      icon: 'fab fa-steam-square fa-fw',
      name: 'steam-square'
    },
    {
      icon: 'fab fa-steam-symbol fa-fw',
      name: 'steam-symbol'
    },
    {
      icon: 'fab fa-sticker-mule fa-fw',
      name: 'sticker-mule'
    },
    {
      icon: 'fab fa-strava fa-fw',
      name: 'strava'
    },
    {
      icon: 'fab fa-stripe fa-fw',
      name: 'stripe'
    },
    {
      icon: 'fab fa-stripe-s fa-fw',
      name: 'stripe-s'
    },
    {
      icon: 'fab fa-studiovinari fa-fw',
      name: 'studiovinari'
    },
    {
      icon: 'fab fa-stumbleupon fa-fw',
      name: 'stumbleupon'
    },
    {
      icon: 'fab fa-stumbleupon-circle fa-fw',
      name: 'stumbleupon-circle'
    },
    {
      icon: 'fab fa-superpowers fa-fw',
      name: 'superpowers'
    },
    {
      icon: 'fab fa-supple fa-fw',
      name: 'supple'
    },
    {
      icon: 'fab fa-suse fa-fw',
      name: 'suse'
    },
    {
      icon: 'fab fa-swift fa-fw',
      name: 'swift'
    },
    {
      icon: 'fab fa-symfony fa-fw',
      name: 'symfony'
    },
    {
      icon: 'fab fa-teamspeak fa-fw',
      name: 'teamspeak'
    },
    {
      icon: 'fab fa-telegram fa-fw',
      name: 'telegram'
    },
    {
      icon: 'fab fa-tencent-weibo fa-fw',
      name: 'tencent-weibo'
    },
    {
      icon: 'fab fa-the-red-yeti fa-fw',
      name: 'the-red-yeti'
    },
    {
      icon: 'fab fa-themeco fa-fw',
      name: 'themeco'
    },
    {
      icon: 'fab fa-themeisle fa-fw',
      name: 'themeisle'
    },
    {
      icon: 'fab fa-think-peaks fa-fw',
      name: 'think-peaks'
    },
    {
      icon: 'fab fa-tiktok fa-fw',
      name: 'tiktok'
    },
    {
      icon: 'fab fa-trade-federation fa-fw',
      name: 'trade-federation'
    },
    {
      icon: 'fab fa-trello fa-fw',
      name: 'trello'
    },
    {
      icon: 'fab fa-tumblr fa-fw',
      name: 'tumblr'
    },
    {
      icon: 'fab fa-tumblr-square fa-fw',
      name: 'tumblr-square'
    },
    {
      icon: 'fab fa-twitch fa-fw',
      name: 'twitch'
    },
    {
      icon: 'fab fa-twitter fa-fw',
      name: 'twitter'
    },
    {
      icon: 'fab fa-twitter-square fa-fw',
      name: 'twitter-square'
    },
    {
      icon: 'fab fa-typo3 fa-fw',
      name: 'typo3'
    },
    {
      icon: 'fab fa-uber fa-fw',
      name: 'uber'
    },
    {
      icon: 'fab fa-ubuntu fa-fw',
      name: 'ubuntu'
    },
    {
      icon: 'fab fa-uikit fa-fw',
      name: 'uikit'
    },
    {
      icon: 'fab fa-umbraco fa-fw',
      name: 'umbraco'
    },
    {
      icon: 'fab fa-uncharted fa-fw',
      name: 'uncharted'
    },
    {
      icon: 'fab fa-uniregistry fa-fw',
      name: 'uniregistry'
    },
    {
      icon: 'fab fa-unity fa-fw',
      name: 'unity'
    },
    {
      icon: 'fab fa-unsplash fa-fw',
      name: 'unsplash'
    },
    {
      icon: 'fab fa-untappd fa-fw',
      name: 'untappd'
    },
    {
      icon: 'fab fa-ups fa-fw',
      name: 'ups'
    },
    {
      icon: 'fab fa-usb fa-fw',
      name: 'usb'
    },
    {
      icon: 'fab fa-usps fa-fw',
      name: 'usps'
    },
    {
      icon: 'fab fa-ussunnah fa-fw',
      name: 'ussunnah'
    },
    {
      icon: 'fab fa-vaadin fa-fw',
      name: 'vaadin'
    },
    {
      icon: 'fab fa-viacoin fa-fw',
      name: 'viacoin'
    },
    {
      icon: 'fab fa-viadeo fa-fw',
      name: 'viadeo'
    },
    {
      icon: 'fab fa-viadeo-square fa-fw',
      name: 'viadeo-square'
    },
    {
      icon: 'fab fa-viber fa-fw',
      name: 'viber'
    },
    {
      icon: 'fab fa-vimeo fa-fw',
      name: 'vimeo'
    },
    {
      icon: 'fab fa-vimeo-square fa-fw',
      name: 'vimeo-square'
    },
    {
      icon: 'fab fa-vimeo-v fa-fw',
      name: 'vimeo-v'
    },
    {
      icon: 'fab fa-vine fa-fw',
      name: 'vine'
    },
    {
      icon: 'fab fa-vk fa-fw',
      name: 'vk'
    },
    {
      icon: 'fab fa-vnv fa-fw',
      name: 'vnv'
    },
    {
      icon: 'fab fa-vuejs fa-fw',
      name: 'vuejs'
    },
    {
      icon: 'fab fa-watchman-monitoring fa-fw',
      name: 'watchman-monitoring'
    },
    {
      icon: 'fab fa-waze fa-fw',
      name: 'waze'
    },
    {
      icon: 'fab fa-weebly fa-fw',
      name: 'weebly'
    },
    {
      icon: 'fab fa-weibo fa-fw',
      name: 'weibo'
    },
    {
      icon: 'fab fa-weixin fa-fw',
      name: 'weixin'
    },
    {
      icon: 'fab fa-whatsapp fa-fw',
      name: 'whatsapp'
    },
    {
      icon: 'fab fa-whatsapp-square fa-fw',
      name: 'whatsapp-square'
    },
    {
      icon: 'fab fa-whmcs fa-fw',
      name: 'whmcs'
    },
    {
      icon: 'fab fa-wikipedia-w fa-fw',
      name: 'wikipedia-w'
    },
    {
      icon: 'fab fa-windows fa-fw',
      name: 'windows'
    },
    {
      icon: 'fab fa-wirsindhandwerk fa-fw',
      name: 'wirsindhandwerk'
    },
    {
      icon: 'fab fa-wix fa-fw',
      name: 'wix'
    },
    {
      icon: 'fab fa-wizards-of-the-coast fa-fw',
      name: 'wizards-of-the-coast'
    },
    {
      icon: 'fab fa-wodu fa-fw',
      name: 'wodu'
    },
    {
      icon: 'fab fa-wolf-pack-battalion fa-fw',
      name: 'wolf-pack-battalion'
    },
    {
      icon: 'fab fa-wordpress fa-fw',
      name: 'wordpress'
    },
    {
      icon: 'fab fa-wordpress-simple fa-fw',
      name: 'wordpress-simple'
    },
    {
      icon: 'fab fa-wpbeginner fa-fw',
      name: 'wpbeginner'
    },
    {
      icon: 'fab fa-wpexplorer fa-fw',
      name: 'wpexplorer'
    },
    {
      icon: 'fab fa-wpforms fa-fw',
      name: 'wpforms'
    },
    {
      icon: 'fab fa-wpressr fa-fw',
      name: 'wpressr'
    },
    {
      icon: 'fab fa-xbox fa-fw',
      name: 'xbox'
    },
    {
      icon: 'fab fa-xing fa-fw',
      name: 'xing'
    },
    {
      icon: 'fab fa-xing-square fa-fw',
      name: 'xing-square'
    },
    {
      icon: 'fab fa-y-combinator fa-fw',
      name: 'y-combinator'
    },
    {
      icon: 'fab fa-yahoo fa-fw',
      name: 'yahoo'
    },
    {
      icon: 'fab fa-yammer fa-fw',
      name: 'yammer'
    },
    {
      icon: 'fab fa-yandex fa-fw',
      name: 'yandex'
    },
    {
      icon: 'fab fa-yandex-international fa-fw',
      name: 'yandex-international'
    },
    {
      icon: 'fab fa-yarn fa-fw',
      name: 'yarn'
    },
    {
      icon: 'fab fa-yelp fa-fw',
      name: 'yelp'
    },
    {
      icon: 'fab fa-yoast fa-fw',
      name: 'yoast'
    },
    {
      icon: 'fab fa-youtube fa-fw',
      name: 'youtube'
    },
    {
      icon: 'fab fa-youtube-square fa-fw',
      name: 'youtube-square'
    },
    {
      icon: 'fab fa-zhihu fa-fw',
      name: 'zhihu'
    },
  ]

  iconsEva: Array<any> = [];

  icon: string = 'si si-user';
  search: string = '';

  constructor(
    private changeRef: ChangeDetectorRef,
    private iconLibraries: NbIconLibraries
  ) { }

  ngOnInit(): void {
    this.initialize();
  }

  async initialize() {
    let iconsEva = [];
    const evaPack: any = await this.iconLibraries.getPack('eva');
    this.iconsEva = evaPack.icons.keys();
    const formatEva = async (iterator) => {
      for (const item of iterator) {
        await iconsEva.push({
          icon: item[0]
        });
      }
    }
    await formatEva(evaPack.icons[Symbol.iterator]());
    this.iconsEva = iconsEva;
  }

  onSelect(icon) {
    if (this.selected != icon) {
      this.select.emit(icon);
    } else {
      this.select.emit(null);
    }
  }

  searchIcon() {
    this.changeRef.detectChanges();
  }

  filterIcon(target = '', exist) {
    if (target) {
      if (exist.indexOf(target) >= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

}
