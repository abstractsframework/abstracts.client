import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaBrowserComponent } from './media-browser.component';
import { NbButtonModule } from '@nebular/theme';



@NgModule({
  declarations: [
    MediaBrowserComponent
  ],
  imports: [
    CommonModule,
    NbButtonModule,
  ],
  exports: [
    MediaBrowserComponent
  ]
})
export class MediaBrowserModule { }
