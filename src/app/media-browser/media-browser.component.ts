import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'abstract-media-browser',
  templateUrl: './media-browser.component.html',
  styleUrls: ['./media-browser.component.scss']
})
export class MediaBrowserComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Input() selectedFilePath: any;
  @Input() extensionAllowed: any = [];
  @Input() showPreview: boolean = false;
  @Output() select: EventEmitter<string> = new EventEmitter<string>();

  files = [];
  fileExtensions = {
    image: ['jpg', 'jpeg', 'bmp', 'png', 'gif', 'svg'],
    video: ['mp4', 'mpg', 'avi', 'mov'],
    audio: ['mp3', 'wav', 'aac'],
    doc: ['txt', 'md'],
    pdf: ['pdf'],
    csv: ['csv'],
    word: ['doc', 'docx'],
    excel: ['xls', 'xlsx'],
    powerpoint: ['ppt', 'pptx'],
    zipper: ['zip', 'rar'],
    code: ['html', 'php', 'xml', 'js', 'jsx', 'ts', 'tsx', 'css', 'json']
  }
  parent = null;
  preview = null;

  constructor() { }

  ngOnInit(): void {
    this.initialize();
  }

  ngOnChanges(): void {
  }

  private async initialize() {
    const findCurrent = async (
      files, 
      filePathToCheck, 
      parentFiles = null, 
      checking: boolean = false
    ) => {
      let found = null;
      for (let file of files) {
        if (!found) {
          if (file.path == filePathToCheck) {
            found = files;
            if (!checking) {
              if (parentFiles && parentFiles.length) {
                const parentCheck = await findCurrent(this.data, parentFiles[0].path, null, true);
                if (parentCheck) {
                  for (let parent of parentCheck) {
                    if (parent.is_directory) {
                      for (let parentSub of parent.sub) {
                        if (parentSub.path == filePathToCheck) {
                          this.parent = parent.path;
                        }
                      }
                    }
                  }
                } else {
                  this.parent = null;
                }
              }
              this.files = files;
            }
          } else {
            found = await findCurrent(file.sub, filePathToCheck, files, checking);
          }
        }
      }
      return found;
    }
    const parent = await findCurrent(this.data, this.selectedFilePath);
    if (!parent) {
      this.files = await JSON.parse(await JSON.stringify(this.data));
    }
  }

  onSelect(file) {
    this.select.emit(file);
    this.preview = file;
  }

  selectFolder(files, parent) {
    this.parent = parent;
    this.files = files;
  }

  async exitFolder() {
    if (this.data && this.files.length) {
      const findParent = async (
        files, 
        filePathToCheck, 
        parentFiles = null, 
        checking: boolean = false
      ) => {
        let found = null;
        for (let file of files) {
          if (!found) {
            if (file.path == filePathToCheck) {
              if (parentFiles) {
                found = parentFiles;
                if (!checking) {
                  if (parentFiles.length) {
                    const parentCheck = await findParent(this.data, parentFiles[0].path, null, true);
                    if (parentCheck) {
                      for (let parent of parentCheck) {
                        if (parent.is_directory) {
                          for (let parentSub of parent.sub) {
                            if (parentSub.path == parentFiles[0].path) {
                              this.parent = parent.path;
                            }
                          }
                        }
                      }
                    } else {
                      this.parent = null;
                    }
                  }
                  this.files = parentFiles;
                }
              }
            } else {
              found = await findParent(file.sub, filePathToCheck, files, checking);
            }
          }
        }
        return found;
      }
      findParent(this.data, this.files[0].path);
    }
  }

}
