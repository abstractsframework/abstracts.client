import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MediaRoutingModule } from './media-routing.module';
import { MediaComponent } from './media.component';
import { MediaViewComponent } from './media-view/media-view.component';
import { MediaTableComponent } from './media-table/media-table.component';
import { MediaCreateComponent } from './media-create/media-create.component';
import { MediaEditComponent } from './media-edit/media-edit.component';


@NgModule({
  declarations: [
    MediaComponent,
    MediaViewComponent,
    MediaTableComponent,
    MediaCreateComponent,
    MediaEditComponent
  ],
  imports: [
    CommonModule,
    MediaRoutingModule
  ]
})
export class MediaModule { }
