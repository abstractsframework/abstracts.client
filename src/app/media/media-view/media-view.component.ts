import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-media-view',
  templateUrl: './media-view.component.html',
  styleUrls: ['./media-view.component.scss']
})
export class MediaViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
