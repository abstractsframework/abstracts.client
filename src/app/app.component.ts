/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import { SeoService } from './@core/utils/seo.service';
import { Plugins } from '@capacitor/core';
import { NbIconLibraries } from '@nebular/theme';

@Component({
  selector: 'abstract-app',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  constructor(
    private analytics: AnalyticsService, 
    private seoService: SeoService,
    private iconLibraries: NbIconLibraries
  ) {
    this.iconLibraries.registerFontPack('fontawesome', { packClass: 'fa' });
    this.iconLibraries.registerFontPack('fontawesome-solid', { packClass: 'fas' });
    this.iconLibraries.registerFontPack('fontawesome-regular', { packClass: 'far' });
    this.iconLibraries.registerFontPack('fontawesome-brand', { packClass: 'fab' });
    this.iconLibraries.registerFontPack('simple-line', { packClass: 'si' });
  }

  ngOnInit(): void {
  }
}
