import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  FormsModule, 
  ReactiveFormsModule 
} from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';

import { 
  NbCardModule, 
  NbActionsModule, 
  NbIconModule, 
  NbSelectModule, 
  NbInputModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbButtonModule, 
  NbBadgeModule, 
  NbListModule, 
  NbUserModule, 
  NbSpinnerModule 
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ClipboardModule } from 'ngx-clipboard';

import { MediaBrowserModule } from '../media-browser/media-browser.module';
import { IconModule } from '../icon/icon.module';

import { ApiRoutingModule } from './api-routing.module';
import { ApiComponent } from './api.component';
import { ApiCreateComponent } from './api-create/api-create.component';
import { ApiEditComponent } from './api-edit/api-edit.component';
import { ApiTableComponent } from './api-table/api-table.component';
import { ApiViewComponent } from './api-view/api-view.component';


@NgModule({
  declarations: [
    ApiComponent,
    ApiCreateComponent,
    ApiEditComponent,
    ApiTableComponent,
    ApiViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    DragDropModule,
    ClipboardModule,
    MediaBrowserModule,
    IconModule,
    ApiRoutingModule
  ],
  exports: [
    ApiCreateComponent,
    ApiEditComponent,
    ApiTableComponent,
    ApiViewComponent
  ]
})
export class ApiModule { }
