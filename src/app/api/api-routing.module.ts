import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApiCreateComponent } from './api-create/api-create.component';
import { ApiEditComponent } from './api-edit/api-edit.component';
import { ApiTableComponent } from './api-table/api-table.component';
import { ApiViewComponent } from './api-view/api-view.component';
import { ApiComponent } from './api.component';

const routes: Routes = [
  {
    path: '',
    component: ApiComponent,
    children: [
      {
        path: '',
        component: ApiTableComponent
      },
      {
        path: 'active',
        component: ApiTableComponent
      },
      {
        path: 'inactive',
        component: ApiTableComponent
      },
      {
        path: 'create',
        component: ApiCreateComponent
      },
      {
        path: ':id',
        component: ApiViewComponent
      },
      {
        path: ':id/edit',
        component: ApiEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApiRoutingModule { }
