import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { ApiService } from '../../.services/api.service';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { SUCC_SUBMIT } from '../../.constances/messages';
import { ModuleService } from '../../.services/module.service';

@Component({
  selector: 'abstract-api-create',
  templateUrl: './api-create.component.html',
  styleUrls: ['./api-create.component.scss']
})
export class ApiCreateComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  /* instances */
  path = '/group';
  formGroup: FormGroup;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  /* instances */
  scopes: any = null;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private api: ApiService,
    private module: ModuleService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      await this.module.list().then((response: any) => {
        this.scopes = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      title: [
        null, 
        Validators.required
      ],
      type: [
        'private'
      ],
      scope: [
        null
      ],
    });
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  async submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.router.navigate([this.path, 'edit', response.id]);
        this.components.showToastStatus('success', 'Success', SUCC_SUBMIT);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const params = {
        title: (value.title) ? value.title : '',
        scope: (value.scope) ? value.scope : [],
        type: (value.type) ? value.type : 'private',
        active: this.active
      };
      this.api.create(params).then(async (response: any) => {
        resolve(response);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}
