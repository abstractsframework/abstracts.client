import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { ApiService } from '../../.services/api.service';
import { NbDialogService } from '@nebular/theme';

import * as moment from 'moment';
import { ClipboardService } from 'ngx-clipboard';
import { ThemeService } from '../../.services/theme.service';
import { ModuleService } from '../../.services/module.service';

@Component({
  selector: 'abstract-api-edit',
  templateUrl: './api-edit.component.html',
  styleUrls: ['./api-edit.component.scss']
})
export class ApiEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  moment = moment;

  /* instances */
  path = '/api';
  formGroup: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  /* instances */
  data: any = null;
  scopes: any = null;
  displaySecret: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private theme: ThemeService,
    private components: ComponentsService,
    private api: ApiService,
    private module: ModuleService,
    private clipboardApi: ClipboardService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async prepare() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.data = state;
    }
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {
      if (!this.data || update) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          await this.api.get(id).then(async (response: any) => {
            this.data = response;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
          await this.module.list().then((response: any) => {
            this.scopes = response;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      }
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    if (this.data) {
      this.formGroup = this.formBuilder.group({
        title: [
          (this.data.title) ? this.data.title : null, 
          Validators.required
        ],
        type: [
          (this.data.type) ? this.data.type : 'private'
        ],
        scope: [
          (this.data.scope) ? this.data.scope : null
        ],
        active: [
          (this.data.active && this.data.active === true) 
          ? true : false
        ]
      });
      this.formGroup.valueChanges.subscribe(() => {
        this.changed = true;
      });
    }
  }

  async submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.initialize(true);
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const id = this.route.snapshot.paramMap.get('id');
      const params = {
        title: (value.title) ? value.title : '',
        scope: (value.scope) ? value.scope : [],
        type: (value.type) ? value.type : 'private',
        active: (value.active) ? '1' : '0',
      };
      this.api.update(id, params).then((response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  copy(title, text) {
    this.components.showToastStatus('success', `Copied ${title}!`, `${title} was copied to clipboard`);
    this.clipboardApi.copyFromContent(text);
  }

  toggleDisplaySecret() {
    this.displaySecret = !this.displaySecret;
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}