import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-api-view',
  templateUrl: './api-view.component.html',
  styleUrls: ['./api-view.component.scss']
})
export class ApiViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
