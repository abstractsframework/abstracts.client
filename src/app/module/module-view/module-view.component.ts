import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-view',
  templateUrl: './module-view.component.html',
  styleUrls: ['./module-view.component.scss']
})
export class ModuleViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
