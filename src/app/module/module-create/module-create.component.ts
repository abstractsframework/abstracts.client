import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { UtilitiesService } from '../../.services/utilities.service';
import { ComponentsService } from '../../.services/components.service';
import { ModuleService } from '../../.services/module.service';
import { UserService } from '../../.services/user.service';
import { NbDialogService } from '@nebular/theme';

import * as moment from 'moment';

import { PageService } from '../../.services/page.service';
import { ReferenceService } from '../../.services/reference.service';
import { AbstractsService } from '../../.services/abstract.service';

@Component({
  selector: 'abstract-create',
  templateUrl: './module-create.component.html',
  styleUrls: ['./module-create.component.scss']
})
export class ModuleCreateComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;
  @ViewChild('dialogIcon') dialogIcon: TemplateRef<any>;
  @ViewChild('dialogSubjectIcon') dialogSubjectIcon: TemplateRef<any>;
  @ViewChild('dialogTemplateFiles') dialogTemplateFiles: TemplateRef<any>;
  @ViewChild('dialogServiceFiles') dialogServiceFiles: TemplateRef<any>;

  moment = moment;

  /* instances */
  path = '/module';
  formGroup: FormGroup;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  /* instances */
  abstract: any = [];
  references: any = [];
  defaultFieldFormArray: any = [];
  pages: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private utils: UtilitiesService,
    private components: ComponentsService,
    private module: ModuleService,
    private page: PageService,
    private abstracts: AbstractsService,
    private reference: ReferenceService,
    private user: UserService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      await this.page.list().then(async (response: any) => {
        this.pages = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  async initializeForm() {
    this.formGroup = this.formBuilder.group({
      name: [
        null, 
        Validators.required
      ],
      description: [
        null
      ],
      link: [
        null
      ],
      icon: [
        null
      ],
      category: [
        null
      ],
      subject: [
        null
      ],
      subject_icon: [
        null
      ],
      key: [
        null
      ],
      database_table: [
        null
      ],
      service: [
        null
      ],
      page_template: [
        null
      ],
      page_template_settings: [
        null
      ],
      page_parent_link_key: [
        ''
      ],
      individual_page_parent_link: [
        false
      ],
      image_crop_quality: [
        75
      ],
      image_crop_thumbnail: [
        true
      ],
      image_crop_thumbnail_aspectratio: [
        true
      ],
      image_crop_thumbnail_quality: [
        75
      ],
      image_crop_thumbnail_width: [
        200
      ],
      image_crop_thumbnail_height: [
        200
      ],
      image_crop_large: [
        true
      ],
      image_crop_large_aspectratio: [
        true
      ],
      image_crop_large_quality: [
        75
      ],
      image_crop_large_width: [
        400
      ],
      image_crop_large_height: [
        400
      ],
      file_hash: [
        false
      ],
      default_controls: this.formBuilder.array(
        []
      ),
      default_field: [],
      order: [
        null
      ],
      page_id: [
        null
      ],
      authentication: [
        false
      ]
    });
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  onDefaultControlChange(event, value) {
    const defaultControlsFormArray: FormArray = this.formGroup.get('default_controls') as FormArray;
    if (event.target.checked) {
      defaultControlsFormArray.push(new FormControl(value));
    } else {
      let index: number = 0;
      for (let control of defaultControlsFormArray.controls) {
        if (control.value == value) {
          defaultControlsFormArray.removeAt(index);
          return;
        }
        index += 1;
      }
    }
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.module.changeObservable.next(true);
        this.router.navigate([this.path]);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const params = {
        name: (value.name) ? value.name : '',
        link: (value.link) ? value.link : '',
        description: (value.description) ? value.description : '',
        icon: (value.icon) ? value.icon : '',
        category: (value.category) ? value.category : '',
        subject: (value.subject) ? value.subject : '',
        subject_icon: (value.subject_icon) ? value.subject_icon : '',
        key: (value.key) ? value.key : '',
        database_table: (value.database_table) ? value.database_table : '',
        service: (value.service) ? value.service : '',
        page_template: (value.page_template) ? value.page_template : '',
        page_template_settings: (value.page_template_settings) ? value.page_template_settings : '',
        page_parent_link_key: (value.page_parent_link_key) ? value.page_parent_link_key : '',
        individual_page_parent_link: (value.individual_page_parent_link) ? '1' : '0',
        image_crop_quality: (value.image_crop_quality) ? value.image_crop_quality : '75',
        image_crop_thumbnail: (value.image_crop_thumbnail) ? '1' : '0',
        image_crop_thumbnail_aspectratio: (value.image_crop_thumbnailAspectratio) ? '1' : '0',
        image_crop_thumbnail_quality: (value.image_crop_thumbnail_quality) ? value.image_crop_thumbnail_quality : '75',
        image_crop_thumbnail_width: (value.image_crop_thumbnail_width) ? value.image_crop_thumbnail_width : '200',
        image_crop_thumbnail_height: (value.image_crop_thumbnail_height) ? value.image_crop_thumbnail_height : '200',
        image_crop_large: (value.image_crop_large) ? '1' : '0',
        image_crop_large_aspectratio: (value.image_crop_largeAspectratio) ? '1' : '0',
        image_crop_large_quality: (value.image_crop_large_quality) ? value.image_crop_large_quality : '75',
        image_crop_large_width: (value.image_crop_large_width) ? value.image_crop_large_width : '400',
        image_crop_large_height: (value.image_crop_large_height) ? value.image_crop_large_height : '400',
        file_hash: (value.file_hash) ? '1' : '0',
        default_controls: (value.default_controls) ? value.default_controls : [],
        table_columns: (value.default_field) ? value.default_field : [],
        authentication: (value.authentication) ? '1' : '0',
        page_id: (value.page_id) ? value.page_id : '',
        order: (value.order) ? value.order : '',
        active: this.active,
      };
      this.module.create(params).then(async (response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  openIcon() {
    this.dialogService.open(
      this.dialogIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        icon: data
      });
    });
  }

  openSubjectIcon() {
    this.dialogService.open(
      this.dialogSubjectIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        subject_icon: data
      });
    });
  }

  openFilesService() {
    this.module.getServiceFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogServiceFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            service: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  openFilesTemplate() {
    this.module.getTemplateFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogTemplateFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            page_template: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}