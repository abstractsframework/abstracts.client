import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../.services/components.service';
import { ModuleService } from '../.services/module.service';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { UtilitiesService } from '../.services/utilities.service';

@Component({
  selector: 'abstract',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss']
})
export class ModuleComponent implements OnInit {

  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogSort') dialogSort: TemplateRef<any>;

  parseInt = parseInt;

  path = '/module';
  active = 'list';
  activeID = null;

  type = '';
  types = [
    {
      title: 'All',
      value: ''
    },
    {
      title: 'Active',
      value: 'active'
    },
    {
      title: 'Inactive',
      value: 'inactive'
    }
  ];

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private module: ModuleService,
    private components: ComponentsService,
    private utilities: UtilitiesService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
  }

  prepare() {
    this.router.events.subscribe((enter: NavigationEnd) => {
      if (enter instanceof NavigationEnd) {
        if (enter.url.indexOf(this.path) >= 0) {
          this.activeID = null;
          if (enter.url.includes('create')) {
            this.active = 'create';
          } else if (enter.url.includes('edit')) {
            const urlParts = enter.url.split('/');
            this.activeID = urlParts[2];
            this.active = 'edit';
          } else {
            this.active = 'list';
            if (enter.url.includes('/active')) {
              this.type = 'active';
            } else if (enter.url.includes('/inactive')) {
              this.type = 'inactive';
            } else {
              this.type = '';
            }
          }
        }
      }
    });
  }
  
  async openSort() {
    let modules = [];
    await this.module.list('', { by: 'order', direction: 'asc' }).then(async (response: any) => {
      let modulesBuilt = [];
      let modulesOfficial = [];
      const sortOfficial = async () => {
        for (let item of response) {
          if (this.parseInt(item.id) > 100) {
            await modulesBuilt.push(item);
          } else {
            await modulesOfficial.push(item);
          }
        }
      }
      await sortOfficial();
      const modulesOfficialSorted = await this.utilities.sortBy(modulesOfficial, 'id', 'number', 'asc');
      console.log(modulesOfficialSorted);
      modules = [...modulesBuilt, ...modulesOfficialSorted];
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', error.message);
    });
    if (modules.length) {
      this.dialogService.open(
        this.dialogSort, {
          context: modules
        }
      );
    }
  }

  async sort(event: CdkDragDrop<string[]>, modules) {
    if (parseInt(modules[event.currentIndex].id) > 100) {
      await moveItemInArray(modules, event.previousIndex, event.currentIndex);
      let errors = [];
      const sort = async () => {
        let position = 1;
        for (let module of modules) {
          await this.module.patch(module.id, { 'order': position.toString() }).catch(() => {
            errors.push(`${module.label} (ID: ${module.id})`);
          });
          position += 1;
        }
      }
      await sort();
      this.module.changeObservable.next(true);
      if (errors.length) {
        this.components.showToastStatus('danger', 'Error', 'Unsuccessfully sorted');
      }
    }
  }

  navigate(childURL = '') {
    if (this.active == 'create' || this.active == 'edit') {
      let context = '';
      if (this.active == 'create') {
        context = 'Do you want to exit create page?';
      } else if (this.active == 'edit') {
        context = 'Do you want to exit edit page?';
      }
      this.dialogService.open(
        this.dialog, {
          context: context
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

  onSelectStatus(event) {
    this.router.navigate([event == 'all' ? this.path : this.path + '/' + event]);
  }

}
