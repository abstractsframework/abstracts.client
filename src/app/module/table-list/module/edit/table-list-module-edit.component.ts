import { AfterViewInit, Component } from '@angular/core';
import { DefaultEditor } from 'ng2-smart-table';

import { ComponentsService } from '../../../../.services/components.service';
import { ModuleService } from '../../../../.services/module.service';

@Component({
  selector: 'abstract-table-list-module-edit',
  templateUrl: './table-list-module-edit.component.html',
  styleUrls: ['./table-list-module-edit.component.scss']
})
export class TableListModuleEditComponent extends DefaultEditor implements AfterViewInit {

  value: string;

  modules: any = [];

  constructor(
    private module: ModuleService,
    private components: ComponentsService
  ) {
    super();
  }

  ngAfterViewInit() {
    this.initialize();
  }

  async initialize() {
    const cellValue = this.cell.getValue();
    if (cellValue && cellValue !== '') {
      this.value = cellValue.id;
    }
    const sort = {
      by: 'id',
      direction: 'asc'
    }
    await this.module.list(null, sort).then(async (response: any) => {
      this.modules = response;
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', error.message);
    });
  }

  async update() {
    await this.modules.forEach(async (item: any) => {
      if (item.id == this.value) {
        this.cell.newValue = item;
      }
    });
  }

}
