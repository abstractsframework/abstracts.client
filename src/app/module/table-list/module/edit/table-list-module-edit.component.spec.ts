import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListModuleEditComponent } from './table-list-module-edit.component';

describe('TableListModuleEditComponent', () => {
  let component: TableListModuleEditComponent;
  let fixture: ComponentFixture<TableListModuleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListModuleEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListModuleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
