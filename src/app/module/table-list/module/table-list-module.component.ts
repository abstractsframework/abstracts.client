import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'abstract-table-list-module',
  templateUrl: './table-list-module.component.html',
  styleUrls: ['./table-list-module.component.scss']
})
export class TableListModuleComponent implements ViewCell, OnInit {

  @Input() value: any;
  @Input() rowData: any;

  renderValue: string;

  constructor() { }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.renderValue = `${this.value.name} (ID: ${this.value.id})`;
  }

}
