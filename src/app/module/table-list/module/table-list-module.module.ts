import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbSelectModule } from '@nebular/theme';

import { TableListModuleComponent } from './table-list-module.component';
import { TableListModuleEditComponent } from './edit/table-list-module-edit.component';

@NgModule({
  declarations: [
    TableListModuleEditComponent,
    TableListModuleComponent
  ],
  imports: [
    CommonModule,
    NbSelectModule
  ],
  exports: [
    TableListModuleEditComponent,
    TableListModuleComponent
  ]
})
export class TableListModuleModule { }
