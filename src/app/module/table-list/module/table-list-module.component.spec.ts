import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListModuleComponent } from './table-list-module.component';

describe('TableListModuleComponent', () => {
  let component: TableListModuleComponent;
  let fixture: ComponentFixture<TableListModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListModuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
