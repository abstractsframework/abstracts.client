import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleCreateComponent } from './module-create/module-create.component';
import { ModuleEditComponent } from './module-edit/module-edit.component';
import { ModuleTableComponent } from './module-table/module-table.component';
import { ModuleViewComponent } from './module-view/module-view.component';
import { ModuleComponent } from './module.component';

const routes: Routes = [
  {
    path: '',
    component: ModuleComponent,
    children: [
      {
        path: '',
        component: ModuleTableComponent
      },
      {
        path: 'active',
        component: ModuleTableComponent
      },
      {
        path: 'inactive',
        component: ModuleTableComponent
      },
      {
        path: 'create',
        component: ModuleCreateComponent
      },
      {
        path: ':id',
        component: ModuleViewComponent
      },
      {
        path: ':id/edit',
        component: ModuleEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleRoutingModule { }
