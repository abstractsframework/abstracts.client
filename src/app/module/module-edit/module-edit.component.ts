import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { ModuleService } from '../../.services/module.service';
import { NbDialogService } from '@nebular/theme';

import * as moment from 'moment';

import { PageService } from '../../.services/page.service';
import { ReferenceService } from '../../.services/reference.service';
import { AbstractsService } from '../../.services/abstract.service';
import { ThemeService } from '../../.services/theme.service';

@Component({
  selector: 'abstract-edit',
  templateUrl: './module-edit.component.html',
  styleUrls: ['./module-edit.component.scss']
})
export class ModuleEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;
  @ViewChild('dialogIcon') dialogIcon: TemplateRef<any>;
  @ViewChild('dialogSubjectIcon') dialogSubjectIcon: TemplateRef<any>;
  @ViewChild('dialogTemplateFiles') dialogTemplateFiles: TemplateRef<any>;
  @ViewChild('dialogServiceFiles') dialogServiceFiles: TemplateRef<any>;

  moment = moment;
  parseInt = parseInt;

  /* instances */
  path = '/module';
  formGroup: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  /* instances */
  data: any = null;
  abstract: any = [];
  references: any = [];
  defaultFieldFormArray: any = [];
  pages: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private theme: ThemeService,
    private components: ComponentsService,
    private module: ModuleService,
    private page: PageService,
    private abstracts: AbstractsService,
    private reference: ReferenceService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async prepare() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.data = state;
    }
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {
      if (!this.data || update) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          await this.module.get(id).then(async (response: any) => {
            this.data = response;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      }
      if (this.data) {
        await this.abstracts.getByKey(this.data.key).then(async (response: any) => {
          this.abstract = response[0];
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
        await this.reference.getByModuleID(this.data.id).then(async (response: any) => {
          let references = response;
          let referencesOfficialPre = [];
          let referencesOfficialPost = [];
          if (this.abstract) {
            if (this.abstract.component_page) {
              await referencesOfficialPre.push(
                {
                  active: '1',
                  label: 'Title',
                  key: 'title',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (this.abstract.component_language) {
              await referencesOfficialPre.push(
                {
                  active: '1',
                  label: 'Language',
                  key: 'language_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (this.abstract.component_user) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Creator',
                  key: 'user_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (this.abstract.component_group) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Group',
                  key: 'group_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (this.abstract.component_module) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Module',
                  key: 'module_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
          }
          this.references = [
            ...referencesOfficialPre, 
            ...references, 
            ...referencesOfficialPost
          ];
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
      await this.page.list().then(async (response: any) => {
        this.pages = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      await this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  async initializeForm() {
    if (this.data) {
      let defaultFieldFormArray = [];
      const formatDataTableFields = async (default_field, exists: boolean = false) => {
        for (let item of default_field) {
          await defaultFieldFormArray.push(
            new FormControl(
              {
                name: (exists) ? item.name : item.key,
                display: (exists) ? item.display : '1'
              }
            )
          );
        }
      }
      if (
        this.data.table_columns 
        && Array.isArray(this.data.table_columns)
        && this.data.table_columns.length
      ) {
        await formatDataTableFields(this.data.table_columns, true);
      } else {
        if (this.references) {
          await formatDataTableFields(this.references);
        } else {
          defaultFieldFormArray = [];
        }
      }
      this.defaultFieldFormArray = defaultFieldFormArray;
      this.formGroup = this.formBuilder.group({
        name: [
          (this.data.name) ? this.data.name : null, 
          Validators.required
        ],
        description: [
          (this.data.description) ? this.data.description : null
        ],
        link: [
          (this.data.link) ? this.data.link : null
        ],
        icon: [
          (this.data.icon) ? this.data.icon : null
        ],
        category: [
          (this.data.category) ? this.data.category : null
        ],
        subject: [
          (this.data.subject) ? this.data.subject : null
        ],
        subject_icon: [
          (this.data.subject_icon) ? this.data.subject_icon : null
        ],
        key: [
          (this.data.key) ? this.data.key : null
        ],
        database_table: [
          (this.data.database_table) ? this.data.database_table : null
        ],
        service: [
          (this.data.service) ? this.data.service : null
        ],
        page_template: [
          (this.data.page_template) ? this.data.page_template : null
        ],
        page_template_settings: [
          (this.data.page_template_settings) ? this.data.page_template_settings : null
        ],
        page_parent_link_key: [
          (this.data.page_parent_link_key) ? this.data.page_parent_link_key : ''
        ],
        individual_page_parent_link: [
          (this.data.individual_page_parent_link && this.data.individual_page_parent_link == '1') 
          ? true : false
        ],
        file_hash: [
          (this.data.file_hash && this.data.file_hash == '1') 
          ? true : false
        ],
        default_controls: this.formBuilder.array(
          (this.data.default_controls) ? this.data.default_controls : []
        ),
        default_field: this.formBuilder.array(this.defaultFieldFormArray),
        order: [
          (this.data.order) ? this.data.order : null
        ],
        page_id: [
          (this.data.page_id) ? this.data.page_id : null
        ],
        authentication: [
          (this.data.authentication && this.data.authentication == '1') 
          ? true : false
        ],
        active: [
          (this.data.active && this.data.active === true) 
          ? true : false
        ]
      });
    }
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  onDefaultControlChange(event, value) {
    const defaultControlsFormArray: FormArray = this.formGroup.get('default_controls') as FormArray;
    if (event.target.checked) {
      defaultControlsFormArray.push(new FormControl(value));
    } else {
      let index: number = 0;
      for (let control of defaultControlsFormArray.controls) {
        if (control.value == value) {
          defaultControlsFormArray.removeAt(index);
          return;
        }
        index += 1;
      }
    }
  }

  onDatabaseFieldChange(event, field) {
    const defaultFieldFormArray: FormArray = this.formGroup.get('default_field') as FormArray;
    const setDataTableFields = (field, value) => {
      let index: number = 0;
      for (let control of defaultFieldFormArray.controls) {
        if (control.value.name == field) {
          defaultFieldFormArray.at(index).patchValue(
            {
              name: field,
              display: value
            }
          );
          return;
        }
        index += 1;
      }
    }
    if (event.target.checked) {
      setDataTableFields(field, '1');
    } else {
      setDataTableFields(field, '0');
    }
  }

  checkDatabaseField(field) {
    const defaultFieldFormArray: FormArray = this.formGroup.get('default_field') as FormArray;
    for (let item of defaultFieldFormArray.controls) {
      if (item.value.name == field) {
        if (item.value.display == '1') {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  selectAllDatabaseField() {
    const defaultFieldFormArray: FormArray = this.formGroup.get('default_field') as FormArray;
    let index: number = 0;
    for (let control of defaultFieldFormArray.controls) {
      defaultFieldFormArray.at(index).patchValue(
        {
          name: control.value.name,
          display: '1'
        }
      );
      this.changeRef.detectChanges();
      index += 1;
    }
  }

  deselectAllDatabaseField() {
    const defaultFieldFormArray: FormArray = this.formGroup.get('default_field') as FormArray;
    let index: number = 0;
    for (let control of defaultFieldFormArray.controls) {
      defaultFieldFormArray.at(index).patchValue(
        {
          name: control.value.name,
          display: '0'
        }
      );
      this.changeRef.detectChanges();
      index += 1;
    }
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.initialize(true);
        this.module.changeObservable.next(true);
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const id = this.route.snapshot.paramMap.get('id');
      const params = {
        name: (value.name) ? value.name : '',
        link: (value.link) ? value.link : '',
        description: (value.description) ? value.description : '',
        icon: (value.icon) ? value.icon : '',
        category: (value.category) ? value.category : '',
        subject: (value.subject) ? value.subject : '',
        subject_icon: (value.subject_icon) ? value.subject_icon : '',
        key: (value.key) ? value.key : '',
        database_table: (value.database_table) ? value.database_table : '',
        service: (value.service) ? value.service : '',
        page_template: (value.page_template) ? value.page_template : '',
        page_template_settings: (value.page_template_settings) ? value.page_template_settings : '',
        page_parent_link_key: (value.page_parent_link_key) ? value.page_parent_link_key : '',
        individual_page_parent_link: (value.individual_page_parent_link) ? '1' : '0',
        file_hash: (value.file_hash) ? '1' : '0',
        default_controls: (value.default_controls) ? value.default_controls : [],
        table_columns: (value.default_field) ? value.default_field : [],
        authentication: (value.authentication) ? '1' : '0',
        page_id: (value.page_id) ? value.page_id : '0',
        order: (value.order) ? value.order : '',
        active: (value.active) ? '1' : '0',
      };
      this.module.update(id, params).then((response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  openIcon() {
    this.dialogService.open(
      this.dialogIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        icon: data
      });
    });
  }

  openSubjectIcon() {
    this.dialogService.open(
      this.dialogSubjectIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        subject_icon: data
      });
    });
  }

  openFilesService() {
    this.module.getServiceFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogServiceFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            service: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  openFilesTemplate() {
    this.module.getTemplateFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogTemplateFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            page_template: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}