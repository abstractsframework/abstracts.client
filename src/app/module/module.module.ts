import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  FormsModule, 
  ReactiveFormsModule 
} from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';

import { 
  NbCardModule, 
  NbActionsModule, 
  NbIconModule, 
  NbSelectModule, 
  NbInputModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbButtonModule, 
  NbBadgeModule, 
  NbListModule, 
  NbUserModule, 
  NbSpinnerModule 
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { TableListBehaviorModule } from '../control/table-list/behavior/table-list-behavior.module';
import { TableListRuleModule } from '../control/table-list/rule/table-list-rule.module';
import { TableListModuleModule } from './table-list/module/table-list-module.module';
import { MediaBrowserModule } from '../media-browser/media-browser.module';
import { IconModule } from '../icon/icon.module';

import { ModuleRoutingModule } from './module-routing.module';
import { ModuleComponent } from './module.component';
import { ModuleCreateComponent } from './module-create/module-create.component';
import { ModuleEditComponent } from './module-edit/module-edit.component';
import { ModuleViewComponent } from './module-view/module-view.component';
import { ModuleTableComponent } from './module-table/module-table.component';

@NgModule({
  declarations: [
    ModuleComponent,
    ModuleCreateComponent,
    ModuleEditComponent,
    ModuleViewComponent,
    ModuleTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    DragDropModule,
    TableListModuleModule,
    TableListBehaviorModule,
    TableListRuleModule,
    MediaBrowserModule,
    IconModule,
    ModuleRoutingModule
  ],
  exports: [
    ModuleTableComponent,
    ModuleCreateComponent,
    ModuleEditComponent,
    ModuleViewComponent
  ]
})
export class ModuleModule { }
