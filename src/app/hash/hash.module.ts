import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HashRoutingModule } from './hash-routing.module';
import { HashComponent } from './hash.component';
import { HashCreateComponent } from './hash-create/hash-create.component';
import { HashTableComponent } from './hash-table/hash-table.component';
import { HashViewComponent } from './hash-view/hash-view.component';


@NgModule({
  declarations: [
    HashComponent,
    HashCreateComponent,
    HashTableComponent,
    HashViewComponent
  ],
  imports: [
    CommonModule,
    HashRoutingModule
  ]
})
export class HashModule { }
