import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HashViewComponent } from './hash-view.component';

describe('HashViewComponent', () => {
  let component: HashViewComponent;
  let fixture: ComponentFixture<HashViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HashViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HashViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
