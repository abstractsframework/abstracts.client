import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NbAuthComponent } from '@nebular/auth';

import { RestService } from '../.services/rest.service';

import { LoginComponent } from './login.component';

const routes: Routes = [
  {
    path: '',
    component: NbAuthComponent,
    canActivate: [RestService],
    children: [
      {
        path: '',
        component: LoginComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
