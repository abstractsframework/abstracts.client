import { Component, OnInit } from '@angular/core';
import { 
  FormBuilder, 
  FormGroup, 
  Validators 
} from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../.services/user.service';

import { StorageService } from '../.services/storage.service';

import { KJUR } from 'jsrsasign';
import { AuthenticationService } from '../.services/authentication.service';

@Component({
  selector: 'abstract-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;

  submitted: boolean = false;
  message: any = null;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private auth: AuthenticationService,
    private user: UserService,
    private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.canActivate();
    this.initializeForm();
  }

  canActivate() {
    this.auth.validateLocalSession().then(() => {
    }).catch(() => {});
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      login: [
        null, 
        Validators.required
      ],
      password: [
        null, 
        Validators.required
      ],
      rememberLogin: [
        null
      ]
    });
  }

  submit() {
    this.message = null;
    this.submitted = true;
    if (this.formGroup.valid) {
      this.login(this.formGroup.value).then((response) => {
        this.message = {
          type: 'success',
          content: response
        }
        this.submitted = false;
        this.router.navigate(['./']);
      }).catch((error) => {
        this.message = {
          type: 'danger',
          content: error
        }
        this.submitted = false;
      });
    }
  }

  async login(value) {
    return new Promise(async (resolve, reject) => {
      this.user.login(
        value.login, 
        value.password
      ).then(async (response: any) => {
        if (response.token) {
          const token = response.token;
          const currentProject = parseInt(await this.storage.get('current-project'));
          const projects = await this.storage.get('projects', true, true);
          if (projects && projects.length && projects[currentProject]) {
            const encryption = projects[currentProject].encryption;
            if (encryption && encryption != '' && encryption.SSLPublicKey) {
              try {
                const isValid = await KJUR.jws.JWS.verify(
                  token, 
                  encryption.SSLPublicKey, 
                  { alg: encryption.SSLAlgorithm }
                );
                if (isValid) {
                  const decoded = await KJUR.jws.JWS.parse(token);
                  if (decoded && decoded.payloadObj) {
                    const session = {
                      id: decoded.payloadObj.id,
                      session_id: decoded.payloadObj.session_id,
                      token: token
                    }
                    await this.auth.emitSessionUpdate(session);
                    resolve('Successfully login');
                  } else {
                    reject('Account is invalid');
                  }
                } else {
                  reject('Unable to authorize token');
                }
              } catch (error) {
                reject(error);
              }
            } else {
              const decoded = atob(token).split('.');
              if (decoded[0] && decoded[1]) {
                const session = {
                  id: decoded[0],
                  session_id: decoded[1],
                  token: token
                }
                await this.auth.emitSessionUpdate(session);
                resolve('Successfully login');
              } else {
                reject('Unable to authorize token');
              }
            }
          } else {
            reject('Unable to authorize token');
          }
        } else if (response) {
          const session = {
            id: response.id,
            session_id: response.session_id
          }
          await this.auth.emitSessionUpdate(session);
          resolve(response.message);
        }
      }).catch(() => {
        reject(false);
      });
    });
  }

}
