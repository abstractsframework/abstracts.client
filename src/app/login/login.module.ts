import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  NbAlertModule,
  NbButtonModule, 
  NbCheckboxModule, 
  NbInputModule, 
  NbSpinnerModule
} from '@nebular/theme';

import { LoginRoutingModule } from './login-routing.module';

import { LoginComponent } from './login.component';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    NbAlertModule,
    NbSpinnerModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
