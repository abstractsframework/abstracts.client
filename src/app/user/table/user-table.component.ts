import { Component, Input, OnChanges, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../../.services/components.service';
import { UserService } from '../../.services/user.service';

import * as moment from 'moment';

@Component({
  selector: 'abstract-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, OnChanges {

  @ViewChild('dialog') dialog: TemplateRef<any>;
  @Input('type') type: any = '';

  settings = {
    mode: 'external',
    actions: {
      add: false
    },
    sort: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      image_path: {
        title: 'Image',
        type: 'html',
        valuePrepareFunction: (value) => {
          return value ? '<div class="text-center"><img class="table-image" src="' + value + '" /></div>' 
          : '<div class="text-center"><div class="table-image"></div></div>'
        }
      },
      name: {
        title: 'Name',
        type: 'html',
        valuePrepareFunction: (value, rowData) => {
          return `${value} ${rowData.last_name} ${rowData.nick_name ? ` (${rowData.nick_name})` : ''}`;
        }
      },
      username: {
        title: 'Username',
        type: 'string'
      },
      email: {
        title: 'Email',
        type: 'string'
      },
      phone: {
        title: 'Phone',
        type: 'string'
      },
      email_verified: {
        title: 'Email Verified',
        type: 'html',
        class: 'status',
        valuePrepareFunction: (value) => {
          return '<div class="text-center"><span class="badge table-badge ' + (value == '1' ? 'info' : 'disabled') + ' text-center">' 
          + (value == '1' ? 'Verified' : 'Unverified') 
          + '</span></div>';
        }
      },
      phone_verified: {
        title: 'Phone Verified',
        type: 'html',
        class: 'status',
        valuePrepareFunction: (value) => {
          return '<div class="text-center"><span class="badge ' + (value == '1' ? 'info' : 'disabled') + ' text-center">' 
          + (value == '1' ? 'Verified' : 'Unverified') 
          + '</span></div>';
        }
      },
      create_at: {
        title: 'Created',
        type: 'html',
        class: 'datetime',
        valuePrepareFunction: (value) => {
          return value 
          ? `<div class="text-center">${moment(value).format('L')}<br />${moment(value).format('LTS')}</div>` 
          : '-';
        }
      },
      active: {
        title: 'Status',
        type: 'html',
        class: 'status',
        valuePrepareFunction: (value) => {
          return '<div class="text-center"><span class="badge ' + (value == '1' ? 'success' : 'disabled') + ' text-center">' 
          + (value == '1' ? 'Active' : 'Inactive') 
          + '</span></div>';
        }
      }
    },
  };

  users: any = null;
  loaded: boolean = false;

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private user: UserService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.initialize(
      this.type == 'active' ? true
      : this.type == 'inactive' ? false
      : ''
    ).then(() => {
      this.loaded = true;
    });
  }

  initialize(active = null) {
		return new Promise<void>(async (resolve) => {
      await this.user.list(active).then((response: any) => {
        this.users = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      resolve();
    });
  }

  openEdit(event) {
    this.router.navigateByUrl(
      '/user/' + event.data.id + '/edit', 
      {
        state: event.data
      }
    );
  }

  delete(event, field = null) {
    let context = 'Do you want to delete' + ' ' + 'ID' + ':' + event.data.id + '?';
    if (field) {
      context = 'Do you want to delete' 
      + ' "' + event.data[field] + '" (' + 'ID' + ': ' + event.data.id + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe((data) => {
      if (data) {
        this.user.delete(event.data.id).then((response: any) => {
          this.initialize(
            this.type == 'active' ? true
            : this.type == 'inactive' ? false
            : ''
          ).then(() => {
            this.loaded = true;
          });
          this.components.showToastStatus('success', 'Success', 'Successfully deleted user');
          if (event.data.id == event.data.id) {
            console.log('logout');
          }
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

}
