import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { Ng2SmartTableComponent } from 'ng2-smart-table';
import { ComponentsService } from '../../.services/components.service';
import { ConnectService } from '../../.services/connect.service';
import { ControlService } from '../../.services/control.service';
import { DeviceService } from '../../.services/device.service';
import { GroupService } from '../../.services/group.service';
import { ModuleService } from '../../.services/module.service';
import { UserService } from '../../.services/user.service';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';

import * as moment from 'moment';
import { ThemeService } from '../../.services/theme.service';

@Component({
  selector: 'abstract-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;
  @ViewChild('dialogResetPassword') dialogResetPassword: TemplateRef<any>;
  @ViewChild('dialogChangePassword') dialogChangePassword: TemplateRef<any>;

  moment = moment;

  /* instances */
  path = '/user';
  formGroup: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;
  submittedGroupAdd: boolean = false;

  /* instances */
  data: any = null;
  groupSearch: string = '';

  controlsLoading: boolean = false;
  controlsSettings = {
    mode: 'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      module: {
        title: 'Module',
        type: 'custom',
        renderComponent: TableListModuleComponent,
        editor: {
          type: 'custom',
          component: TableListModuleEditComponent,
        }
      },
      rules: {
        title: 'Rules',
        type: 'custom',
        renderComponent: TableListRuleComponent,
        editor: {
          type: 'custom',
          component: TableListRuleEditComponent,
        }
      },
      behaviors: {
        title: 'Behaviors',
        type: 'custom',
        renderComponent: TableListBehaviorComponent,
        editor: {
          type: 'custom',
          component: TableListBehaviorEditComponent,
        }
      }
    },
  };

  imageOption: any = null;

  memberPageSize = 2;
  members: any = {
    data: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };
  groups: Array<any> = [];
  connects: Array<any> = [];
  devices: Array<any> = [];
  controls: Array<any> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private theme: ThemeService,
    private components: ComponentsService,
    private group: GroupService,
    private module: ModuleService,
    private user: UserService,
    private device: DeviceService,
    private connect: ConnectService,
    private control: ControlService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async prepare() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.data = await this.format(state);
    }
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {

      await this.loadMembers();
      await this.loadGroups();

      const inform = async (data) => {
        this.data = await this.format(data);
        this.imageOption = {
          label: 'Select Image',
          labelLoading: 'Loading...',
          statusFileType: 'Image not display',
          statusFileSize: 'File is too big',
          statusNoSupport: 'Your browser does not support image cropping',
          statusImageTooSmall: 'Image is too small',
          statusContentLength: 'File is probably too big',
          statusUnknownResponse: 'An unknown error occurred',
          statusUploadSuccess: 'Saved',
          buttonCancelLabel: 'Cancel',
          buttonConfirmLabel: 'Confirm',
          buttonRotateTitle: 'Rotate',
          download: false,
          push: true,
          instantEdit: true,
          devicePixelRatio: 'auto',
          initialImage: (data && data.image_path) 
            ? data.image_path
              : null,
          service: this.imageService.bind(this),
          didInit: this.initializeImage.bind(this),
          didRemove: this.removeImage.bind(this)
        }
      }

      if (!this.data || update) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          await this.user.get(id).then(async (response: any) => {
            await inform(response);
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      } else if (this.data) {
        await inform(this.data);
      }

      this.loadConnects();
      this.loadDevices();
      this.loadControls();
      this.initializeForm();
      this.changed = false;
    
      resolve();

    });
  }

  initializeForm() {
    if (this.data) {
      this.formGroup = this.formBuilder.group({
        name: [
          (this.data.name) ? this.data.name : null
        ],
        last_name: [
          (this.data.last_name) ? this.data.last_name : null
        ],
        nick_name: [
          (this.data.nick_name) ? this.data.nick_name : null
        ],
        username: [
          (this.data.username) ? this.data.username : null,
          Validators.required
        ],
        email: [
          (this.data.email) ? this.data.email : null
        ],
        phone: [
          (this.data.phone) ? this.data.phone : null
        ],
        email_verified: [
          (this.data.email_verified == '1') ? true : false
        ],
        phone_verified: [
          (this.data.phone_verified == '1') ? true : false
        ],
        face_verified: [
          (this.data.face_verified == '1') ? true : false
        ],
        ndid_verified: [
          (this.data.ndid_verified == '1') ? true : false
        ]
      });
      this.formGroup.valueChanges.subscribe(() => {
        this.changed = true;
      });
    }
  }

  async format(value) {
    if (
      value.controls 
      && value.controls != ''
      && value.controls.length
    ) {
      for (let i = 0; i < value.controls.length; i++) {
        if (
          value.controls[i].module 
          && value.controls[i].module != ''
          && value.controls[i].module.length
        ) {
          await this.module.get(value.controls[i].module).then(async (response: any) => {
            const module = {
              value: response.id,
              title: response.name + ' ' + '(ID: ' + response.id + ')'
            }
            value.controls[i].module = module;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
        if (
          value.controls[i].rules 
          && value.controls[i].rules != ''
          && value.controls[i].rules.length
        ) {
          if (!Array.isArray(value.controls[i].rules)) {
            value.controls[i].rules = value.controls[i].rules.split(',');
          }
        }
        if (
          value.controls[i].behaviors 
          && value.controls[i].behaviors != ''
          && value.controls[i].behaviors.length
        ) {
          let behaviors = value.controls[i].behaviors;
          if (!Array.isArray(value.controls[i].behaviors)) {
            behaviors = value.controls[i].behaviors.split(',');
          }
          const cellValue = {
            view: (behaviors.includes('view')) ? true : false,
            create: (behaviors.includes('create')) ? true : false,
            update: (behaviors.includes('update')) ? true : false,
            delete: (behaviors.includes('delete')) ? true : false,
          }
          value.controls[i].behaviors = cellValue;
        }
      }
    }
    return value;
  }

  async loadGroups() {
    await this.group.list().then(async (response: any) => {
      if (this.members.length) {
        await response.forEach(async (group, index) => {
          await this.members.forEach(async member => {
            if (group.id == member.id) {
              response[index].disabled = true;
            }
          });
        });
      }
      this.groups = response;
      this.changeRef.detectChanges();
    }).catch(() => {

    });
  }

  private initializeImage(data: any, slim: any) {
    
  };

  private async imageService(formdata: any, progress: any, success: any, failure: any, slim: any) {

    const progressScale = 1000;
    let elapsed = 0;
    
    await progress(elapsed, progressScale);
    let progressInterval = setInterval(async () => {
      elapsed += 1;
      await progress(elapsed, progressScale);
      if (elapsed == (progressScale * (90/100))) {
        await clearInterval(progressInterval);
      }
    }, 10);

    await formdata.append('id', this.data ? this.data.id : null);
    await formdata.append('active', '1');
    await formdata.append('target', 'output_image_' + slim._uid);

    if (this.data) {
      await formdata.append(
        'image_old', 
        (this.data.image_path) 
        ? this.data.image_path
          : ''
      );
    }

    if (elapsed < 200) {
      elapsed = 200;
    }

    await this.updateImage(formdata).then(async (responseUpdate: any) => {

      if (elapsed < 400) {
        elapsed = 400;
      }

      if (elapsed < 600) {
        elapsed = 600;
      }

      if (elapsed < 800) {
        elapsed = 800;
      }

      this.components.showToastStatus('success', 'Success', 'Successfully uploaded');
      slim._showButtons();

      await clearInterval(progressInterval);
      elapsed = 1000;
      await progress(elapsed, progressScale);

    }).catch(async error => {
      progress(0, progressScale);
      this.components.showToastStatus('danger', 'Error', error);
    });

  };

  private async updateImage(formData) {
    return await new Promise((resolve, reject) => {
      this.user.uploadFiles(formData).then((response: any) => {
        resolve(response.message);
      }).catch(errorMessage => {
        reject(errorMessage);
      });
    });
  }

  private async removeImage(data:any, slim:any) {

    let formdata = await new FormData();
  
    const id = this.route.snapshot.paramMap.get('id');
    await formdata.append('id', id);
    await formdata.append('active', '1');

    await formdata.append(
      'image_old', ''
    );
    
    await this.updateImage(formdata).catch(error => {
      this.components.showToastStatus('danger', 'Error', error);
    });
    
  }

  async loadMembers() {
    if (!this.members.loading) {

      this.members.loading = true;
      this.members.placeholders = new Array(this.memberPageSize);

      const id = this.route.snapshot.paramMap.get('id');
      const start = this.members.length;
      const limit = this.memberPageSize;
      await this.group.getMemberByUser(id, start, limit).then(async (response: any) => {
        if (response.length) {
          await response.forEach(async member => {
            this.members.placeholders = [];
            const param = {
              id: member.id,
              user: member.user,
              name: member.name + (member.last_name ? ' ' + member.last_name : ''),
              title: 'ID: ' + member.id
            }
            await this.members.data.push(param);
            this.members.loading = false;
            this.members.pageToLoadNext++;
          });
        }
      }).catch(() => {
  
      });

    }
  }

  async loadDevices() {
    const id = this.route.snapshot.paramMap.get('id');
    await this.device.getByUser(id).then(async (response: any) => {
      if (response.length) {
        this.devices = response;
      }
    }).catch(() => {

    });
  }

  async loadConnects() {
    const id = this.route.snapshot.paramMap.get('id');
    await this.connect.getByUser(id).then(async (response: any) => {
      if (response.length) {
        this.connects = response;
      }
    }).catch(() => {

    });
  }

  async loadControls() {
    this.controlsLoading = true;
    const id = this.route.snapshot.paramMap.get('id');
    await this.control.getByUser(id, ['module_id']).then(async (response: any) => {
      if (response.length) {
        this.controls = await this.formatControls(response);
      }
    }).catch(() => {

    });
    this.controlsLoading = false;
  }

  async formatControls(controls) {
    if (controls) {
      for (let i = 0; i < controls.length; i++) {
        if (controls[i].module_id) {
          await this.module.get(controls[i].module_id).then(async (response: any) => {
            const module = {
              value: response.id,
              title: response.name + ' ' + '(ID: ' + response.id + ')'
            }
            controls[i].module = module;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
        if (controls[i].rules) {
          if (!Array.isArray(controls[i].rules)) {
            controls[i].rules = controls[i].rules.split(',');
          }
        }
        if (controls[i].behaviors) {
          let behaviors = controls[i].behaviors;
          if (!Array.isArray(controls[i].behaviors)) {
            behaviors = controls[i].behaviors.split(',');
          }
          const cellValue = {
            view: (behaviors.includes('view')) ? true : false,
            create: (behaviors.includes('create')) ? true : false,
            update: (behaviors.includes('update')) ? true : false,
            delete: (behaviors.includes('delete')) ? true : false,
          }
          controls[i].behaviors = cellValue;
        }
      }
    }
    return controls;
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      const value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.initialize(true);
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const id = this.route.snapshot.paramMap.get('id');
      const params = {
        name: (value.name) ? value.name : '',
        last_name: (value.last_name) ? value.last_name : '',
        nick_name: (value.nick_name) ? value.nick_name : '',
        username: (value.username) ? value.username : '',
        email: (value.email) ? value.email : '',
        phone: (value.phone) ? value.phone : '',
        email_verified: (value.email_verified) ? '1' : '0',
        phone_verified: (value.phone_verified) ? '1' : '0',
        face_verified: (value.face_verified) ? '1' : '0',
        ndid_verified: (value.ndid_verified) ? '1' : '0',
        active: this.data.active,
      };
      
      const getParamsControl = async (controller: any, controlID: any = null) => {
        let behaviors = [];
        if (controller.behaviors.view) {
          await behaviors.push('view');
        }
        if (controller.behaviors.create) {
          await behaviors.push('create');
        }
        if (controller.behaviors.update) {
          await behaviors.push('update');
        }
        if (controller.behaviors.delete) {
          await behaviors.push('delete');
        }
        if (controlID) {
          return {
            id: controlID,
            user: id,
            rules: (controller.rules && controller.rules != '') 
            ? controller.rules.join(',') : "",
            behaviors: behaviors,
            group_id: controller.group_id ? controller.group_id : '',
            module_id: controller.module.value,
            active: controller.active
          }
        } else {
          return {
            user: id,
            rules: (controller.rules && controller.rules != '') 
            ? controller.rules.join(',') : "",
            behaviors: behaviors,
            group_id: controller.group_id ? controller.group_id : '',
            module_id: controller.module ? controller.module.value : '',
            active: controller.active
          }
        }
      }

      this.user.update(id, params).then((response: any) => {
        if (!this.controlsLoading) {
          this.control.getByUser(id).then(async (responseControl: any) => {
            let errors = [];
            if (responseControl && responseControl.length) {
              let controllers = [];
              if (this.controller) {
                await this.controller.source.list().then(async (data: any) => {
                  controllers = data;
                });
              }
              const controlSave = async () => {
                for (let controller of controllers) {
                  let exists = 0;
                  const controlCheck = async () => {
                    for (let check of responseControl) {
                      if (check.id === controller.id) {
                        exists += 1;
                        const paramsControl = await getParamsControl(controller, check.id);
                        await this.control.update(id, paramsControl).catch(async (error) => {
                          await errors.push(error);
                        });
                      }
                    }
                  }
                  await controlCheck();
                  if (!exists) {
                    const paramsControl = await getParamsControl(controller);
                    await this.control.create(paramsControl).catch(async (error) => {
                      await errors.push(error);
                    });
                  }
                }
              }
              await controlSave();
              const controlSync = async () => {
                for (let control of responseControl) {
                  let exists = 0;
                  const controlCheck = async () => {
                    for (let check of controllers) {
                      if (check.id === check.id) {
                        exists += 1;
                      }
                    }
                  }
                  await controlCheck();
                  if (!exists) {
                    await this.control.delete(control.id).catch(async (error) => {
                      await errors.push(error);
                    });
                  }
                }
              }
              await controlSync();
            }
            if (errors.length) {
              this.components.showToastStatus('danger', 'Error', 'Error on saving Controls');
            }
          }).catch((e) => {
            console.log(e);
            this.components.showToastStatus('danger', 'Error', 'Unable to save Controls');
          });
        } else {
          console.log(555);
          this.components.showToastStatus('danger', 'Error', 'Unable to save Controls');
        }
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  async submitCreateMember() {
    if (this.groupSearch) {
      this.submittedGroupAdd = true;
      const id = this.route.snapshot.paramMap.get('id');
      await this.group.getMemberByUser(id, '', '').then(async (response: any) => {
        let exists = 0;
        if (response.length) {
          await response.forEach(member => {
            if (member.group_id == this.groupSearch) {
              exists += 1;
            }
          });
        }
        if (!exists) {
          await this.addMember(this.groupSearch, id).then(async () => {
            await this.groups.forEach(async group => {
              if (group.id == this.groupSearch) {
                const param = {
                  id: group.id,
                  name: group.name,
                  title: 'ID: ' + group.id
                }
                await this.members.data.push(param);
                await this.initialize();
                this.groupSearch = '';
                this.submittedGroupAdd = false;
              }
            });
            this.components.showToastStatus('success', 'Success', 'Successfully added user');
          }).catch((error) => {
            this.submittedGroupAdd = false;
            this.components.showToastStatus('danger', 'Error', error);
          });
        } else {
          this.submittedGroupAdd = false;
          this.components.showToastStatus('warning', 'Duplicate', 'This user has already added');
        }
      }).catch((error) => {
        this.submittedGroupAdd = false;
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
  }

  addMember(id, userID) {
    return new Promise(async (resolve, reject) => {
      this.group.addMember(id, userID).then(async (response: any) => {
        await this.initialize(true);
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  removeMember(member) {
    const id = this.route.snapshot.paramMap.get('id');
    let context = 'Do you want to delete' + ' ' + 'ID' + ':' + member.id + '?';
    if (member.name) {
      context = 'Do you want to delete' 
      + ' "' + member.name + '" (' + 'ID' + ': ' + member.id + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe(async (data) => {
      if (data) {
        await this.group.removeMember(id, member.user).then(async (response: any) => {
          for(let i = 0; i < this.members.length; i++){ 
            if (this.members[i].id === member.id) { 
              await this.members.splice(i, 1); 
            }
          }
          await this.initialize();
          this.groupSearch = '';
          this.components.showToastStatus('success', 'Success', 'Successfully deleted member');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  deleteDevice(device) {
    let context = 'Do you want to delete device' + ' ' + device.platform + '?';
    if (device.name) {
      context = 'Do you want to delete device' 
      + ' "' + device.name + '" (' + device.platform + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe(async (data) => {
      if (data) {
        await this.device.delete(device.id).then(async (response: any) => {
          for(let i = 0; i < this.devices.length; i++){ 
            if (this.devices[i].id === device.id) { 
              await this.devices.splice(i, 1); 
            }
          }
          this.components.showToastStatus('success', 'Success', 'Successfully deleted device');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  deleteConnect(connect) {
    let context = 'Do you want to unlink connect from' + ' ' + connect.app + '?';
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe(async (data) => {
      if (data) {
        await this.connect.delete(connect.id).then(async (response: any) => {
          for(let i = 0; i < this.connects.length; i++){ 
            if (this.connects[i].id === connect.id) { 
              await this.connects.splice(i, 1); 
            }
          }
          this.components.showToastStatus('success', 'Success', 'Successfully unlinked connect');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  resetPassword() {
    if (this.data) {
      this.dialogService.open(
        this.dialogResetPassword, {
          context: 'Do you want to reset password for this user?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          if (this.data.email) {
            this.user.resetPasswordEmail(this.data.email).then(() => {
              this.components.showToastStatus('success', 'Success', 'Successfully reset');
            }).catch(errorMessage => {
              this.components.showToastStatus('danger', 'Error', errorMessage);
            });
          } else if (this.data.phone) {
            this.user.resetPasswordPhone(this.data.phone).then(() => {
              this.components.showToastStatus('success', 'Success', 'Successfully reset');
            }).catch(errorMessage => {
              this.components.showToastStatus('danger', 'Error', errorMessage);
            });
          } else {
            this.components.showToastStatus('danger', 'Error', 'User must have email or phone to reset password');
          }
        }
      });
    }
  }

  changePassword() {
    if (this.data) {
      this.dialogService.open(
        this.dialogChangePassword
      ).onClose.subscribe((data) => {
        if (data.submit) {
          this.user.changePassword(
            data.passwordCurrent,
            data.passwordNew,
            data.passwordConfirm
          ).then((response: any) => {
            this.components.showToastStatus('success', 'Success', 'Successfully reset');
          }).catch(errorMessage => {
            this.components.showToastStatus('danger', 'Error', errorMessage);
          });
        }
      });
    }
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}