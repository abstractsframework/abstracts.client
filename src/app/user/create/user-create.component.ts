import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../../.services/components.service';
import { UserService } from '../../.services/user.service';

import * as moment from 'moment';

@Component({
  selector: 'abstract-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {
  
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  moment = moment;

  /* instances */
  path = '/user';
  formGroup: FormGroup;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;
  submittedGroupAdd: boolean = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private user: UserService
  ) { }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      name: [
        null
      ],
      last_name: [
        null
      ],
      nick_name: [
        null
      ],
      username: [
        null,
        Validators.required
      ],
      password: [
        null,
        Validators.required
      ],
      passwordConfirm: [
        null,
        Validators.required
      ],
      email: [
        null
      ],
      phone: [
        null
      ]
    });
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      const value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.router.navigate([this.path]);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {

      const params = {
        name: (value.name) ? value.name : '',
        last_name: (value.last_name) ? value.last_name : '',
        nick_name: (value.nick_name) ? value.nick_name : '',
        username: (value.username) ? value.username : '',
        password: (value.password) ? value.password : '',
        confirm_password: (value.passwordConfirm) ? value.passwordConfirm : '',
        passcode: (value.passcode) ? value.passcode : '',
        email: (value.email) ? value.email : '',
        phone: (value.phone) ? value.phone : '',
        image: '',
        active: this.active,
      };

      this.user.create(params).then(async (response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });

    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}
