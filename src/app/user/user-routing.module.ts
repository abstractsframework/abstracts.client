import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCreateComponent } from './create/user-create.component';
import { UserEditComponent } from './edit/user-edit.component';
import { UserTableComponent } from './table/user-table.component';
import { UserComponent } from './user.component';
import { UserViewComponent } from './view/user-view.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: '',
        component: UserTableComponent
      },
      {
        path: 'active',
        component: UserTableComponent
      },
      {
        path: 'inactive',
        component: UserTableComponent
      },
      {
        path: 'create',
        component: UserCreateComponent
      },
      {
        path: ':id',
        component: UserViewComponent
      },
      {
        path: ':id/edit',
        component: UserEditComponent
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
