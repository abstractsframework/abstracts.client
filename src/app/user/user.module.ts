import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  NbActionsModule, 
  NbBadgeModule, 
  NbButtonModule, 
  NbCardModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbIconModule, 
  NbInputModule,
  NbListModule,
  NbSelectModule,
  NbSpinnerModule,
  NbUserModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableListModuleModule } from '../module/table-list/module/table-list-module.module';
import { TableListBehaviorModule } from '../control/table-list/behavior/table-list-behavior.module';
import { TableListRuleModule } from '../control/table-list/rule/table-list-rule.module';

import { SlimModule } from '../.directives/slim/slim.module';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserCreateComponent } from './create/user-create.component';
import { UserEditComponent } from './edit/user-edit.component';
import { UserViewComponent } from './view/user-view.component';
import { UserTableComponent } from './table/user-table.component';


@NgModule({
  declarations: [
    UserComponent,
    UserCreateComponent,
    UserEditComponent,
    UserViewComponent,
    UserTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    TableListModuleModule,
    TableListBehaviorModule,
    TableListRuleModule,
    UserRoutingModule,
    SlimModule,
  ],
  exports: [
    UserTableComponent,
    UserCreateComponent,
    UserEditComponent,
    UserViewComponent
  ]
})
export class UserModule { }
