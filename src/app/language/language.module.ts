import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LanguageRoutingModule } from './language-routing.module';
import { LanguageComponent } from './language.component';
import { LanguageCreateComponent } from './language-create/language-create.component';
import { LanguageEditComponent } from './language-edit/language-edit.component';
import { LanguageTableComponent } from './language-table/language-table.component';
import { LanguageViewComponent } from './language-view/language-view.component';


@NgModule({
  declarations: [
    LanguageComponent,
    LanguageCreateComponent,
    LanguageEditComponent,
    LanguageTableComponent,
    LanguageViewComponent
  ],
  imports: [
    CommonModule,
    LanguageRoutingModule
  ]
})
export class LanguageModule { }
