import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-language-view',
  templateUrl: './language-view.component.html',
  styleUrls: ['./language-view.component.scss']
})
export class LanguageViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
