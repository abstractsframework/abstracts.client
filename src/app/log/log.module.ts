import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogRoutingModule } from './log-routing.module';
import { LogComponent } from './log.component';
import { LogTableComponent } from './log-table/log-table.component';
import { LogViewComponent } from './log-view/log-view.component';


@NgModule({
  declarations: [
    LogComponent,
    LogTableComponent,
    LogViewComponent
  ],
  imports: [
    CommonModule,
    LogRoutingModule
  ]
})
export class LogModule { }
