import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentComponent } from './document.component';
import { DocumentViewComponent } from './view/document-view.component';
import { PageRoutingModule } from './document-routing.module';
import { NbAccordionModule, NbButtonModule, NbCardModule, NbListModule } from '@nebular/theme';



@NgModule({
  declarations: [
    DocumentComponent,
    DocumentViewComponent
  ],
  imports: [
    CommonModule,
    NbListModule,
    NbAccordionModule,
    NbCardModule,
    NbButtonModule,
    PageRoutingModule
  ],
  exports: [
    DocumentViewComponent
  ]
})
export class DocumentModule { }
