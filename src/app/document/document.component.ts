import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { AbstractsService } from '../.services/abstract.service';
import { ReferenceService } from '../.services/reference.service';
import { BuiltService } from '../.services/built.service';
import { ComponentsService } from '../.services/components.service';
import { ModuleService } from '../.services/module.service';

@Component({
  selector: 'abstract-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  path = 'document/';

  modules = [];
  data = null;
  builtData: any = null;
  initialized = false;

  parameterHeaderAccess = JSON.stringify({ TOKEN: '[TOKEN]' }, null, 2);
  parameterHeaderAuthorization = JSON.stringify({ Authorization: 'Bearer [LOGIN TOKEN]' }, null, 2);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private components: ComponentsService,
    private abstracts: AbstractsService,
    private reference: ReferenceService,
    private module: ModuleService,
    private built: BuiltService
  ) { }

  ngOnInit(): void {
    this.initialize();
  }

  async initialize() {
    await this.module.list('', { by: 'name', 'direction': 'asc' }).then((response: any) => {
      this.modules = response;
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', error.message);
    });
  }

  navigate(childURL = '') {
    this.router.navigate([this.path + childURL]);
  }

}
