import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentViewComponent } from './view/document-view.component';
import { DocumentComponent } from './document.component';

const routes: Routes = [
  {
    path: '',
    component: DocumentComponent
  },
  {
    path: ':modulePath',
    component: DocumentViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
