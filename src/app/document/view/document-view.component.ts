import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { AbstractsService } from '../../.services/abstract.service';
import { ReferenceService } from '../../.services/reference.service';
import { BuiltService } from '../../.services/built.service';
import { ComponentsService } from '../../.services/components.service';
import { ModuleService } from '../../.services/module.service';
import { StorageService } from '../../.services/storage.service';

@Component({
  selector: 'abstract-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.scss']
})
export class DocumentViewComponent implements OnInit {

  path = 'document/';

  modules = [];
  data = null;
  builtData: any = null;
  initialized = false;

  inputTypes: any = {
    "input-text": "string",
    "textarea": "string",
    "text-editor": "string",
    "input-number": "number",
    "input-decimal": "number",
    "input-password": "string",
    "select": "string",
    "select-select2": "string",
    "select-multiple": "string",
    "select-multiple-select2": "string",
    "radio": "string",
    "radio-inline": "string",
    "checkbox": "string",
    "checkbox-inline": "string",
    "checkbox-radio": "string",
    "switch": "string",
    "input-file": "File",
    "input-file-multiple": "Files",
    "input-file-multiple-drop": "Files",
    "file-selector": "string",
    "image-upload": "Files",
    "input-date": "date",
    "input-datetime": "datetime",
    "input-time": "date",
    "input-tags": "Array",
    "input-color": "string",
    "range-slider": "string",
    "range-slider-double": "string:string",
    "input-multiple": "Array"
  };

  functions = [];

  parameterExampleFilters = {
    user_id: "1",
    active: "1"
  }
  parameterExampleFiltersString = JSON.stringify(this.parameterExampleFilters, null, 2);
  
  parameterExampleExtendedCommand = [
    {
      conjunction: "",
      key: "`name`",
      operator: "LIKE",
      value: "'%Creator%'"
    },
    {
      conjunction: "OR",
      key: "`last_name`",
      operator: "LIKE",
      value: "'%Creator%'"
    }
  ]
  parameterExampleExtendedCommandString = JSON.stringify(this.parameterExampleExtendedCommand, null, 2);
  
  projects: any = [];
  currentProject = null;
  server = '';

  modulesOfficial = [
    'user'
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private components: ComponentsService,
    private storage: StorageService,
    private abstracts: AbstractsService,
    private reference: ReferenceService,
    private module: ModuleService,
    private built: BuiltService
  ) { }

  ngOnInit(): void {
    this.initialize();
  }

  async initialize() {

    await this.loadProject();

    this.data = null;
    this.builtData = null;
    this.initialized = false;
    this.changeRef.detectChanges();
    const modulePath = this.route.snapshot.paramMap.get('modulePath');
    if (modulePath) {
      const key = modulePath.replace(/-/g, '_');
      let moduleData: any = {};
      await this.abstracts.getByKey(key).then(async (response: any) => {
        let abstract: any = null;
        if (
          (response && response.length)
          || this.modulesOfficial.includes(modulePath)
        ) {
          let abstractID = null;
          if (response && response.length) {
            abstract = response[0];
            abstractID = abstract.id;
          } else {
            if (modulePath == 'user') {
              abstractID = '6';
            }
          }
          if (abstractID) {
            await this.module.get(abstractID).then(async (responseModule: any) => {
              if (responseModule) {
                moduleData = responseModule;
              }
              if (abstract) {
                moduleData.abstract = abstract;
                moduleData.abstract.references = [];
              } else {
                if (abstractID == '6') {
                  abstract = {
                    id: abstractID,
                    name: moduleData.name,
                    key: moduleData.key,
                    description: moduleData.description,
                    form_method: 'post',
                    component_module: '',
                    component_group: '',
                    component_user: '1',
                    component_language: '',
                    component_page: '',
                    component_media: '',
                    component_commerce: '',
                    data_sortable: '',
                    database_engine: 'MyISAM',
                    database_collation: 'utf8mb4_general_ci',
                    template: moduleData.page_template,
                    icon: moduleData.icon,
                    category: moduleData.category,
                    subject: moduleData.subject,
                    subject_icon: moduleData.subject_icon,
                    order: moduleData.order,
                    active: moduleData.active
                  }
                }
                if (abstract) {
                  moduleData.abstract = abstract;
                  moduleData.abstract.references = [];
                }
              }
              let references = [];
              let referencesOfficialPre = [];
              let referencesOfficialPost = [];
              if (abstract.component_page) {
                await referencesOfficialPre.push(
                  {
                    active: '1',
                    label: 'Title',
                    key: 'title',
                    type: 'input-text',
                    pseudo: true
                  }
                );
              }
              if (abstract.component_language) {
                await referencesOfficialPre.push(
                  {
                    active: '1',
                    label: 'Language',
                    key: 'language_id',
                    type: 'input-text',
                    pseudo: true
                  }
                );
              }
              await this.reference.getByModuleID(abstractID).then(async (responseReference: any) => {
                if (responseReference && responseReference.length) {
                  references = responseReference;
                  const getMultiples = async () => {
                    let referenceIndex = 0;
                    for (let reference of references) {
                      references[referenceIndex].stacks = [];
                      references[referenceIndex].values = [];
                      references[referenceIndex].options = [];
                      if (reference.default_value) {
                        references[referenceIndex].values = reference.default_value;
                      }
                      if (reference.input_option == 'dynamic') {
                        await this.built.getInputListByKey(
                          moduleData.service, 
                          moduleData.key, 
                          reference.key
                        ).then(async (responseInputList: any) => {
                          if (responseInputList) {
                            references[referenceIndex].options = responseInputList;
                          }
                        }).catch((error) => {
                          console.log(error);
                        });
                      } else {
                        if (reference.input_option_static_value) {
                          references[referenceIndex].options = reference.input_option_static_value;
                        }
                      }
                      if (reference.type == 'input-multiple') {
                        await this.reference.listMulti(reference.id).then(async (responseMultiple: any) => {
                          if (responseMultiple) {
                            references[referenceIndex].stacks = responseMultiple;
                            let stackIndex = 0;
                            for (let stack of responseMultiple) {
                              references[referenceIndex].stacks[stackIndex].stacks = [];
                              references[referenceIndex].stacks[stackIndex].values = [];
                              references[referenceIndex].stacks[stackIndex].options = [];
                              if (stack.default_value) {
                                references[referenceIndex].stacks[stackIndex].values = stack.default_value;
                              } else {
                                references[referenceIndex].stacks[stackIndex].values = [];
                              }
                              if (stack.input_option == 'dynamic') {
                                await this.built.getInputListByKey(
                                  moduleData.service, 
                                  moduleData.key, 
                                  stack.key
                                ).then(async (responseInputList: any) => {
                                  if (responseInputList) {
                                    references[referenceIndex].stacks[stackIndex].options = responseInputList;
                                  }
                                });
                              } else {
                                if (stack.input_option_static_value) {
                                  references[referenceIndex].stacks[stackIndex].options = stack.input_option_static_value;
                                } else {
                                  references[referenceIndex].stacks[stackIndex].options = [];
                                }
                              }
                              stackIndex += 1;
                            }
                          }
                        });
                      }
                      referenceIndex += 1;
                    }
                  }
                  await getMultiples();
                } else {
                  if (abstractID == '6') {
                    references = [
                      {
                        active: '1',
                        label: 'Username',
                        key: 'username',
                        type: 'input-text',
                        require: true,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Password',
                        key: 'password',
                        type: 'input-password',
                        require: true,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Passcode',
                        key: 'passcode',
                        type: 'input-password',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Email',
                        key: 'email',
                        type: 'input-text',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Phone',
                        key: 'phone',
                        type: 'input-text',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Name',
                        key: 'name',
                        type: 'input-text',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Last Name',
                        key: 'last_name',
                        type: 'input-text',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Nick Name',
                        key: 'nick_name',
                        type: 'input-text',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Image',
                        key: 'image',
                        type: 'image-upload',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Verified by Email',
                        key: 'email_verified',
                        type: 'switch',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Verified by Phone',
                        key: 'phone_verified',
                        type: 'switch',
                        require: false,
                        pseudo: true
                      },
                      {
                        active: '1',
                        label: 'Verified by National Digital ID',
                        key: 'ndid_verified',
                        type: 'switch',
                        require: false,
                        pseudo: true
                      }
                    ];
                  }
                }
                await referencesOfficialPost.push(
                  {
                    active: '1',
                    label: 'Created Date',
                    key: 'create_at',
                    type: 'input-datetime',
                    pseudo: true
                  }
                );
                if (abstract.component_user) {
                  await referencesOfficialPost.push(
                    {
                      active: '1',
                      label: 'Creator',
                      key: 'user_id',
                      type: 'input-text',
                      pseudo: true
                    }
                  );
                }
                if (abstract.component_group) {
                  await referencesOfficialPost.push(
                    {
                      active: '1',
                      label: 'Group',
                      key: 'group_id',
                      type: 'input-text',
                      pseudo: true
                    }
                  );
                }
                if (abstract.component_module) {
                  await referencesOfficialPost.push(
                    {
                      active: '1',
                      label: 'Module',
                      key: 'module_id',
                      type: 'input-text',
                      pseudo: true
                    }
                  );
                }
                moduleData.abstract.references = [
                  ...referencesOfficialPre, 
                  ...references, 
                  ...referencesOfficialPost
                ];
              }).catch((error) => {
                this.components.showToastStatus('danger', 'Error', error);
              });
              this.data = moduleData;
              this.format();
            }).catch((error) => {
              this.components.showToastStatus('danger', 'Error', error.message);
            });
          } else {
            this.components.showToastStatus('danger', 'Error', 'Module not found');
          }
        } else {
          this.components.showToastStatus('danger', 'Error', 'Module not found');
        }
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
    this.initialized = true;
    this.changeRef.detectChanges();
  }

  async format() {
    if (this.data) {

      this.functions = [];

      if (this.data.id == '6') {
        
      }

      let responseGet: any = {
        id: '[string]'
      };
      let parametersCreateValues: any = {};
      let parametersUpdateValues: any = {
        id: '[string]'
      };
      const formatGet = async () => {
        if (this.data.abstract && this.data.abstract && this.data.abstract.references) {
          for (let reference of this.data.abstract.references) {
            responseGet[reference.key] = `[${this.inputTypes[reference.type]}]`;
            parametersCreateValues[reference.key] = `[${this.inputTypes[reference.type]}]`;
            parametersUpdateValues[reference.key] = `[${this.inputTypes[reference.type]}]`;
          }
        }
      }
      await formatGet();
      responseGet.active = '[1|0]';
      responseGet.create_at = '[date]';
      parametersCreateValues.active = '[1|0]';
      parametersUpdateValues.active = '[1|0]';

      let parametersGet = [
        {
          name: 'start',
          types: ['number', 'string'],
          values: [],
          require: false,
          description: 'Index of record to start with',
        },
        {
          name: 'limit',
          types: ['number', 'string'],
          values: [],
          require: false,
          description: 'Number of records to select',
        },
        {
          name: 'active',
          types: ['number', 'string', 'boolean'],
          values: ['1', '0', 'true', 'false'],
          require: false,
          description: 'Status of record to get (set to NULL to get both)',
        },
        {
          name: 'sort_by',
          types: ['string'],
          values: [],
          require: false,
          description: 'Reference key to sort records',
        },
        {
          name: 'sort_direction',
          types: ['string'],
          values: ['asc', 'desc'],
          require: false,
          description: 'Direction to sort records',
        },
        {
          name: 'filters',
          types: ['filters'],
          values: [],
          require: false,
          description: 'Filter records by reference key',
        },
        {
          name: 'extended_command',
          types: ['extended_command'],
          values: [],
          require: false,
          description: 'Add extended command to select records',
        }
      ];

      let parametersSubmit = (this.data.abstract && this.data.abstract && this.data.abstract.references)
      ? this.data.abstract.references.map(
        ({key, type, default_value, require, placeholder}) => (
          {
            name: key, 
            types: [this.inputTypes[type]], 
            values: default_value ? default_value : [],
            require: true,
            description: placeholder
          }
        )
      ) : [];
      await parametersSubmit.push(
        {
          name: 'active',
          types: ['number', 'string', 'boolean'],
          values: ['1', '0', 'true', 'false'],
          require: true,
          description: '',
        }
      );
      await delete parametersSubmit.create_at;
      await delete parametersSubmit.user_id;

      this.functions = [
        {
          name: 'create_VARIABLE',
          methods: ['post'],
          controls: ['create'],
          parameters: parametersSubmit,
          parametersType: 'JSON',
          parametersValues: JSON.stringify(parametersCreateValues, null, 2),
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: { id: '[string]' }
          }, null, 2),
          description: ''
        },
        {
          name: 'update_VARIABLE',
          methods: ['post'],
          controls: ['update'],
          parameters: [
            {
            name: 'id',
            types: ['number', 'string'],
            values: [],
            require: true,
            description: 'Add extended command to select records',
            }, ...parametersSubmit
          ],
          parametersValues: JSON.stringify(parametersUpdateValues, null, 2),
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: {}
          }, null, 2),
          description: ''
        },
        {
          name: 'patch_VARIABLE',
          methods: ['post'],
          controls: ['update'],
          parameters: [
            {
              name: 'id',
              types: ['number', 'string'],
              values: [],
              require: false,
              description: 'ID of record',
            },
            {
              name: 'key',
              types: ['string'],
              values: [],
              require: true,
              description: 'Reference key to patch',
            }, {
              name: 'value',
              types: ['any'],
              values: [],
              require: true,
              description: 'Value to patch',
            }
          ],
          parametersType: 'JSON',
          parametersValues: JSON.stringify({ id: '[number|string]', key: '[string]', value: '[string]' }, null, 2),
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: {}
          }, null, 2),
          description: ''
        },
        {
          name: 'patch_VARIABLE_multiples',
          methods: ['post'],
          controls: ['update'],
          parameters: [
            {
              name: 'id',
              types: ['number', 'string'],
              values: [],
              require: false,
              description: 'ID of record',
            }, 
            {
              name: 'fields',
              types: ['Array'],
              values: ['[ { key: [string], value: [any] } ]'],
              require: true,
              description: 'Fields of reference key and value to patch',
            }
          ],
          parametersType: 'JSON',
          parametersValues: JSON.stringify({ id: '[number|string]', fields: [{key: '[string]', value: '[string]'}] }, null, 2),
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: {}
          }, null, 2),
          description: ''
        },
        {
          name: 'delete_VARIABLE',
          methods: ['post'],
          controls: ['delete'],
          parameters: [{
            name: 'id',
            types: ['number', 'string'],
            values: [],
            require: true,
            description: 'Add extended command to select records',
          }],
          parametersValues: JSON.stringify({ id: '[number|string]' }, null, 2),
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: {}
          }, null, 2),
          description: ''
        },
        {
          name: 'get_VARIABLE_by_id',
          methods: ['get', 'post'],
          controls: ['view'],
          parameters: [
            {
              name: 'id',
              types: ['number', 'string'],
              values: [],
              require: false,
              description: 'ID of record',
            },
            {
              name: 'active',
              types: ['number', 'string', 'boolean'],
              values: ['1', '0', 'true', 'false'],
              require: false,
              description: 'Status of record to get (set to NULL to get both)',
            },
          ],
          parametersType: 'JSON',
          parametersValues: '',
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: responseGet
          }, null, 2),
          description: ''
        },
        {
          name: 'get_VARIABLE_all',
          methods: ['get', 'post'],
          controls: ['view'],
          parameters: parametersGet,
          parametersType: 'JSON',
          parametersValues: '',
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: responseGet
          }, null, 2),
          description: ''
        },
        {
          name: 'count_VARIABLE_all',
          methods: ['get', 'post'],
          controls: ['view'],
          parameters: parametersGet,
          parametersType: 'JSON',
          parametersValues: '',
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: '[number]'
          }, null, 2),
          description: ''
        },
        {
          name: 'get_VARIABLE_all_excluding',
          methods: ['get', 'post'],
          controls: ['view'],
          parameters: [
            {
              name: 'id',
              types: ['number', 'string'],
              values: [],
              require: false,
              description: 'Excluded ID of record',
            }, ...parametersGet
          ],
          parametersType: 'JSON',
          parametersValues: '',
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: responseGet
          }, null, 2),
          description: ''
        },
        {
          name: 'count_VARIABLE_all_excluding',
          methods: ['get', 'post'],
          controls: ['view'],
          parameters: [
            {
              name: 'id',
              types: ['number', 'string'],
              values: [],
              require: false,
              description: 'Excluded ID of record',
            }, ...parametersGet
          ],
          parametersType: 'JSON',
          parametersValues: '',
          response: JSON.stringify({ 
            status: '[boolean]',
            message: '[string]',
            data: '[number]'
          }, null, 2),
          description: ''
        },
      ]

      if (this.data.abstract && this.data.abstract.component_user == '1') {
        this.functions = [
          ...this.functions,
          {
            name: 'get_VARIABLE_all_by_user_id',
            methods: ['get', 'post'],
            controls: ['view'],
            parameters: [
              {
                name: 'user_id',
                types: ['number', 'string'],
                values: [],
                require: false,
                description: 'User ID',
              }, ...parametersGet
            ],
            parametersType: 'JSON',
            parametersValues: '',
            response: JSON.stringify({ 
              status: '[boolean]',
              message: '[string]',
              data: responseGet
            }, null, 2),
            description: ''
          },
          {
            name: 'count_VARIABLE_all_by_user_id',
            methods: ['get', 'post'],
            controls: ['view'],
            parameters: [
              {
                name: 'user_id',
                types: ['number', 'string'],
                values: [],
                require: false,
                description: 'User ID',
              }, ...parametersGet
            ],
            parametersType: 'JSON',
            parametersValues: '',
            response: JSON.stringify({ 
              status: '[boolean]',
              message: '[string]',
              data: '[number]'
            }, null, 2),
            description: ''
          }
        ];
      }

      if (this.data.id == '6') {
        this.functions = [
          {
            name: 'login',
            methods: ['get', 'post'],
            controls: [],
            parameters: [
              {
                name: 'username',
                types: ['string'],
                values: [],
                require: true,
                description: '',
              },
              {
                name: 'password',
                types: ['string'],
                values: [],
                require: true,
                description: '',
              }
            ],
            parametersType: 'JSON|Headers',
            parametersValues: 'Authorization: Basic <Base64([username]:[password])>',
            response: JSON.stringify({ 
              status: '[boolean]',
              message: '[string]',
              data: {
                id: '[string]',
                username: '[string]',
                email: '[string]',
                name: '[string]',
                last_name: '[string]',
                nick_name: '[string]',
                image: '[string]',
                image_thumbnail_path: '[string]',
                image_large_thumbnail_path: '[string]',
                phone: '[string]',
                email_verified: '[string]',
                phone_verified: '[string]',
                ndid_verified: '[string]',
                password_set: '[boolean]',
                passcode_set: '[boolean]',
                create_at: '[datetime]',
                active: '[string]',
                user_id: '[string]',
              },
              token: '[string]'
            }, null, 2),
            description: ''
          },
          ...this.functions
        ];
      }

      let hasUpload = [];
      const formatReferences = async () => {
        if (this.data.abstract && this.data.abstract && this.data.abstract.references) {
          for (let reference of this.data.abstract.references) {
            if (reference.input_option == 'dynamic') {
              await this.functions.push(
                {
                  name: 'get_VARIABLE_' + reference.input_option_dynamic_module + '_by_' + reference.key,
                  methods: ['post'],
                  controls: ['create', 'update', 'delete'],
                  parameters: [
                    {
                      name: reference.key,
                      types: ['any'],
                      values: [],
                      require: true,
                      description: 'Target value of records',
                    }, 
                    {
                      name: 'active',
                      types: ['number', 'string', 'boolean'],
                      values: ['1', '0', 'true', 'false'],
                      require: false,
                      description: 'Status of record to get (set to NULL to get both)',
                    },
                  ],
                  parametersType: 'JSON',
                  parametersValues: '',
                  response: JSON.stringify({ 
                    status: '[boolean]',
                    message: '[string]',
                    data: '[' + reference.input_option_dynamic_module + ']'
                  }, null, 2),
                  description: ''
                }
              );
              await this.functions.push(
                {
                  name: 'get_VARIABLE_' + reference.key + '_all',
                  methods: ['post'],
                  controls: ['create', 'update'],
                  parameters: parametersGet,
                  parametersType: 'JSON',
                  parametersValues: '',
                  response: JSON.stringify({ 
                    status: '[boolean]',
                    message: '[string]',
                    data: '[Array<' + reference.input_option_dynamic_module + '>]'
                  }, null, 2),
                  description: ''
                }
              );
              await this.functions.push(
                {
                  name: 'count_VARIABLE_' + reference.key + '_all',
                  methods: ['post'],
                  controls: ['create', 'update'],
                  parameters: parametersGet,
                  parametersType: 'JSON',
                  parametersValues: '',
                  response: JSON.stringify({ 
                    status: '[boolean]',
                    message: '[string]',
                    data: '[number]'
                  }, null, 2),
                  description: ''
                }
              );
            }
            if (
              reference.type == 'input-file'
              || reference.type == 'input-file-multiple'
              || reference.type == 'input-file-multiple-drop'
              || reference.type == 'image-upload'
            ) {
              await this.functions.push(
                {
                  name: 'remove_VARIABLE_' + reference.key + '_file',
                  methods: ['post'],
                  controls: ['create', 'update', 'delete'],
                  parameters: [
                    {
                      name: 'id',
                      types: ['number', 'string'],
                      values: [],
                      require: true,
                      description: 'ID of records',
                    }, 
                    {
                      name: 'file',
                      types: ['string'],
                      values: [],
                      require: true,
                      description: 'File path',
                    }, 
                  ],
                  parametersType: 'JSON',
                  parametersValues: JSON.stringify({ id: '[number|string]', file: '[string]' }, null, 2),
                  response: JSON.stringify({ 
                    status: '[boolean]',
                    message: '[string]',
                    data: {}
                  }, null, 2),
                  description: ''
                }
              );
              hasUpload.push({
                name: reference.key,
                types: ['Files|blob'],
                values: [],
                require: false,
                description: 'Binary file(s) to upload',
              });
              hasUpload.push({
                name: `${reference.key}_remove`,
                types: ['string'],
                values: [],
                require: false,
                description: 'File path to remove',
              });
            }
          }
        }
      }
      await formatReferences();
      if (hasUpload.length) {
        await this.functions.push(
          {
            name: 'upload_VARIABLE_files',
            methods: ['post'],
            controls: ['update'],
            parameters: [
              {
                name: 'id',
                types: ['number', 'string'],
                values: [],
                require: true,
                description: 'ID of records',
              }, 
              ...hasUpload
            ],
            parametersType: 'FormData',
            parametersValues: 
            `id: [number|string]\n${hasUpload[0].name}: [binary|blob]\n${hasUpload[0].name}_remove: [string]`,
            response: JSON.stringify({ 
              status: '[boolean]',
              message: '[string]',
              data: {}
            }, null, 2),
            description: ''
          }
        );
      }

    }
  }

  getServiceAlias(path) {
    return path ? path.replace('.php', '') : '';
  }

  async loadProject() {
    const projects: any = await this.storage.get('projects', true, true);
    if (projects && projects.length) {
      this.projects = projects;
    }
    const currentProject = await this.storage.get('current-project');
    if (currentProject) {
      this.currentProject = parseInt(currentProject);
    }
    if (this.projects && this.projects.length) {
      if (this.currentProject) {
        this.server = this.projects[this.currentProject].host;
      }
    }
  }

  inform(name) {
    let functionName = name;
    if (this.data) {
      functionName = name.replace('VARIABLE', this.data.key);
    }
    return functionName;
  }

  scrollTo(hash: string) {
    if (hash) {
      setTimeout(() => {
        const scroll = document.getElementsByClassName('scrollable-container') as any;
        const element = document.getElementsByClassName(hash.replace('#', '')) as any;
        if (scroll && scroll.length && element && element.length) {
          console.log(element[element.length - 1].offsetTop);
          scroll[0].scroll({
            top: element[element.length - 1].offsetTop + 150,
            left: 0,
            behavior: 'smooth'
          });
        }
      }, 120);
    }
  }

  navigate(childURL = '') {
    this.router.navigate([this.path + childURL]);
  }

}
