import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  resetScrollObservable = new Subject<any>();
  onResetScroll = this.resetScrollObservable.asObservable();

  constructor() { }
}
