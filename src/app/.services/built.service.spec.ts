import { TestBed } from '@angular/core/testing';

import { BuiltService } from './built.service';

describe('BuiltService', () => {
  let service: BuiltService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuiltService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
