import { ERR_INVALID_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';

import { Plugins } from '@capacitor/core';
const { Device } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  ready: boolean = null;

  isNative: boolean = null;
  isiOS: boolean = null;
  isAndroid: boolean = null;
  isElectron: boolean = null;
  isWeb: boolean = null;

  isInitialized: boolean = false;

  constructor() { }

  async initialize() {
    await this.initializeDevice();
    this.isInitialized = true;
  }

  async initializeDevice() {
    return await Device.getInfo().then((info) => {
      if (info.platform != 'web') {
        this.isNative = true;
        if (info.platform == 'ios') {
          this.isiOS = true;
        } else if (info.platform == 'android') {
          this.isAndroid = true;
        } else if (info.platform == 'electron') {
          this.isElectron = true;
        }
      } else {
        this.isWeb = true;
      }
    });
  }

}
