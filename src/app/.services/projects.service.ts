import { ERR_INVALID_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  switchObservable = new Subject<any>();
  onSwitch = this.switchObservable.asObservable();

  constructor() { }
}
