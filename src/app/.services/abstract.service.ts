import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import { 
  EXPORT_ABSTRACT,
  GENERATE_ABSTRACT,
	IMPORT_ABSTRACT, 
	PATCH_ABSTRACT, 
	REGENERATE_ABSTRACT, 
	SYNC_ABSTRACT, 
	URL_ABSTRACT
} from '../.constances/rest';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class AbstractsService {

  constructor(
		private http: HttpClient,
    private rest: RestService
  ) { }

  list(active: any = '', sort = { by: 'create_at', direction: 'desc' }) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_ABSTRACT}list${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  getByKey(key) {
		return new Promise(async (resolve, reject) => {

      let options = {
        limit: '1'
      }

			const payload = {
				key: key
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_ABSTRACT}list${this.rest.handleQuery(options)}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}
		});
	}

  get(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_ABSTRACT}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  build(id) {
		return new Promise(async (resolve, reject) => {

      const payload = {
        id: id
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_ABSTRACT}${id}/build`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  buildClean(id) {
		return new Promise(async (resolve, reject) => {

      const payload = {
        id: id
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_ABSTRACT}${id}/build?clean=1`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  create(values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_ABSTRACT}create`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  update(id, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.put(
				await this.rest.route(`${URL_ABSTRACT}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  patch(id: string|number, value: any) {
		return new Promise(async (resolve, reject) => {

			const payload = value;

			const request: Observable<any> = await this.http.patch(
				await this.rest.route(`${URL_ABSTRACT}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_ABSTRACT}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  sync() {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_ABSTRACT}sync`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  import(payload) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_ABSTRACT}abstracts`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  export() {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_ABSTRACT}abstracts`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}


}
