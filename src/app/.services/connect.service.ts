import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import { 
  GET_CONNECT_BY_USER_ID, 
	URL_CONNECT
} from '../.constances/rest';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectService {

  constructor(
		private http: HttpClient,
    private rest: RestService
  ) { }

  list(active: any = '', sort = { by: 'create_at', direction: 'desc' }) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_CONNECT}list${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  get(id) {
		return new Promise(async (resolve, reject) => {

      const payload = {
        id: id
      }

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_CONNECT}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  getByUser(userID) {
		return new Promise(async (resolve, reject) => {

      const payload = {
        user_id: userID,
        sort_by: 'create_at',
        sort_direction: 'desc'
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(GET_CONNECT_BY_USER_ID),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_CONNECT}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}
  
}
