import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import {
	GET_PAGE_MAIN_ALL, 
	PATCH_PAGE, 
	URL_PAGE
} from '../.constances/rest';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import { RestService } from './rest.service';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  constructor(
		private http: HttpClient,
    private rest: RestService
  ) { }

  list(active: any = '', sort = { by: 'title', direction: 'asc' }) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_PAGE}list${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  listMain(active: any = '', sort = { by: 'title', direction: 'asc' }) {
		return new Promise(async (resolve, reject) => {

      let options: any = {};
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${GET_PAGE_MAIN_ALL}${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  get(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_PAGE}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  create(values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_PAGE}create`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  update(id, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.put(
				await this.rest.route(`${URL_PAGE}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  patch(id: string|number, value: any) {
		return new Promise(async (resolve, reject) => {

			const payload = value;

			const request: Observable<any> = await this.http.patch(
				await this.rest.route(`${URL_PAGE}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_PAGE}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

}
