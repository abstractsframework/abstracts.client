import { ERR_INVALID_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';
import { 
  Resolve, 
	CanActivate, 
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';

import { Subject, Observable, lastValueFrom } from 'rxjs';

import { StorageService } from '../.services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  session: any = null;

  token: any = null;

  private sessionObservable = new Subject<any>();
  onSessionUpdate = this.sessionObservable.asObservable();

  private tokenObservable = new Subject<any>();
  onTokenUpdate = this.tokenObservable.asObservable();

  constructor(
    private router: Router,
    private storage: StorageService
  ) {

  }

  async canActivate(routeSnapshot: ActivatedRouteSnapshot)
	: Promise<Observable<any> | Promise<any> | any> {
		return this.validateLocalSession().then((response: any) => {
      return response;
    }).catch(async () => {
      const projects: any = await this.storage.get('projects', true, true);
      console.log('projects', projects);
      if (projects && projects.length) {
        return true;
        // this.router.navigate(['./projects']);
      } else {
        this.router.navigate(['./setup']);
      }
      return null;
    });
  }

  async resolve(): Promise<Observable<any> | Promise<any> | any> {
    return this.initialize().then((response: any) => {
      return response;
    }).catch(() => {
      return null;
    });
  }

  initialize() {
    return new Promise(async (resolve, reject) => {
      if (!this.session) {
        let currentProject = '0';
        if (await this.storage.get('current-project')) {
          currentProject = await this.storage.get('current-project');
        }
        const sessionStored: string = await this.storage.get('session.' + currentProject, true, true);
        if (sessionStored) {
          await this.emitSessionUpdate(sessionStored);
        } else {
          reject('Invalid session');
        }
      }
      resolve(this.session);
    });
  }

  async emitSessionUpdate(session) {
    this.session = session;
    this.sessionObservable.next(session);
    let currentProject = '0';
    if (await this.storage.get('current-project')) {
      currentProject = await this.storage.get('current-project');
    }
    await this.storage.set('session.' + currentProject, session, true, true);
  }

  async emitTokenUpdate(token) {
    this.token = token;
    this.tokenObservable.next(token);
  }
  
  validateLocalSession() {
    return new Promise(async (resolve, reject) => {
      let currentProject = '0';
      if (await this.storage.get('current-project')) {
        currentProject = await this.storage.get('current-project');
      }
      const session: any = await this.storage.get('session.' + currentProject, true, true);
			if (session && session != '' && session.id && session.session_id) {
        console.log(session);
				resolve(true);
			} else {
				reject(false);
			}
		});
	}

}
