import { ERR_INVALID_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { 
  Resolve, 
	CanActivate, 
  Router, 
  ActivatedRouteSnapshot
} from '@angular/router';

import { Subject, Observable } from 'rxjs';

import { EnvironmentService } from './environment.service';
import { StorageService } from './storage.service';
import { AuthenticationService } from './authentication.service';
import { API_CACHE } from '../.constances/config';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  project: any = null;
  url: null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private env: EnvironmentService,
    private storage: StorageService,
		private auth: AuthenticationService
  ) { }

  async canActivate(routeSnapshot: ActivatedRouteSnapshot)
	: Promise<Observable<any> | Promise<any> | any> {
		return this.validateAPICredential().then((response: any) => {
      return response;
    }).catch(async () => {
      const projects: any = await this.storage.get('projects', true, true);
      console.log('projects', projects);
      if (projects && projects.length) {
        return true;
        // this.router.navigate(['./projects']);
      } else {
        this.router.navigate(['./setup']);
      }
      return null;
    });
  }

  initialize() {
    return new Promise(async (resolve) => {
      if (this.env.isNative == null) {
        await this.env.initializeDevice();
      }
      this.project = await this.retrieveProject();
      const currentProject = await this.storage.get('current-project');
      const projects = await this.storage.get('projects', true, true);
      if (projects && projects.length && projects[currentProject]) {
        this.url = projects[currentProject].host;
      }
      resolve(true);
    });
  }

  async retrieveProject() {
    if (this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    const currentProject = await this.storage.get('current-project');
    const projects = await this.storage.get('projects', true, true);
    if (projects && projects.length && projects[currentProject]) {
      const project = projects[currentProject];
      this.project = project;
      return project;
    } else {
      return null;
    }
  }

  validateAPICredential() {
    return new Promise(async (resolve, reject) => {
      const currentProject = await this.storage.get('current-project');
      const projects = await this.storage.get('projects', true, true);
      console.log(projects);
      if (projects && projects.length && projects[currentProject]) {
        const credential = projects[currentProject].credential;
        if (credential && credential != '' && credential.key && credential.secret) {
          resolve(true);
        } else {
          reject(false);
        }
      } else {
        reject(false);
      }
		});
	}

  async retrieveHeader() {

    const project = await this.retrieveProject();

    let token = '';
    let userID = '';
    let sessionID = '';
    if (this.auth.session) {
      if (this.auth.session.token && this.auth.session.token != '') {
        token = this.auth.session.token;
      } else if (
        this.auth.session.id && this.auth.session.id != ''
        && this.auth.session.session_id && this.auth.session.session_id != ''
      ) {
        userID = this.auth.session.id;
        sessionID = this.auth.session.session_id;
      }
    }

    const currentToken = await btoa(project.credential.key + ':' + project.credential.secret);

    const headers = await new HttpHeaders()
      .set('Token', (currentToken) ? currentToken : '')
      .set('Authorization', 'Bearer ' + token);

    return headers;
    
  }

  async route(url) {
    const currentProject = await this.storage.get('current-project');
    const projects = await this.storage.get('projects', true, true);
    if (projects && projects.length && projects[currentProject]) {
      this.url = projects[currentProject].host;
    }
    let suffix = '';
    if (!API_CACHE) {
      const timestap = `v=${new Date().getTime()}`
      if (url.indexOf('?') >= 0) {
        suffix = `&${timestap}`
      } else {
        suffix = `?${timestap}`
      }
    }
    return this.url + url + suffix;
  }

  handleQuery(options: any) {
    let prefix = '';
    const optionQuery = new URLSearchParams(options);
    if (Object.keys(options).length) {
      prefix = '?';
    }
    return `${prefix}${optionQuery}`;
  }

}