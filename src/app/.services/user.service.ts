import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import { 
	LOGIN, 
	GET_USER_BY_SIGNIFICANT, 
	UPLOAD_USER_FILES,
	FILE_CONTENT,
	RESET_USER_PASSWORD_EMAIL,
	RESET_USER_PASSWORD_PHONE,
	UPDATE_USER_PASSWORD,
	URL_USER
} from '../.constances/rest';

import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, lastValueFrom } from 'rxjs';

import { RestService } from './rest.service';
import { StorageService } from './storage.service';
import { AuthenticationService } from './authentication.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
		private http: HttpClient,
    private router: Router,
		private rest: RestService,
		private storage: StorageService,
		private auth: AuthenticationService
  ) { 

	}

	login(username, password) {
		return new Promise(async (resolve, reject) => {

			if (!this.rest.project) {
				await this.rest.initialize();
			}
			if (this.rest.project) {
				
				const currentToken = await btoa(this.rest.project.credential.key + ':' + this.rest.project.credential.secret);

				const authorization = await btoa(username + ':' + password);
				const headers = new HttpHeaders()
					.set('Token', (currentToken) ? currentToken : '')
					.set('Authorization', 'Basic ' + authorization);

				const request: Observable<any> = await this.http.get(
					await this.rest.route(`${URL_USER}login?remember=1`),
					{ headers: headers }
				);
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			}

		});
	}

  logout(redirect: boolean = false) {
    return new Promise(async (resolve, reject) => {
      let currentProject = '0';
      if (await this.storage.get('current-project')) {
        currentProject = await this.storage.get('current-project');
      }
			await this.storage.remove('session.' + currentProject, true);
      const projects: any = await this.storage.get('projects', true, true);
			if (!redirect) {
				if (projects && projects.length) {
					this.router.navigate(['./projects']);
				} else {
					this.router.navigate(['./login']);
				}
			} else {
				if (projects && projects.length) {
					this.router.navigate(['./projects']);
				} else {
					this.router.navigate(['./login']);
				}
			}
      resolve(true);
    });
  }

	getCurrentUser() {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_USER}${this.auth.session.id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

	list(
    active: boolean|null = null, 
    sort: any = { by: 'create_at', direction: 'desc' }, 
    start: number|null = null,
    limit: number|null = null,
    search: string|null = null,
    returnReferences: any = false
	) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
        if (sort.by) {
          options.sort_by = Array.isArray(sort.by) ? sort.by.join(',') : sort.by;
        }
        if (sort.direction) {
          options.sort_direction = Array.isArray(sort.direction) ? sort.direction.join(',') : sort.direction;
        }
			}
			if (limit) {
				options.limit = limit;
        if (start) {
          options.start = start;
        }
			}
			if (returnReferences === true) {
				options.return_references = '1';
			} else if (returnReferences) {
				options.return_references = Array.isArray(returnReferences) ? returnReferences.join(',') : returnReferences;
      }
      let payload: any = {};
      let conjunction = '';
      payload.extensions = [];
			if (search) {
        await payload.extensions.push(
          {
            conjunction: conjunction,
            extensions: [
              {
                conjunction: '',
                key: 'name',
                operator: 'LIKE',
                value: `'%${search}%'`,
              },
              {
                conjunction: 'OR',
                key: 'id',
                operator: '=',
                value: `'%${search}%'`,
              },
              {
                conjunction: 'OR',
                key: 'email',
                operator: 'LIKE',
                value: `'%${search}%'`,
              }
            ]
          }
        );
        conjunction = 'AND';
			}
			
			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_USER}list${this.rest.handleQuery(options)}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

	get(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_USER}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

	getBySignificant(significant) {
		return new Promise(async (resolve, reject) => {

      const payload = {
        significant: significant
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(GET_USER_BY_SIGNIFICANT),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

	async uploadFiles(payload) {
		return new Promise(async (resolve, reject) => {
			const request: Observable<any> = await this.http.post(
				await this.rest.route(UPLOAD_USER_FILES),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}
		});
	}

  async retrieveContentURL(URL, field, decoration = null) {
		if (!this.auth.session) {
			await this.auth.initialize();
		}
		if (!this.rest.project) {
			await this.rest.initialize();
		}
		if (this.rest.project) {
			const nonce = new Date().getTime();
			const token = btoa(`${this.rest.project.credential.key}:${this.rest.project.credential.secret}:${nonce}`);
			let urlDecoration = '';
			if (decoration) {
				urlDecoration = '&decoration=' + decoration;
			}
			return await this.rest.route(FILE_CONTENT) + 
			'field=' + field + 
			'&authorization=' + this.auth.session.token + 
			'&token=' + token + 
			urlDecoration + 
			'&url=' + encodeURI(URL);
		} else {
			return '';
		}
  }

  create(values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_USER}create`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  update(id, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.put(
				await this.rest.route(`${URL_USER}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_USER}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  resetPasswordEmail(email) {
		return new Promise(async (resolve, reject) => {

			const payload = {
        email: email
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(RESET_USER_PASSWORD_EMAIL),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  resetPasswordPhone(phone) {
		return new Promise(async (resolve, reject) => {

			const payload = {
        phone: phone
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(RESET_USER_PASSWORD_PHONE),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  changePassword(passwordCurrent, passwordNew, passwordConfirm) {
		return new Promise(async (resolve, reject) => {

			const payload = {
        current_password: passwordCurrent,
        password: passwordNew,
        confirm_password: passwordConfirm
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(UPDATE_USER_PASSWORD),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

}
