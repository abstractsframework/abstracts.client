import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import { 
	GET_GROUP_BY_MEMBER_USER, 
	GET_GROUP_MEMBER_BY_GROUP_ID, 
	CREATE_GROUP_MEMBER, 
	DELETE_GROUP_MEMBER, 
	URL_GROUP
} from '../.constances/rest';

import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, lastValueFrom } from 'rxjs';
import { RestService } from './rest.service';


@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
		private http: HttpClient,
    private rest: RestService
  ) { }

  list(active: any = '', sort = { by: 'order', direction: 'asc' }, returnReferences: any = false) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (returnReferences) {
				if (returnReferences === true) {
					options.return_references = '1';
				} else {
					options.return_references = returnReferences.isArray() ? returnReferences.join(',') : returnReferences;
				}
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}
			
			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_GROUP}list${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}
			
		});
	}

  get(id, reference: any = false) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (reference) {
				if (reference === true) {
					options.return_references = '1';
				} else {
					options.reference = reference.join(",");
				}
			}
			
			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_GROUP}${id}${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  create(values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_GROUP}create`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  update(id, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.put(
				await this.rest.route(`${URL_GROUP}${id}`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_GROUP}${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  getMemberByUser(userID, start: any = '', limit: any = '') {
		return new Promise(async (resolve, reject) => {

      const payload = {
        user: userID,
				start: start,
				limit: limit
      }

			const request: Observable<any> = await this.http.post(
				await this.rest.route(GET_GROUP_BY_MEMBER_USER),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  getMembers(groupID, start: any = '', limit: any = '') {
		return new Promise(async (resolve, reject) => {

      let options: any = {};
			if (limit) {
				options.limit = limit;
        if (start) {
          options.start = start;
        }
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`${URL_GROUP}${groupID}/members${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  addMember(id, userID) {
		return new Promise(async (resolve, reject) => {

			const payload = {
				id: id,
				user: userID
			};

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`${URL_GROUP}${id}/members`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  removeMember(id, userID) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.delete(
				await this.rest.route(`${URL_GROUP}${id}/members?user=${userID}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

}
