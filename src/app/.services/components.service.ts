import { ERR_INVALID_RESPONSE } from '../.constances/messages';
import { TOAST_DURATION } from '../.constances/config';

import { Injectable } from '@angular/core';

import {
  NbComponentStatus,
  NbGlobalPosition,
  NbGlobalLogicalPosition,
  NbToastrService,
  NbDuplicateToastBehaviour
} from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  constructor(
    private toastrService: NbToastrService
  ) { }

  showToastStatus(type: NbComponentStatus, title: string, body: string) {

    let icon = 'alert-circle-outline';
    if (type == 'success' as NbComponentStatus) {
      icon = 'checkmark-circle-2-outline';
    } else if (type == 'danger' as NbComponentStatus) {
      icon = 'alert-triangle-outline';
    }

    const config = {
      status: type,
      destroyByClick: true,
      duration: TOAST_DURATION,
      hasIcon: false,
      icon: icon,
      position: NbGlobalLogicalPosition.TOP_END as NbGlobalPosition,
      preventDuplicates: false,
      duplicatesBehaviour: 'previous' as NbDuplicateToastBehaviour,
      toastClass: 'toast-status-' + type
    };

    const titleContent = title ? `${title}` : '';

    this.toastrService.show(body, titleContent, config);

  }

}
