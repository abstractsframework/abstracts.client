import { ERR_INVALID_RESPONSE, ERR_UNKNOWN_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { RestService } from './rest.service';
import { Observable, lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuiltService {

  constructor(
		private http: HttpClient,
    private rest: RestService
  ) { }

  list(endpoint, key, active: any = '', sort = { by: 'create_at', direction: 'desc' }) {
		return new Promise(async (resolve, reject) => {

			let options: any = {}
			if (active === true) {
				options.active = true;
			} else if (active === false) {
				options.active = false;
			} else if (active) {
				options.active = active;
			}
			if (sort) {
				options.sort_by = sort.by;
				options.sort_direction = sort.direction;
			}

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/list${this.rest.handleQuery(options)}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				console.log(response);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  get(endpoint, key, id) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.get(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/${id}`),
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  create(endpoint, key, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/create`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  update(endpoint, key, values) {
		return new Promise(async (resolve, reject) => {

			const payload = values;

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/update`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  delete(endpoint, key, id) {
		return new Promise(async (resolve, reject) => {

			const payload = {
				id: id
			};

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/delete`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  upload(endpoint, key, payload) {
		return new Promise(async (resolve, reject) => {

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/upload`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  remove(endpoint, key, inputKey, id, file) {
		return new Promise(async (resolve, reject) => {

			const payload = {
				id: id,
				file: file
			};

			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/remove_file`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

  getInputListByKey(endpoint, key, inputKey) {
		return new Promise(async (resolve, reject) => {

			const payload = {
				active: true
			};
			console.log(`api/${endpoint.replace('.php', '')}/get_${key}_${inputKey}_all`);
			const request: Observable<any> = await this.http.post(
				await this.rest.route(`api/${endpoint.replace('.php', '')}/get_${key}_${inputKey}_all`),
				payload,
				{ headers: await this.rest.retrieveHeader() }
			)
			try {
				const response: any = await lastValueFrom(request);
				if (response) {
					resolve(response);
				} else {
					reject(ERR_INVALID_RESPONSE);
				}
			} catch (response) {
				reject(
					(response && response.error) 
          ? response.error
          : { message: ERR_UNKNOWN_RESPONSE }
				);
			}

		});
	}

}
