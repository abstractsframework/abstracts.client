import { ERR_INVALID_RESPONSE } from '../.constances/messages';
import { STORAGE_SECRET } from '../.constances/credentials';
import { STORAGE_KEY_PREFIX } from '../.constances/config';

import { Injectable } from '@angular/core';

import { Plugins } from '@capacitor/core';
const { Storage, Device } = Plugins;

import SecureLS from 'secure-ls';

import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private secureLocalStorage = null;

  constructor(
    private env: EnvironmentService
  ) { }

  async initialize() {
    const device = await Device.getInfo();
    this.secureLocalStorage = await new SecureLS({
      encodingType: 'aes',
      isCompression: true,
      encryptionSecret: STORAGE_SECRET + device.uuid
    });
  }

  async set(keyName: string = '', data, isObject: boolean = false, encryption: boolean = false) {
    let dataToStore = data;
    if (isObject) {
      if (dataToStore) {
        dataToStore = JSON.stringify(dataToStore);
      } else {
        dataToStore = '';
      }
    }
    if (!this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    if (this.env.isNative) {
      await Storage.set({
        key: STORAGE_KEY_PREFIX + keyName,
        value: dataToStore
      });
    } else {
      if (encryption) {
        if (!this.secureLocalStorage) {
          await this.initialize();
        }
        await this.secureLocalStorage.set(STORAGE_KEY_PREFIX + keyName, dataToStore);
      } else {
        await localStorage.setItem(STORAGE_KEY_PREFIX + keyName, dataToStore);
      }
    }
    return true;
  }

  async get(keyName: string = '', isObject: boolean = false, encryption: boolean = false) {
    let dataStored: any = null;
    if (!this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    if (this.env.isNative) {
      const dataNativeStored = await Storage.get({
        key: STORAGE_KEY_PREFIX + keyName
      });
      dataStored = dataNativeStored.value;
    } else {
      if (encryption) {
        if (!this.secureLocalStorage) {
          await this.initialize();
        }
        dataStored = await this.secureLocalStorage.get(STORAGE_KEY_PREFIX + keyName);
      } else {
        dataStored = await localStorage.getItem(STORAGE_KEY_PREFIX + keyName);
      }
    }
    let data = dataStored;
    if (isObject) {
      if (dataStored) {
        let isJSON = true;
        try {
          await JSON.parse(dataStored)
        } catch {
          isJSON = false;
        }
        if (isJSON) {
          data = await JSON.parse(dataStored);
        } else {
          data = null;
        }
      } else {
        data = null;
      }
    } else {
      if (!dataStored) {
        data = null;
      }
    }
    return data;
  }

  async remove(keyName: string = '', encryption: boolean = false) {
    if (!this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    if (this.env.isNative) {
      await Storage.remove({
        key: STORAGE_KEY_PREFIX + keyName
      });
    } else {
      if (encryption) {
        if (!this.secureLocalStorage) {
          await this.initialize();
        }
        await this.secureLocalStorage.remove(STORAGE_KEY_PREFIX + keyName);
      } else {
        await localStorage.removeItem(STORAGE_KEY_PREFIX + keyName);
      }
    }
    return true;
  }

  async keys(encryption: boolean = false) {
    let keys = null;
    if (!this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    if (this.env.isNative) {
      keys = await Storage.keys();
    } else {
      if (encryption) {
        if (!this.secureLocalStorage) {
          await this.initialize();
        }
        keys = await this.secureLocalStorage.keys();
      } else {
        for(var i =0; i < localStorage.length; i++){
          await keys.push(localStorage.getItem(localStorage.key(i)));
        }
      }
    }
    return keys;
  }

  async clear() {
    if (!this.env.isNative == null) {
      await this.env.initializeDevice();
    }
    if (this.env.isNative) {
      await Storage.clear();
    } else {
      if (!this.secureLocalStorage) {
        await this.initialize();
      }
      await this.secureLocalStorage.clear();
      await localStorage.clear();
    }
    return true;
  }

}
