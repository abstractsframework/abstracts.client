import { ERR_INVALID_RESPONSE } from '../.constances/messages';

import { Injectable } from '@angular/core';

import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  async sortBy(item, fieldName = null, type = null, direction = 'desc') {
    if (item) {
      if (fieldName) {
        if (type == 'number' || type == 'timestamp') {
          if (direction == 'desc') {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              parseInt(a[fieldName]) < parseInt(b[fieldName]) ? 1 : parseInt(a[fieldName]) === parseInt(b[fieldName]) ? 0 : -1
              : 1
            );
          } else {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              parseInt(a[fieldName]) > parseInt(b[fieldName]) ? 1 : parseInt(a[fieldName]) === parseInt(b[fieldName]) ? 0 : -1
              : 1
            );
          }
        } else if (type == 'decimal') {
          if (direction == 'desc') {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              parseFloat(a[fieldName]) < parseFloat(b[fieldName]) ? 1 : parseFloat(a[fieldName]) === parseFloat(b[fieldName]) ? 0 : -1
              : 1
            );
          } else {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              parseFloat(a[fieldName]) > parseFloat(b[fieldName]) ? 1 : parseFloat(a[fieldName]) === parseFloat(b[fieldName]) ? 0 : -1
              : 1
            );
          }
        } else if (type == 'string') {
          if (direction == 'desc') {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              a[fieldName].toString() < b[fieldName].toString() ? 1 : a[fieldName].toString() === b[fieldName].toString() ? 0 : -1
              : 1
            );
          } else {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              a[fieldName].toString() > b[fieldName].toString() ? 1 : a[fieldName].toString() === b[fieldName].toString() ? 0 : -1
              : 1
            );
          }
        } else if (type == 'datetime') {
          if (direction == 'desc') {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              (a, b) => moment(a[fieldName]).format('X') < moment(b[fieldName]).format('X') ? 1 : moment(a[fieldName]).format('X') === moment(b[fieldName]).format('X') ? 0 : -1
              : 1
            );
          } else {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              (a, b) => moment(a[fieldName]).format('X') > moment(b[fieldName]).format('X') ? 1 : moment(a[fieldName]).format('X') === moment(b[fieldName]).format('X') ? 0 : -1
              : 1
            );
          }
        } else {
          if (direction == 'desc') {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              (a, b) => a[fieldName] < b[fieldName] ? 1 : a[fieldName] === b[fieldName] ? 0 : -1
              : 1
            );
          } else {
            return await item.sort(
              (a, b) => 
              (a[fieldName] !== undefined && b[fieldName] !== undefined && a[fieldName] !== null && b[fieldName] !== null) ? 
              (a, b) => a[fieldName] > b[fieldName] ? 1 : a[fieldName] === b[fieldName] ? 0 : -1
              : 1
            );
          }
        }
      } else {
        return item;
      }
    }
  }

  serialize(mixedValue) {
  
    let val, key, okey;
    let ktype = '';
    let vals = '';
    let count = 0;
  
    let _utf8Size = ((str) => {
      return ~-encodeURI(str).split(/%..|./).length;
    });
  
    let _getType = ((inp) => {
      let match;
      let key;
      let cons;
      let types;
      let type = typeof inp;
  
      if (type === 'object' && !inp) {
        return 'null';
      }
  
      if (type === 'object') {
        if (!inp.constructor) {
          return 'object';
        }
        cons = inp.constructor.toString()
        match = cons.match(/(\w+)\(/)
        if (match) {
          cons = match[1].toLowerCase();
        }
        types = ['boolean', 'number', 'string', 'array'];
        for(key in types) {
          if (cons === types[key]) {
            type = types[key];
            break;
          }
        }
      }
      return type;
    });
  
    let type = _getType(mixedValue);
  
    switch(type) {
      case 'function':
        val = '';
        break;
      case 'boolean':
        val = 'b:' + (mixedValue ? '1' : '0');
        break;
      case 'number':
        val = (Math.round(mixedValue) === mixedValue ? 'i' : 'd') + ':' + mixedValue;
        break;
      case 'string':
        val = 's:' + _utf8Size(mixedValue) + ':"' + mixedValue + '"';
        break;
      case 'array':
      case 'object':
        val = 'a';
        /*
        if (type === 'object') {
          let objname = mixedValue.constructor.toString().match(/(\w+)\(\)/);
          if (objname === undefined) {
            return;
          }
          objname[1] = serialize(objname[1]);
          val = 'O' + objname[1].substring(1, objname[1].length - 1);
        }
        */
  
        for(key in mixedValue) {
          if (mixedValue.hasOwnProperty(key)) {
            ktype = _getType(mixedValue[key])
            if (ktype === 'function') {
              continue;
            }
  
            okey = (key.match(/^[0-9]+$/) ? parseInt(key, 10) : key);
            vals += this.serialize(okey) + this.serialize(mixedValue[key]);
            count++;
          }
        }
        val += ':' + count + ':{' + vals + '}';
        break;
      case 'undefined':
      default:
        // Fall-through
        // if the JS object has a property which contains a null value,
        // the string cannot be unserialized by PHP
        val = 'N';
        break;
    }
    if (type !== 'object' && type !== 'array') {
      val += ';'
    }
  
    return val;

  }
  
}
