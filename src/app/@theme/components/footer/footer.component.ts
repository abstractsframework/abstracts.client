import { Component } from '@angular/core';
import packageInfo from '../../../../../package.json';

@Component({
  selector: 'ngx-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  version: string = packageInfo.version;

}
