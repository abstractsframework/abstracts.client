import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

import { UserService } from '../../../.services/user.service';
import { StorageService } from '../../../.services/storage.service';
import { RestService } from '../../../.services/rest.service';
import { ComponentsService } from '../../../.services/components.service';
import { ProjectsService } from '../../../.services/projects.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  account: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    {
      value: 'cosmic',
      name: 'Cosmic',
    },
    {
      value: 'corporate',
      name: 'Corporate',
    },
  ];

  currentTheme = 'default';

  userMenu = [ { title: 'Profile' }, { title: 'Log out', data: 'logout' } ];

  projectOptions: any = [];
  currentProject = null;

  private switchSubscription: Subscription;

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private storage: StorageService,
    private user: UserService,
    private projects: ProjectsService,
    private rest: RestService,
    private components: ComponentsService
  ) {
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    // this.userService.getUsers()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((users: any) => this.user = users.nick);

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
    
    this.menuService.onItemClick().subscribe((event) => {
      if (event.item.data == 'logout') {
        this.user.logout();
      }
    });

    this.loadProject();

    this.subscribeSwitch();

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.unsubscribeSwitch();
  }

  async loadProject() {
    const projects: any = await this.storage.get('projects', true, true);
    if (projects && projects.length) {
      this.projectOptions = projects;
    }
    const currentProject = await this.storage.get('current-project');
    if (currentProject) {
      this.currentProject = parseInt(currentProject);
    }
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  async switchProject(index) {
    await this.storage.set('current-project', await index.toString());
    this.rest.initialize().then(async () => {
      this.projects.switchObservable.next(index);
      this.components.showToastStatus('success', 'Success', 'Switched project');
    });
  }

  private subscribeSwitch() {
    this.switchSubscription = this.projects.onSwitch.subscribe(async (data: any) => {
      if (data && data != this.currentProject.toString()) {
        this.currentProject = data;
      }
    });
  }

  private unsubscribeSwitch() {
    this.switchSubscription.unsubscribe();
  }

}
