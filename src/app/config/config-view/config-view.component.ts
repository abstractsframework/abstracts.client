import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-config-view',
  templateUrl: './config-view.component.html',
  styleUrls: ['./config-view.component.scss']
})
export class ConfigViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
