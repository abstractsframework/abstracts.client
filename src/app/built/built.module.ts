import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  NbActionsModule, 
  NbBadgeModule, 
  NbButtonModule, 
  NbCardModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbIconModule, 
  NbInputModule,
  NbListModule,
  NbSelectModule,
  NbSpinnerModule,
  NbTagModule,
  NbTimepickerModule,
  NbUserModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableListModuleModule } from '../module/table-list/module/table-list-module.module';
import { TableListBehaviorModule } from '../control/table-list/behavior/table-list-behavior.module';
import { TableListRuleModule } from '../control/table-list/rule/table-list-rule.module';

import { SlimModule } from '../.directives/slim/slim.module';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { BuiltRoutingModule } from './built-routing.module';
import { BuiltComponent } from './built.component';
import { BuiltCreateComponent } from './create/built-create.component';
import { BuiltEditComponent } from './edit/built-edit.component';
import { BuiltTableComponent } from './table/built-table.component';
import { BuiltViewComponent } from './view/built-view.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { TagInputModule } from 'ngx-chips';
import { IconModule } from '../icon/icon.module';
import { EditorModule } from '@tinymce/tinymce-angular';


@NgModule({
  declarations: [
    BuiltComponent,
    BuiltCreateComponent,
    BuiltEditComponent,
    BuiltTableComponent,
    BuiltViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbTimepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    IconModule,
    DragDropModule,
    TagInputModule,
    NbTagModule,
    NgxDropzoneModule,
    EditorModule,
    TableListModuleModule,
    TableListBehaviorModule,
    TableListRuleModule,
    BuiltRoutingModule,
    SlimModule,
  ],
  exports: [
    BuiltCreateComponent,
    BuiltEditComponent,
    BuiltTableComponent,
    BuiltViewComponent
  ]
})
export class builtModule { }
