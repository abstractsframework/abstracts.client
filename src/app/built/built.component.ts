import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { AbstractsService } from '../.services/abstract.service';
import { ReferenceService } from '../.services/reference.service';
import { BuiltService } from '../.services/built.service';
import { ComponentsService } from '../.services/components.service';
import { ModuleService } from '../.services/module.service';

@Component({
  selector: 'abstract-built',
  templateUrl: './built.component.html',
  styleUrls: ['./built.component.scss']
})
export class BuiltComponent implements OnInit {

  @ViewChild('dialog') dialog: TemplateRef<any>;

  path = '/built';
  active = 'list';
  activeID = null;
  data = null;
  builtData: any = null;
  initialized = false;

  type = '';
  types = [
    {
      title: 'All',
      value: ''
    },
    {
      title: 'Active',
      value: 'active'
    },
    {
      title: 'Inactive',
      value: 'inactive'
    }
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private components: ComponentsService,
    private abstracts: AbstractsService,
    private reference: ReferenceService,
    private module: ModuleService,
    private built: BuiltService

  ) {
    this.prepare();
  }

  ngOnInit(): void {
  }

  async prepare() {
    this.router.events.subscribe((enter: NavigationEnd) => {
      if (enter instanceof NavigationEnd) {
        this.activeID = null;
        if (enter.url.indexOf('/built') >= 0) {
          if (enter.url.includes('create')) {
            this.active = 'create';
          } else if (enter.url.includes('edit')) {
            const urlParts = enter.url.split('/');
            this.activeID = urlParts[3] ? urlParts[3] : null;
            this.active = 'edit';
          } else {
            this.active = 'list';
            if (enter.url.includes('/active')) {
              this.type = 'active';
            } else if (enter.url.includes('/inactive')) {
              this.type = 'inactive';
            } else {
              this.type = '';
            }
          }
          this.initialize();
        }
      }
    });
  }

  async initialize() {
    this.data = null;
    this.builtData = null;
    this.initialized = false;
    this.changeRef.detectChanges();
    const modulePath = this.route.snapshot.paramMap.get('modulePath');
    if (modulePath) {
      const key = modulePath.replace(/-/g, '_');
      this.path = `/built/${key.replace(/_/g, '-')}`;
      await this.abstracts.getByKey(key).then(async (response: any) => {
        await this.module.get(response[0].id).then(async (responseModule: any) => {
          if (responseModule) {
            const moduleData = responseModule;
            moduleData.abstract = response[0];
            moduleData.abstract.references = [];
            let references = [];
            let referencesOfficialPre = [];
            let referencesOfficialPost = [];
            if (response[0].component_page) {
              await referencesOfficialPre.push(
                {
                  active: '1',
                  label: 'Title',
                  key: 'title',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (response[0].component_language) {
              await referencesOfficialPre.push(
                {
                  active: '1',
                  label: 'Language',
                  key: 'language_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (response[0].component_user) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Creator',
                  key: 'user_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (response[0].component_group) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Group',
                  key: 'group_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            if (response[0].component_module) {
              await referencesOfficialPost.push(
                {
                  active: '1',
                  label: 'Module',
                  key: 'module_id',
                  type: 'input-text',
                  pseudo: true
                }
              );
            }
            moduleData.abstract.references = [
              ...referencesOfficialPre, 
              ...references, 
              ...referencesOfficialPost
            ];
            this.data = moduleData;
            console.log(this.data);
            this.initializeData();
          } else {
            this.components.showToastStatus('danger', 'Error', 'Permission denied');
          }
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
    this.initialized = true;
    this.changeRef.detectChanges();
  }

  async initializeData() {
    if (this.active == 'edit') {
      if (this.data && this.activeID) {
        await this.built.get(this.data.service, this.data.key, this.activeID).then(async (response: any) => {
          this.builtData = response;
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    }
  }

  navigate(childURL = '') {
    if (this.active == 'create' || this.active == 'edit') {
      let context = '';
      if (this.active == 'create') {
        context = 'Do you want to exit create page?';
      } else if (this.active == 'edit') {
        context = 'Do you want to exit edit page?';
      }
      this.dialogService.open(
        this.dialog, {
          context: context
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
    console.log(this.path + childURL);
  }

  onSelectStatus(event) {
    this.router.navigate([event == 'all' ? this.path : this.path + '/' + event]);
  }

}