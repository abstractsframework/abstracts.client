import { Component, Input, OnChanges, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { ComponentsService } from '../../.services/components.service';
import { BuiltService } from '../../.services/built.service';

import * as moment from 'moment';
import { SUCC_SUBMIT } from '../../.constances/messages';

@Component({
  selector: 'abstract-built-table',
  templateUrl: './built-table.component.html',
  styleUrls: ['./built-table.component.scss']
})
export class BuiltTableComponent implements OnInit, OnChanges {

  @ViewChild('dialog') dialog: TemplateRef<any>;

  @Input('builder') builder: any = null;
  @Input('type') type: any = '';

  columnPre: any = {
    id: {
      title: 'ID',
      type: 'html',
      class: 'id',
      valuePrepareFunction: (value) => {
        return `<div class="text-center">${value}</div>`
      }
    }
  }
  
  columnPost: any = {
    create_at: {
      title: 'Created',
      type: 'html',
      class: 'datetime',
      valuePrepareFunction: (value) => {
        return value 
        ? `<div class="text-center">${moment(value).format('L')}<br />${moment(value).format('LTS')}</div>` 
        : '-';
      }
    },
    active: {
      title: 'Status',
      type: 'html',
      class: 'status',
      valuePrepareFunction: (value) => {
        return '<div class="badge-wrapper text-center"><span class="badge ' + (value == '1' ? 'success' : 'disabled') + ' text-center">' 
        + (value == '1' ? 'Active' : 'Inactive') 
        + '</span></div>';
      }
    }
  }

  settings: any = {
    mode: 'external',
    actions: {
      add: false
    },
    sort: true,
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    }
  };

  list: any = null;
  loaded: boolean = false;

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private built: BuiltService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.initialize(
      this.type == 'active' ? true
      : this.type == 'inactive' ? false
      : ''
    ).then(() => {
      this.loaded = true;
    });
  }

  async initialize(active = null) {
    let columns: any = {};
    if (this.builder) {

      await this.built.list(this.builder.service, this.builder.key, active).then((response: any) => {
        this.list = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      
      columns.id = this.columnPre.id;
      const formatColumns = async (tableColumns = null) => {
        if (this.builder.abstract.references && this.builder.abstract.references.length) {
          for (let reference of this.builder.abstract.references) {
            let display = 0;
            if (!tableColumns) {
              if (reference.active === true) {
                display = 1;
              }
            } else {
              const findDisplay = async () => {
                for (let item of tableColumns) {
                  if (
                    reference.key == item.name
                    && item.display == '1'
                  ) {
                    display += 1;
                    return;
                  }
                }
              }
              await findDisplay();
            }
            if (display) {
              let type = 'string';
              let className = 'text';
              let value: any = (value) => {
                return value;
              };
              if (reference.type == 'input-number') {
                type = 'number';
                className = 'number';
              } else if (
                reference.type == 'input-date'
                || reference.type == 'input-datetime'
              ) {
                type = 'html';
                className = 'datetime';
                value = (value) => {
                  if (reference.type == 'input-date') {
                    return value 
                    ? `<div class="text-center">${moment(value).format('L')}<br />${moment(value).format('LTS')}</div>` 
                    : '-';
                  } else {
                    return value 
                    ? `<div class="text-center">${moment(value).format('LTS')}</div>` 
                    : '-';
                  }
                }
              } else if (
                reference.type == 'image-upload'
                || reference.type == 'image-cropper'
              ) {
                type = 'html';
                className = 'image';
                value = (value, rowData) => {
                  return value 
                  ? `<div class="text-center"><img class="table-image" src="${
                    rowData[`${reference.key}_thumbnail_path`] 
                    ? rowData[`${reference.key}_thumbnail_path`]
                    : rowData[`${reference.key}_path`] 
                      ? rowData[`${reference.key}_path`]
                      : value
                  }" /></div>` 
                  : '<div class="text-center"><div class="table-image"></div></div>';
                }
              } else if (reference.type == 'textarea') {
                type = 'html';
                className = 'text-editor';
                value = (value) => {
                  return value 
                  ? `<div class="text-preview">${value}</div>` 
                  : '';
                }
              } else if (reference.type == 'text-editor') {
                type = 'html';
                className = 'text-editor';
                value = (value) => {
                  return value 
                  ? `<div class="text-preview">${value.replace(/(<([^>]+)>)/gi, "")}</div>` 
                  : '';
                }
              } else if (reference.type == 'switch') {
                console.log(reference);
                type = 'html';
                className = 'switch';
                value = (value) => {
                  let valueText = value;
                  if (!reference.default_value || !reference.default_value.length || reference.default_value[0] == '1') {
                    if (value && value == '1') {
                      valueText = '<i class="fa-solid fa-check"></i>';
                    } else {
                      valueText = '<i class="fa-solid fa-xmark"></i>';
                    }
                    return `
                    <div class="badge-wrapper text-center">
                    <span class="badge ${value == '1' ? 'success' : 'disabled'} text-center">
                    ${valueText}
                    </span>
                    </div>`;
                  } else {
                    return value ? `
                    <div class="badge-wrapper text-center">
                    <span class="badge text-center">
                    ${valueText}
                    </span>
                    </div>` : '';
                  }
                }
              }
              let column = {
                title: reference.label,
                type: type,
                class: className,
                valuePrepareFunction: value
              }
              columns[reference.key] = column;
            }
          }
        } else {
          if (this.list && this.list.length) {
            for (let key of Object.keys(this.list[0])) {
              columns[key] = {
                title: key,
                type: 'string'
              }
            }
          }
        }
      }
      if (
        this.builder.table_columns 
        && Array.isArray(this.builder.table_columns)
        && this.builder.table_columns.length
      ) {
        await formatColumns(this.builder.table_columns);
      } else {
        await formatColumns();
      }
      columns.create_at = this.columnPost.create_at;
      columns.active = this.columnPost.active;
      this.settings.columns = columns;

    }
  }

  openEdit(event) {
    if (this.builder) {
      this.router.navigateByUrl(
        `/built/${this.builder.key}/${event.data.id}/edit`, 
        {
          state: event.data
        }
      );
    }
  }

  delete(event, field = null) {
    let context = 'Do you want to delete' + ' ' + 'ID' + ':' + event.data.id + '?';
    if (field) {
      context = 'Do you want to delete' 
      + ' "' + event.data[field] + '" (' + 'ID' + ': ' + event.data.id + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe((data) => {
      if (data) {
        this.built.delete(this.builder.service, this.builder.key, event.data.id).then(async (response: any) => {
          this.initialize(
            this.type == 'active' ? true
            : this.type == 'inactive' ? false
            : ''
          ).then(() => {
            this.loaded = true;
          });
          this.components.showToastStatus('success', 'Success', SUCC_SUBMIT);
        }).catch((errorMessage) => {
          this.components.showToastStatus('danger', 'Error', errorMessage);
        });
      }
    });
  }

}
