import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltTableComponent } from './built-table.component';

describe('BuiltTableComponent', () => {
  let component: BuiltTableComponent;
  let fixture: ComponentFixture<BuiltTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuiltTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiltTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
