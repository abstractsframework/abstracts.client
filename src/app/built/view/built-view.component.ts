import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-built-view',
  templateUrl: './built-view.component.html',
  styleUrls: ['./built-view.component.scss']
})
export class BuiltViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
