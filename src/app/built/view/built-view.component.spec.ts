import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltViewComponent } from './built-view.component';

describe('BuiltViewComponent', () => {
  let component: BuiltViewComponent;
  let fixture: ComponentFixture<BuiltViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuiltViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiltViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
