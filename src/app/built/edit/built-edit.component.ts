import { Component, Input, OnChanges, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { TINY_API_KEY } from '../../.constances/credentials';
import { BuiltService } from '../../.services/built.service';

import * as moment from 'moment';
import { ThemeService } from '../../.services/theme.service';

@Component({
  selector: 'abstract-built-edit',
  templateUrl: './built-edit.component.html',
  styleUrls: ['./built-edit.component.scss']
})
export class BuiltEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  @Input('builder') builder: any = null;
  @Input('data') data: any = null;

  moment = moment;

  tinyAPIKey = TINY_API_KEY;
  tinyPlugins = 'advlist autolink link image lists charmap preview anchor pagebreak searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking table directionality emoticons code';
  tinyToolbar = 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | forecolor backcolor color | print preview | code';

  /* instances */
  path = '/group';
  formGroup: FormGroup;
  id = null;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  controlsSettings = {
    mode: 'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      module: {
        title: 'Module',
        type: 'custom',
        renderComponent: TableListModuleComponent,
        editor: {
          type: 'custom',
          component: TableListModuleEditComponent,
        }
      },
      rules: {
        title: 'Rules',
        type: 'custom',
        renderComponent: TableListRuleComponent,
        editor: {
          type: 'custom',
          component: TableListRuleEditComponent,
        }
      },
      behaviors: {
        title: 'Behaviors',
        type: 'custom',
        renderComponent: TableListBehaviorComponent,
        editor: {
          type: 'custom',
          component: TableListBehaviorEditComponent,
        }
      }
    },
  };

  imageUploaders: any = {};
  filesImagesPreview = {};

  formImages: any = [];
  formFiles: any = {};
  formFilesRemove: any = {};

  constructor(
    private router: Router,
    private theme: ThemeService,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private built: BuiltService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {
      if (this.builder) {
        this.path = `/built/${this.builder.key}`;
        this.initializeForm();
        this.changed = false;
      }
      resolve();
    });
  }

  async initializeForm() {
    if (this.builder && this.data) {
      let formGroup: any = {};
      const buildForm = async (references) => {
        for (let reference of references) {
          if (
            reference.type != 'input-multiple'
            && reference.type != 'checkbox'
            && reference.type != 'checkbox-inline'
            && reference.type != 'image-upload'
            && reference.type != 'input-file'
            && reference.type != 'input-file-multiple'
            && reference.type != 'input-file-multiple-drop'
          ) {
            let validators = [];
            if (reference.require == '1') {
              await validators.push(Validators.required);
            }
            let value = (this.data[reference.key]) ? this.data[reference.key] : null;
            let properties: any = value;
            if (reference.disabled == '1') {
              properties = {
                value: '1',
                disabled: true
              };
            }
            formGroup[reference.key] = [
              properties,
              validators
            ];
          } else if (
            reference.type == 'checkbox'
            || reference.type == 'checkbox-inline'
          ) {

          } else if (reference.type == 'image-upload') {
            formGroup[reference.key] = [(this.data[reference.key]) ? this.data[reference.key] : null];
            this.imageUploaders[reference.key] = {
              name: reference.key,
              path: `${reference.key}_path`,
              option: {
                label: reference.placeholder ? reference.placeholder : 'Select',
                labelLoading: 'Loading...',
                statusFileType: 'Invalid file type',
                statusFileSize: 'File is too big',
                statusNoSupport: 'Your browser does not support image cropping',
                statusImageTooSmall: 'Image is too small',
                statusContentLength: 'File is probably too big',
                statusUnknownResponse: 'An unknown error occurred',
                statusUploadSuccess: 'Saved',
                buttonCancelLabel: 'Cancel',
                buttonConfirmLabel: 'Confirm',
                buttonRotateTitle: 'Rotate',
                download: false,
                push: true,
                instantEdit: false,
                devicePixelRatio: 'auto',
                initialImage: (this.data[`${reference.key}_path`]) ? this.data[`${reference.key}_path`] : null,
                service: (formData: any, progress: any, success: any, failure: any, slim: any) => {
                  progress(0, 100);
                  if (!this.formImages[reference.key]) {
                    this.formImages[reference.key] = new FormData();
                  }
                  formData.append('target', `slim_output_${slim._uid}`);
                  formData.append(`${reference.key}_old`, '');
                  this.formImages[reference.key] = formData;
                  progress(100, 100);
                  slim._stopProgress();
                  slim._showButtons();
                },
                didRemove: (data: any, slim: any) => {
                  if (!this.formImages[reference.key]) {
                    this.formImages[reference.key] = new FormData();
                  }
                  if (this.formImages[reference.key].has(`${reference.key}_old`)) {
                    this.formImages[reference.key].set(`${reference.key}_old`, '');
                  } else {
                    this.formImages[reference.key].append(`${reference.key}_old`, '');
                  }
                  slim._stopProgress();
                }
              }
            };
          } else if (reference.type == 'input-file-multiple-drop') {
            formGroup[reference.key] = [(this.data[reference.key]) ? this.data[reference.key] : null];
            this.formFiles[reference.key] = [] as File[];
          }
        }
      }
      if (this.builder.abstract) {
        if (this.builder.abstract.references) {
          if (Array.isArray(this.builder.abstract.references) && this.builder.abstract.references.length) {
            await buildForm(this.builder.abstract.references);
          }
        }
      }
      this.formGroup = await this.formBuilder.group(formGroup);
      this.formGroup.valueChanges.subscribe(() => {
        this.changed = true;
      });
    }
  }

  onMultipleFilesDrop(key, event) {
    if (!this.formFiles[key]) {
      this.formFiles[key] = [] as File[];
    }
    for (let file of event.addedFiles) {
      if (file.type.indexOf("image/") === 0) {
        if (!this.filesImagesPreview[key]) {
          this.filesImagesPreview[key] = {} as any;
        }
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.filesImagesPreview[key][file.name] = reader.result;
        };
      }
    }
    this.formFiles[key].push(...event.addedFiles);
  }

  onMultipleFilesRemove(key, event) {
    if (this.formFiles[key] && this.formFiles[key].length) {
      this.formFiles[key].splice(this.formFiles[key].indexOf(event), 1);
    }
  }

  onMultipleFilesOldRemove(key, event) {
    if (!this.formFilesRemove[key]) {
      this.formFilesRemove[key] = [];
    }
    this.formFilesRemove[key].push(event);
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      if (this.data) {
        let params = {
          id: this.data.id,
          active: this.active
        };
        const buildParams = async (references) => {
          for (let reference of references) {
            if (!reference.pseudo) {
              if (
                reference.type != 'input-multiple'
                && reference.type != 'checkbox'
                && reference.type != 'checkbox-inline'
              ) {
                params[reference.key] = value[reference.key] ? value[reference.key] : '';
              } else {
                params[reference.key] = '';
              }
              console.log(reference.key, params[reference.key]);
            }
          }
        }
        if (this.builder.abstract) {
          if (this.builder.abstract.references) {
            if (Array.isArray(this.builder.abstract.references) && this.builder.abstract.references.length) {
              await buildParams(this.builder.abstract.references);
            }
          }
        }
        this.built.update(this.builder.service, this.builder.key, params).then(async (response: any) => {

          let files = 0;
          let drops = 0;
          let images = 0;
          let errors = [];

          // Remove files
          if (Object.keys(this.formFilesRemove).length) {
            const remove = async (formFilesRemove) => {
              for (let key of Object.keys(formFilesRemove)) {
                await this.built.remove(this.builder.service, this.builder.key, key, response.id, formFilesRemove[key]).catch(async errorMessage => {
                  await errors.push(errorMessage);
                });
              }
            }
            await remove(this.formFilesRemove);
          }

          const prepare = async (references) => {
            for (let reference of references) {
              if (
                reference.type == 'input-file' 
                || reference.type == 'input-file-multiple'
              ) {
                files += 1;
              } else if (reference.type == 'input-file-multiple-drop') {
                drops += 1;
              } else if (reference.type == 'image-upload') {
                images += 1;
              }
            }
          }
          if (this.builder.abstract) {
            if (this.builder.abstract.references) {
              if (Array.isArray(this.builder.abstract.references) && this.builder.abstract.references.length) {
                await prepare(this.builder.abstract.references);
              }
            }
          }

          if (files) {
            const forms: any = document.getElementsByName("form_built");
            if (forms[0]) {
              let formData = new FormData(forms[0]);
              await formData.append('id', response.id);
              await this.built.upload(
                this.builder.service, 
                this.builder.key, 
                formData
              ).catch(async errorMessage => {
                await errors.push(errorMessage);
              });
              forms[0].reset();
              this.initialize(true);
            }
          }

          if (drops) {
            const uploadFiles = async () => {
              if (Object.keys(this.formFiles).length) {
                const upload = async (formFiles) => {
                  for (let key of Object.keys(formFiles)) {
                    const uploadByOne = async () => {
                      for (let file of formFiles[key]) {
                        let formData = new FormData();
                        await formData.append('id', response.id);
                        await formData.append(`${key}[]`, file);
                        await this.built.upload(
                          this.builder.service, 
                          this.builder.key, 
                          formData
                        ).catch(async errorMessage => {
                          await errors.push(errorMessage);
                        });
                      }
                    }
                    await uploadByOne();
                  }
                }
                await upload(this.formFiles);
              }
            }
            await uploadFiles();
          }

          if (images) {
            const uploadImages = async () => {
              let formData = new FormData();
              if (Object.keys(this.formImages).length) {
                const upload = async (formImages) => {
                  for (let key of Object.keys(formImages)) {
                    const infoString = await formImages[key].get(key);
                    if (infoString) {
                      const info = JSON.parse(infoString);
                      if (info && info.output) {
                        const image = await formImages[key].get(info.output.field);
                        await formData.append('id', response.id);
                        await formData.append(key, image);
                      }
                    }
                  }
                }
                await upload(this.formImages);
                await this.built.upload(
                  this.builder.service, 
                  this.builder.key, 
                  formData
                ).catch(async errorMessage => {
                  await errors.push(errorMessage);
                });
              }
            }
            await uploadImages();
          }

          if (!errors.length) {
            resolve(response.message);
          } else {
            reject('Found some errors');
          }
        }).catch((error) => {
          reject(error.message);
        });
      }
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}
