import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltEditComponent } from './built-edit.component';

describe('BuiltEditComponent', () => {
  let component: BuiltEditComponent;
  let fixture: ComponentFixture<BuiltEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuiltEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiltEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
