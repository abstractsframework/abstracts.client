import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuiltCreateComponent } from './built-create.component';

describe('BuiltCreateComponent', () => {
  let component: BuiltCreateComponent;
  let fixture: ComponentFixture<BuiltCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuiltCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuiltCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
