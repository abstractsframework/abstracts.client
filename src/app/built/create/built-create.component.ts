import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { GroupService } from '../../.services/group.service';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { TINY_API_KEY } from '../../.constances/credentials';
import { BuiltService } from '../../.services/built.service';

@Component({
  selector: 'abstract-built-create',
  templateUrl: './built-create.component.html',
  styleUrls: ['./built-create.component.scss']
})
export class BuiltCreateComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  @Input('builder') builder: any = null;

  tinyAPIKey = TINY_API_KEY;
  tinyPlugins = 'advlist autolink link image lists charmap preview anchor pagebreak searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking table directionality emoticons code';
  tinyToolbar = 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link unlink anchor | forecolor backcolor color | print preview | code';

  /* instances */
  path = '/group';
  formGroup: FormGroup;
  id = null;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  controlsSettings = {
    mode: 'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      module: {
        title: 'Module',
        type: 'custom',
        renderComponent: TableListModuleComponent,
        editor: {
          type: 'custom',
          component: TableListModuleEditComponent,
        }
      },
      rules: {
        title: 'Rules',
        type: 'custom',
        renderComponent: TableListRuleComponent,
        editor: {
          type: 'custom',
          component: TableListRuleEditComponent,
        }
      },
      behaviors: {
        title: 'Behaviors',
        type: 'custom',
        renderComponent: TableListBehaviorComponent,
        editor: {
          type: 'custom',
          component: TableListBehaviorEditComponent,
        }
      }
    },
  };

  imageUploaders: any = {};
  filesImagesPreview = {};

  formImages: any = [];
  formFiles: any = {};
  formFilesRemove: any = {};

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private group: GroupService,
    private built: BuiltService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      if (this.builder) {
        this.path = `/built/${this.builder.key}`;
        this.initializeForm();
        this.changed = false;
      }
      resolve();
    });
  }

  async initializeForm() {
    if (this.builder) {
      let formGroup: any = {};
      const buildForm = async (references) => {
        for (let reference of references) {
          console.log(reference);
          if (
            reference.type != 'input-multiple'
            && reference.type != 'checkbox'
            && reference.type != 'checkbox-inline'
            && reference.type != 'image-upload'
            && reference.type != 'input-file'
            && reference.type != 'input-file-multiple'
            && reference.type != 'input-file-multiple-drop'
          ) {
            let validators = [];
            if (reference.require == '1') {
              await validators.push(Validators.required);
            }
            let value = null;
            if (
              reference.type == 'checkbox-radio'
              || reference.type == 'switch'
            ) {
              if (reference.default_switch == '1') {
                if (reference.values && reference.values.length) {
                  value = reference.values[0];
                } else {
                  value = '1';
                }
              }
            }
            let properties: any = value;
            if (reference.disabled == '1') {
              properties = {
                value: '1',
                disabled: true
              };
            }
            formGroup[reference.key] = [
              properties,
              validators
            ];
          } else if (
            reference.type == 'checkbox'
            || reference.type == 'checkbox-inline'
          ) {

          } else if (reference.type == 'image-upload') {
            formGroup[reference.key] = [''];
            this.imageUploaders[reference.key] = {
              name: reference.key,
              path: `${reference.key}_path`,
              option: {
                label: reference.placeholder ? reference.placeholder : 'Select',
                labelLoading: 'Loading...',
                statusFileType: 'Invalid file type',
                statusFileSize: 'File is too big',
                statusNoSupport: 'Your browser does not support image cropping',
                statusImageTooSmall: 'Image is too small',
                statusContentLength: 'File is probably too big',
                statusUnknownResponse: 'An unknown error occurred',
                statusUploadSuccess: 'Saved',
                buttonCancelLabel: 'Cancel',
                buttonConfirmLabel: 'Confirm',
                buttonRotateTitle: 'Rotate',
                download: false,
                push: true,
                instantEdit: false,
                devicePixelRatio: 'auto',
                initialImage: null,
                service: (formData: any, progress: any, success: any, failure: any, slim: any) => {
                  progress(0, 100);
                  if (!this.formImages[reference.key]) {
                    this.formImages[reference.key] = new FormData();
                  }
                  formData.append('target', `slim_output_${slim._uid}`);
                  formData.append(`${reference.key}_old`, '');
                  this.formImages[reference.key] = formData;
                  progress(100, 100);
                  slim._stopProgress();
                  slim._showButtons();
                },
                didRemove: (data: any, slim: any) => {
                  if (!this.formImages[reference.key]) {
                    this.formImages[reference.key] = new FormData();
                  }
                  if (this.formImages[reference.key].has(`${reference.key}_old`)) {
                    this.formImages[reference.key].set(`${reference.key}_old`, '');
                  } else {
                    this.formImages[reference.key].append(`${reference.key}_old`, '');
                  }
                  slim._stopProgress();
                }
              }
            };
          } else if (reference.type == 'input-file-multiple-drop') {
            formGroup[reference.key] = [''];
            this.formFiles[reference.key] = [] as File[];
          }
        }
      }
      if (this.builder.abstract) {
        if (this.builder.abstract.references) {
          if (Array.isArray(this.builder.abstract.references) && this.builder.abstract.references.length) {
            await buildForm(this.builder.abstract.references);
          }
        }
      }
      console.log(formGroup);
      this.formGroup = await this.formBuilder.group(formGroup);
      this.formGroup.valueChanges.subscribe(() => {
        this.changed = true;
      });
    }
  }

  onMultipleFilesDrop(key, event) {
    if (!this.formFiles[key]) {
      this.formFiles[key] = [] as File[];
    }
    for (let file of event.addedFiles) {
      if (file.type.indexOf("image/") === 0) {
        if (!this.filesImagesPreview[key]) {
          this.filesImagesPreview[key] = {} as any;
        }
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.filesImagesPreview[key][file.name] = reader.result;
        };
      }
    }
    this.formFiles[key].push(...event.addedFiles);
    console.log(this.formFiles);
  }

  onMultipleFilesRemove(key, event) {
    if (this.formFiles[key] && this.formFiles[key].length) {
      this.formFiles[key].splice(this.formFiles[key].indexOf(event), 1);
    }
  }

  onMultipleFilesOldRemove(key, event) {
    if (!this.formFilesRemove[key]) {
      this.formFilesRemove[key] = [];
    }
    this.formFilesRemove[key].push(event);
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.router.navigate([this.path]);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      let params = {
        active: this.active
      };
      const buildParams = async (references) => {
        for (let reference of references) {
          if (!reference.pseudo) {
            if (
              reference.type != 'input-multiple'
              && reference.type != 'checkbox'
              && reference.type != 'checkbox-inline'
            ) {
              params[reference.key] = value[reference.key] ? value[reference.key] : '';
            } else {
              params[reference.key] = '';
            }
          }
        }
      }
      if (this.builder.abstract) {
        if (this.builder.abstract.references) {
          if (Array.isArray(this.builder.abstract.references) && this.builder.abstract.references.length) {
            await buildParams(this.builder.abstract.references);
          }
        }
      }
      this.built.create(this.builder.service, this.builder.key, params).then(async (response: any) => {
        let errors = [];
        if (Object.keys(this.formFilesRemove).length) {
          const remove = async (formFilesRemove) => {
            for (let key of Object.keys(formFilesRemove)) {
              await this.built.remove(this.builder.service, this.builder.key, key, response.id, formFilesRemove[key]).catch(async errorMessage => {
                await errors.push(errorMessage);
              });
            }
          }
          await remove(this.formFilesRemove);
        }
        console.log(this.formFiles);
        if (Object.keys(this.formFiles).length) {
          // this.formFiles.id = response.id;
          // await this.built.upload(this.builder.service, this.builder.key, this.formFiles).then(async (response: any) => {
          //   if (!response || !response.status) {
          //     if (response.message) {
          //       await errors.push(response.message);
          //     } else {
          //       await errors.push(false);
          //     }
          //   }
          // }).catch(async errorMessage => {
          //   await errors.push(errorMessage);
          // });
          const upload = async (formFiles) => {
            for (let key of Object.keys(formFiles)) {
              const formData = new FormData();
              await formData.append('id', response.id);
              // console.log(formFiles[key].name);
              // await formData.append(key, formFiles[key]);
              const element: any = document.querySelector(`#${key}`);
              if (element) {
                await element.setAttribute('name', key);
                await formData.append(key, element.files);
                console.log(element);
                console.log(element.files);
                await this.built.upload(this.builder.service, this.builder.key, formData).catch(async errorMessage => {
                  await errors.push(errorMessage);
                });
              }
              console.log(formData);
            }
          }
          await upload(this.formFiles);
        }
        console.log(this.formImages);
        if (Object.keys(this.formImages).length) {
          const upload = async (formImages) => {
            for (let key of Object.keys(formImages)) {
              await formImages[key].append('id', response.id);
              await this.built.upload(this.builder.service, this.builder.key, formImages[key]).catch(async errorMessage => {
                await errors.push(errorMessage);
              });
            }
          }
          await upload(this.formImages);
        }
        if (!errors.length) {
          resolve(response.message);
        } else {
          reject('Found some errors');
        }
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}
