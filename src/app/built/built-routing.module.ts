import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BuiltCreateComponent } from './create/built-create.component';
import { BuiltEditComponent } from './edit/built-edit.component';
import { BuiltComponent } from './built.component';
import { BuiltTableComponent } from './table/built-table.component';
import { BuiltViewComponent } from './view/built-view.component';

const routes: Routes = [
  {
    path: ':modulePath',
    component: BuiltComponent,
    children: [
      {
        path: '',
        component: BuiltComponent
      },
      {
        path: 'active',
        component: BuiltTableComponent
      },
      {
        path: 'inactive',
        component: BuiltTableComponent
      },
      {
        path: 'create',
        component: BuiltCreateComponent
      },
      {
        path: ':id',
        component: BuiltViewComponent
      },
      {
        path: ':id/edit',
        component: BuiltEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuiltRoutingModule { }
