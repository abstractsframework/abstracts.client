import { NbMenuItem } from '@nebular/theme';

export const MENU_DASHBOARD: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/',
  }
];

export const MENU_ABSTRACT: NbMenuItem[] = [
  {
    title: 'Management',
    group: true,
  },
  {
    title: 'Projects',
    icon: 'radio-button-off-outline',
    link: '/projects',
    children: [
      {
        title: 'Projects',
        link: '/projects',
      },
      {
        title: 'New project',
        icon: 'plus-outline',
        link: '/projects/setup',
      }
    ]
  },
  {
    title: 'Commerce',
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'Payment',
        icon: 'credit-card-outline',
        link: '/payment',
      },
      {
        title: 'Purchase',
        icon: 'shopping-bag-outline',
        link: '/purchase',
      },
      {
        title: 'Transaction',
        icon: 'flip-2-outline',
        link: '/transaction',
      },
      {
        title: 'In-ad',
        icon: 'radio-outline',
        link: '/payment',
      }
    ],
  },
  {
    title: 'Users',
    icon: 'people-outline',
    children: [
      {
        title: 'User',
        icon: 'person-outline',
        link: '/user',
      },
      {
        title: 'Group',
        icon: 'people-outline',
        link: '/group',
      },
      {
        title: 'Control',
        icon: 'shield-outline',
        link: '/control',
      },
      {
        title: 'Device',
        icon: 'smartphone-outline',
        link: '/control',
      },
      {
        title: 'Connect',
        icon: 'link-outline',
        link: '/control',
      },
      {
        title: 'Audience',
        icon: 'eye-outline',
        link: '/control',
      }
    ],
  },
  {
    title: 'Directory',
    icon: 'folder-outline',
    children: [
      {
        title: 'Page',
        icon: 'file-text-outline',
        link: '/page',
      },
      {
        title: 'Media',
        icon: 'image-outline',
        link: '/media',
      }
    ],
  },
  {
    title: 'System',
    icon: 'layers-outline',
    children: [
      {
        title: 'Abstract',
        icon: 'cube-outline',
        link: '/abstract',
      },
      {
        title: 'Module',
        icon: 'grid-outline',
        link: '/module',
      },
      {
        title: 'API',
        icon: 'cast-outline',
        link: '/api',
      },
      {
        title: 'Hash',
        icon: 'lock-outline',
        link: '/hash',
      },
      {
        title: 'Language',
        icon: 'globe-2-outline',
        link: '/language',
      },
      {
        title: 'Log',
        icon: 'activity-outline',
        link: '/log',
      },
      {
        title: 'Process',
        icon: 'play-circle-outline',
        link: '/log',
      },
      {
        title: 'Configuration',
        icon: 'options-2-outline',
        link: '/settings',
      },
      {
        title: 'Document',
        icon: 'book-outline',
        link: '/document',
      }
    ],
  },
  {
    title: 'Settings',
    icon: 'settings-2-outline',
    link: '/settings',
  }
];
