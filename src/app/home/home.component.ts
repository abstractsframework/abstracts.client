import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ModuleService } from '../.services/module.service';
import { ComponentsService } from '../.services/components.service';

import { MENU_DASHBOARD, MENU_ABSTRACT } from './home-menu';
import { ProjectsService } from '../.services/projects.service';
import { Subscription } from 'rxjs';
import { NbLayoutScrollService } from '@nebular/theme';
import { ThemeService } from '../.services/theme.service';

@Component({
  selector: 'abstract-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  menu = [];
  categories = [];
  categoriesOther = {
    title: 'Other',
    subjects: []
  };

  private switchSubscription: Subscription;
  private changeSubscription: Subscription;
  private resetScrollSubscription: Subscription;

  constructor(
    private components: ComponentsService,
    private module: ModuleService,
    private projects: ProjectsService,
    private theme: ThemeService,
    private nbLayoutScrollService: NbLayoutScrollService,
    private changeRef: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    this.initialize();
    this.subscribeSwitch();
    this.subscribeChange();
    this.subscribeResetScroll();
  }

  ngOnDestroy(): void {
    this.initialize();
    this.unsubscribeSwitch();
    this.unsubscribeChange();
    this.unsubscribeResetScroll();
  }

  async initialize() {

    let menu: any = this.menu;
    let menuUser = [];

    const formatMenu = async (items) => {

      const formatIcon = (itemIcon) => {
        let icon = null;
        if (itemIcon) {
          if (itemIcon.indexOf('si ') === 0) {
            icon = { icon: itemIcon.replace('si ', ''), pack: 'simple-line' }
          } else 
          if (
            itemIcon.indexOf('fa ') === 0 
            || itemIcon.indexOf('far ') === 0
            || itemIcon.indexOf('fas ') === 0
          ) {
            let pack = 'fontawesome';
            if (itemIcon.indexOf('fas') === 0) {
              pack = 'fontawesome-solid';
            } else if (itemIcon.indexOf('far') === 0) {
              pack = 'fontawesome-regular';
            } else if (itemIcon.indexOf('fab') === 0) {
              pack = 'fontawesome-regular';
            }
            icon = { 
              icon: itemIcon.replace('fa ', '').replace('far ', '').replace('fas ', '').replace(' fa-fw', ''), 
              pack: pack
            }
          } else if (itemIcon.indexOf('eva ')) {
            icon = { 
              icon: itemIcon.replace('eva ', '')
            }
          }
        }
        return icon;
      }

      let categories = [];
      let categoriesOther = JSON.parse(JSON.stringify(this.categoriesOther));
      for (let item of items) {
        if (item.id > 100) {
          if (item.category) {
            const checkCategoryExist = async (itemCategory) => {
              let exists = null;
              let index = 0;
              for (let category of categories) {
                if (category.title == itemCategory) {
                  exists = index;
                }
                index += 1;
              }
              return exists;
            }
            const categoryExists = await checkCategoryExist(item.category);
            if (categoryExists === null) {
              await categories.push({
                title: item.category,
                subjects: [
                  {
                    title: item.subject,
                    icon: item.subject_icon,
                    items: [item]
                  }
                ]
              });
            } else {
              const checkSubjectExist = async (categoryIndex, itemSubject) => {
                let exists = null;
                let index = 0;
                for (let subject of categories[categoryIndex].subjects) {
                  if (subject.title == itemSubject) {
                    exists = index;
                  }
                  index += 1;
                }
                return exists;
              }
              const subjectExists = await checkSubjectExist(categoryExists, item.subject);
              if (subjectExists === null) {
                await categories[categoryExists].subjects.push(
                  {
                    title: item.subject,
                    icon: item.subject_icon,
                    items: [item]
                  }
                );
              } else {
                await categories[categoryExists].subjects[subjectExists].items.push(item);
              }
            }
          } else {
            const checkSubjectExist = async (itemSubject) => {
              let exists = null;
              let index = 0;
              for (let subject of categoriesOther.subjects) {
                if (subject.title == itemSubject) {
                  exists = index;
                }
                index += 1;
              }
              return exists;
            }
            const subjectExists = await checkSubjectExist(item.subject);
            if (subjectExists === null) {
              await categoriesOther.subjects.push(
                {
                  title: item.subject,
                  icon: item.subject_icon,
                  items: [item]
                }
              );
            } else {
              await categoriesOther.subjects[subjectExists].items.push(item);
            }
          }
        }
      }
      if (categoriesOther && categoriesOther.subjects && categoriesOther.subjects.length) {
        await categories.push(categoriesOther);
      }

      this.categories = categories;

      const setMenu = async () => {
        for (let category of this.categories) {
          await menuUser.push({
            title: category.title,
            group: true
          });
          const setSubject = async (subjects) => {
            for (let subject of subjects) {
              if (subject.title) {
                let formattedItems = [];
                const formatItem = async (items) => {
                  for (let item of items) {
                    await formattedItems.push({
                      title: item.name,
                      icon: formatIcon(item.icon),
                      link: `/built/${item.key.replace(/_/g, '-')}`
                    });
                  }
                }
                await formatItem(subject.items);
                await menuUser.push({
                  title: subject.title,
                  icon: formatIcon(subject.icon),
                  children: formattedItems
                });
              } else {
                const setItem = async (items) => {
                  for (let item of items) {
                    await menuUser.push({
                      title: item.name,
                      icon: formatIcon(item.icon),
                      link: `/built/${item.key.replace(/_/g, '-')}`
                    });
                  }
                }
                await setItem(subject.items);
              }
            }
          }
          await setSubject(category.subjects);
        }
      }
      await setMenu();
      
    }

    await this.module.list('1', { by: 'order', direction: 'asc' }).then(async (response: any) => {
      const menuResponse: Array<any> = response;
      await formatMenu(menuResponse);
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', error.message);
    });
    menu = [...MENU_DASHBOARD, ...menuUser, ...MENU_ABSTRACT];

    this.menu = menu;

  }

  onItemClick(item) {

  }

  onHoverItem(item) {
    
  }

  onSelectItem(item) {
    
  }

  private subscribeSwitch() {
    this.switchSubscription = this.projects.onSwitch.subscribe(async (data: any) => {
      if (data) {
        await this.initialize();
        this.changeRef.detectChanges();
      }
    });
  }

  private unsubscribeSwitch() {
    this.switchSubscription.unsubscribe();
  }

  private subscribeChange() {
    this.changeSubscription = this.module.onChange.subscribe(async (data: any) => {
      if (data) {
        this.menu = [];
        this.categories = [];
        await this.initialize();
        this.changeRef.detectChanges();
      }
    });
  }

  private unsubscribeChange() {
    this.changeSubscription.unsubscribe();
  }

  private subscribeResetScroll() {
    this.resetScrollSubscription = this.theme.onResetScroll.subscribe(async (data: any) => {
      if (data) {
        this.nbLayoutScrollService.scrollTo(0, 0);
      }
    });
  }

  private unsubscribeResetScroll() {
    this.resetScrollSubscription.unsubscribe();
  }

}
