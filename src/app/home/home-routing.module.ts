import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';

import { PaymentComponent } from '../payment/payment.component';
import { PurchaseComponent } from '../purchase/purchase.component';
import { TransactionComponent } from '../transaction/transaction.component';

import { AuthenticationService } from '../.services/authentication.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthenticationService],
    resolve: {
      authSession: AuthenticationService
    },
    children: [
      {
        path: '',
        loadChildren: () => import('../dashboard/dashboard.module')
          .then(m => m.DashboardModule),
      },
      {
        path: 'projects',
        loadChildren: () => import('../projects/projects.module')
          .then(m => m.ProjectsModule),
      },
      {
        path: 'projects/setup',
        loadChildren: () => import('../setup/setup.module')
          .then(m => m.SetupModule),
      },
      {
        path: 'projects/login',
        loadChildren: () => import('../login/login.module')
          .then(m => m.LoginModule),
      },
      {
        path: 'built',
        loadChildren: () => import('../built/built.module')
          .then(m => m.builtModule),
      },
      {
        path: 'document',
        loadChildren: () => import('../document/document.module')
          .then(m => m.DocumentModule),
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module')
          .then(m => m.SettingsModule),
      },
      {
        path: 'abstract',
        loadChildren: () => import('../abstract/abstract.module')
          .then(m => m.Abstracts),
      },
      {
        path: 'module',
        loadChildren: () => import('../module/module.module')
          .then(m => m.ModuleModule),
      },
      {
        path: 'user',
        loadChildren: () => import('../user/user.module')
          .then(m => m.UserModule),
      },
      {
        path: 'group',
        loadChildren: () => import('../group/group.module')
          .then(m => m.GroupModule),
      },
      {
        path: 'language',
        loadChildren: () => import('../language/language.module')
          .then(m => m.LanguageModule),
      },
      {
        path: 'page',
        loadChildren: () => import('../page/page.module')
          .then(m => m.PageModule),
      },
      {
        path: 'media',
        loadChildren: () => import('../media/media.module')
          .then(m => m.MediaModule),
      },
      {
        path: 'log',
        loadChildren: () => import('../log/log.module')
          .then(m => m.LogModule),
      },
      {
        path: 'api',
        loadChildren: () => import('../api/api.module')
          .then(m => m.ApiModule),
      },
      {
        path: 'hash',
        loadChildren: () => import('../hash/hash.module')
          .then(m => m.HashModule),
      },
      {
        path: 'control',
        loadChildren: () => import('../control/control.module')
          .then(m => m.ControlModule),
      },
      {
        path: 'payment',
        component: PaymentComponent,
      },
      {
        path: 'purchase',
        component: PurchaseComponent,
      },
      {
        path: 'transaction',
        component: TransactionComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
