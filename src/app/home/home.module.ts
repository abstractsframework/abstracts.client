import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbLayoutModule, NbMenuModule, NbSidebarModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';

// import { ECommerceModule } from '../pages/e-commerce/e-commerce.module';
// import { MiscellaneousModule } from '../pages/miscellaneous/miscellaneous.module';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    NbLayoutModule,
    NbSidebarModule,
    
    // ECommerceModule,
    // MiscellaneousModule,
    HomeRoutingModule,
  ]
})
export class HomeModule { }
