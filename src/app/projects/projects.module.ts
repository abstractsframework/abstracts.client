import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './projects.component';
import { FormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbIconModule, NbListModule } from '@nebular/theme';


@NgModule({
  declarations: [
    ProjectsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbListModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    ProjectsRoutingModule
  ]
})
export class ProjectsModule { }
