import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../.services/authentication.service';
import { ComponentsService } from '../.services/components.service';
import { ProjectsService } from '../.services/projects.service';
import { RestService } from '../.services/rest.service';
import { StorageService } from '../.services/storage.service';
import { UserService } from '../.services/user.service';

@Component({
  selector: 'abstract-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit, OnDestroy {

  items: any = [];
  currentProject = null;
  currentProjectSession = null;

  private switchSubscription: Subscription;

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private components: ComponentsService,
    private storage: StorageService,
    public user: UserService,
    private projects: ProjectsService,
    private rest: RestService
  ) { }

  ngOnInit(): void {
    this.load();
    this.subscribeSwitch();
  }

  ngOnDestroy(): void {
    this.unsubscribeSwitch();
  }

  async load() {
    const currentProject = await this.storage.get('current-project');
    if (currentProject) {
      this.currentProject = parseInt(currentProject);
      this.currentProjectSession = await this.storage.get('session.' + currentProject, true, true);
    }
    const projects: any = await this.storage.get('projects', true, true);
    if (projects && projects.length) {
      this.items = projects;
    }
  }

  login() {
    this.router.navigate(['./login']);
  }

  async remove(index) {
    const projects = await this.storage.get('projects', true, true);
    if (projects && projects.length && projects[index]) {
      await projects.splice(index, 1);
      await this.storage.set('projects', projects, true, true);
      await this.storage.set('current-project', '0');
      await this.storage.remove('session.', index.toString());
      this.auth.session = null;
      await this.auth.initialize();
      this.load();
      this.components.showToastStatus('success', 'Success', 'Removed project');
    } else {
      this.components.showToastStatus('danger', 'Error', 'Project not found');
    }
  }

  async switch(index) {
    await this.storage.set('current-project', await index.toString());
    this.rest.initialize().then(async () => {
      await this.load();
      this.projects.switchObservable.next(index);
      this.components.showToastStatus('success', 'Success', 'Switched project');
    });
  }

  private subscribeSwitch() {
    this.switchSubscription = this.projects.onSwitch.subscribe(async (data: any) => {
      if (data) {
        await this.load();
        this.currentProject = data;
      }
    });
  }

  private unsubscribeSwitch() {
    this.switchSubscription.unsubscribe();
  }

}
