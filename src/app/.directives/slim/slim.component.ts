/*
 * Slim v5.3.1 - Image Cropping Made Easy
 * Copyright (c) 2020 Rik Schennink - https://pqina.nl/slim
 */
declare var require: any;

const SlimLib = require('./slim.commonjs');

import { DOCUMENT } from '@angular/common';
// Angular core
import { ViewChild, Component, Input, ElementRef, OnInit, AfterViewInit, Inject } from '@angular/core';

@Component({
	selector: 'slim',
	template: '<div #root><ng-content></ng-content></div>'
})

export class Slim implements OnInit, AfterViewInit {

	@ViewChild('root') element: ElementRef;

	@Input('options') options: any = {};
	
  constructor(
    @Inject(DOCUMENT) private doc
	) {

	}

	ngOnInit() {}

	ngAfterViewInit() {

		if (this.options.initialImage) {
			const img = this.doc.createElement('img');
			img.setAttribute('alt', '');
			img.src = this.options.initialImage;
			this.element.nativeElement.appendChild(img);
		}

		SlimLib.create(this.element.nativeElement, this.options);
	}

};