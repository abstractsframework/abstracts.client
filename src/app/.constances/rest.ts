export const URL_STRATEGY: string = '/';

export const URL: string = 'api/';

export const URL_API = URL + 'api' + URL_STRATEGY;
export const GET_KEY: string = URL_API + 'get_secret';
export const GET_API_ALL: string = URL_API + 'list';
export const GET_API_BY_ID: string = URL_API + 'get';
export const CREATE_API: string = URL_API + 'create';
export const UPDATE_API: string = URL_API + 'update';
export const PATCH_API: string = URL_API + 'patch';
export const DELETE_API: string = URL_API + 'delete';
export const GET_API_SCOPE_ALL: string = URL_API + 'get_scope_all';

export const URL_ABSTRACT = URL + 'abstracts' + URL_STRATEGY;
export const GET_ABSTRACT_ALL: string = URL_ABSTRACT + 'list';
export const GET_ABSTRACT_BY_ID: string = URL_ABSTRACT + 'get';
export const CREATE_ABSTRACT: string = URL_ABSTRACT + 'create';
export const UPDATE_ABSTRACT: string = URL_ABSTRACT + 'update';
export const PATCH_ABSTRACT: string = URL_ABSTRACT + 'patch';
export const DELETE_ABSTRACT: string = URL_ABSTRACT + 'delete';
export const GENERATE_ABSTRACT: string = URL_ABSTRACT + 'generate';
export const REGENERATE_ABSTRACT: string = URL_ABSTRACT + 'regenerate';
export const SYNC_ABSTRACT: string = URL_ABSTRACT + 'sync_from';
export const IMPORT_ABSTRACT: string = URL_ABSTRACT + 'import';
export const EXPORT_ABSTRACT: string = URL_ABSTRACT + 'export';

export const URL_REFERENCE = URL + 'reference' + URL_STRATEGY;
export const GET_REFERENCE_ALL: string = URL_REFERENCE + 'list';
export const GET_REFERENCE_MULTI_ALL: string = URL_REFERENCE + 'get_input_multiple_all';
export const GET_REFERENCE_BY_ID: string = URL_REFERENCE + 'get';
export const CREATE_REFERENCE: string = URL_REFERENCE + 'create';
export const UPDATE_REFERENCE: string = URL_REFERENCE + 'update';
export const PATCH_REFERENCE: string = URL_REFERENCE + 'patch';
export const DELETE_REFERENCE: string = URL_REFERENCE + 'delete';
export const GET_REFERENCE_MODULE: string = URL_REFERENCE + 'list';
export const GET_REFERENCE_INPUT_LIST_DYNAMIC_MODULE_ALL: string = URL_REFERENCE + 'get_input_option_dynamic_all';
export const GET_REFERENCE_INPUT_LIST_DYNAMIC_ID_COLUMN_ALL: string = URL_REFERENCE + 'get_input_option_dynamic_value_key_all';
export const GET_REFERENCE_INPUT_LIST_DYNAMIC_VALUE_COLUMN_ALL: string = URL_REFERENCE + 'get_input_option_dynamic_label_key_all';

export const URL_MODULE = URL + 'module' + URL_STRATEGY;
export const GET_MODULE_ALL: string = URL_MODULE + 'list';
export const GET_MODULE_BY_ID: string = URL_MODULE + 'get';
export const CREATE_MODULE: string = URL_MODULE + 'create';
export const UPDATE_MODULE: string = URL_MODULE + 'update';
export const PATCH_MODULE: string = URL_MODULE + 'patch';
export const DELETE_MODULE: string = URL_MODULE + 'delete';
export const GET_MODULE_TEMPLATE_FILES: string = URL_MODULE + 'get_template_files';
export const GET_MODULE_SERVICE_FILES: string = URL_MODULE + 'get_service_files';

export const URL_PAGE = URL + 'page' + URL_STRATEGY;
export const GET_PAGE_ALL: string = URL_PAGE + 'get_page_all';
export const GET_PAGE_MAIN_ALL: string = URL_PAGE + 'get_page_main_language_all';
export const GET_PAGE_BY_ID: string = URL_PAGE + 'get_page_by_id';
export const CREATE_PAGE: string = URL_PAGE + 'create_page';
export const UPDATE_PAGE: string = URL_PAGE + 'update_page';
export const PATCH_PAGE: string = URL_PAGE + 'patch_page';
export const DELETE_PAGE: string = URL_PAGE + 'delete_page';

export const URL_USER = URL + 'user' + URL_STRATEGY;
export const LOGIN: string = URL_USER + 'login';
export const GET_USER_ALL: string = URL_USER + 'list';
export const GET_USER_BY_ID: string = URL_USER + 'get';
export const GET_USER_BY_SIGNIFICANT: string = URL_USER + 'get_by_significant';
export const UPLOAD_USER_FILES: string = URL_USER + 'upload_files';
export const FILE_CONTENT: string = URL_USER + 'content?';
export const CREATE_USER: string = URL_USER + 'create';
export const UPDATE_USER: string = URL_USER + 'update';
export const DELETE_USER: string = URL_USER + 'delete';
export const UPDATE_USER_PASSWORD: string = URL_USER + 'update_password';
export const RESET_USER_PASSWORD_EMAIL: string = URL_USER + 'request_password_reset_via_email';
export const RESET_USER_PASSWORD_PHONE: string = URL_USER + 'request_password_reset_via_phone';

export const URL_GROUP = URL + 'group' + URL_STRATEGY;
export const GET_GROUP_ALL: string = URL_GROUP + 'list';
export const GET_GROUP_BY_ID: string = URL_GROUP + 'get';
export const CREATE_GROUP: string = URL_GROUP + 'create';
export const UPDATE_GROUP: string = URL_GROUP + 'update';
export const PATCH_GROUP: string = URL_GROUP + 'patch';
export const PATCH_MULTI_GROUP: string = URL_GROUP + 'patch_multiple';
export const DELETE_GROUP: string = URL_GROUP + 'delete';
export const GET_GROUP_BY_MEMBER_USER: string = URL_GROUP + 'list_by_member_user';
export const GET_GROUP_MEMBER_BY_GROUP_ID: string = URL_GROUP + 'get_member_all_by_id';
export const CREATE_GROUP_MEMBER: string = URL_GROUP + 'create_member';
export const DELETE_GROUP_MEMBER: string = URL_GROUP + 'delete_member';

export const URL_DEVICE = URL + 'device' + URL_STRATEGY;
export const GET_DEVICE_ALL: string = URL_DEVICE + 'get_device_all';
export const GET_DEVICE_BY_ID: string = URL_DEVICE + 'get_device_by_id';
export const DELETE_DEVICE: string = URL_DEVICE + 'delete_device';
export const GET_DEVICE_BY_USER_ID: string = URL_DEVICE + 'get_device_all_by_user_id';

export const URL_CONNECT = URL + 'connect' + URL_STRATEGY;
export const GET_CONNECT_ALL: string = URL_CONNECT + 'get_connect_all';
export const GET_CONNECT_BY_ID: string = URL_CONNECT + 'get_connect_by_id';
export const DELETE_CONNECT: string = URL_CONNECT + 'delete_connect';
export const GET_CONNECT_BY_USER_ID: string = URL_CONNECT + 'get_connect_all_by_user_id';

export const URL_CONTROL = URL + 'control' + URL_STRATEGY;
export const GET_CONTROL_ALL: string = URL_CONTROL + 'get_control_all';
export const GET_CONTROL_BY_ID: string = URL_CONTROL + 'get_control_by_id';
export const CREATE_CONTROL: string = URL_CONTROL + 'create_control';
export const UPDATE_CONTROL: string = URL_CONTROL + 'update_control';
export const DELETE_CONTROL: string = URL_CONTROL + 'delete_control';
export const GET_CONTROL_BY_USER: string = URL_CONTROL + 'get_control_all_by_user';