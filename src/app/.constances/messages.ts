export const ERR_INVALID_RESPONSE: string 
= "Invalid response";

export const ERR_UNKNOWN_RESPONSE: string 
= "Unknown error";

export const SUCC_SUBMIT: string 
= "Successful";