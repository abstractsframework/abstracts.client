export const DEVELOPER_MODE = true;
export const ANIMATION: boolean = true;
export const API_CACHE: boolean = false;
export const LANGUAGES_PATH: string = '../assets/languages/';
export const STORAGE_KEY_PREFIX: string = 'abstract.';
export const TOAST_DURATION: number = 3000;