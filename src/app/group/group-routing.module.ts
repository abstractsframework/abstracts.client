import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupTableComponent } from './table/group-table.component';
import { GroupViewComponent } from './view/group-view.component';
import { GroupCreateComponent } from './create/group-create.component';
import { GroupEditComponent } from './edit/group-edit.component';

import { GroupComponent } from './group.component';

const routes: Routes = [
  {
    path: '',
    component: GroupComponent,
    children: [
      {
        path: '',
        component: GroupTableComponent
      },
      {
        path: 'active',
        component: GroupTableComponent
      },
      {
        path: 'inactive',
        component: GroupTableComponent
      },
      {
        path: 'create',
        component: GroupCreateComponent
      },
      {
        path: ':id',
        component: GroupViewComponent
      },
      {
        path: ':id/edit',
        component: GroupEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
