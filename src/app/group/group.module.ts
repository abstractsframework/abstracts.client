import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  NbActionsModule, 
  NbBadgeModule, 
  NbButtonModule, 
  NbCardModule, 
  NbCheckboxModule, 
  NbDatepickerModule, 
  NbIconModule, 
  NbInputModule,
  NbListModule,
  NbSelectModule,
  NbSpinnerModule,
  NbUserModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableListModuleModule } from '../module/table-list/module/table-list-module.module';
import { TableListBehaviorModule } from '../control/table-list/behavior/table-list-behavior.module';
import { TableListRuleModule } from '../control/table-list/rule/table-list-rule.module';

import { GroupTableComponent } from './table/group-table.component';
import { GroupCreateComponent } from './create/group-create.component';
import { GroupEditComponent } from './edit/group-edit.component';
import { GroupViewComponent } from './view/group-view.component';

import { GroupRoutingModule } from './group-routing.module';

import { GroupComponent } from './group.component';



@NgModule({
  declarations: [
    GroupComponent,
    GroupTableComponent,
    GroupCreateComponent,
    GroupEditComponent,
    GroupViewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    TableListModuleModule,
    TableListBehaviorModule,
    TableListRuleModule,
    GroupRoutingModule,
  ],
  exports: [
    GroupTableComponent,
    GroupCreateComponent,
    GroupEditComponent,
    GroupViewComponent
  ]
})
export class GroupModule { }
