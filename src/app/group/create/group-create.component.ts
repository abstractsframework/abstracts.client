import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { GroupService } from '../../.services/group.service';
import { ModuleService } from '../../.services/module.service';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'abstract-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: ['./group-create.component.scss']
})
export class GroupCreateComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  /* instances */
  path = '/group';
  formGroup: FormGroup;
  active: true | false = true;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;

  controlsSettings = {
    mode: 'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      module: {
        title: 'Module',
        type: 'custom',
        renderComponent: TableListModuleComponent,
        editor: {
          type: 'custom',
          component: TableListModuleEditComponent,
        }
      },
      rules: {
        title: 'Rules',
        type: 'custom',
        renderComponent: TableListRuleComponent,
        editor: {
          type: 'custom',
          component: TableListRuleEditComponent,
        }
      },
      behaviors: {
        title: 'Behaviors',
        type: 'custom',
        renderComponent: TableListBehaviorComponent,
        editor: {
          type: 'custom',
          component: TableListBehaviorEditComponent,
        }
      }
    },
  };

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private group: GroupService,
    private module: ModuleService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      name: [
        null, 
        Validators.required
      ]
    });
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  async format(value) {
    if (
      value.controls 
      && value.controls != ''
      && value.controls.length
    ) {
      for (let i = 0; i < value.controls.length; i++) {
        if (
          value.controls[i].module 
          && value.controls[i].module != ''
          && value.controls[i].module.length
        ) {
          await this.module.get(value.controls[i].module).then(async (response: any) => {
            const module = {
              value: response.id,
              title: response.name + ' ' + '(ID: ' + response.id + ')'
            }
            value.controls[i].module = module;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
        if (
          value.controls[i].rules 
          && value.controls[i].rules != ''
          && value.controls[i].rules.length
        ) {
          if (!Array.isArray(value.controls[i].rules)) {
            value.controls[i].rules = value.controls[i].rules.split(',');
          }
        }
        if (
          value.controls[i].behaviors 
          && value.controls[i].behaviors != ''
          && value.controls[i].behaviors.length
        ) {
          let behaviors = value.controls[i].behaviors;
          if (!Array.isArray(value.controls[i].behaviors)) {
            behaviors = value.controls[i].behaviors.split(',');
          }
          const cellValue = {
            view: (behaviors.includes('view')) ? true : false,
            create: (behaviors.includes('create')) ? true : false,
            update: (behaviors.includes('update')) ? true : false,
            delete: (behaviors.includes('delete')) ? true : false,
          }
          value.controls[i].behaviors = cellValue;
        }
      }
    }
    return value;
  }

  async submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let controls = [];
      if (this.controller) {
        await this.controller.source.list().then(async (data: any) => {
          for (let i = 0; i < data.length; i++) {
            let behaviors = [];
            if (data[i].behaviors.create) {
              await behaviors.push('create');
            }
            if (data[i].behaviors.update) {
              await behaviors.push('update');
            }
            if (data[i].behaviors.delete) {
              await behaviors.push('delete');
            }
            if (data[i].behaviors.view) {
              await behaviors.push('view');
            }
            const param = {
              behaviors: behaviors.join(','),
              module: data[i].module.value,
              rules: (data[i].rules && data[i].rules != '') ? data[i].rules.join(',') : [],
            }
            await controls.push(param);
          }
        });
      }
      let value = this.formGroup.value;
      value.controls = controls;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.router.navigate([this.path]);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const params = {
        name: (value.name) ? value.name : '',
        order: '',
        controls: (value.controls) ? value.controls : '',
        active: this.active
      };
      this.group.create(params).then(async (response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit create page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}
