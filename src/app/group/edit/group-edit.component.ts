import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { GroupService } from '../../.services/group.service';
import { ModuleService } from '../../.services/module.service';
import { UserService } from '../../.services/user.service';
import { TableListBehaviorComponent } from '../../control/table-list/behavior/table-list-behavior.component';
import { TableListBehaviorEditComponent } from '../../control/table-list/behavior/edit/table-list-behavior-edit.component';
import { TableListRuleComponent } from '../../control/table-list/rule/table-list-rule.component';
import { TableListRuleEditComponent } from '../../control/table-list/rule/edit/table-list-rule-edit.component';
import { TableListModuleComponent } from '../../module/table-list/module/table-list-module.component';
import { TableListModuleEditComponent } from '../../module/table-list/module/edit/table-list-module-edit.component';
import { NbDialogService } from '@nebular/theme';

import * as moment from 'moment';
import { ThemeService } from '../../.services/theme.service';

@Component({
  selector: 'abstract-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss']
})
export class GroupEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;

  moment = moment;

  /* instances */
  path = '/group';
  formGroup: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;
  submittedUserAdd: boolean = false;

  /* instances */
  data: any = null;
  userSearch: string = '';

  controlsLoading: boolean = false;
  controlsSettings = {
    mode: 'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      module_id_reference: {
        title: 'Module',
        type: 'custom',
        renderComponent: TableListModuleComponent,
        editor: {
          type: 'custom',
          component: TableListModuleEditComponent,
        }
      },
      rules: {
        title: 'Rules',
        type: 'custom',
        renderComponent: TableListRuleComponent,
        editor: {
          type: 'custom',
          component: TableListRuleEditComponent,
        }
      },
      behaviors: {
        title: 'Behaviors',
        type: 'custom',
        renderComponent: TableListBehaviorComponent,
        editor: {
          type: 'custom',
          component: TableListBehaviorEditComponent,
        }
      }
    },
  };

  memberPageSize = 2;
  members: any = {
    data: [],
    placeholders: [],
    loading: false,
    pageToLoadNext: 1,
  };

  controls: Array<any> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private theme: ThemeService,
    private components: ComponentsService,
    private group: GroupService,
    private module: ModuleService,
    private user: UserService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async prepare() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.data = state;
    }
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {
      this.controlsLoading = true;
      if (!this.data || update) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          await this.group.get(id, true).then(async (response: any) => {
            this.data = response;
            this.loadMembers();
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      }
      this.controls = this.data.controls;
      this.initializeForm();
      this.changed = false;
      this.controlsLoading = false;
      resolve();
    });
  }

  initializeForm() {
    if (this.data) {
      this.formGroup = this.formBuilder.group({
        name: [
          (this.data.name) ? this.data.name : null, 
          Validators.required
        ],
        active: [
          (this.data.active && this.data.active === true) 
          ? true : false
        ]
      });
      this.formGroup.valueChanges.subscribe(() => {
        this.changed = true;
      });
    }
  }

  async loadMembers() {
    if (!this.members.loading) {

      this.members.loading = true;
      this.members.placeholders = new Array(this.memberPageSize);

      const id = this.route.snapshot.paramMap.get('id');
      const start = this.members.data.length.toString();
      const limit = this.memberPageSize.toString();
      this.group.getMembers(id, start, limit).then((response: any) => {
        response.forEach(async user => {
          this.members.placeholders = [];
          const param = {
            id: user.id,
            name: user.name + (user.last_name ? ' ' + user.last_name : ''),
            title: user.email,
            picture: user.image_reference ? user.image_reference.thumbnail : null
          }
          if (user.active === null) {
            param.name = "N/A";
          }
          await this.members.data.push(param);
          this.members.loading = false;
          this.members.pageToLoadNext++;
        });
      }).catch(() => {
  
      });

    }
  }

  async submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      if (this.controller) {
        value.controls = this.controller.source.data.map((data: any) => {
          return {
            module_id: data.module_id_reference.id,
            behaviors: data.behaviors,
            rules: data.rules
          }
        });
      }
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.initialize(true);
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const id = this.route.snapshot.paramMap.get('id');
      const params = {
        name: (value.name) ? value.name : '',
        controls: (value.controls) ? value.controls : '',
        order: this.data.order,
        active: (value.active) ? '1' : '0',
      };
      this.group.update(id, params).then((response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  async submitCreateMember() {
    if (this.userSearch != '') {
      const id = this.route.snapshot.paramMap.get('id');
      this.submittedUserAdd = true;
      await this.user.list(null, null, 0, 1, this.userSearch).then(async (response: any) => {
        let exists = 0;
        if (this.members.data.length) {
          await this.members.data.forEach(member => {
            if (member.id == response[0].id) {
              exists += 1;
            }
          });
        }
        if (!exists) {
          await this.addMember(id, response[0].id).then(async () => {
            const param = {
              id: response[0].id,
              name: response[0].name + ' ' + response[0].last_name,
              title: response[0].email,
              picture: response[0].image_reference ? response[0].image_reference.thumbnail : null
            }
            await this.members.data.push(param);
            await this.initialize();
            this.userSearch = '';
            this.submittedUserAdd = false;
            this.components.showToastStatus('success', 'Success', 'Successfully added member');
          }).catch((error) => {
            this.submittedUserAdd = false;
            this.components.showToastStatus('danger', 'Error', error);
          });
        } else {
          this.submittedUserAdd = false;
          this.components.showToastStatus('warning', 'Duplicate', 'This member has already added');
        }
      }).catch((error) => {
        this.submittedUserAdd = false;
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
  }

  addMember(id, userID) {
    return new Promise(async (resolve, reject) => {
      this.group.addMember(id, userID).then(async (response: any) => {
        await this.initialize(true);
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  removeMember(member) {
    const id = this.route.snapshot.paramMap.get('id');
    let context = 'Do you want to delete' + ' ' + 'ID' + ':' + member.id + '?';
    if (member.name) {
      context = 'Do you want to delete' 
      + ' "' + member.name + '" (' + 'ID' + ': ' + member.id + ')?';
    } else if (member.title) {
      context = 'Do you want to delete' 
      + ' "' + member.title + '" (' + 'ID' + ': ' + member.id + ')?';
    }
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe(async (data) => {
      if (data) {
        await this.group.removeMember(id, member.id).then(async (response: any) => {
          for(let i = 0; i < this.members.data.length; i++){
            if (this.members.data[i].id === member.id) { 
              await this.members.data.splice(i, 1); 
            }
          }
          await this.initialize();
          this.userSearch = '';
          this.components.showToastStatus('success', 'Success', 'Successfully deleted member');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}