/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbTimepickerModule,
  NbToastrModule,
  NbWindowModule,
} from '@nebular/theme';
import { EditorModule } from '@tinymce/tinymce-angular';

import { TagInputModule } from 'ngx-chips';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ClipboardModule } from 'ngx-clipboard';

import { ThemeService } from './.services/theme.service';
import { ProjectsService } from './.services/projects.service';
import { RestService } from './.services/rest.service';
import { EnvironmentService } from './.services/environment.service';
import { UtilitiesService } from './.services/utilities.service';
import { ComponentsService } from './.services/components.service';
import { SettingsService } from './.services/settings.service';
import { AuthenticationService } from './.services/authentication.service';
import { AbstractsService } from './.services/abstract.service';
import { ReferenceService } from './.services/reference.service';
import { ModuleService } from './.services/module.service';
import { UserService } from './.services/user.service';
import { GroupService } from './.services/group.service';
import { LanguageService } from './.services/language.service';
import { PageService } from './.services/page.service';
import { MediaService } from './.services/media.service';
import { LogService } from './.services/log.service';
import { ApiService } from './.services/api.service';
import { HashService } from './.services/hash.service';
import { ControlService } from './.services/control.service';
import { ConnectService } from './.services/connect.service';
import { DeviceService } from './.services/device.service';
import { PaymentService } from './.services/payment.service';
import { PurchaseService } from './.services/purchase.service';
import { TransactionService } from './.services/transaction.service';

import { LoginModule } from './login/login.module';
import { SetupModule } from './setup/setup.module';
import { ProcessCreateComponent } from './process/process-create/process-create.component';
import { ProcessEditComponent } from './process/process-edit/process-edit.component';
import { ProcessTableComponent } from './process/process-table/process-table.component';
import { ProcessViewComponent } from './process/process-view/process-view.component';
import { ConfigCreateComponent } from './config/config-create/config-create.component';
import { ConfigEditComponent } from './config/config-edit/config-edit.component';
import { ConfigTableComponent } from './config/config-table/config-table.component';
import { ConfigViewComponent } from './config/config-view/config-view.component';
import { ProcessComponent } from './process/process.component';

@NgModule({
  declarations: [
    AppComponent,
    ProcessCreateComponent,
    ProcessEditComponent,
    ProcessTableComponent,
    ProcessViewComponent,
    ConfigCreateComponent,
    ConfigEditComponent,
    ConfigTableComponent,
    ConfigViewComponent,
    ProcessComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbTimepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot(),
    EditorModule,
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    TagInputModule,
    NgxDropzoneModule,
    ClipboardModule,

    LoginModule,
    SetupModule

  ],
  providers: [

    ThemeService,
    ProjectsService,
    RestService,
    EnvironmentService,
    UtilitiesService,
    ComponentsService,
    SettingsService,
    AuthenticationService,
    AbstractsService,
    ReferenceService,
    ModuleService,
    UserService,
    GroupService,
    LanguageService,
    PageService,
    MediaService,
    LogService,
    ApiService,
    HashService,
    ConnectService,
    ControlService,
    DeviceService,
    PaymentService,
    PurchaseService,
    TransactionService

  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
