import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DefaultEditor } from 'ng2-smart-table';

@Component({
  selector: 'abstract-table-list-rule-edit',
  templateUrl: './table-list-rule-edit.component.html',
  styleUrls: ['./table-list-rule-edit.component.scss']
})
export class TableListRuleEditComponent extends DefaultEditor implements AfterViewInit {

  rules = [];

  constructor() {
    super();
  }

  ngAfterViewInit() {
    this.initialize();
  }

  async initialize() {
    const cellValue = this.cell.getValue();
    if (cellValue && cellValue !== '') {
      this.rules = cellValue.map((value: any) => {
        return {
          display: value,
          value: value
        }
      });
    }
  }

  async update(event = null) {
    this.cell.newValue = this.rules.map((value: any) => {
      return value.value;
    });
  }

}
