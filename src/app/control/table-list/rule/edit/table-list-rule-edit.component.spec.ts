import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListRuleEditComponent } from './table-list-rule-edit.component';

describe('TableListRuleEditComponent', () => {
  let component: TableListRuleEditComponent;
  let fixture: ComponentFixture<TableListRuleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListRuleEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListRuleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
