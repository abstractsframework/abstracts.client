import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TagInputModule } from 'ngx-chips';

import { TableListRuleComponent } from './table-list-rule.component';
import { TableListRuleEditComponent } from './edit/table-list-rule-edit.component';


@NgModule({
  declarations: [
    TableListRuleComponent,
    TableListRuleEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
  ],
  exports: [
    TableListRuleComponent,
    TableListRuleEditComponent
  ]
})
export class TableListRuleModule { }
