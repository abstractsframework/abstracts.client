import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListRuleComponent } from './table-list-rule.component';

describe('TableListRuleComponent', () => {
  let component: TableListRuleComponent;
  let fixture: ComponentFixture<TableListRuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListRuleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
