import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'abstract-table-list-rule',
  templateUrl: './table-list-rule.component.html',
  styleUrls: ['./table-list-rule.component.scss']
})
export class TableListRuleComponent implements ViewCell, OnInit {

  @Input() value: any;
  @Input() rowData: any;

  renderValue: string;

  constructor() { }

  ngOnInit(): void {
    this.initialize();
  }

  async initialize() {
    if (this.value) {
      this.renderValue = this.value.join(', ');
    }
  }

}
