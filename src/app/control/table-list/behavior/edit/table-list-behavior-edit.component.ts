import { Component, AfterViewInit } from '@angular/core';
import { DefaultEditor } from 'ng2-smart-table';

@Component({
  selector: 'abstract-table-list-behavior-edit',
  templateUrl: './table-list-behavior-edit.component.html',
  styleUrls: ['./table-list-behavior-edit.component.scss']
})
export class TableListBehaviorEditComponent extends DefaultEditor implements AfterViewInit {

  value: any = {
    view: false,
    create: false,
    update: false,
    delete: false,
  };

  constructor() {
    super();
  }

  ngAfterViewInit() {
    this.initialize();
  }

  async initialize() {
    const cellValue = this.cell.getValue();
    if (cellValue && cellValue !== '') {
      if (cellValue.includes('create')) {
        this.value.create = true;
      }
      if (cellValue.includes('update')) {
        this.value.update = true;
      }
      if (cellValue.includes('delete')) {
        this.value.delete = true;
      }
      if (cellValue.includes('view')) {
        this.value.view = true;
      }
    }
  }

  async update() {
    let newValue = [];
    if (this.value.create) {
      newValue.push('create');
    }
    if (this.value.update) {
      newValue.push('update');
    }
    if (this.value.delete) {
      newValue.push('delete');
    }
    if (this.value.view) {
      newValue.push('view');
    }
    this.cell.newValue = newValue;
  }

}
