import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListBehaviorEditComponent } from './table-list-behavior-edit.component';

describe('TableListBehaviorEditComponent', () => {
  let component: TableListBehaviorEditComponent;
  let fixture: ComponentFixture<TableListBehaviorEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListBehaviorEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListBehaviorEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
