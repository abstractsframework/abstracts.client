import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListBehaviorComponent } from './table-list-behavior.component';

describe('TableListBehaviorComponent', () => {
  let component: TableListBehaviorComponent;
  let fixture: ComponentFixture<TableListBehaviorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListBehaviorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListBehaviorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
