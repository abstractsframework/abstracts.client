import { Component, Input, OnInit } from '@angular/core';

import { ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'abstract-table-list-behavior',
  templateUrl: './table-list-behavior.component.html',
  styleUrls: ['./table-list-behavior.component.scss']
})
export class TableListBehaviorComponent implements ViewCell, OnInit {

  @Input() value: any;
  @Input() rowData: any;

  renderValue: string;

  constructor() { }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.renderValue = this.value.join(' / ');
  }

}
