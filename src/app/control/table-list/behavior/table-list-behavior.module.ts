import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NbCheckboxModule } from '@nebular/theme';

import { TableListBehaviorComponent } from './table-list-behavior.component';
import { TableListBehaviorEditComponent } from './edit/table-list-behavior-edit.component';


@NgModule({
  declarations: [
    TableListBehaviorComponent,
    TableListBehaviorEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbCheckboxModule
  ],
  exports: [
    TableListBehaviorComponent,
    TableListBehaviorEditComponent
  ]
})
export class TableListBehaviorModule { }
