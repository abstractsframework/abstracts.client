import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AbstractComponent } from './abstract.component';
import { AbstractCreateComponent } from './create/abstract-create.component';
import { AbstractEditComponent } from './edit/abstract-edit.component';
import { AbstractTableComponent } from './table/abstract-table.component';
import { AbstractViewComponent } from './view/abstract-view.component';

const routes: Routes = [
  {
    path: '',
    component: AbstractComponent,
    children: [
      {
        path: '',
        component: AbstractTableComponent
      },
      {
        path: 'active',
        component: AbstractTableComponent
      },
      {
        path: 'inactive',
        component: AbstractTableComponent
      },
      {
        path: 'create',
        component: AbstractCreateComponent
      },
      {
        path: ':id',
        component: AbstractViewComponent
      },
      {
        path: ':id/edit',
        component: AbstractEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbstractRoutingModule { }
