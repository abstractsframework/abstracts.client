import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'abstract-abstract-view',
  templateUrl: './abstract-view.component.html',
  styleUrls: ['./abstract-view.component.scss']
})
export class AbstractViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
