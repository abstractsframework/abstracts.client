import { INPUTS, INPUTS_STACK } from '../../.constances/inputs';

import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { ModuleService } from '../../.services/module.service';
import { NbDialogService } from '@nebular/theme';

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import * as moment from 'moment';

import { AbstractsService } from '../../.services/abstract.service';
import { ReferenceService } from '../../.services/reference.service';
import { ThemeService } from '../../.services/theme.service';

@Component({
  selector: 'abstract-abstract-edit',
  templateUrl: './abstract-edit.component.html',
  styleUrls: ['./abstract-edit.component.scss']
})
export class AbstractEditComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;
  @ViewChild('dialogIcon') dialogIcon: TemplateRef<any>;
  @ViewChild('dialogSubjectIcon') dialogSubjectIcon: TemplateRef<any>;
  @ViewChild('dialogTemplateFiles') dialogTemplateFiles: TemplateRef<any>;
  @ViewChild('dialogError') dialogError: TemplateRef<any>;
  @ViewChild('dialogBuild') dialogBuild: TemplateRef<any>;
  @ViewChild('dialogBuildClean') dialogBuildClean: TemplateRef<any>;

  moment = moment;
  parseInt = parseInt;

  /* instances */
  path = '/abstract';
  formGroup: FormGroup;
  formGroupReference: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  changedReference: boolean = false;
  changedMultiple: boolean = false;
  submitted: boolean = false;
  submittedReference: boolean = false;
  submittedMultiple: boolean = false;
  submittedBuild: boolean = false;
  submittedBuildClean: boolean = false;

  /* instances */
  data: any = null;
  dataReference: any = null;
  dataMultiple: any = null;
  abstract: any = [];
  references: any = [];
  multiples: any = [];
  defaultFieldFormArray: any = [];
  sourceModules: any = [];
  sourceModuleReferences: any = [];
  sourceModuleMultiples: any = [];

  inputs = INPUTS;
  inputsKey = Object.keys(this.inputs);
  inputsMultiple = INPUTS_STACK;
  inputsKeyMultiple = Object.keys(this.inputsMultiple);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private changeRef: ChangeDetectorRef,
    private theme: ThemeService,
    private components: ComponentsService,
    private module: ModuleService,
    private abstracts: AbstractsService,
    private reference: ReferenceService
  ) {
    this.prepare();
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async prepare() {
    const navigation = this.router.getCurrentNavigation();
    if (navigation && navigation.extras && navigation.extras.state) {
      const state = this.router.getCurrentNavigation().extras.state;
      this.data = state;
    }
  }

  async initialize(update: boolean = false) {
		return new Promise<void>(async (resolve) => {
      if (!this.data || update) {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
          await this.abstracts.get(id).then(async (response: any) => {
            this.data = response;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      }
      await this.module.list().then((response: any) => {
        this.sourceModules = response;
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
      if (this.data) {
        await this.abstracts.getByKey(this.data.key).then(async (response: any) => {
          this.abstract = response[0];
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
        await this.loadReferences().then(() => {
          this.initializeFormReference();
          this.changedReference = false;
          this.changedMultiple = false;
        });
      }
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    if (this.data) {
      this.formGroup = this.formBuilder.group({
        name: [
          (this.data.name) ? this.data.name : null, 
          Validators.required
        ],
        key: [
          (this.data.key) ? this.data.key : null
        ],
        description: [
          (this.data.description) ? this.data.description : null
        ],
        form_method: [
          (this.data.form_method) ? this.data.form_method : 'post'
        ],
        component_module: [
          (this.data.component_module && this.data.component_module == '1') 
          ? true : false
        ],
        component_group: [
          (this.data.component_group && this.data.component_group == '1') 
          ? true : false
        ],
        component_user: [
          (this.data.component_user && this.data.component_user == '1') 
          ? true : false
        ],
        component_language: [
          (this.data.component_language && this.data.component_language == '1') 
          ? true : false
        ],
        component_page: [
          (this.data.component_page && this.data.component_page == '1') 
          ? true : false
        ],
        component_media: [
          (this.data.component_media && this.data.component_media == '1') 
          ? true : false
        ],
        component_commerce: [
          (this.data.component_commerce && this.data.component_commerce == '1') 
          ? true : false
        ],
        data_sortable: [
          (this.data.data_sortable && this.data.data_sortable == '1') 
          ? true : false
        ],
        database_engine: [
          (this.data.database_engine) ? this.data.database_engine : null
        ],
        database_collation: [
          (this.data.database_collation) ? this.data.database_collation : null
        ],
        template: [
          (this.data.template) ? this.data.template : null
        ],
        icon: [
          (this.data.icon) ? this.data.icon : null
        ],
        category: [
          (this.data.category) ? this.data.category : null
        ],
        subject: [
          (this.data.subject) ? this.data.subject : null
        ],
        subject_icon: [
          (this.data.subject_icon) ? this.data.subject_icon : null
        ],
        order: [
          (this.data.order) ? this.data.order : null
        ],
        active: [
          (this.data.active && this.data.active === true) 
          ? true : false
        ]
      });
    }
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  initializeFormReference(multiple = false) {
    const formGroup = this.formBuilder.group({
      type: [
        this.dataReference ? (this.dataReference.type) ? this.dataReference.type : null : null, 
        !multiple ? Validators.required : []
      ],
      label: [
        this.dataReference ? (this.dataReference.label) ? this.dataReference.label : null : null, 
        !multiple ? [
          Validators.required,
          Validators.maxLength(50)
        ] : []
      ],
      key: [
        this.dataReference ? (this.dataReference.key) ? this.dataReference.key : null : null,
        !multiple ? Validators.maxLength(50) : []
      ],
      module: [
        this.dataReference ? (this.dataReference.module) ? this.dataReference.module : null : null
      ],
      reference: [
        multiple ? this.dataMultiple ? (this.dataMultiple.reference) ? this.dataMultiple.reference : null : null : null
      ],
      placeholder: [
        this.dataReference ? (this.dataReference.placeholder) ? this.dataReference.placeholder : null : null
      ],
      help: [
        this.dataReference ? (this.dataReference.help) ? this.dataReference.help : null : null
      ],
      require: [
        this.dataReference ? (this.dataReference.require) ? this.dataReference.require : null : null
      ],
      readonly: [
        this.dataReference ? (this.dataReference.readonly) ? this.dataReference.readonly : null : null
      ],
      disable: [
        this.dataReference ? (this.dataReference.disable) ? this.dataReference.disable : null : null
      ],
      hidden: [
        this.dataReference ? (this.dataReference.hidden) ? this.dataReference.hidden : null : null
      ],
      validate_string_min: [
        this.dataReference ? (this.dataReference.validate_string_min) ? this.dataReference.validate_string_min : null : null
      ],
      validate_string_max: [
        this.dataReference ? (this.dataReference.validate_string_max) ? this.dataReference.validate_string_max : null : null
      ],
      validate_number_min: [
        this.dataReference ? (this.dataReference.validate_number_min) ? this.dataReference.validate_number_min : null : null
      ],
      validate_number_max: [
        this.dataReference ? (this.dataReference.validate_number_max) ? this.dataReference.validate_number_max : null : null
      ],
      validate_datetime_min: [
        this.dataReference ? (this.dataReference.validate_datetime_min) ? this.dataReference.validate_datetime_min : null : null
      ],
      validate_datetime_max: [
        this.dataReference ? (this.dataReference.validate_datetime_max) ? this.dataReference.validate_datetime_max : null : null
      ],
      validate_password_equal_to: [
        this.dataReference ? (this.dataReference.validate_password_equal_to) ? this.dataReference.validate_password_equal_to : null : null
      ],
      validate_email: [
        this.dataReference ? (this.dataReference.validate_email) ? this.dataReference.validate_email : null : null
      ],
      validate_password: [
        this.dataReference ? (this.dataReference.validate_password) ? this.dataReference.validate_password : null : null
      ],
      validate_url: [
        this.dataReference ? (this.dataReference.validate_url) ? this.dataReference.validate_url : null : null
      ],
      validate_no_spaces: [
        this.dataReference ? (this.dataReference.validate_no_spaces) ? this.dataReference.validate_no_spaces : null : null
      ],
      validate_no_special_characters: [
        this.dataReference ? (this.dataReference.validate_no_special_characters) ? this.dataReference.validate_no_special_characters : null : null
      ],
      validate_no_specialchar_hard: [
        this.dataReference ? (this.dataReference.validate_no_specialchar_hard) ? this.dataReference.validate_no_specialchar_hard : null : null
      ],
      validate_uppercase_only: [
        this.dataReference ? (this.dataReference.validate_uppercase_only) ? this.dataReference.validate_uppercase_only : null : null
      ],
      validate_lowercase_only: [
        this.dataReference ? (this.dataReference.validate_lowercase_only) ? this.dataReference.validate_lowercase_only : null : null
      ],
      validate_number: [
        this.dataReference ? (this.dataReference.validate_number) ? this.dataReference.validate_number : null : null
      ],
      validate_decimal: [
        this.dataReference ? (this.dataReference.validate_decimal) ? this.dataReference.validate_decimal : null : null
      ],
      validate_unique: [
        this.dataReference ? (this.dataReference.validate_unique) ? this.dataReference.validate_unique : null : null
      ],
      default_value: [
        this.dataReference ? (this.dataReference.default_value) ? this.dataReference.default_value : null : null
      ],
      default_switch: [
        this.dataReference ? (this.dataReference.default_switch) ? this.dataReference.default_switch : null : null
      ],
      input_option: [
        this.dataReference ? (this.dataReference.input_option) ? this.dataReference.input_option : null : null
      ],
      input_option_static_value: [
        this.dataReference ? (this.dataReference.input_option_static_value) ? this.dataReference.input_option_static_value : null : null
      ],
      input_option_dynamic_module: [
        this.dataReference ? (this.dataReference.input_option_dynamic_module) ? this.dataReference.input_option_dynamic_module : null : null
      ],
      input_option_dynamic_value_key: [
        this.dataReference ? (this.dataReference.input_option_dynamic_value_key) ? this.dataReference.input_option_dynamic_value_key : null : null
      ],
      input_option_dynamic_label_key: [
        this.dataReference ? (this.dataReference.input_option_dynamic_label_key) ? this.dataReference.input_option_dynamic_label_key : null : null
      ],
      image_quality: [
        this.dataReference ? (this.dataReference.image_quality) ? this.dataReference.image_quality : 75 : 75
      ],
      image_thumbnail: [
        this.dataReference ? (this.dataReference.image_thumbnail && this.dataReference.image_thumbnail == '1') 
        ? true : false : false
      ],
      image_thumbnail_aspectratio: [
        this.dataReference ? (this.dataReference.image_thumbnail_aspectratio && this.dataReference.image_thumbnail_aspectratio == '1') 
        ? true : false : false
      ],
      image_thumbnail_quality: [
        this.dataReference ? (this.dataReference.image_thumbnail_quality) ? this.dataReference.image_thumbnail_quality : 75 : 75
      ],
      image_thumbnail_width: [
        this.dataReference ? (this.dataReference.image_thumbnail_width) ? this.dataReference.image_thumbnail_width : 200 : 200
      ],
      image_thumbnail_height: [
        this.dataReference ? (this.dataReference.image_thumbnail_height) ? this.dataReference.image_thumbnail_height : 200 : 200
      ],
      image_large: [
        this.dataReference ? (this.dataReference.image_large && this.dataReference.image_large == '1') 
        ? true : false : false
      ],
      image_large_aspectratio: [
        this.dataReference ? (this.dataReference.image_large_aspectratio && this.dataReference.image_large_aspectratio == '1') 
        ? true : false : false
      ],
      image_large_quality: [
        this.dataReference ? (this.dataReference.image_large_quality) ? this.dataReference.image_large_quality : 75 : 75
      ],
      image_large_width: [
        this.dataReference ? (this.dataReference.image_large_width) ? this.dataReference.image_large_width : 400 : 400
      ],
      image_large_height: [
        this.dataReference ? (this.dataReference.image_large_height) ? this.dataReference.image_large_height : 400 : 400
      ],
      file_type: [
        this.dataReference ? (this.dataReference.file_type) ? this.dataReference.file_type : null : null
      ],
      file_hash: [
        this.dataReference ? (this.dataReference.file_hash) ? this.dataReference.file_hash : '0' : '0'
      ],
      date_format: [
        this.dataReference ? (this.dataReference.date_format) ? this.dataReference.date_format : null : null
      ],
      color_format: [
        this.dataReference ? (this.dataReference.color_format) ? this.dataReference.color_format : null : null
      ],
      input_multiple_format: [
        this.dataReference ? (this.dataReference.input_multiple_format) ? this.dataReference.input_multiple_format : null : null
      ],
      upload_folder: [
        this.dataReference ? (this.dataReference.upload_folder) ? this.dataReference.upload_folder : null : null
      ],
      image_width: [
        this.dataReference ? (this.dataReference.image_width) ? this.dataReference.image_width : null : null
      ],
      image_height: [
        this.dataReference ? (this.dataReference.image_height) ? this.dataReference.image_height : null : null
      ],
      image_width_ratio: [
        this.dataReference ? (this.dataReference.image_width_ratio) ? this.dataReference.image_width_ratio : null : null
      ],
      image_height_ratio: [
        this.dataReference ? (this.dataReference.image_height_ratio) ? this.dataReference.image_height_ratio : null : null
      ],
      grid_width: [
        this.dataReference ? (this.dataReference.grid_width) ? this.dataReference.grid_width : null : 12
      ],
      alignment: [
        this.dataReference ? (this.dataReference.alignment) ? this.dataReference.alignment : null : 'left'
      ],
      order: [
        this.dataReference ? (this.dataReference.order) ? this.dataReference.order : null : null
      ],
      active: [
        this.dataReference ? (this.dataReference.active && this.dataReference.active === true) 
        ? true : false : true
      ],
      multiple: (!multiple) ? this.formBuilder.array([this.initializeFormReference(true)]) as FormArray : null
    });
    if (!multiple) {
      formGroup.valueChanges.subscribe(() => {
        this.changedReference = true;
      });
      formGroup.controls.type.valueChanges.subscribe((value) => {
        if (value == 'input-mulitple') {
          this.loadMultiples();
        } else {
          this.multiples = [];
        }
      });
      formGroup.controls.multiple.valueChanges.subscribe(() => {
        this.changedMultiple = true;
      });
      this.formGroupReference = formGroup;
    }
    return formGroup;
  }

  async loadReferences() {
		return await new Promise<void>(async (resolve) => {
      if (this.data) {
        await this.abstracts.get(this.data.id).then(async (response: any) => {
          this.data = response;
          this.references = response.references;
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
      resolve();
    });
  }

  async loadMultiples() {
		return await new Promise<void>(async (resolve) => {
      if (this.dataReference) {
        if (this.dataReference.type == 'input-multiple') {
          await this.reference.listMulti(this.dataReference.id).then(async (response: any) => {
            this.multiples = response;
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
        }
      }
      resolve();
    });
  }

  async sortReferences(event: CdkDragDrop<string[]>) {
    await moveItemInArray(this.references, event.previousIndex, event.currentIndex);
    let errors = [];
    const sort = async () => {
      let position = 1;
      for (let reference of this.references) {
        await this.reference.patch(reference.id, { 'order': position.toString() }).catch(() => {
          errors.push(`${reference.label} (ID: ${reference.id})`);
        });
        position += 1;
      }
    }
    await sort();
    if (errors.length) {
      this.components.showToastStatus('danger', 'Error', 'Unsuccessfully sorted');
    }
  } 

  async sortMultiples(event: CdkDragDrop<string[]>) {
    await moveItemInArray(this.multiples, event.previousIndex, event.currentIndex);
    let errors = [];
    const sort = async () => {
      let position = 1;
      for (let multiple of this.multiples) {
        await this.reference.patch(multiple.id, { 'order': position.toString() }).catch(() => {
          errors.push(`${multiple.label} (ID: ${multiple.id})`);
        });
        position += 1;
      }
    }
    await sort();
    if (errors.length) {
      this.components.showToastStatus('danger', 'Error', 'Unsuccessfully sorted');
    }
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then(async (response: any) => {
        this.submitted = false;
        this.changed = false;
        this.initialize(true);
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const id = this.route.snapshot.paramMap.get('id');
      const params = {
        name: (value.name) ? value.name : '',
        key: (value.key) ? value.key : '',
        description: (value.description) ? value.description : '',
        form_method: (value.form_method) ? value.form_method : 'post',
        component_module: (value.component_module) ? '1' : '',
        component_group: (value.component_group) ? '1' : '',
        component_user: (value.component_user) ? '1' : '',
        component_language: (value.component_language) ? '1' : '',
        component_page: (value.component_page) ? '1' : '',
        component_media: (value.component_media) ? '1' : '',
        component_commerce: (value.component_commerce) ? '1' : '',
        data_sortable: (value.data_sortable) ? '1' : '',
        database_engine: (value.database_engine) ? value.database_engine : 'MyISAM',
        database_collation: (value.database_collation) ? value.database_collation : 'utf8mb4_general_ci',
        template: (value.template) ? value.template : '',
        icon: (value.icon) ? value.icon : '',
        category: (value.category) ? value.category : '',
        subject: (value.subject) ? value.subject : '',
        subject_icon: (value.subject_icon) ? value.subject_icon : '',
        order: (value.order) ? value.order : this.sourceModules.length,
        active: (value.active) ? '1' : '0'
      };
      this.abstracts.update(id, params).then((response: any) => {
        resolve(response.message);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  editReference(reference: any) {
    const initialEdit = () => {
      this.reference.get(reference.id).then(async (response: any) => {
        this.dataReference = response;
        this.formGroupReference.patchValue(
          this.formatFormGroupReference(this.dataReference)
        );
        this.multiples = null;
        this.dataMultiple = null;
        this.formGroupReference.controls.multiple.patchValue(
          [ this.formatFormGroupReference(this.dataMultiple, true) ]
        );
        this.changedReference = false;
        this.changedMultiple = false;
        this.loadMultiples();
        if (this.dataReference.input_option_dynamic_module) {
          this.onSelectModuleSourceReference(
            this.dataReference.input_option_dynamic_module,
            this.dataReference.input_option_dynamic_value_key,
            this.dataReference.input_option_dynamic_label_key
          );
        }
        this.theme.resetScrollObservable.next(true);
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
    if (this.changedReference || this.changedMultiple) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to discard reference?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.changedReference = null;
          initialEdit();
        }
      });
    } else {
      initialEdit();
    }
  }

  editMultiple(multiple: any) {
    const initialEdit = () => {
      this.reference.get(multiple.id).then(async (response: any) => {
        this.dataMultiple = response;
        this.formGroupReference.controls.multiple.patchValue(
          [ this.formatFormGroupReference(this.dataMultiple, true) ]
        );
        this.changedMultiple = false;
        if (this.dataMultiple.input_option_dynamic_module) {
          this.onSelectModuleSourceMultiple(
            this.dataMultiple.input_option_dynamic_module,
            this.dataMultiple.input_option_dynamic_value_key,
            this.dataMultiple.input_option_dynamic_label_key
          );
        }
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
    if (this.changedMultiple) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to discard multiple?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.changedMultiple = null;
          initialEdit();
        }
      });
    } else {
      initialEdit();
    }
  }

  discardReference(force = false) {
    const initialDiscard = () => {
      this.dataReference = null;
      this.formGroupReference.patchValue(
        this.formatFormGroupReference(this.dataReference)
      );
      this.changedReference = false;
      this.changedMultiple = false;
    }
    if (!force && (this.changedReference || this.changedMultiple)) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to discard reference?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          initialDiscard();
        }
      });
    } else {
      initialDiscard();
    }
  }

  discardMultiple(force = false) {
    const initialDiscard = () => {
      this.dataMultiple = null;
      this.formGroupReference.controls.multiple.patchValue(
        [ this.formatFormGroupReference(this.dataMultiple, true) ]
      );
      this.changedMultiple = false;
    }
    if (!force && this.changedMultiple) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to discard multiple?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          initialDiscard();
        }
      });
    } else {
      initialDiscard();
    }
  }

  async submitReference() {
    this.submittedReference = true;
    if (this.formGroupReference.valid) {
      let value = this.formGroupReference.value;

      const formatDefaultValue = async (items) => {
        let default_value = [];
        if (Array.isArray(items) && items.length) {
          for (let item of items) {
            await default_value.push(item.value);
          }
        }
        return default_value;
      }
      value.default_value = await (await formatDefaultValue(value.default_value)).join(',');

      const formatListStaticValue = async (items) => {
        let input_option_static_value = [];
        if (Array.isArray(items) && items.length) {
          for (let item of items) {
            await input_option_static_value.push(item.value);
          }
        }
        return input_option_static_value;
      }
      value.input_option_static_value = await (await formatListStaticValue(value.input_option_static_value)).join(',');
      
      await this.saveReference(value).then((response: any) => {
        this.submittedReference = false;
        this.changedReference = false;
        if (!this.dataReference) {
          this.discardReference(true);
        }
        this.loadReferences();
        this.theme.resetScrollObservable.next(true);
        this.components.showToastStatus('success', 'Success', response);
      }).catch((error) => {
        this.submittedReference = false;
        this.components.showToastStatus('danger', 'Error', error);
      });

    }
  }

  async submitMultiple() {
    this.submittedMultiple = true;
    if (this.formGroupReference.controls.multiple.valid) {
      let value = this.formGroupReference.controls.multiple.value[0];

      const formatDefaultValue = async (items) => {
        let default_value = [];
        if (Array.isArray(items) && items.length) {
          for (let item of items) {
            await default_value.push(item.value);
          }
        }
        return default_value;
      }
      value.default_value = await (await formatDefaultValue(value.default_value)).join(',');

      const formatListStaticValue = async (items) => {
        let input_option_static_value = [];
        if (Array.isArray(items) && items.length) {
          for (let item of items) {
            await input_option_static_value.push(item.value);
          }
        }
        return input_option_static_value;
      }
      value.input_option_static_value = await (await formatListStaticValue(value.input_option_static_value)).join(',');

      if (value.type && value.label && value.key) {
        await this.saveReference(value, true).then((response: any) => {
          this.submittedMultiple = false;
          this.changedMultiple = false;
          if (!this.dataMultiple) {
            this.discardMultiple(true);
          }
          this.loadMultiples();
          this.components.showToastStatus('success', 'Success', response);
        }).catch((error) => {
          this.submittedMultiple = false;
          this.components.showToastStatus('danger', 'Error', error);
        });
      } else {
        let context = 'Please complete all required fields'
        if (!value.type) {
          context = 'Multi-input requires "Type"';
        } else if (!value.label) {
          context = 'Multi-input requires "Label"';
        } else if (!value.parameter) {
          context = 'Multi-input requires "Key"';
        }
        this.dialogService.open(
          this.dialogError, {
            context: context
          }
        );
      }
    }
  }

  saveReference(value, multiple = false) {
    return new Promise(async (resolve, reject) => {
      if (this.data) {
        const params = {
          type: (value.type) ? value.type : '',
          label: (value.label) ? value.label : '',
          key: (value.key) ? value.key : '',
          module: this.data.id,
          reference: multiple ? (this.dataReference) ? this.dataReference.id : '' : '',
          placeholder: (value.placeholder) ? value.placeholder : '',
          help: (value.help) ? value.help : '',
          require: (value.require) ? '1' : '',
          readonly: (value.readonly) ? '1' : '',
          disable: (value.disable) ? '1' : '',
          hidden: (value.hidden) ? '1' : '',
          validate_number: (value.validate_number) ? '1' : '',
          validate_number_min: (value.validate_number_min) ? value.validate_number_min : null,
          validate_number_max: (value.validate_number_max) ? value.validate_number_max : null,
          validate_decimal: (value.validate_decimal) ? '1' : '',
          validate_decimal_min: (value.validate_decimal_min) ? value.validate_decimal_min : null,
          validate_decimal_max: (value.validate_decimal_max) ? value.validate_decimal_max : null,
          validate_string_min: (value.validate_string_min) ? value.validate_string_min : null,
          validate_string_max: (value.validate_string_max) ? value.validate_string_max : null,
          validate_datetime: (value.validate_datetime) ? value.validate_datetime : '',
          validate_datetime_min: (value.validate_datetime_min) ? value.validate_datetime_min : null,
          validate_datetime_max: (value.validate_datetime_max) ? value.validate_datetime_max : null,
          validate_email: (value.validate_email) ? '1' : '',
          validate_url: (value.validate_url) ? '1' : '',
          validate_password: (value.validate_password) ? '1' : '',
          validate_password_equal_to: (value.validate_password_equal_to) ? value.validate_password_equal_to : '',
          validate_no_spaces: (value.validate_no_spaces) ? '1' : '',
          validate_no_special_characters: (value.validate_no_special_characters) ? value.validate_no_special_characters : '',
          validate_no_specialchar_hard: (value.validate_no_specialchar_hard) ? '1' : '',
          validate_uppercase_only: (value.validate_uppercase_only) ? '1' : '',
          validate_lowercase_only: (value.validate_lowercase_only) ? '1' : '',
          validate_unique: (value.validate_unique) ? '1' : '',
          prefix: (value.prefix) ? value.prefix : '',
          suffix: (value.suffix) ? value.suffix : '',
          default_value: (value.default_value) ? value.default_value : '',
          default_switch: (value.default_switch) ? '1' : '',
          input_option: (value.input_option) ? value.input_option : '',
          input_option_static_value: (value.input_option_static_value) ? value.input_option_static_value : '',
          input_option_dynamic_module: (value.input_option_dynamic_module) ? value.input_option_dynamic_module : '',
          input_option_dynamic_value_key: (value.input_option_dynamic_value_key) ? value.input_option_dynamic_value_key : '',
          input_option_dynamic_label_key: (value.input_option_dynamic_label_key) ? value.input_option_dynamic_label_key : '',
          image_quality: (value.image_quality) ? value.image_quality : '75',
          image_thumbnail: (value.image_thumbnail) ? '1' : '0',
          image_thumbnail_aspectratio: (value.image_thumbnailAspectratio) ? '1' : '0',
          image_thumbnail_quality: (value.image_thumbnail_quality) ? value.image_thumbnail_quality : '75',
          image_thumbnail_width: (value.image_thumbnail_width) ? value.image_thumbnail_width : '200',
          image_thumbnail_height: (value.image_thumbnail_height) ? value.image_thumbnail_height : '200',
          image_large: (value.image_large) ? '1' : '0',
          image_large_aspectratio: (value.image_largeAspectratio) ? '1' : '0',
          image_large_quality: (value.image_large_quality) ? value.image_large_quality : '75',
          image_large_width: (value.image_large_width) ? value.image_large_width : '400',
          image_large_height: (value.image_large_height) ? value.image_large_height : '400',
          file_type: (value.file_type) ? value.file_type : '',
          file_hash: (value.file_hash) ? value.file_hash : '0',
          date_format: (value.date_format) ? value.date_format : '',
          color_format: (value.color_format) ? value.color_format : '',
          input_multiple_format: (value.input_multiple_format) ? value.input_multiple_format : '',
          upload_folder: (value.upload_folder) ? value.upload_folder : '' ,
          image_width: (value.image_width) ? value.image_width : null,
          image_height: (value.image_height) ? value.image_height : null,
          image_width_ratio: (value.image_width_ratio) ? value.image_width_ratio : null,
          image_height_ratio: (value.image_height_ratio) ? value.image_height_ratio : null,
          grid_width: (value.grid_width) ? value.grid_width : '12',
          alignment: (value.alignment) ? value.alignment : 'left',
          order: (value.order) ? value.order : this.references.length,
          active: (value.active) ? '1' : '0'
        };
        if (!this.dataReference || (multiple && !this.dataMultiple)) {
          this.reference.create(params).then(async (response: any) => {
            resolve(response.message);
          }).catch((error) => {
            reject(error.message);
          });
        } else {
          this.reference.update((this.dataReference ? this.dataReference.id : null), params).then(async (response: any) => {
            resolve(response.message);
          }).catch((error) => {
            reject(error.message);
          });
        }
      }
    });
  }

  formatFormGroupReference(value, multiple = false) {
    return {
      type: value ? (value.type) ? value.type : null : null,
      label: value ? (value.label) ? value.label : null : null,
      key: value ? (value.key) ? value.key : null : null,
      module: value ? (value.module) ? value.module : null : null,
      reference: value ? multiple ? (value.reference) ? value.reference : null : null : null,
      placeholder: value ? (value.placeholder) ? value.placeholder : null : null,
      help: value ? (value.help) ? value.help : null : null,
      require: value ? (value.require) ? value.require : null : null,
      readonly: value ? (value.readonly) ? value.readonly : null : null,
      disable: value ? (value.disable) ? value.disable : null : null,
      hidden: value ? (value.hidden) ? value.hidden : null : null,
      validate_string_min: value ? (value.validate_string_min) ? value.validate_string_min : null : null,
      validate_string_max: value ? (value.validate_string_max) ? value.validate_string_max : null : null,
      validate_number_min: value ? (value.validate_number_min) ? value.validate_number_min : null : null,
      validate_number_max: value ? (value.validate_number_max) ? value.validate_number_max : null : null,
      validate_datetime_min: value ? (value.validate_datetime_min) ? value.validate_datetime_min : null : null,
      validate_datetime_max: value ? (value.validate_datetime_max) ? value.validate_datetime_max : null : null,
      validate_password_equal_to: value ? (value.validate_password_equal_to) ? value.validate_password_equal_to : null : null,
      validate_email: value ? (value.validate_email) ? value.validate_email : null : null,
      validate_password: value ? (value.validate_password) ? value.validate_password : null : null,
      validate_url: value ? (value.validate_url) ? value.validate_url : null : null,
      validate_no_spaces: value ? (value.validate_no_spaces) ? value.validate_no_spaces : null : null,
      validate_no_special_characters: value ? (value.validate_no_special_characters) ? value.validate_no_special_characters : null : null,
      validate_no_specialchar_hard: value ? (value.validate_no_specialchar_hard) ? value.validate_no_specialchar_hard : null : null,
      validate_uppercase_only: value ? (value.validate_uppercase_only) ? value.validate_uppercase_only : null : null,
      validate_lowercase_only: value ? (value.validate_lowercase_only) ? value.validate_lowercase_only : null : null,
      validate_number: value ? (value.validate_number) ? value.validate_number : null : null,
      validate_decimal: value ? (value.validate_decimal) ? value.validate_decimal : null : null,
      validate_unique: value ? (value.validate_unique) ? value.validate_unique : null : null,
      prefix: value ? (value.prefix) ? value.prefix : null : null,
      suffix: value ? (value.suffix) ? value.suffix : null : null,
      default_value: value ? (value.default_value) ? value.default_value : null : null,
      default_switch: value ? (value.default_switch) ? value.default_switch : null : null,
      input_option: value ? (value.input_option) ? value.input_option : null : null,
      input_option_static_value: value ? (value.input_option_static_value) ? value.input_option_static_value : null : null,
      input_option_dynamic_module: value ? (value.input_option_dynamic_module) ? value.input_option_dynamic_module : null : null,
      input_option_dynamic_value_key: value ? (value.input_option_dynamic_value_key) ? value.input_option_dynamic_value_key : null : null,
      input_option_dynamic_label_key: value ? (value.input_option_dynamic_label_key) ? value.input_option_dynamic_label_key : null : null,
      file_type: value ? (value.file_type) ? value.file_type : null : null,
      date_format: value ? (value.date_format) ? value.date_format : null : null,
      color_format: value ? (value.color_format) ? value.color_format : null : null,
      input_multiple_format: value ? (value.input_multiple_format) ? value.input_multiple_format : null : null,
      upload_folder: value ? (value.upload_folder) ? value.upload_folder : null : null,
      image_width: value ? (value.image_width) ? value.image_width : null : null,
      image_height: value ? (value.image_height) ? value.image_height : null : null,
      image_width_ratio: value ? (value.image_width_ratio) ? value.image_width_ratio : null : null,
      image_height_ratio: value ? (value.image_height_ratio) ? value.image_height_ratio : null : null,
      grid_width: value ? (value.grid_width) ? value.grid_width : null : 12,
      alignment: value ? (value.alignment) ? value.alignment : null : 'left',
      order: value ? (value.order) ? value.order : null : null,
      active: value ? (value.active && value.active === true) 
        ? true : false : true,
    }
  }

  deleteReference(reference) {
    let context = 'Do you want to delete' 
    + ' "' + reference.label + '" (' + 'ID' + ': ' + reference.id + ')?';
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe((data) => {
      if (data) {
        this.reference.delete(reference.id).then(() => {
          this.loadReferences();
          this.components.showToastStatus('success', 'Success', 'Successfully deleted reference');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  deleteMultiple(multiple) {
    let context = 'Do you want to delete' 
    + ' "' + multiple.label + '" (' + 'ID' + ': ' + multiple.id + ')?';
    this.dialogService.open(
      this.dialog, {
        context: context
      }
    ).onClose.subscribe((data) => {
      if (data) {
        this.reference.delete(multiple.id).then((response: any) => {
          this.components.showToastStatus('success', 'Success', 'Successfully deleted multiple');
        }).catch((error) => {
          this.components.showToastStatus('danger', 'Error', error.message);
        });
      }
    });
  }

  async onSelectModuleSourceReference(moduleID, idColumn = null, valueColumn = null) {
    this.sourceModuleReferences = [];
    // let moduleID = null;
    // const getModulesID = (key) => {
    //   let id = null;
    //   if (this.sourceModules && this.sourceModules.length) {
    //     for (let module of this.sourceModules) {
    //       if (module.key == key) {
    //         id = module.id
    //       }
    //     }
    //   }
    //   return id;
    // }
    // moduleID = await getModulesID(key);
    if (moduleID) {
      this.reference.getByModuleID(moduleID).then(async (response: any) => {
        this.sourceModuleReferences = response;
        this.formGroupReference.patchValue({
          input_option_dynamic_value_key: idColumn,
          input_option_dynamic_label_key: valueColumn
        });
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
  }

  async onSelectModuleSourceMultiple(moduleID, idColumn = null, valueColumn = null) {
    this.sourceModuleMultiples = [];
    // let moduleID = null;
    // const getModulesID = (key) => {
    //   let id = null;
    //   if (this.sourceModules && this.sourceModules.length) {
    //     for (let module of this.sourceModules) {
    //       if (module.key == key) {
    //         id = module.id
    //       }
    //     }
    //   }
    //   return id;
    // }
    // moduleID = await getModulesID(key);
    if (moduleID) {
      this.reference.getByModuleID(moduleID).then(async (response: any) => {
        this.sourceModuleMultiples = response;
        const multipleFormArray: FormArray = this.formGroup.get('multiple') as FormArray;
        multipleFormArray.at(0).patchValue({
          multiple_input_option_dynamic_value_key: idColumn,
          multiple_input_option_dynamic_label_key: valueColumn
        });
      }).catch((error) => {
        this.components.showToastStatus('danger', 'Error', error.message);
      });
    }
  }

  openIcon() {
    this.dialogService.open(
      this.dialogIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        icon: data
      });
    });
  }

  openSubjectIcon() {
    this.dialogService.open(
      this.dialogSubjectIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        subject_icon: data
      });
    });
  }

  openFilesTemplate() {
    this.module.getTemplateFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogTemplateFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            template: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  build() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.dialogService.open(
        this.dialogBuild, {
          context: 'All of your manually edit codes at file generated by Abstract will be wiped'
        }
      ).onClose.subscribe(async (data) => {
        if (data) {
          this.submittedBuild = true;
          await this.abstracts.build(id).then(async (response: any) => {
            this.module.changeObservable.next(true);
            this.components.showToastStatus('success', 'Success', 'Successfulle built module');
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
          this.submittedBuild = false;
        }
      });
    }
  }

  buildClean() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.dialogService.open(
        this.dialogBuildClean, {
          context: 'All of your existing data added to this table, manually edit codes at file generated by Abstract will be wiped'
        }
      ).onClose.subscribe(async (data) => {
        if (data) {
          this.submittedBuildClean = true;
          await this.abstracts.buildClean(id).then(async (response: any) => {
            this.module.changeObservable.next(true);
            this.components.showToastStatus('success', 'Success', 'Successfulle built module and wiped all data in this table');
          }).catch((error) => {
            this.components.showToastStatus('danger', 'Error', error.message);
          });
          this.submittedBuildClean = false;
        }
      });
    }
  }

  navigate(childURL = '') {
    if (this.changed || this.changedReference) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}