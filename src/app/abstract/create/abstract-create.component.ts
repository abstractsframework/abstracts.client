import { INPUTS, INPUTS_STACK } from '../../.constances/inputs';

import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Ng2SmartTableComponent } from 'ng2-smart-table';

import { ComponentsService } from '../../.services/components.service';
import { ModuleService } from '../../.services/module.service';
import { NbDialogService } from '@nebular/theme';

import * as moment from 'moment';

import { AbstractsService } from '../../.services/abstract.service';
import { SUCC_SUBMIT } from '../../.constances/messages';

@Component({
  selector: 'abstract-abstract-create',
  templateUrl: './abstract-create.component.html',
  styleUrls: ['./abstract-create.component.scss']
})
export class AbstractCreateComponent implements OnInit {
  
  @ViewChild('controller') controller: Ng2SmartTableComponent;
  @ViewChild('dialog') dialog: TemplateRef<any>;
  @ViewChild('dialogDiscard') dialogDiscard: TemplateRef<any>;
  @ViewChild('dialogIcon') dialogIcon: TemplateRef<any>;
  @ViewChild('dialogSubjectIcon') dialogSubjectIcon: TemplateRef<any>;
  @ViewChild('dialogTemplateFiles') dialogTemplateFiles: TemplateRef<any>;

  moment = moment;
  parseInt = parseInt;

  /* instances */
  path = '/abstract';
  formGroup: FormGroup;
  formGroupReference: FormGroup;

  loaded: boolean = false;
  changed: boolean = false;
  submitted: boolean = false;
  active: true | false = true;

  /* instances */
  sourceModules: any = [];

  inputs = INPUTS;
  inputsKey = Object.keys(this.inputs);
  inputsMultiple = INPUTS_STACK;
  inputsKeyMultiple = Object.keys(this.inputsMultiple);

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private components: ComponentsService,
    private module: ModuleService,
    private abstracts: AbstractsService
  ) {
  }

  ngOnInit(): void {
    this.initialize().then(() => {
      this.loaded = true;
    });
  }

  async initialize() {
		return new Promise<void>(async (resolve) => {
      this.initializeForm();
      this.changed = false;
      resolve();
    });
  }

  initializeForm() {
    this.formGroup = this.formBuilder.group({
      name: [
        null, 
        Validators.required
      ],
      key: [
        null
      ],
      description: [
        null
      ],
      form_method: [
        'post'
      ],
      component_module: [
        false
      ],
      component_group: [
        false
      ],
      component_user: [
        false
      ],
      component_language: [
        false
      ],
      component_page: [
        false
      ],
      component_media: [
        false
      ],
      component_commerce: [
        false
      ],
      data_sortable: [
        false
      ],
      database_engine: [
        null
      ],
      database_collation: [
        null
      ],
      template: [
        null
      ],
      icon: [
        null
      ],
      category: [
        null
      ],
      subject: [
        null
      ],
      subject_icon: [
        null
      ],
      order: [
        null
      ],
      active: [
        true
      ]
    });
    this.formGroup.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }

  submit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      let value = this.formGroup.value;
      this.save(value).then((response: any) => {
        this.submitted = false;
        this.changed = false;
        this.navigate(`/${response.id}/edit`);
        this.components.showToastStatus('success', 'Success', SUCC_SUBMIT);
      }).catch((error) => {
        this.submitted = false;
        this.components.showToastStatus('danger', 'Error', error);
      });
    }
  }

  save(value) {
    return new Promise(async (resolve, reject) => {
      const params = {
        name: (value.name) ? value.name : '',
        key: (value.key) ? value.key : '',
        description: (value.description) ? value.description : '',
        form_method: (value.form_method) ? value.form_method : 'post',
        component_module: (value.component_module) ? '1' : '',
        component_group: (value.component_group) ? '1' : '',
        component_user: (value.component_user) ? '1' : '',
        component_language: (value.component_language) ? '1' : '',
        component_page: (value.component_page) ? '1' : '',
        component_media: (value.component_media) ? '1' : '',
        component_commerce: (value.component_commerce) ? '1' : '',
        data_sortable: (value.data_sortable) ? '1' : '',
        database_engine: (value.database_engine) ? value.database_engine : 'MyISAM',
        database_collation: (value.database_collation) ? value.database_collation : 'utf8mb4_general_ci',
        template: (value.template) ? value.template : '',
        icon: (value.icon) ? value.icon : '',
        category: (value.category) ? value.category : '',
        subject: (value.subject) ? value.subject : '',
        subject_icon: (value.subject_icon) ? value.subject_icon : '',
        order: (value.order) ? value.order : this.sourceModules.length,
        active: (value.active) ? '1' : '0'
      };
      this.abstracts.create(params).then((response: any) => {
        resolve(response);
      }).catch((error) => {
        reject(error.message);
      });
    });
  }

  openIcon() {
    this.dialogService.open(
      this.dialogIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        icon: data
      });
    });
  }

  openSubjectIcon() {
    this.dialogService.open(
      this.dialogSubjectIcon, {}
    ).onClose.subscribe((data) => {
      this.formGroup.patchValue({
        subject_icon: data
      });
    });
  }

  openFilesTemplate() {
    this.module.getTemplateFiles().then(async (response: any) => {
      this.dialogService.open(
        this.dialogTemplateFiles, {
          context: response
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.formGroup.patchValue({
            template: data
          });
        }
      });
    }).catch((error) => {
      this.components.showToastStatus('danger', 'Error', 'Permission denied');
    });
  }

  navigate(childURL = '') {
    if (this.changed) {
      this.dialogService.open(
        this.dialogDiscard, {
          context: 'Do you want to exit edit page?'
        }
      ).onClose.subscribe((data) => {
        if (data) {
          this.router.navigate([this.path + childURL]);
        }
      });
    } else {
      this.router.navigate([this.path + childURL]);
    }
  }

}