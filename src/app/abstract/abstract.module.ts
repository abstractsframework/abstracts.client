import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AbstractRoutingModule } from './abstract-routing.module';
import { AbstractComponent } from './abstract.component';
import { AbstractCreateComponent } from './create/abstract-create.component';
import { AbstractEditComponent } from './edit/abstract-edit.component';
import { AbstractViewComponent } from './view/abstract-view.component';
import { AbstractTableComponent } from './table/abstract-table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { NbCardModule, NbActionsModule, NbIconModule, NbSelectModule, NbInputModule, NbCheckboxModule, NbDatepickerModule, NbButtonModule, NbBadgeModule, NbListModule, NbUserModule, NbSpinnerModule, NbTabsetModule, NbTimepickerModule, NbProgressBarModule, NbTagModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TableListBehaviorModule } from '../control/table-list/behavior/table-list-behavior.module';
import { TableListRuleModule } from '../control/table-list/rule/table-list-rule.module';
import { IconModule } from '../icon/icon.module';
import { MediaBrowserModule } from '../media-browser/media-browser.module';
import { TableListModuleModule } from '../module/table-list/module/table-list-module.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  declarations: [
    AbstractComponent,
    AbstractCreateComponent,
    AbstractEditComponent,
    AbstractViewComponent,
    AbstractTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatListModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbActionsModule,
    NbIconModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbTimepickerModule,
    NbButtonModule,
    NbBadgeModule,
    NbListModule,
    NbUserModule,
    NbSpinnerModule,
    NbTabsetModule,
    NbProgressBarModule,
    NbTagModule,
    IconModule,
    DragDropModule,
    TagInputModule,
    TableListModuleModule,
    TableListBehaviorModule,
    TableListRuleModule,
    MediaBrowserModule,
    AbstractRoutingModule
  ],
  exports: [
    AbstractCreateComponent,
    AbstractEditComponent,
    AbstractViewComponent,
    AbstractTableComponent
  ]
})
export class Abstracts { }
